// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Phone extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
		this.signals = [
			'com.alsatux.phone:/call/added',
			'com.alsatux.phone:/call/state/changed',
			'com.alsatux.phone:/call/deleted',
			'com.alsatux.phone:/call/dtmf/incoming'
		];
		let _Modules = global._PP._Core._Modules;
		this.parent = {
			'_Modem': _Modules.get('pp.core.modem'),
			'_Sound': _Modules.get('pp.core.sound'),
			'_Contacts': _Modules.get('com.alsatux.contacts'),
			'_Prefs': _Modules.get('com.alsatux.prefs')
		}
		this._Shell = new PP_Shell(this);
		this.scripts = 'bash ' + ROOT_PATH + '/app/com.alsatux.phone/scripts/';
		this._ModemVoiceManager = new ModemVoiceManager(this);
	}
	async init () {
		try {
			await this.defaults();
			this.set('tbl', {
				'calls': new Calls_Tbl(this, 99999, Call_Object)
			});
			this.set('slots', {
				'calls': new Calls_Slots(this),		
				'voice': new Modem_Voice_Slots(this)	
			});
			await this._ModemVoiceManager.init();
			await this.get_last_backup();
			this.debug(1,`${this.tbl.calls.length()} calls found !`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Error ${err} ! Module disabled !`);
			throw err;
		}
	}
	import (json) {
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(json, 'calls', Object)) {
			// we trust our own datas, so no check needed...			
			for (let uid in json.calls) {
				if (uid === null) { // dev errors...
					continue;
				}
				let call = json.calls[uid];
				if (call.timestamp_start !== null && call.state_code==7) {  // dev errors...
					let _Call_Object = new Call_Object(this);
					_Call_Object.import_from_storage(call);
					this.tbl.calls.obj[uid] = _Call_Object;
				}
			}
		}
	}
	export () {
		return JSON.stringify({
			calls: this.tbl.calls.export_to_storage()
		});
	}
	leds_start () {
		this.debug(3,'Leds activated...');
		this._Shell.exec(`${this.scripts}/leds.start.sh`);
	}
	leds_stop () {
		this.debug(3,'Leds deactivated...');
		this._Shell.exec(`${this.scripts}/leds.stop.sh`);
	}
}
