// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Calls_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'calls', {
			// notification StateChanged will be propagated to all clients
			// so no need to callbacks here
			'com.alsatux.phone:/call/accept': null,
			'com.alsatux.phone:/call/deflect': null,
			'com.alsatux.phone:/call/multiparty/join': null,
			'com.alsatux.phone:/call/multiparty/leave': null,
			'com.alsatux.phone:/call/hangup': null,
			'com.alsatux.phone:/call/dtmf/outgoing': null
		});
	}
	async handler (ws, data) {
		try {
			let _Status = new PP_Status();
			_Status.code(0);
			let _Call = null;
			if (ws.has_key( data.args, 'dbus_path', String)) {
				_Call = this._Module.tbl.calls.find_dbus_path(data.args.dbus_path);
			}
			if (_Call === null) {
				throw _PP.tr('MISSING_OR_BAD_DBUS_PATH');
			} else {
				this.debug(3,'Call found using client datas !');										
			}
			switch (data.route) {				
				case 'com.alsatux.phone:/call/accept':
					await _Call.accept();
					break;
				case 'com.alsatux.phone:/call/deflect':
					if (!ws.has_key(data.args, 'number', String)) {
						throw this._Module.tr('MISSING_DEFLECT_NUMBER');
					}
					await _Call.deflect(data.args.number);
					break;
				case 'com.alsatux.phone:/call/multiparty/join':
					await _Call.join_multiparty();
					break;
				case 'com.alsatux.phone:/call/multiparty/leave':
					await _Call.leave_multiparty();
					break;
				case 'com.alsatux.phone:/call/hangup': 
					await _Call.hangup();
					break;
				case 'com.alsatux.phone:/call/dtmf/outgoing':
					if (!ws.has_key(data.args, 'symbol', String)) {
						throw this._Module.tr('DTMF_SYMBOL_MISSING');
					}
					this.debug(3,'Sending DTMF...');
					await _Call.send_dtmf(data.args.symbol);
					break;
				default:
					throw this._Module.tr('UNKNOWN_ROUTE');
			}
			ws._Status.code(0);
			_PP._WS.send_signal({
				args: {
					call: _Call.export()
				},
				callback: this.callbacks[data.route]
			},_Status);
		} catch (err) {
			ws._Status.code(1);
			ws._Status.err(err);
		}
	}
}			