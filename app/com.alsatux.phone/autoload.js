global.register_module({
	manifest: {
		version: '2.0',
		title: 'Echo',
		name: 'phone', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.phone', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/app/com.alsatux.phone',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/com.alsatux.phone',
			format: 'json'
		}
	},
	files: [
		'app/com.alsatux.phone/call.object',
		'app/com.alsatux.phone/calls.slots',
		'app/com.alsatux.phone/calls.tbl',
		'app/com.alsatux.phone/modem.voice.manager',
		'app/com.alsatux.phone/modem.voice.slots',
		'app/com.alsatux.phone/module'
	],
	main: 'Phone',
	options: {
		parents: [ // linked modules
			'pp.core.modem',
			'pp.core.sound',
			'com.alsatux.contacts'
		]
	}
});
