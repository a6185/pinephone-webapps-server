// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class ModemVoiceManager extends PP_Class {
	constructor (_Module, dbus_path) {
		super(_Module,'modem_voice_manager');
		this.dbus_path = dbus_path; // /org/freedesktop/ModemManager1/Modem/' + number
	}
	async init () {
		try {
			let _ModemManager = this._Module.get_parent('_Modem')._ModemManager;
			_ModemManager.on('org.freedesktop.ModemManager1.Modem.Voice', 'CallAdded', this.on_external_dbus_notifications.bind(this));
			_ModemManager.on('org.freedesktop.ModemManager1.Modem.Voice', 'CallDeleted', this.on_external_dbus_notifications.bind(this));
			let _Ctrl = _ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice;
			if (_Ctrl !== null) {
				let dbus_paths = await _Ctrl._DBus.list_calls();
				let calls = await this.get_calls_states(dbus_paths[0]);
				calls.forEach(_Call => {
					_Call.handle();
				});
			}
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async add_call (dbus_path) {
		let _Call = this._Module.tbl.calls.find_dbus_path(dbus_path);
		if (_Call === null) {
			_Call = new Call_Object(this._Module);
			_Call.set('dbus_path',dbus_path);
			await _Call.attach_ctrl();
			this._Module.tbl.calls.add(_Call);
		}
		return _Call;
	}
	async get_calls_states (dbus_paths) {
		try {
			let calls = [];
			this.debug(3,dbus_paths); // array of dbus_paths
			for (let i=0;i<dbus_paths.length;i++) {
				let dbus_path = dbus_paths[i];
				let _Call = await this.add_call(dbus_path);		
				calls.push(_Call);
			}
			return calls;
		} catch (err) {
			this.debug(1,`Get calls states failed: ${err}`);
		}
	}	
	/* DBUS */
	async on_external_dbus_notifications (_Ctrl, signal, ...args) {
		try {
			let dbus_path = args[0];
			let _Call = null;
			let _Status = new PP_Status();
			this.debug(3,`Signal ${signal}: path=${dbus_path}`);
			switch (signal) {
				case 'CallAdded':
					_Call = await this.add_call(dbus_path);
					_Call.set('timestamp_start',(new Date()));
					_Call.handle();
					let contact = null;
					if (this._Module.get_parent('_Contacts') !== null) {
						contact = this._Module.get_parent('_Contacts').tbl.contacts.get_contact_by_tel(_Call.get('number'));
					}
					_Call.set('contact', contact);
					// avoid writing non TERMINATED calls (to do what ?)
					_PP._WS.send_signal({
						args: {
							call: _Call.obj
						},
						callback: 'com.alsatux.phone:/call/added'
					},_Status);
					if (_Call.get('direction')==1) {
						let _Screen = new PP_Screen(this._Module);
						_Screen.unlock();
						/* not working (too long)...
						let _PP_Shell = new PP_Shell();
						_PP_Shell.exec(_PP_Shell.pkexec + '/usr/local/src/pinephone/server/app/phone/open.browser.sh')
						*/
					}
					break;
				case 'CallDeleted':
					_Call = this._Module.tbl.calls.find_dbus_path(dbus_path);
					if (_Call !== null) {
						_Call.set('dbus_path',null);
						await this._Module._IO.write();
						_PP._WS.send_signal({
							args: {
								call: _Call.obj
							},
							callback: 'com.alsatux.phone:/call/deleted'
						},_Status);						
					}
					break;
			}
		} catch (err) {
			this.debug(1,`Handling notification [${signal}] failed ! (${err})`);
		}
	}
	/* SLOTS WEB */
	async create_call (number) {
		try {
			let final_number = this._Module.get_parent('_Prefs')._Fn.clean_phone_number(number);
			let _Ctrl = this.get_ctrl();
			//let final_number = number.replace(/[^0-9]+/g,'');
			let dbus_path = await _Ctrl._DBus.create_call(final_number);
		} catch (err) {
			this.debug(1,`Dial failed: ${err}`);
		}
	}
	// should be used if documentation said the truth...
	async delete_call (dbus_path) {
		// Delete a Call from the list of calls.
		// The call will be hangup if it is still active.
		try {
			let _Ctrl = this.get_ctrl();
			await _Ctrl._DBus.delete_call(dbus_path);
		} catch (err) {
			this.debug(1,`Dial failed: ${err}`);
		}
	}
	get_ctrl () {
		let _Ctrl = this._Module.get_parent('_Modem')._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice;
		if (_Ctrl === null) {
			throw 'Interface modem voice not found !';
		}		
		return _Ctrl;
	}
	async hangup_all () {
		// Hangup all active calls.
		// Depending on how the device implements the action, calls on hold or in waiting state may also be terminated.
		// No error is returned if there are no ongoing calls. 
		try {
			let _Ctrl = this.get_ctrl();
			await _Ctrl._DBus.hangup_all();
		} catch (err) {
			this.debug(1,`Hangup all failed: ${err}`);
			return err;
		}
	}
	async hold_and_accept () {
		try {
			let _Ctrl = this.get_ctrl();
			await _Ctrl._DBus.hold_and_accept();		
		} catch (err) {
			this.debug(1,`Hold and accept failed: ${err}`);
			return err;
		}
	}
	async hangup_and_accept () {
		try {
			let _Ctrl = this.get_ctrl();
			await _Ctrl._DBus.hangup_and_accept();		
		} catch (err) {
			this.debug(1,`Hangup and accept failed: ${err}`);
			return err;
		}
	}
	async transfer () {
		try {
			let _Ctrl = this.get_ctrl();
			await _Ctrl._DBus.transfer();		
		} catch (err) {
			this.debug(1,`Transfer failed: ${err}`);
			return err;
		}
	}
}