#!/bin/bash

MODEM=$(mmcli -L | grep -oE 'Modem\/[0-9]+' | head -n 1 | cut -d'/' -f2)

mmcli -m $MODEM --voice-list-calls

echo "Call number to hangup ?"
read n

COMM="mmcli -m $MODEM -o /org/freedesktop/ModemManager1/Call/$n --hangup"
echo "$COMM"
$COMM

COMM="mmcli -m $MODEM  --voice-delete-call $n"
echo "$COMM"
$COMM


