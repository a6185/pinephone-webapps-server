#!/bin/bash

PATH=$PATH:/usr/bin

# thx to https://lasonotheque.org/search?q=telephone

pidoff incoming.call.start.sh && killall incoming.call.start.sh > /dev/null 2>&1

# use amixer scontrols to find available sound slots !
amixer -q set 'Line Out' 40%
aplay -q /usr/local/src/pinephone/sound/bird.wav &

exit 0
