#!/bin/bash

PATH=$PATH:/usr/bin

pidoff incoming.call.start.sh && killall incoming.call.start.sh > /dev/null 2>&1

pkill aplay >/dev/null 2>&1
sleep 1
amixer -q set 'Line Out' 100%

exit 0
