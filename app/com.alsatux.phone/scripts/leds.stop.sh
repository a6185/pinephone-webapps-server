#!/bin/bash

PATH=$PATH:/usr/bin

pkill leds.start.sh >/dev/null 2>&1

echo 0 > /sys/class/leds/blue\:indicator/brightness
echo 0 > /sys/class/leds/green\:indicator/brightness
echo 0 > /sys/class/leds/red\:indicator/brightness

exit 0
