#!/bin/bash

PATH=$PATH:/usr/bin
COUNT=3

function blink {
	while [ $COUNT -gt 0 ]; do
		echo 1 > /sys/class/leds/blue\:indicator/brightness
		echo 0 > /sys/class/leds/green\:indicator/brightness
		echo 0 > /sys/class/leds/red\:indicator/brightness
		sleep 1
		echo 1 > /sys/class/leds/blue\:indicator/brightness
		echo 1 > /sys/class/leds/green\:indicator/brightness
		echo 1 > /sys/class/leds/red\:indicator/brightness
		sleep 1
		echo 0 > /sys/class/leds/blue\:indicator/brightness
		echo 0 > /sys/class/leds/green\:indicator/brightness
		echo 1 > /sys/class/leds/red\:indicator/brightness
		sleep 1
		echo 0 > /sys/class/leds/blue\:indicator/brightness
		echo 0 > /sys/class/leds/green\:indicator/brightness
		echo 0 > /sys/class/leds/red\:indicator/brightness
		sleep 1
		let "COUNT-=1"
	done
}

blink &

exit 0
