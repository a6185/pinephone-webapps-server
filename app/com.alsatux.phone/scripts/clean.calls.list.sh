#!/bin/bash

modem=$(mmcli -K -L|tail -n1|awk '{print $3}')
calls=$(mmcli -m $modem -K --voice-list-calls|awk '{print $3}')

for call in $calls; do
	cmd="mmcli -m $modem --voice-delete-call=$call";
	echo $cmd
	$cmd
done
