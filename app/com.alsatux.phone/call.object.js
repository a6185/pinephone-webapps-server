// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Call_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'call_object');
		this.obj = {
			uid: null,
			dbus_path: null,
			timestamp_start: null,
			duration: 0,
			// call properties:
			number: '', // call number
			direction: 0, // 0 = unknown, 1 = incoming, 2 = outgoing
			state_code: 0,
			state: 'UNKNOWN',
			reason_code: 0,
			reason: 'UNKNOWN',
			multiparty: 0, // 0 or 1
			audioport: '', // ?
			audioformat: {},
			contact: {
				uid: null,
				fn: '',
				ln: ''
			}				
		};
		this._Ctrl = null;
		this.const = Object.freeze({							
			MM_CALL_STATE: {
				0: 'UNKNOWN', // default state for a new outgoing call.
				1: 'DIALING', // outgoing call started. Wait for free channel.
				2: 'RINGING_OUT', // outgoing call attached to GSM network, waiting for an answer.
				3: 'RINGING_IN', // incoming call is waiting for an answer.
				4: 'ACTIVE', // call is active between two peers.
				5: 'HELD', // held call (by +CHLD AT command).
				6: 'WAITING', // waiting call (by +CCWA AT command). /** Incoming call is waiting for pickup, while another call is in progress. */
				7: 'TERMINATED' // call is terminated.
			},
			MM_CALL_STATE_REASON: {
				0: 'UNKNOWN', // Default value for a new outgoing call.
				1: 'OUTGOING_STARTED', // Outgoing call is started.
				2: 'INCOMING_NEW', // Received a new incoming call.
				3: 'ACCEPTED', // Dialing or Ringing call is accepted.
				4: 'TERMINATED', // Call is correctly terminated.
				5: 'REFUSED_OR_BUSY', // Remote peer is busy or refused call.
				6: 'ERROR', // Wrong number or generic network error.
				7: 'AUDIO_SETUP_FAILED', // Error setting up audio channel.
				8: 'TRANSFERRED', // Call has been transferred.
				9: 'DEFLECTED' // Call has been deflected to a new number.
			}
		});
	}
	debug (level, msg) {
		global._Console.debug(level,`>phone_call:: ${msg}`);
	}
	async attach_ctrl () {
		try {
			this._Ctrl = new DBus_Ctrl__MM1__MM1_Call__MM1_Call(this._Module, this, this.get('dbus_path'));
			await this._Ctrl.init();
			this.update_state();
			//this.handle();
		}	catch (err) {
			this.debug(3,`Unable to attach DBus Ctrl - EXCEPTION: ${err}`);
		}	
	}
	import_from_storage (obj) {
		if (obj.uid !== null && obj.timestamp_start !== null) {
			'uid,number,timestamp_start,duration,direction,multiparty,state_code,state,reason_code,reason'.split(',').forEach(key => {
				this.set(key,obj[key]);
			});		
			// search for empty contacts
			let search = true;
			let hasKey = global._PP._Fn.hasKey;
			if (hasKey(obj,'contact',Object)) {
				if (hasKey(obj.contact,'uid',String) && hasKey(obj.contact,'fn',String) && hasKey(obj.contact,'ln',String)) {
					if (obj.contact.uid.length) {
						search = false;
					}
				}
			}
			if (search) {
				let contact = null;
				if (this._Module.get_parent('_Contacts') !== null) {
					contact = this._Module.get_parent('_Contacts').tbl.contacts.get_contact_by_tel(obj.number);
				}				
				this.set('contact', contact);
			} else {
				this.set('contact', obj.contact);
			}
		}
	}
	export_to_storage () {
		let obj = {};
		'uid,number,timestamp_start,duration,direction,multiparty,state_code,state,reason_code,reason,contact'.split(',').forEach(key => {
			obj[key] = this.get(key);
		});
		return obj;
	}
	now2ymd () {
		let d = new Date();
		return sprintf('%04d-%02d-%02d',d.getFullYear(),d.getMonth()+1,d.getDate());
	}
	is_active () {
		return ('45'.indexOf(this.get('state_code')) != -1);
	}
	async terminated () {
		// the doc says "hangup the client don't call StateChanged" but its wrong...
		// do not set dbus_path to null ! (will be set by CallDeleted signal)
		// this.set('dbus_path',null);
		this._Ctrl = null;
		if (this.get('timestamp_start') !== null) {
			this.set('duration',(new Date())-this.get('timestamp_start'));
		}
		// delete call in calls list
		await this._Module._ModemVoiceManager.delete_call(this.get('dbus_path'));
		// fix sound parameters
		let online_calls = this._Module.tbl.calls.online_calls();
		if (!online_calls.length) {
			this.debug(3,`Return to audio mode`);
			await  this._Module.get_parent('_Sound')._Sound_Manager.set_call_audio_mode(0);
		} else {
			this.debug(3,`Some calls left - return to audio mode aborted`);
		}
		// be sure to stop external signals
		this._Module.leds_stop();
	}
	async handle () {
		try {
			let online_calls = this._Module.tbl.calls.online_calls();
			if (this.get('direction')==1) { // incoming
				switch (this.get('state_code')) {
					case 3: // RINGING IN
						this._Module.leds_start();
						// remember: ringing is on client side
						break;
					case 4: // ACTIVE
						this.debug(3,`Go to phone mode`);
						await this._Module.get_parent('_Sound')._Sound_Manager.set_call_audio_mode(1);
						this._Module.leds_stop();
						// remember: ringing is on client side
						break;
					case 6: // WAITING
						// should play double beep - TODO
						this._Module.leds_start();
						break;
					case 7: // TERMINATED
						this.terminated();
						break;
				}
			} else { // 2 = outgoing
				switch (this.get('state_code')) {
					case 0: // UNKNOWN
						if (this._Ctrl !== null) {
							// Start ();
							// If the outgoing call has not yet been started, start it.
							// Applicable only if state is MM_CALL_STATE_UNKNOWN and direction is MM_CALL_DIRECTION_OUTGOING.
							await this._Ctrl._DBus.start();
						}
						break;
					case 1: // DIALING
						// should play DTMF number on client side
						break;
					case 2: // RINGING OUT
						// need a sound on client side
						break;
					case 4: // ACTIVE
						this.debug(3,`Go to phone mode`);
						await this._Module.get_parent('_Sound')._Sound_Manager.set_call_audio_mode(1);
						this._Module.leds_stop();
						break;
					case 5: // HELD
						this._Module.leds_start();
						break;
					case 6: // WAITING
						this._Module.leds_start();
						break;
					case 7: // TERMINATED
						this.terminated();
						break;
				}
			}
		} catch (err) {
			this.debug(3,`Handle error for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
	update_state () {
		this.debug(3,'Reading voice call state of ' + this.get('dbus_path'));
		let properties = this._Ctrl.get('properties');
		for (let key in properties) {
			let value = properties[key];
			switch(key) {
				case 'Number':
					this.set('number',value);
					break;
				case 'State':
					this.set('state_code',value);
					this.set('state',this.const.MM_CALL_STATE[value]);
					break;
				case 'StateReason':
					this.set('reason_code',value);
					this.set('reason',this.const.MM_CALL_STATE_REASON[value]);
					break;
				case 'Direction':
					this.set('direction',value); // 1 = incoming, 2 = outgoing
					break;
				case 'Multiparty':
					this.set('multiparty',(value=='true' ? 1 : 0));
					break;
				case 'AudioPort':
					this.set('audioport',value);
					break;
				case 'AudioFormat':
					this.set('audioformat',value);
					break;
			}
		}
		this.debug(3,'Reading voice call properties ok for ' + this.get('dbus_path'));
		if (DEBUG==3) {
			global._Console.inspect(this.obj);
		}	
	}
	/* DBUS SIGNALS */
	async on_dbus_notifications (_Ctrl, signal, ...args) {
		try {
			let _Status = new PP_Status();
			switch (signal) {
				case 'DtmfReceived':
					let symbol = args[0].toString(); // DTMF tone identifier [0-9A-D*#]
					this.debug(3,`Incoming DTMF symbol for ${this.get('dbus_path')} : ${symbol}`);
					_PP._WS.send_signal({
						args: {
							call: this.obj,
							symbol: symbol,
							callback: 'com.alsatux.phone:/call/dtmf/incoming'
						}
					},_Status);
					break;
				case 'StateChanged':
					// TAKE CARE ! Hangup doesn't call StateChanged...
					let state_old = args[0];
					let state_new = args[1];
					let reason_code = args[2];
					this.debug(3,`StateChanged : old state=${state_old} state=${this.const.MM_CALL_STATE[state_old]}`);
					this.debug(3,`StateChanged : new state=${state_new} state=${this.const.MM_CALL_STATE[state_new]}`);
					this.debug(3,`StateChanged : reason code=${reason_code} state=${this.const.MM_CALL_STATE_REASON[reason_code]}`);
					this.set('state_code',state_new);
					this.set('state',this.const.MM_CALL_STATE[state_new]);
					this.set('reason_code',reason_code);
					this.set('reason',this.const.MM_CALL_STATE_REASON[reason_code]);
					this.handle();
					_PP._WS.send_signal({
						args: {
							call: this.obj
						},
						callback: 'com.alsatux.phone:/call/state/changed'
					}, _Status);
					break;
			}
		} catch (err) {
			this.debug(1,`Handling notification [${signal}] failed !`);
		}
	}
	/* WEB SLOTS */
	async accept () {
		// Applicable only if state is MM_CALL_STATE_RINGING_IN and direction is MM_CALL_DIRECTION_INCOMING.
		try {
			this.debug(3,`Accept incoming call ${this.get('dbus_path')}...`);
			if (this._Ctrl !== null && this.get('direction')==1 && this.get('state_code')==3) {
				await this._Ctrl._DBus.accept();
			}
		} catch (err) {
			this.debug(3,`Accept failed for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
	async deflect (number) {
		// Deflect an incoming or waiting call to a new number. This call will be considered terminated once the deflection is performed.
		// Applicable only if state is MM_CALL_STATE_RINGING_IN or MM_CALL_STATE_WAITING and direction is MM_CALL_DIRECTION_INCOMING.
		try {
			this.debug(3,`Deflect incoming call ${this.get('dbus_path')}...`);
			if (this._Ctrl !== null && this.get('direction')==1 && '36'.indexOf(this.get('state_code'))!=-1) {
				// not working : got error :
				// Deflect failed for /org/freedesktop/ModemManager1/Call/50 - error: DBusError: Cannot run sequence: 'Could not open serial device ttyUSB2: it has been forced close'
				await this._Ctrl._DBus.deflect(number);
			}
		} catch (err) {
			this.debug(3,`Deflect failed for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
	async join_multiparty () {
		// Join the currently held call into a single multiparty call with another already active call.
		// The calls will be flagged with the 'Multiparty' property while they are part of the multiparty call.
		// Applicable only if state is MM_CALL_STATE_HELD.
		try {
			if (this._Ctrl !== null && this.get('state_code')==5 && this.get('multiparty')==0) {
				await this._Ctrl._DBus.join_multiparty();
			}
		} catch (err) {
			this.debug(3,`Join multiparty failed for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
	async leave_multiparty () {
		// If this call is part of an ongoing multiparty call, detach it from the multiparty call, put the multiparty call on hold, and activate this one alone. This operation makes this call private again between both ends of the call.
		// Applicable only if state is MM_CALL_STATE_ACTIVE or MM_CALL_STATE_HELD and the call is a multiparty call.
		try {
			if (this._Ctrl !== null && '45'.indexOf(this.get('state_code'))!=-1 && this.get('multiparty')==1) { // ACTIVE and in MULTIPARTY
				await this._Ctrl._DBus.leave_multiparty();
			}
		} catch (err) {
			this.debug(3,`Leave multiparty failed for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
	async hangup () {
		// Hangup the active call.
		// Applicable only if state is MM_CALL_STATE_UNKNOWN. 
		// BUT THAT'S WRONG !
		try {
			if (this._Ctrl !== null) {
				/* version 2022 */
				await this._Ctrl._DBus.hangup();
				/* version 2023 : try again... */
				/* Still not working ! Documentation is FALSE !
				if (this.get('state_code')==0) {
					await this._Ctrl._DBus.hangup();
				} else {
					// extra: we use the Voice Manager to delete && hangup the call in all others states !
					await this._Module._ModemVoiceManager.delete_call(this.get('dbus_path'));
				}
				*/
			}
		} catch (err) {
			this.debug(3,`Hangup failed for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
	async send_dtmf (symbol) {
		// Send a DTMF tone (Dual Tone Multi-Frequency) (only on supported modem).
		// Applicable only if state is MM_CALL_STATE_ACTIVE. 
		// IN s dtmf: DTMF tone identifier [0-9A-D*#].
		try {
			if (this._Ctrl !== null && this.get('state_code')==4) {
				await this._Ctrl._DBus.send_dtmf(symbol);
			}
		} catch (err) {
			this.debug(3,`Sending DTMF symbol failed for ${this.get('dbus_path')} - error: ${err}`);
		}
	}
}