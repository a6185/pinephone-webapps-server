// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Calls_Tbl extends PP_Tbl {
	constructor (...args) {
		super(...args);
	}
	add (_Object) {		
		this.debug(3,`Adding call...`);
		let uid = this.next_uid();
		_Object.set('uid',uid);
		this.debug(3,`Adding call uid "${uid}"...`);
		this.set(uid, _Object);
	}
	del (_Object) {
		this.debug(3,`Deleting call uid "${_Object.get('uid')}"...`);
		_Object.set('dbus_path',null);
		_Object.set('timestamp_end',(new Date()).toISOString());
	}
	online_calls () {
		let calls = [];
		for (let uid in this.obj) {
			let _Object  = this.obj[uid];
			if (_Object.is_active()) { // ACTIVE OR HELD
				calls.push(_Object);
				let dbus_path = _Object.get('dbus_path');
				let state_code = _Object.get('state_code');
				this.debug(3,`Online call found for ${dbus_path} - state code was ${state_code}.`);
			}
		}
		this.debug(3,`Online calls: ${calls.length}`);
		return calls;
	}
	// to avoid conflicts between backup calls and modem calls
	// modem calls have priority on backup calls, so we clean
	// backup calls
	merge (_Call) { // incoming modem call (reference)
		let same = [];
		for (let uid in this.obj) {
			let _Object  = this.obj[uid];
			if (_Object.get('uid')==_Call.get('uid')) { // ignore modem call
				continue;
			}
			if (_Object.get('timestamp_start')==_Call.get('timestamp_start') && _Object.get('number')==_Call.get('number') && _Object.get('direction')==_Call.get('direction')) {
				same.push(_Object);
			}
		}
		if (same.length) {
			same.forEach(_Object => {
				let uid = _Object.get('uid');
				delete this.obj[uid];
			});
		}
	}
	export_to_storage () {
		let rows = {};
		for (let uid in this.obj) {
			let _Object  = this.obj[uid];
			rows[uid] = _Object.export_to_storage();
		}
		return rows;
	}
}