// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Modem_Voice_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'modem_voice_slots', {
			'com.alsatux.phone:/calls/list': 'com.alsatux.phone:/calls/list',
			'com.alsatux.phone:/calls/add': null, // will be returned by CallAdded notification !
			// 'com.alsatux.phone:/calls/del': 'com.alsatux.phone:/call/deleted', // should be used instead of hangup() if the documentation said the truth...
			'com.alsatux.phone:calls/hangup/all': 'com.alsatux.phone:/calls/list'
		});
	}
	async handler (ws, data) {
		try {
			let _Status = new PP_Status();
			switch (data.route) {
				case 'com.alsatux.phone:/calls/list':
					return {
						calls: this._Module.tbl.calls.export()
					};
					break;
				case 'com.alsatux.phone:/calls/add':
					if (!ws.has_key(data.args, 'number', String)) {
						throw this._Module.tr('MISSING_OR_BAD_NUMBER');
					}
					if (!data.args.number.length) {
						throw this._Module.tr('MISSING_OR_BAD_NUMBER');
					}
					await this._Module._ModemVoiceManager.create_call(data.args.number);					
					break;					
				// should be used instead of hangup() if the documentation said the truth...
				/*
				case 'com.alsatux.phone:/calls/del':
					let _Call = null;
					if (ws.has_key( data.args, 'dbus_path', String)) {
						_Call = this._Module.tbl.calls.find_dbus_path(data.args.dbus_path);
					}
					if (_Call === null) {
						throw _PP.tr('MISSING_OR_BAD_DBUS_PATH');
					} else {
						this.debug(3,'Call found using client datas !');										
					}
					await this._Module._ModemVoiceManager.delete_call(data.args.dbus_path);
					_PP._WS.send_signal({
						args: {
							call: _Call.obj
						},
						callback: this.callbacks[data.route]
					},_Status);
					break;					
				*/
				case 'com.alsatux.phone:/calls/hangup/all':
					await this._Module._ModemVoiceManager.hangup_all();
					_PP._WS.send_signal({
						args: {
							calls: this._Module.tbl.calls.export()
						},
						callback: this.callbacks[data.route]
					},_Status);
					break;
			}	
		} catch (err) {
			this.debug(1, err);
			throw err;
		}
	}
}								