global.register_module({
	manifest: {
		version: '2.0',
		title: 'Cockatoo',
		name: 'contacts', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.contacts', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/app/com.alsatux.contacts',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/com.alsatux.contacts',
			format: 'json'
		}
	},
	files: [
		'app/com.alsatux.contacts/contact.object',
		'app/com.alsatux.contacts/contacts.slots',
		'app/com.alsatux.contacts/contacts.tbl',
		'app/com.alsatux.contacts/module'
	],
	main: 'Contacts',
	options: {
		parents: [ // linked modules
			'com.alsatux.prefs'
		]
	}
});
