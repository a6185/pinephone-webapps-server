// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Contacts extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
		let _Modules = global._PP._Core._Modules;
		this.parent = {
			'_Prefs': _Modules.get('com.alsatux.prefs')
		};
	}
	async init () {
		try {
			await this.defaults();
			this.set('tbl', {
				'contacts': new Contacts_Tbl(this, 999999, Contact_Object)
			});
			this.set('slots', {
				'contacts': new Contacts_Slots(this)		
			});
			await this.get_last_backup();
			let _Contact = new Contact_Object(this);
			for (let part in _Contact.const.CONTACT_RECORD.types) {
				_Contact.const.CONTACT_RECORD.types[part] = _Contact.const.CONTACT_RECORD.types[part].map(item => { return this.tr(item)});
				//global._Console.inspect(_Contact.CONTACT_RECORD.types[part]);
			}
			this.debug(1,this.tbl.contacts.length() + ' contacts found !');
			return this; // mandatory !
		} catch (err) {
			this.debug(1,'${err} - Module disabled !');
			throw err;
		}
	}
	import (json) {
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(json, 'contacts', Object)) {
			// we trust our own datas, so no check needed...
			for (let uid in json.contacts) {
				this.tbl.contacts.set(uid, (new Contact_Object(this)).upd(json.contacts[uid]));
			}
		}
	}
	export () {
		return JSON.stringify({
			contacts: this.tbl.contacts.export()
		});
	}
	import_from_client (data) {
		try {
			for (let uid in data.contacts) {
				this.tbl.contacts.add(data.contacts[uid]);
			}
		} catch (err) {
			throw(sprintf('%s%s (%s)',_PP.tr('DATAS_CORRUPTED'),_PP.tr('!'),err));
		}
	}
	async action (ws, data, type) {
		try {
			let action = this.get_action(data.route);
			let _Tbl = this.tbl[type + 's'];
			if (!ws.has_key(data.args, type, Object)) {
				throw this.tr('MISSING_OR_BAD_TYPE');
			}
			let obj = _Tbl[action](data.args[type]);
			let result = await this._IO.write();
			return obj;
		} catch (err) {
			throw err;
		}
	}	
}

