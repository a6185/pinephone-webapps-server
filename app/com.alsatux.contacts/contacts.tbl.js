// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Contacts_Tbl extends PP_Tbl {
	constructor (...args) {
		super(...args);
	}
	now2ymd () {
		let d = new Date();
		return global._PP._Fn.sprintf('%04d-%02d-%02d',d.getFullYear(),d.getMonth()+1,d.getDate());
	}
	add (obj) {		
		let duplicated = false;
		try {
			this.exists_object(obj);
			duplicated = true;
		} catch (err) {
			let _Object = new this.objectclass(this._Module);
			_Object.check_structure(obj, ['uid']);
			_Object.upd(obj);
			let uid = this.next_uid();
			_Object.set('uid',uid);
			_Object.set('published',this.now2ymd());
			_Object.set('updated',this.now2ymd());
			this.obj[uid] = _Object;
			return _Object.export();
		}
		if (duplicated) {
			throw global._PP._Fn.sprintf('%s%s',_PP.tr('DUPLICATE_UID'),_PP.tr('!'));
		}
	}
	upd (obj) {
		this.exists_object(obj);
		if (parseInt(obj.uid,10) == 0 ) {
			throw global._PP._Fn.sprintf('%s%s',_PP.tr('UNABLE_TO_CHANGE_DEFAULT_VALUE'),_PP.tr('!'));
		}
		let _Object = new this.objectclass(this._Module);
		_Object.check_structure(obj, []);
		_Object.upd(obj);
			_Object.set('updated',this.now2ymd());
		this.obj[obj.uid] = _Object;
		return _Object.export();
	}
	filter_kw (hash, needle) {
		if (hash==null) { // null date value for example
			return false;
		}
		switch (hash.constructor) {
			case String: 
				if (hash.length<100 && hash.search(needle)!=-1) {
					return true;
				}
				break;
			case Date: 
				let d = hash.to_yyyymmdd();
				if (d.search(needle)!=-1) {
					return true;
				}
				break;
			case Array:
				for (let i=0;i<hash.length;i++) {
					if (this.filter_kw(hash[i],needle)) {
						return true;
					}
				}
				break;
			case Object:
				for (let key in hash) {
					if (hash.hasOwnProperty(key)) {
						//console.log(key + ':' + hash[key] + ' - ' + hash[key].constructor);
						if (this.filter_kw(hash[key],needle)) {
							return true;
						}
					}
				}
				break;
		}
		return false;
	}
	filter_clean_keywords (keywords) {
		let re = /\s+/;
		let kws = keywords.split(re);
		let kws2 = [];
		for (let i=0;i<kws.length;i++) {
			let kw = kws[i].trim();
			if (kw.length) {
				kws2.push(kw);
			}
		}
		return kws2;
	}
	filter_kws (keywords) {
		let contacts_found = [];
		let kws = keywords.trim();
		if (!kws.length) {
			return contacts_found;
		}
		kws = this.filter_clean_keywords(kws);
		if (!kws.length) {
			return contacts_found;
		}
		for (let uid in this.obj) {
			if (!this.obj.hasOwnProperty(uid)) {
				continue;
			}
			let contact = this.obj[uid].obj;
			let found = false;
			kws.forEach(kw => {
				if (!found) {
					let pattern = '.*' + kw + '.*';
					//console.log(pattern);
					let re = new RegExp(pattern,'i');
					if (this.filter_kw(contact,re)) {
						found = true;
					}
				}
			});
			if (found) {
				contacts_found.push(contact);
			}
		}
		return contacts_found;
	}
	filter_by_part (part, key, value) {
		let contacts_found = [];
		for (let uid in this.obj) {
			let contact = this.obj[uid];
			if (contact.obj.hasOwnProperty(part)) {
				for (let uid2 in contact.obj[part]) {
					let obj2 = contact.obj[part][uid2];					
					if (obj2.hasOwnProperty(key)) {
						let val1 = obj2[key];
						if (part=='tel') {
							let v1 = val1.replace(/[^0-9]+/,'');
							let v2 = value.replace(/[^0-9]+/,'');
							if (v1.substr(-9)==v2.substr(-9)) {
								contacts_found.push(contact.export());
							}
						} else {
							if (val1==value) {
								contacts_found.push(contact.export());
							}
						}
					}
				}	
			}
		}
		return contacts_found;
	}
	get_header (uid) {
		// for phone, smms, ... modules
		if (!this.obj.hasOwnProperty(uid)) {
			return null;
		}
		let contact = this.obj[uid].obj;
		let fn = contact.hasOwnProperty('givenName') ? contact.givenName.join(', ') : '';
		let ln = contact.hasOwnProperty('familyName') ? contact.familyName.join(', ') : '';
		return {
			uid: uid,
			fn: fn,
			ln: ln
		}
	}
	get_contact_by_tel (number) {
		if (this._Contacts !== null) {
			let contacts = this.filter_by_part('tel', 'value', number);
			if (contacts.length) {
				return this.get_header(contacts[0].uid);
			}
		}	
		return null;	
	}
	import (obj) { // tbl, reset, override
		let mydb = {};
		for (let uid in obj.tbl) {
			let _Object = new Contact_Object(this._Module, obj.tbl[uid])
			_Object.check_structure([]);
			mydb[uid] = _Object;
		}
		if (obj.reset) {
			this.obj = mydb;
		} else {
			for (let uid in mydb) {
				if (Object.keys(this.obj).includes(uid)) {
					if (!obj.override) {
						throw global._PP._Fn.sprintf('%s"%s"%s',_PP.tr('DUPLICATE_UID'),_PP.tr(':'),uid,_PP.tr('!'));
					}
				}
				this.obj[uid] = mydb[uid];
			}
		}
	}
}