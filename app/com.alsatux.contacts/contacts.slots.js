// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Contacts_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'contacts', {
			'com.alsatux.contacts:/ls': null,
			'com.alsatux.contacts:/add': 'com.alsatux.contacts:/added',
			'com.alsatux.contacts:/upd': 'com.alsatux.contacts:/updated',
			'com.alsatux.contacts:/del': 'com.alsatux.contacts:/deleted',
			'com.alsatux.contacts:/import': 'com.alsatux.contacts:/read',
			'com.alsatux.contacts:/search': null,
			'com.alsatux.contacts:/search/bypart': null
		});
	}
	async handler (ws, data) {
		try {
			let result = null;
			let _Status = new PP_Status();
			switch (data.route) {
				case 'com.alsatux.contacts:/ls':		
					let _Pref_Object = this._Module.get_parent('_Prefs').tbl.prefs.find_by_cat_key('default','date_pattern');
					this.debug(1,`List successfull...`);
					return {
						db: this._Module.tbl.contacts.export(),
						date_pattern: { // from Prefs module...
							key: _Pref_Object.obj.key,
							value: _Pref_Object.obj.val
						}						
					};
					break;
				case 'com.alsatux.contacts:/add':
				case 'com.alsatux.contacts:/upd':
				case 'com.alsatux.contacts:/del':
					result = await this._Module.action(ws, data, 'contact');
					this.debug(1,`${data.route} successfull...`);
					_PP._WS.send_signal({
						args: {
							'contact': result
						},
						callback: this.callbacks[data.route]
					}, _Status);
					return {};
					break;
				case 'com.alsatux.contacts:/import':
					if (!ws.has_key(data.args, 'db', Object)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					if (!ws.has_key(data.args, 'reset', Boolean)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					if (!ws.has_key(data.args, 'override', Boolean)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					result = await this.action(ws,data.route,data.args);
					_PP._WS.send_signal({
						args: {
							db: this._Module.tbl.contacts.export()
						},
						callback: '/contact/read'
					}, _Status);
					this.debug(1,`Import successfull...`);
					return {};
					break;
				case 'com.alsatux.contacts:/search':
					if (!ws.has_key(data.args, 'keywords', String)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					return {
						contacts: this._Module.tbl.contacts.filter_kws(data.args.keywords)
					};
					break;
				case 'com.alsatux.contacts:/search/bypart':
					if (!ws.has_key(data.args, 'part', String)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					if (!ws.has_key(data.args, 'key', String)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					if (!ws.has_key(data.args, 'value', String)) {
						throw this._Module.tr('MISSING_OR_BAD_ARG');
					}
					return {
						contacts: this._Module.tbl.contacts.filter_by_part(data.args.part, data.args.key, data.args.value)
					};
					break;
			} // end switch
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}
}


