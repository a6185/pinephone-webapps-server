// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Contact_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'contact_object');
		this.const = Object.freeze({
			CONTACT_RECORD: {
				arrays: 'name,honorificPrefix,givenName,additionalName,familyName,honorificSuffix,nickname,category,org,jobTitle,note,key'.split(','),
				strings: 'uid,sex,genderIdentity'.split(','),
				dates: 'bday,anniversary,published,updated'.split(','),
				parts: {
					adr: 'streetAddress,locality,postalCode,region,countryName,type'.split(','),
					tel: 'value,carrier,type'.split(','),
					email: 'value,type'.split(','),
					url: 'value,type'.split(','),
					impp: 'value,type'.split(',')
				},
				types: {
					adr: 'home,work'.split(','),
					tel: 'mobile,home,work,personal,faxHome,faxOffice,faxOther,another'.split(','),
					email: 'personal,home,work'.split(','),
					url: 'personal,home,work'.split(','),
					impp: 'personal,home,work'.split(',')
				}
			},
			CONTACT_MAX_PHOTO_SIZE: 500 // Ko
		});
	}
	check_type (key,value,types) {
		let label = key; // to translate...
		this.check_field(label,'is_array',key,value);
		value.forEach(type => {
			let label2 = type; // to translate...
			this.check_field(label2,'is_string',key,type,0,255);
		});
	}
	check (obj,key,value,type) {
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		let sprintf = global._PP._Fn.sprintf;
		switch (type) {
			case 'UID':
				this.check_field(label, 'is_string', key, value, 6, 6);
				if (value.length && value.match(/^\d{6}$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),key,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'String':
				if (key=='note') {
					this.check_field(label,'is_string',key,value,0,10000);
				} else {
					this.check_field(label,'is_string',key,value,0,255);
				}
				break;
			case 'Date':
				this.check_field(label,'is_date',key,value,'YYYY-MM-DD');
				break;
			case 'Photo':
				if (value.length>this.const.CONTACT_MAX_PHOTO_SIZE*1024) {
					throw sprintf('%s "%s" %s (> %d Ko) !',_PP.tr('THE_PHOTO'),key,_PP.tr('IS_TOO_LONG'), this.const.CONTACT_MAX_PHOTO_SIZE);
				}
				if (value.match(/^data:image\/jpe?g;base64,([0-9a-zA-Z+\/]{4})*(([0-9a-zA-Z+\/]{2}==)|([0-9a-zA-Z+\/]{3}=))?$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_PHOTO'),key,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'Array': 
				this.check_field(label,'is_array',key,value);
				value.forEach(value2 => {
					this.check(key,value2,'String');
				});
				break;
		}
	}
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/4');
		this.check_object(obj);
		// uid (mandatory, even if empty)		
		if (bypass.indexOf('uid')==-1) {
			this.check(obj,'uid',obj.uid,'UID');
		}
		// name|honorificPrefix|givenName|additionalName|familyName|honorificSuffix|nickname|category|org|jobTitle|note|key		
		this.const.CONTACT_RECORD.arrays.forEach(key => {
			if (obj.hasOwnProperty(key)) {
				this.check(obj,key,obj[key],'Array');
			}
		});
		// uid|sex|genderIdentity
		this.const.CONTACT_RECORD.strings.forEach(key => {
			if (key!='uid' && obj.hasOwnProperty(key)) {
				this.check(obj,key,obj[key],'String');
			}
		});
		// bday|anniversary|published|updated
		this.const.CONTACT_RECORD.dates.forEach(key => {
			if (obj.hasOwnProperty(key)) {
				this.check(obj,key,obj[key],'Date');
			}
		});
		// parts
		this.debug(3,'check_structure 2/4');
		Object.keys(this.const.CONTACT_RECORD.parts).forEach(part => {			
			if (obj.hasOwnProperty(part)) { // optionnal
				let fields = this.const.CONTACT_RECORD.parts[part];
				for (let seq in obj[part]) {
					let obj2 = obj[part][seq];
					fields.forEach(field => {
						if (obj2.hasOwnProperty(field)) {
							switch (field) {
								case 'type':
									this.check_type(field,obj2[field],this.const.CONTACT_RECORD.types[part]);
									break;
								default:
									this.check(obj,field,obj2[field],'String');
									break;
							}
						}
					});
				}				
			}
		});
		// photos
		this.debug(3,'check_structure 3/4');
		if (obj.hasOwnProperty('photo')) {
			let label = 'photo'; // to translate
			let value = obj['photo'];
			this.check_field(label, 'not_null', 'photo', value);
			this.check_field(label, 'not_undefined', 'photo', value);
			this.check_field(label, 'is_object', 'photo', value);
			for (let seq in value) {
				let photo = value[seq];
				this.check(obj,seq,photo,'Photo');
			}
		}
		this.debug(3,'check_structure 4/4');
	}
}

