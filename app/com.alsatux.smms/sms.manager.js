// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMS_Manager {
	constructor (_Module) {
		this._Module = _Module;
	}
	debug (level, msg) {
		global._Console.debug(level,"<sms_manager>" + msg + "</sms_manager>\n");
	}
	now () {
		let d = new Date();
		d = d - 2000; // to avoid conflicts order in autotests
		// 2022-08-02T21:07:01+02:00
		return d.toISOString().substr(0,10)+'T'+d.toLocaleString().substr(11,8)+d.toTimeString().substr(12,3)+':00';
	}
	get_contact (number) {
		let contact = null;
		if (this._Module.get_parent('_Contacts') !== null) {
			contact = this._Module.get_parent('_Contacts').tbl.contacts.get_contact_by_tel(number);
		}
		return contact;		
	}
	async add (sms) {					
		let number = sms.content.number;
		let hasKey = global._PP._Fn.hasKey
		// MMS can be empty, when we get an error while sending MMS...
		if (!sms.content.text.length && !sms.parts.length) {
			return;
		}
		if (!hasKey(this._Module._DB,number,Object)) {
			this._Module._DB[number] = {
				contact: this.get_contact(number),
				_SMS_Tbl: new SMS_Tbl(this._Module, 999999,SMS_Object)
			};
		} else {
			if (this._Module._DB[number].contact === null) {
				this._Module._DB[number].contact = this.get_contact(number);
			}
		}
		let contact = this._Module._DB[number].contact;
		let obj = {
			uid: null,
			text: sms.content.text,
			parts: sms.parts,
			timestamp: sms.properties.timestamp,
			received: sms.received
		};
		let _SMS_Object = new SMS_Object(this._Module);
		_SMS_Object.import(obj);
		this._Module._DB[number]._SMS_Tbl.add(_SMS_Object);
		obj.uid = _SMS_Object.get('uid');
		await this._Module.export_to_fs(number);
		let messages = this._Module.export_to_clients();
		let _Status = new PP_Status();
		_PP._WS.send_signal({
			args: {
				number: number,
				contact: contact,
				message: obj,
				messages: messages
			},
			callback: 'com.alsatux.smms:/added'
		}, _Status);		
	}
	message_delete_by_uid (number, uid) {
		// TODO
	}
}