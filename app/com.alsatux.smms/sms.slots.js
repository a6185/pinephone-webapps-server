// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMS_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'smms', {
			'com.alsatux.smms:/read': null,
			'com.alsatux.smms:/get': null,
			'com.alsatux.smms:/add': 'com.alsatux.smms:/added',
			'com.alsatux.smms:/del': 'com.alsatux.smms:/deleted'
		});
	}
	check_number (ws, data) {
		if (!ws.has_key(data.args, 'number', String)) {
			throw this._Module.tr('MISSING_OR_BAD_NUMBER');
		}
		if (!data.args.number.length) {
			throw this._Module.tr('MISSING_OR_BAD_NUMBER');
		}
	}
	async handler (ws, data) {
		try {	
			ws._Status.code(0);
			switch (data.route) {
				case 'com.alsatux.smms:/read':
					let messages = this._Module.export_to_clients();
					return {
						messages: messages
					};
					break;
				case 'com.alsatux.smms:/get':
					this.check_number(ws, data);
					let number = data.args.number;
					let hasKey = global._PP._Fn.hasKey;
					if (hasKey(this._Module._DB, number, Object)) {
						return {
							contact: this._Module._DB[number].contact,
							number: number,
							messages: this._Module._DB[number]._SMS_Tbl.export()
						}
					} else {
						throw this._Module.tr('UNKNOWN_SENDER');	
					}
					break;
				case 'com.alsatux.smms:/add':
					this.check_number(ws, data);
					let obj = null;
					if (ws.has_key(data.args, 'text', String) && data.args.text.length) { // sms
						obj = {
							number: data.args.number,
							text: data.args.text
						};
					} else {
						if (ws.has_key(data.args, 'data', Array) && data.args.data.length) { // mms
							obj = {
								number: data.args.number,
								data: data.args.data
							};
						} else {
							throw this._Module.tr('EMPTY_SMS');	
						}
						//throw this._Module.tr('MMS_NOT_SUPPORTED_YET');
					}
					if (obj !== null) {
						await this._Module._SMMS_Manager.message_create(obj);
					}
					return {};
					break;
				case 'com.alsatux.smms:/del':
					this.check_number(ws, data);
					if (!ws.has_key(data.args, 'uid', String)) {
						throw this._Module.tr('MISSING_OR_BAD_UID');
					}
					if (!data.args.uid.length) {
						throw this._Module.tr('MISSING_OR_BAD_UID');
					}
					this._Module._SMS_Manager.message_delete_by_uid(data.args.number, data.args.uid);
					return {
						args: {
							number: uid
						},
						callback: 'com.alsatux.smms:/deleted'
					};
					break;
			} // end switch
			this.debug(1,`Handler ${data.route} ok...`);
		} catch(err) {
			ws._Status.code(1);
			ws._Status.err(err);
			this.debug(1,`Handler ${data.route} failed: ${err}`);
		}
	}
}