#!/bin/bash

modem=$(mmcli -L)

echo "Status:"
mmcli -m ${modem} --messaging-status

cat << EOM

This option states which STORAGE to use for SMS messages.  Possible values for STORAGE include:
'sm'   SIM card storage area.
'me'   Mobile equipment storage area.
'mt'   Sum of SIM and Mobile equipment storages
'sr'   Status report message storage area.
'bm'   Broadcast message storage area.
'ta'   Terminal adaptor message storage area.

Read:
/org/freedesktop/ModemManager1/SMS/3 (receiving)
mmcli -s 3
  -----------------------
  General    |      path: /org/freedesktop/ModemManager1/SMS/3
  -----------------------
  Content    |    number: +33678959921
  -----------------------
  Properties |  pdu type: deliver
             |     state: receiving
             |   storage: me
             |      smsc: +33695000664
             | timestamp: 2021-08-23T22:08:21+0

To delete
mmcli -m X --messaging-delete-sms=Y

EOM

echo "Message list:"
mmcli -m ${modem} --messaging-list-sms
