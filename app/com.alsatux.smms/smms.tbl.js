// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMMS_Tbl extends PP_Tbl {
	constructor (...args) {
		super(...args);
	}
	add (_Object) {		
		this.debug(3,`Adding smms...`);
		let uid = this.next_uid();
		_Object.set('uid',uid);
		this.debug(3,`Adding smms uid "${uid}"...`);
		this.set(uid, _Object);
	}
	upd (_Object) {
		let uid = _Object.get('uid');
		if (this.obj.hasOwnProperty(uid)) {
			_Object.set('uid',uid);
			this.set(uid, _Object);
		}
	}
	del (_Object) {
		this.debug(3,`Deleting smms uid "${_Object.get('uid')}"...`);
		_Object.set('dbus_path',null);
		//_Object.set('datetime_end',(new Date()).toISOString());
	}
}