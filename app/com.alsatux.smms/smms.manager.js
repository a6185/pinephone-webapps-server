// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMMS_Manager {
	constructor (_Module) {
		this._Module = _Module;
		this._ModemManager = _Module.get_parent('_Modem')._ModemManager;
		this.message_list_retrieved = false;
		this._Storage = new PP_Storage(this);
		// My provider (Free 2€) force to use 2G/3G mode when sending SMS...
		this.smsCurrentModes = [ 6, 4 ]; 
		this.oldCurrentModes = null; // old modem mode before sending SMS
	}
	async init () {
		try {
			await this._Storage.init();
			if (this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging !== null) {
				this._ModemManager.on('org.freedesktop.ModemManager1.Modem.Messaging', 'Added', this.on_external_dbus_notifications.bind(this));
				// external apps like chatty can delete an internal object
				this._ModemManager.on('org.freedesktop.ModemManager1.Modem.Messaging', 'Deleted', this.on_external_dbus_notifications.bind(this));
				await this.message_list();
			}
			//this._ModemManager.on('org.freedesktop.DBus.ObjectManager', 'InterfacesAdded', this.on_external_dbus_notifications.bind(this));
			//_ModemManager.on('org.freedesktop.DBus.ObjectManager', 'InterfacesRemoved', this.on_external_dbus_notifications.bind(this));
			//_ModemManager.on('org.freedesktop.ModemManager1.Modem', 'StateChanged', this.on_external_dbus_notifications.bind(this));
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	debug (level, msg) {
		global._Console.debug(level,"<smms_manager>" + msg + "</smms_manager>\n");
	}
	async message_list() {
		this.debug(3,'Retreiving message list');
		let _Ctrl = this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging;
		if (_Ctrl !== null && !this.message_list_retrieved) {
			let message_list = _Ctrl.get('properties').Messages;
			for (let i=0;i<message_list.length;i++) {
				let _SMMS_Object = await this.message_add(message_list[i],null);
				let sms = _SMMS_Object.obj;
				if ([1,32].indexOf(sms.properties.pdu_type)!=-1) { // DELIVER
					sms.received = true;
				}
				if ([2,33].indexOf(sms.properties.pdu_type)!=-1) { // SUBMIT
					sms.received = false;
				}
				_SMMS_Object.obj = sms;
				this._Module._SMS_Manager.add(sms);
				await this.message_delete(message_list[i]);
			}
			this.message_list_retrieved = true;			
		}
	}
	check_messaging () {
		let _Ctrl = this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging;
		if (_Ctrl === null) {
			throw "Messaging interface not found - did you unlock your SIM ?";
		}
		return _Ctrl;
	}
  async message_create (obj) {
		this.debug(3,'Create SMS/MMS');
		/*
			obj = {
				number: 0102030405,
				text: 'Tous ces mots pour ne rien dire...'
			};
			
			OR
			
			obj = {
				number: 0102030405,
				data: ???
			};
		*/
		let _Ctrl = this.check_messaging();
		let hasKey = global._PP._Fn.hasKey;
		if (!hasKey(obj,'number',String) || !obj.number.length) {
			throw 'Bad or empty number !';
		}
		let final_number = this._Module.get_parent('_Prefs')._Fn.clean_phone_number(obj.number);
		const Variant = _Ctrl._DBus.dbus_next.Variant;
		let obj2 = null;
		let now = new Date();		
		// Properties of Dbus::create() should be Number, Text/Data with uppercase... documentation or implementation error ?
		if (hasKey(obj,'text',String) && obj.text.length) { // sms
			obj2 = {
				number: new Variant('s', final_number), 
				text: new Variant('s', obj.text)
			};
		} else {
			if (hasKey(obj,'data',Array) && obj.data.length) { // mms
				obj2 = {
					number: new Variant('s', final_number), 
					data: new Variant('ay', Buffer.from(obj.data.map(c => '0x'+c)))
				};
			}
		}
		if (obj2 !== null) {
			let dbus_path = await _Ctrl._DBus.create(obj2);
			return dbus_path;
		}
		return null;
	}
	async message_add (dbus_path,received) {
		try {
			this.debug(3,'Add SMS/MMS');
			let _SMMS_Object = new SMMS_Object(this._Module, this, dbus_path, received);
			await _SMMS_Object.init();
			this._Module.tbl.smms.add(_SMMS_Object);
			return _SMMS_Object;
		} catch (err) {
			this.debug(1,`Unable to add SMS/MMS : ${err}`);
			return null;
		}
	}
	async store (_SMMS_Object) {
		this.debug(3,'prepare downloading mms...');
		try {
			let sms = _SMMS_Object.obj;
			let dbus_path = sms.dbus_path;
			this.debug(3,'Trying to store SMS/MMS...');
			// remember: outgoing messages have no timestamp !
			let timestamp = sms.properties.timestamp;
			let filename = this._Module.sender2fs(sms.content.number);
			let mmsdata = sms.content.data.map(s => (s).toString(16)).map(x => x.length<2 ? '0'+x : x).join('').toUpperCase();
			let _MMSDecoder = new MMSDecoder(mmsdata);
			_MMSDecoder.parse();
			let prop = _MMSDecoder.mms;
			let d = new Date(timestamp);
			let v = parseInt(prop['EXPIRY'].value,10)*1000;
			let expiry = prop['EXPIRY'].type=='seconds' ? (new Date(d.getTime()+v)) : new Date(v);
			let mms = {
				sender: sms.content.number,
				relpath: this._Module.get('storage').relpath + '/' + filename,
				abspath: this._Module.get('storage').abspath + '/' + filename,
				date: new Date(timestamp),
				expiry: expiry,
				prop: prop,
				parts: _MMSDecoder.PARTS
			}
			this._Storage.mkdir('sdcard1',mms.relpath);
			await this.download(mms);
		} catch (err) {
			this.debug(3,`Unable to store ${dbus_path}: ${err}`);
		} 
	}
	async download (mms) {
		this.debug(3,'downloading mms...');
		let fs = global._PP._Nodes.get('file-system');
		let _GSMData = new GSMData(this._Module);
		await _GSMData.on();
		if (mms.prop['CONTENTLOCATION'].length) {
			let filename = `${mms.abspath}/${mms.date.to_yyyymmddhhiiss()}.dat`;
			// avoid override existing files
			if (!this._Storage.exists(filename)) {
				let proxy = MMS_PROXY.length ? '--proxy ' + MMS_PROXY : ''; // cf. core/config.js
				this._Shell.exec(`curl --interface wwan0 ${proxy} "${prop['CONTENTLOCATION']}" -o ${filename}`,{ 
					timeout: 30000
				});
				if (this._Storage.exists(filename)) { // success
					this._Storage.chown(filename,STORAGE_USER,STORAGE_GROUP);
					this._Storage.chmod(filename,STORAGE_FILE);
					this.debug(3,'downloading mms ok !');
				}
			}			
		}
		await _GSMData.off();
	}
	async message_delete (dbus_path) {
		this.debug(3,'Delete SMS/MMS');
		let _SMMS_Object = this._Module.tbl['smms'].find_dbus_path(dbus_path);
		if (_SMMS_Object !== null) {
			let _Ctrl = this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging;
			if (_Ctrl !== null) {
				await _Ctrl._DBus.delete(dbus_path);
			}
			this._Module.tbl.smms.delete_dbus_path(dbus_path);
		}
	}
	async handle (_SMMS_Object) {
		this.debug(3,'Handle');
		/*
			MMSmsState: {
				// State of a given SMS.
				MM_SMS_STATE_UNKNOWN: 0, // State unknown or not reportable.
				MM_SMS_STATE_STORED: 1, // The message has been neither received nor yet sent.
				MM_SMS_STATE_RECEIVING: 2, // The message is being received but is not yet complete.
				MM_SMS_STATE_RECEIVED: 3, // The message has been completely received.
				MM_SMS_STATE_SENDING: 4, // The message is queued for delivery.
				MM_SMS_STATE_SENT: 5 // The message was successfully sent.
			},
			MMSmsPduType: {
				// Type of PDUs used in the SMS.
				MM_SMS_PDU_TYPE_UNKNOWN: 0, // Unknown type.
				MM_SMS_PDU_TYPE_DELIVER: 1, // 3GPP Mobile-Terminated (MT) message.
				MM_SMS_PDU_TYPE_SUBMIT: 2, // 3GPP Mobile-Originated (MO) message.
				MM_SMS_PDU_TYPE_STATUS_REPORT: 3, // 3GPP status report (MT).
				MM_SMS_PDU_TYPE_CDMA_DELIVER: 32, // 3GPP2 Mobile-Terminated (MT) message. Since 1.2.
				MM_SMS_PDU_TYPE_CDMA_SUBMIT: 33, // 3GPP2 Mobile-Originated (MO) message. Since 1.2.
				MM_SMS_PDU_TYPE_CDMA_CANCELLATION: 34, // 3GPP2 Cancellation (MO) message. Since 1.2.
				MM_SMS_PDU_TYPE_CDMA_DELIVERY_ACKNOWLEDGEMENT: 35, // 3GPP2 Delivery Acknowledgement (MT) message. Since 1.2.
				MM_SMS_PDU_TYPE_CDMA_USER_ACKNOWLEDGEMENT: 36, // 3GPP2 User Acknowledgement (MT or MO) message. Since 1.2.
				MM_SMS_PDU_TYPE_CDMA_READ_ACKNOWLEDGEMENT: 37 // 3GPP2 Read Acknowledgement (MT or MO) message. Since 1.2.
			},
		*/
		let sms = _SMMS_Object.obj;		
		switch (sms.properties.state) {
			case 0: // UNKNOWN				
				if (sms.properties.pdu_type == 2) { // SUBMIT
					this.oldCurrentModes = await this._ModemManager.get_interface_property('org.freedesktop.ModemManager1.Modem','CurrentModes');
					this.debug(3,`oldCurrentModes = ${this.oldCurrentModes}`);
					// TODO: check supported mode !
					// let supported_modes = await this._ModemManager.get_interface_property('org.freedesktop.ModemManager1.Modem','SupportedModes');
					// this.debug(3,`SupportedModes = ${supported_modes}`);
					// switch to 2G/3G
					await this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem._DBus.set_current_modes(this.smsCurrentModes);
					await _SMMS_Object._Ctrl._DBus.send();
				}
				break;
			case 3: // RECEIVED
			case 5: // SENT
				if (sms.properties.state == 5) {
					// return to 2G/3G/4G
					await this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem._DBus.set_current_modes(this.oldCurrentModes);
				}
				if (sms.content.data.length) { // MMS
					await this.store(_SMMS_Object);
				}
				this._Module._SMS_Manager.add(sms);
				// remove dbus_path
				await this.message_delete(sms.dbus_path);
				break
		}		
	}
	async on_properties_changed (_SMMS_Object, old_sms, new_sms) {
		this.debug(3,'PropertiesChanged');
		await this.handle(_SMMS_Object);
	}
	async on_external_dbus_notifications (_Ctrl, signal, ...args) {
		try {
			this.debug(3,`Incoming external DBus notifications from ${_Ctrl.origin} with signal ${signal}...`);
			let _SMMS_Object = null;
			let dbus_path = null;
			if (_Ctrl === this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging) {
				dbus_path = args[0];
				switch (signal) {
					case 'Added':
						let received = args[1];
						_SMMS_Object = await this.message_add(dbus_path, received);
						await this.handle(_SMMS_Object);
						let _Screen = new PP_Screen(this._Module);
						_Screen.unlock();
						break;
					case 'Deleted':
					  // dbus_path message has been deleted by us or external program
						_SMMS_Object = this._Module.tbl.smms.find_dbus_path(dbus_path);
						if (_SMMS_Object !== null) {
							// no need to check the controller here
							this._Module.tbl.smms.delete_dbus_path(dbus_path);
						}
						break;
				}
			}
			if (_Ctrl === this._ModemManager._DBus_Ctrl__MM1__MM1__DBus_ObjectManager) {
				switch (signal) {
					case 'InterfacesAdded':
						dbus_path = args[0];
						let dict = args[1];
						if (dbus_path == 'org.freedesktop.ModemManager1.Modem.Messaging') {
							await this.init();
						}
						break;
					/*case 'InterfacesRemoved':
						dbus_path = args[0];
						let interfaces = args[1];
						break;*/
				}			
			}
			/*if (_Ctrl === this._ModemManager._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem) {
				switch (signal) {
					case 'StateChanged':
					let old_state = args[0];
					let new_state = args[1];
					let reason = args[2];*/
					// The "StateChanged" signal
					// StateChanged (i old, i new, u reason);
					// The modem's state (see "State") changed. 
					// i old: A MMModemState value, specifying the new state.
					// i new: A MMModemState value, specifying the new state.
					// u reason: A MMModemStateChangeReason value, specifying the reason for this state change.
					/*
					MMModemState: {
						// Enumeration of possible modem states.
						MM_MODEM_STATE_FAILED: -1, // The modem is unusable.
						MM_MODEM_STATE_UNKNOWN: 0, // State unknown or not reportable.
						MM_MODEM_STATE_INITIALIZING: 1, // The modem is currently being initialized.
						MM_MODEM_STATE_LOCKED: 2, // The modem needs to be unlocked.
						MM_MODEM_STATE_DISABLED: 3, // The modem is not enabled and is powered down.
						MM_MODEM_STATE_DISABLING: 4, // The modem is currently transitioning to the MM_MODEM_STATE_DISABLED state.
						MM_MODEM_STATE_ENABLING: 5, // The modem is currently transitioning to the MM_MODEM_STATE_ENABLED state.
						MM_MODEM_STATE_ENABLED: 6, // The modem is enabled and powered on but not registered with a network provider and not available for data connections.
						MM_MODEM_STATE_SEARCHING: 7, // The modem is searching for a network provider to register with.
						MM_MODEM_STATE_REGISTERED: 8, // The modem is registered with a network provider, and data connections and messaging may be available for use.
						MM_MODEM_STATE_DISCONNECTING: 9, // The modem is disconnecting and deactivating the last active packet data bearer. This state will not be entered if more than one packet data bearer is active and one of the active bearers is deactivated.
						MM_MODEM_STATE_CONNECTING: 10, // The modem is activating and connecting the first packet data bearer. Subsequent bearer activations when another bearer is already active do not cause this state to be entered.
						MM_MODEM_STATE_CONNECTED: 11 // One or more packet data bearers is active and connected.
					},
					MMModemStateChangeReason: {
						// Enumeration of possible reasons to have changed the modem state.
						MM_MODEM_STATE_CHANGE_REASON_UNKNOWN: 0, // Reason unknown or not reportable.
						MM_MODEM_STATE_CHANGE_REASON_USER_REQUESTED: 1, // State change was requested by an interface user.
						MM_MODEM_STATE_CHANGE_REASON_SUSPEND: 2, // State change was caused by a system suspend.
						MM_MODEM_STATE_CHANGE_REASON_FAILURE: 3 // State change was caused by an unrecoverable error.
					}
					*/
					/*if (new_state==8 || new_state==11) {
						await this.message_list();
					}
					break;
				}
			}*/
		} catch (err) {
			this.debug(1,`Handling notification [${signal}] failed !`);
		}
	}	
}
