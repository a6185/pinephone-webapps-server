/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';
const SMMS_LANG = {
	msg: [
	],
	key: [
		'!','!',' !',
		'MISSING_MESSAGE_OBJECT','Message object is missing','L\'objet message est manquant',
		'MISSING_OR_BAD_MODEM','Missing or bad modem','Modem manquant ou inconnu',
		'MISSING_OR_BAD_DBUS_PATH','Missing or bad dbus path','Chemin DBUS manquant ou erroné',
		'MISSING_OR_BAD_ARGS','Missing or bad arguments','Arguments manquants ou erronés',
		'MISSING_OR_BAD_NUMBER','Missing or bad number','Numéro manquant ou erroné',
		'EMPTY_SMS','SMS/MMS is empty','Le message est vide'
	]
};