// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMS_Tbl extends PP_Tbl {
	constructor (...args) {
		super(...args);
	}
	add (_Object) {		
		this.debug(3,`Adding sms...`);
		let uid = this.next_uid();
		_Object.set('uid',uid);
		this.debug(3,`Adding sms uid "${uid}"...`);
		this.obj[uid] = _Object;
	}
	upd (_Object) {
		let uid = _Object.get('uid');
		if (this.obj.hasOwnProperty(uid)) {
			_Object.set('uid',uid);
			this.obj[uid] = _Object;
		}
	}
	del (_Object) {
		this.debug(3,`Deleting sms uid "${_Object.get('uid')}"...`);
	}
	get_last_smms () {
		let m = [];
		for (let uid in this.obj) {
			m.push(this.obj[uid]);
		}
		if (m.length>1) {
			// sort by date
			m.sort((a,b) => {
				return new Date(b.obj.timestamp)-new Date(a.obj.timestamp);
			});
		}
		return m.shift().export();
	}
} 