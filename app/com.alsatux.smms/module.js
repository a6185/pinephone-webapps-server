// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMMS extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
		let _Modules = global._PP._Core._Modules;
		this.parent = {
			'_Modem': _Modules.get('pp.core.modem'),
			'_Contacts': _Modules.get('com.alsatux.contacts'),
			'_Prefs': _Modules.get('com.alsatux.prefs')
		};
		this._SMMS_Manager = new SMMS_Manager(this);
		this._SMS_Manager = new SMS_Manager(this);
		this._DB = {};
		this._Storage = new PP_Storage(this);
	}
	async init () {
		try {
			await this.defaults();
			this.set('tbl', {
				'smms': new SMMS_Tbl(this, 999999, SMMS_Object)
			});
			this.set('slots', {
				'sms': new SMS_Slots(this)
			});
			let n = await this.import_from_fs();
			this._SMMS_Manager.init();
			this.debug(1,n + ' SMS/MMS found !');
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Error ${err} ! Module disabled !`);
			throw err;
		}
	}
	async import_from_fs () {
		let n = 0;
		let glob = global._PP._Nodes.get('glob');
		let files = glob.sync(this.obj.storage.abspath + '/*.js');
		let _Contacts = this.get_parent('_Contacts');
		for (let i=0;i<files.length;i++) {
			let file = files[i];
			let filename = file.split('/').pop();
			let sender = filename.replace(/.js$/,'');
			let res = await this._Storage.read(this.obj.storage.media, this.obj.storage.relpath, filename, {
				encoding: 'utf-8',
			});
			if (res.data.content.length) {
				let json = JSON.parse(res.data.content);
				let contact = json.contact;
				if (contact === null || contact === undefined) {
					if (_Contacts !== null) {
						contact = _Contacts.tbl.contacts.get_contact_by_tel(sender);
					}
				}
				let db_created = false;
				let _SMS_Object;
				for (let uid in json.message) {
					let message = json.message[uid];
					if (message.timestamp.length) {
						if (!db_created) {
							this._DB[sender] = {
								contact: contact,
								_SMS_Tbl: new SMS_Tbl(this, 999999,SMS_Object)
							};
							db_created = true;
						}
						let ignored = false
						for (let i in this._DB[sender]._SMS_Tbl.obj) {
							let obj = this._DB[sender]._SMS_Tbl.obj[i].obj;
							/*global._Console.debug(3,obj);
							global._Console.debug(3,message);
								debugger;*/
							//if (obj.timestamp == message.timestamp && obj.data == message.data && obj.text == message.text && obj.received == message.received) {
							if (obj.data == message.data && obj.text == message.text && obj.received == message.received) {
								ignored = true;
								break;
							}
						}
						if (!ignored) {
							_SMS_Object = new SMS_Object(this);
							_SMS_Object.import(message);
							this._DB[sender]._SMS_Tbl.add(_SMS_Object);
							n++;
						}
					}
				}
			}
		}
		/*for (let sender in this._DB) {
			if (this._DB.hasOwnProperty(sender)) {
				this.export_to_fs(sender);
			}
		}*/
		return n;
	}
	sender2fs (sender) {
		// take care : sender can contains a number or a name !
		let file = sender.replace(/[^A-Z0-9\+]+/i,'');
		if (!file.length) {
			file = "john.doe";
		}
		return file;
	}
	async export_to_fs (sender) {
		try {
			let file = this.sender2fs(sender) + '.js';
			if (!this._DB.hasOwnProperty(sender)) {
				return;
			}
			let content = JSON.stringify({
				contact: this._DB[sender].contact,
				message: this._DB[sender]._SMS_Tbl.export()
			});
			await this._Storage.write(this.obj.storage.media, this.obj.storage.relpath, file, content, {
				encoding: 'utf-8',
			});
		} catch (err) {
			this.debug(1,`export_to_fs failed: ${err}`);
		}
	}
	export_to_clients () {
		let obj = {};
		for (let sender in this._DB) {
			let _SMS_Tbl = this._DB[sender]._SMS_Tbl;
			let last = _SMS_Tbl.get_last_smms();
			obj[sender] = {
				contact: this._DB[sender].contact,
				last: last
			};
		}
		return obj;
	}
	import (json) {
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(json, 'smms', Object)) {
			// we trust our own datas, so no check needed...
			debugger;
			for (let uid in json.smms) {
				let _SMMS_Object = new SMMS_Object(this);
				_SMMS_Object.import(json.smms[uid]);
				this.tbl.smms.obj[uid] = _SMMS_Object;
			}
		}
	}
	export () {
		return JSON.stringify({
			smms: this.tbl.smms.export()
		});
	}
}

