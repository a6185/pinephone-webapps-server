// Version 0.81 //

'use strict';
// javascript translation from PHP: Jean Luc Biellmann - contact@alsatux.com

/**
 * Copyright (C) 2004-2009 Jonatan Heyman
 *
 * This file is part of the PHP application MMS Decoder.
 *
 * MMS Decoder is free software; you can redistribute it and/or
 * modify it under the terms of the Affero General Public License as
 * published by Affero, Inc.; either version 1 of the License, or
 * (at your option) any later version.
 *
 * MMS Decoder is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero General Public License for more details.
 *
 * You should have received a copy of the Affero General Public
 * License in the COPYING file that comes with The Affero Project; if
 * not, write to Affero, Inc., 510 Third Street, Suite 225, San
 * Francisco, CA 94107 USA.
 */

/*-------------------------------*
 * The MMS header decoding class *
 *-------------------------------*/
class MMSDecoder {

	constructor (data) {

		/*---------------------------------------------------*
		* Constants                                         *
		*                                                   *
		* http://wapforum.org/                              *
		* WAP-209-MMSEncapsulation-20020105-a               *
		* Table 8                                           *
		*                                                   *
		* The values are enconded using WSP 7bit encoding.  *
		* Read more about how to decode this here:          *
		* http://www.nowsms.com/discus/messages/12/3287.html*
		*                                                   *
		* Example from the above adress:                    *
		* 7Bit 0D =  0001101                                *
		* 8Bit 0D = 10001101 = 8D                           *
		*---------------------------------------------------*/

		this.MMS_DECODER_DEBUG = 0;	/* Print parseerrors? Print values while they are parsed? If you enable this,
			  getting the binary encoded confirmation message whensending MMS from mobiles
			  will not work. This is only for development purpose. */

		this.const = Object.freeze({

			BCC: '81',
			CC: '82',
			CONTENT_LOCATION: '83',
			CONTENT_TYPE: '84',
			DATE: '85',
			DELIVERY_REPORT: '86',
			DELIVERY_TIME: '87',
			EXPIRY: '88',
			FROM: '89',
			MESSAGE_CLASS: '8A',
			MESSAGE_ID: '8B',
			MESSAGE_TYPE: '8C',
			MMS_VERSION: '8D',
			MESSAGE_SIZE: '8E',
			PRIORITY: '8F',
			READ_REPLY: '90',
			REPORT_ALLOWED: '91',
			RESPONSE_STATUS: '92',
			RESPONSE_TEXT: '93',
			SENDER_VISIBILITY: '94',
			STATUS: '95',
			SUBJECT: '96',
			TO: '97',
			TRANSACTION_ID: '98',

			/*--------------------------*
			* Array of header contents *
			*--------------------------*/
			mmsMessageTypes: {
				'80': 'm-send-req',
				'81': 'm-send-conf',
				'82': 'm-notification-ind',
				'83': 'm-notifyresp-ind',
				'84': 'm-retrieve-conf',
				'85': 'm-acknowledge-ind',
				'86': 'm-delivery-ind',
				'00': null
			},

			/*--------------------------*
			* Some other useful arrays *
			*--------------------------*/
			mmsYesNo: {
				'80': 1,
				'81': 0,
				'00': null
			},

			mmsPriority: {
				'80': 'Low',
				'81': 'Normal',
				'82': 'High',
				'00': null
			},

			mmsMessageClass: {
				'80': 'Personal',
				'81': 'Advertisement',
				'82': 'Informational',
				'83': 'Auto'
			},

			mmsContentTypes: {
				'00': '*/*',
				'01': 'text/*',
				'02': 'text/html',
				'03': 'text/plain',
				'04': 'text/x-hdml',
				'05': 'text/x-ttml',
				'06': 'text/x-vCalendar',
				'07': 'text/x-vCard',
				'08': 'text/vnd.wap.wml',
				'09': 'text/vnd.wap.wmlscript',
				'0A': 'text/vnd.wap.wta-event',
				'0B': 'multipart/*',
				'0C': 'multipart/mixed',
				'0D': 'multipart/form-data',
				'0E': 'multipart/byterantes',
				'0F': 'multipart/alternative',
				'10': 'application/*',
				'11': 'application/java-vm',
				'12': 'application/x-www-form-urlencoded',
				'13': 'application/x-hdmlc',
				'14': 'application/vnd.wap.wmlc',
				'15': 'application/vnd.wap.wmlscriptc',
				'16': 'application/vnd.wap.wta-eventc',
				'17': 'application/vnd.wap.uaprof',
				'18': 'application/vnd.wap.wtls-ca-certificate',
				'19': 'application/vnd.wap.wtls-user-certificate',
				'1A': 'application/x-x509-ca-cert',
				'1B': 'application/x-x509-user-cert',
				'1C': 'image/*',
				'1D': 'image/gif',
				'1E': 'image/jpeg',
				'1F': 'image/tiff',
				'20': 'image/png',
				'21': 'image/vnd.wap.wbmp',
				'22': 'application/vnd.wap.multipart.*',
				'23': 'application/vnd.wap.multipart.mixed',
				'24': 'application/vnd.wap.multipart.form-data',
				'25': 'application/vnd.wap.multipart.byteranges',
				'26': 'application/vnd.wap.multipart.alternative',
				'27': 'application/xml',
				'28': 'text/xml',
				'29': 'application/vnd.wap.wbxml',
				'2A': 'application/x-x968-cross-cert',
				'2B': 'application/x-x968-ca-cert',
				'2C': 'application/x-x968-user-cert',
				'2D': 'text/vnd.wap.si',
				'2E': 'application/vnd.wap.sic',
				'2F': 'text/vnd.wap.sl',
				'30': 'application/vnd.wap.slc',
				'31': 'text/vnd.wap.co',
				'32': 'application/vnd.wap.coc',
				'33': 'application/vnd.wap.multipart.related',
				'34': 'application/vnd.wap.sia',
				'35': 'text/vnd.wap.connectivity-xml',
				'36': 'application/vnd.wap.connectivity-wbxml',
				'37': 'application/pkcs7-mime',
				'38': 'application/vnd.wap.hashed-certificate',
				'39': 'application/vnd.wap.signed-certificate',
				'3A': 'application/vnd.wap.cert-response',
				'3B': 'application/xhtml+xml',
				'3C': 'application/wml+xml',
				'3D': 'text/css',
				'3E': 'application/vnd.wap.mms-message',
				'3F': 'application/vnd.wap.rollover-certificate',
				'40': 'application/vnd.wap.locc+wbxml',
				'41': 'application/vnd.wap.loc+xml',
				'42': 'application/vnd.syncml.dm+wbxml',
				'43': 'application/vnd.syncml.dm+xml',
				'44': 'application/vnd.syncml.notification',
				'45': 'application/vnd.wap.xhtml+xml',
				'46': 'application/vnd.wv.csp.cir',
				'47': 'application/vnd.oma.dd+xml',
				'48': 'application/vnd.oma.drm.message',
				'49': 'application/vnd.oma.drm.content',
				'4A': 'application/vnd.oma.drm.rights+xml',
				'4B': 'application/vnd.oma.drm.rights+wbxml'
			},

			// character set (mibenum numbers by IANA, ored with '80')
			mmsCharSet: {
				'EA': 'utf-8',
				'83': 'ASCII', // ascii
				'84': 'iso-8859-1',
				'85': 'iso-8859-2',
				'86': 'iso-8859-3',
				'87': 'iso-8859-4'
			}
		});
		if (data.indexOf('AF84')!=-1) {
			this.data = data.replace(/.*AF84/,''); // AF 84 = WAP PUSH APPLICATION
		} else {
			this.data = data;	// The unparsed MMS data in an array of the ascii numbers
		}
		this.data_original = data;
		this.pos = 0;	// The current parsing position of the data array
		this.PARTS = [];
		// The parsed data will be saved in these variables

		this.mms = {
			BBC: null,
			CC: null,
			CONTENTLOCATION: null,
			CONTENTTYPE: null,
			DATE: null,
			DELIVERYREPORT: null,
			DELIVERYTIME: null,
			EXPIRY: null,
			FROM: null,
			MESSAGECLASS: null,
			MESSAGEID: null,
			MESSAGETYPE: null,
			MMSVERSIONMAJOR: null,
			MMSVERSIONMINOR: null,
			MESSAGESIZE: null,
			PRIORITY: null,
			READREPLY: null,
			REPORTALLOWED: null,
			RESPONSESTATUS: null,
			RESPONSETEXT: null,
			SENDERVISIBILITY: null,
			STATUS: null,
			SUBJECT: null,
			TO: null,
			TRANSACTIONID: null,
			MMSVERSIONRAW: null, 	// used for the m-send-conf (confirmation answer)
			CONTENTTYPE_PARAMS: null	// parameter-values for the MMS content-type
		};
	}

	decode (byte) {
		let uint = parseInt(byte,16);
		if (this.MMS_DECODER_DEBUG) console.log('decode: code=' + byte + ' / uint=' + uint);
		return uint;
	}

	// This function is called when the data is to be parsed
	parse () {
		try {
			// Reset position
			this.pos = 0;
			// parse the header
			while (this.parseHeader());
			// Header done, fetch parts, but make sure the header was parsed correctly
			if (this.mms.CONTENTTYPE == 'application/vnd.wap.multipart.related' || this.mms.CONTENTTYPE == 'application/vnd.wap.multipart.mixed')
				while (this.parseParts());
			else
				return 0;
			return 1;
		} catch (e) {
			console.log(e);
			return 1;
		}
	}

	cur_byte () {
		if (this.data.length<2) {
			return null;
		}
		return this.data.substr(0,2);
	}

	read_byte () {
		if (this.data.length<2) {
			return null;
		}
		let byte = this.data.substr(0,2);
		this.data = this.data.substr(2);
		this.debug('pos: ' + this.pos + ' / byte = ' + byte + ' / remaining length = ' + this.data.length);
		this.pos+=2;
		return byte;
	}

	chr (code) { // '43' -> '+'
		return String.fromCharCode(code);
	}

	ord (char) { // '+' -> 43
		return char.chatCodeAt(0);
	}

	/*---------------------------------------------------*
	 * This function checks what kind of field is to be  *
	 * parsed at the moment                              *
	 *                                                   *
	 * If true is returned, the class will go on and     *
	 * and continue decode the header. If false, the     *
	 * class will end the header decoding.               *
	 *---------------------------------------------------*/
	parseHeader () {

		this.debug('parseHeader');
		let byte = this.read_byte();
		if (byte === null)
			return 0;
		switch (byte) {
			case this.const.BCC:
				this.mms.BBC = this.parseEncodedStringValue();
				this.debug('BCC', this.mms.BBC);
				break;
			case this.const.CC:
				this.mms.CC = this.parseEncodedStringValue();
				this.debug('CC', this.mms.CC);
				break;
			case this.const.CONTENT_LOCATION:
				this.mms.CONTENTLOCATION = this.parseTextString();
				this.debug('Content-location', this.mms.CONTENTLOCATION);
				break;
			case this.const.CONTENT_TYPE:
				if (this.decode(byte) <= 31) { /* Content-general-form */
					len = this.parseValueLength();
					// check if next byte is in range of 32-127. Then we have a Extension-media which is a textstring
					if (this.decode(byte) > 31 && this.decode(byte) < 128)
						this.mms.CONTENTTYPE = this.parseTextString();
					else {
						// we have Well-known-media; which is an integer
						this.mms.CONTENTTYPE = this.const.mmsContentTypes[this.parseIntegerValue()];
					}
				} else if (this.decode(byte) < 128) { /* Constrained-media - Extension-media*/
					this.mms.CONTENTTYPE = this.parseTextString();
				} else /* Constrained-media - Short Integer*/
					this.mms.CONTENTTYPE = this.const.mmsContentTypes[this.parseShortInteger()];
				let next_byte = this.data.substr(0,2);
				// Ok, now we have parsed the content-type of the message, let's see if there are any parameters
				let noparams = false;
				while (!noparams) {
					switch (next_byte) {
						case '89': // Start, textstring
							this.read_byte();
							this.parseTextString();
							break;
						case '8A': // type, constrained media
							byte = this.read_byte();
							if (this.decode(byte) < 128) { /* Constrained-media - Extension-media*/
								byte = this.read_byte();
								this.parseTextString();
							} else // Constraind-media Short Integer
								this.mms.CONTENTTYPE_PARAMS[type] = this.parseShortInteger();
							break;
						default:
							noparams = 1;
							break;
					}
				}

				this.debug('Content-type', this.mms.CONTENTTYPE);
				// content-type parsed, that means we have reached the end of the header
				return 0;
			case this.const.DATE: /* In seconds from 1970-01-01 00:00 GMT */
				this.mms.DATE = this.parseDate();
				this.debug('Date', this.mms.DATE);
				break;
			case this.const.DELIVERY_REPORT:		/* Yes | No */
				this.mms.DELIVERYREPORT = this.const.mmsYesNo[ this.read_byte() ];
				this.debug('Delivery-report', this.mms.DELIVERYREPORT);
				break;
			case this.const.DELIVERY_TIME:
				this.debug('Delivery-time', this.mms.DELIVERYTIME);
				break;
			case this.const.EXPIRY:
				this.mms.EXPIRY = this.parseDate();
				this.debug('Expiry', this.mms.EXPIRY);
				break;
			case this.const.FROM:
				/**
				* TODO: make better encoding for this field
				* The encoding mechanism works like this:
				*  From-value = ['01' or VALUE-length] ['80' or '81'] [Optional: Encoded-String-Value]
				* If we have '80' we have an encoded-string-value ('80' is part of that string).
				* If we have '81' this is a insert-adress-token which means that the MMSC is supposed
				* to insert it, which means that we can't retrieve the sender.
				*/

				this.mms.FROM = this.parseEncodedStringValue();
				this.debug('From', this.mms.FROM);
				break;
			case this.const.MESSAGE_CLASS:
				this.mms.MESSAGECLASS = this.const.mmsMessageClass[ this.parseMessageClassValue() ];
				this.debug('Message-class', this.mms.MESSAGECLASS);
				break;
			case this.const.MESSAGE_ID:		/* Text string */
				this.mms.MESSAGEID = this.parseTextString();
				this.debug('Message-id', this.mms.MESSAGEID);
				break;
			case this.const.MESSAGE_TYPE:
				this.mms.MESSAGETYPE = this.const.mmsMessageTypes[ this.read_byte() ];
				this.debug('Message-type', this.const.mmsMessageTypes[this.mms.MESSAGETYPE]);
				break;
			case this.const.MMS_VERSION:
				/**
				* The version number (1.0) is encoded as a WSP short integer, which
				* is a 7 bit value.
				*
				* The three most significant bits (001) are used to encode a major
				* version number in the range 1-7. The four least significant
				* bits (0000) contain a minor version number in the range 1-14.
				*/
				this.mms.MMSVERSIONRAW = this.read_byte();
				let version = this.decode(this.mms.MMSVERSIONRAW);
				this.mms.MMSVERSIONMAJOR = (version & '0x70') >> 4;
				this.mms.MMSVERSIONMINOR = (version & '0x0F');
				this.debug('MMS-version', this.mms.MMSVERSIONMAJOR + '.' + this.mms.MMSVERSIONMINOR);
				break;
			case this.const.MESSAGE_SIZE:		/* Long integer */
				this.mms.MESSAGESIZE = this.parseLongInteger();
				this.debug('Message-size', this.mms.MESSAGESIZE);
				break;
			case this.const.PRIORITY:			/* Low | Normal | High */
				this.mms.PRIORITY = this.const.mmsPriority[ this.read_byte() ];
				this.debug('Priority', this.mms.PRIORITY);
				break;
			case this.const.READ_REPLY:		/* Yes | No */
				this.mms.READREPLY = this.const.mmsYesNo[ this.read_byte() ];
				this.debug('Read-reply', this.mms.READREPLY);
				break;
			case this.const.REPORT_ALLOWED:		/* Yes | No */
				this.mms.REPORTALLOWED = this.const.mmsYesNo[ this.read_byte() ];
				this.debug('Report-allowed', this.mms.REPORTALLOWED);
				break;
			case this.const.RESPONSE_STATUS:
				this.mms.RESPONSESTATUS = this.read_byte();
				this.debug('Response-status', this.mms.RESPONSESTATUS);
				break;
			case this.const.RESPONSE_TEXT:		/* Encoded string value */
				this.mms.RESPONSETEXT = this.parseEncodedStringValue();
				this.debug('Response-text', this.mms.RESPONSETEXT);
				break;
			case this.const.SENDER_VISIBILITY:		/* Hide | show */
				this.mms.SENDERVISIBILITY = this.const.mmsYesNo[ this.read_byte() ];
				this.debug('Sender-visibility', this.mms.SENDERVISIBILITY);
				break;
			case this.const.STATUS:
				this.mms.STATUS = this.read_byte();
				this.debug('Status', this.mms.STATUS);
				break;
			case this.const.SUBJECT:
				this.mms.SUBJECT = this.parseEncodedStringValue();
				this.debug('Subject', this.mms.SUBJECT);
				break;
			case this.const.TO:
				this.mms.TO = this.parseEncodedStringValue();
				this.debug('To', this.mms.TO);
				break;
			case this.const.TRANSACTION_ID:
				this.mms.TRANSACTIONID = this.parseTextString();
				this.debug('Transaction-id', this.mms.TRANSACTIONID);
				break;
			default:
				if (this.decode(byte) > 127) {
					this.debug('Parse error:', 'Unknown field (' + byte + ') !', this.pos-2);
				} else {
					this.debug('Parse error:', 'Value encountered when expecting field !', this.pos-2);
				}
				break;
		}

		return true;
	}

	parseDate() {
		// Total of bytes to read
		let octetcount = this.read_byte();
		// Error checking
		if (this.decode(octetcount) > 30)
			throw('Parse error: Short-length-octet (' + octetcount + ') > 30 in Long-integer at offset ' + this.pos-2 + '!\n');
		let token = this.read_byte();
		let longint = '';
		let value = this.parseLongInteger();
		if (token == '81') { // relative-token
			this.debug('Date relative: ' + value + ' s.');
			return {
				type: 'seconds',
				value: value
			};
		}
		if (token == '80') { // absolute-token
			this.debug('Date absolute: ' + date('Y-m-d- H:i:s',value) + '.');
			return {
				type: 'date',
				value: value
			};
		}
		throw('Parse error: unknown date at offset ' + this.pos-2 + '!\n');
	}

	/*-------------------------------------------------------------------*
	 * Parse message-class                                               *
	 * message-class-value = Class-identifier | Token-text               *
	 * Class-idetifier = Personal | Advertisement | Informational | Auto *
	 *-------------------------------------------------------------------*/
	parseMessageClassValue() {
		if (this.decode(this.cur_byte()) > 127) {
			// the byte is one of these 128=personal, 129=advertisement, 130=informational, 131=auto
			return this.read_byte();
		} else
			return this.parseTextString();
	}

	/*----------------------------------------------------------------*
	 * Parse Text-string                                              *
	 * text-string = [Quote <Octet 127>] text [End-string <Octet 00>] *
	 *----------------------------------------------------------------*/
	parseTextString() {
		let str = '';
		// Remove quote
		if (this.cur_byte() == '7F') {
			this.read_byte();
		}

		while (this.cur_byte() != '00') {
			//str += this.chr(this.read_byte());
			str += String.fromCharCode(parseInt(this.read_byte(),16));
		}
		this.read_byte();
		return str;
	}


	/*------------------------------------------------------------------------*
	 * Parse Encoded-string-value                                             *
	 *                                                                        *
	 * Encoded-string-value = Text-string | Value-length Char-set Text-string *
	 *                                                                        *
	 *------------------------------------------------------------------------*/
	parseEncodedStringValue() {
		if (this.decode(this.cur_byte()) <= 31) {
			let len = this.parseValueLength();
			let mibenum = this.decode(this.read_byte());
			let charset = '';
			// handle unknown charsets
			if (Object.keys(this.const.mmsCharSet).indexOf(mibenum)!=-1) {
				charset = this.const.mmsCharSet[mibenum];
			}

			let raw = this.parseTextString();
			// the only case we can handle currently is utf8 since character encoding support
			// in native PHP is so lousy
			if (charset == 'utf-8')
			   raw = new TextDecoder('utf-8').decode(raw);
			return raw;
			//for (i = 0; i < len-1; i++)
			//	str .= chr( this.data[this.pos++] );
			//return str;
		} else
			return this.parseTextString();
	}


	/*--------------------------------------------------------------------------------*
	 * Parse Value-length                                                             *
	 * Value-length = Short-length<Octet 0-30> | Length-quote<Octet 31> Length<Uint>  *
	 *                                                                                *
	 * A list of content-types of a MMS message can be found here:                    *
	 * http://www.wapforum.org/wina/wsp-content-type.htm                              *
	 *--------------------------------------------------------------------------------*/
	parseValueLength() {
		let byte = this.decode(this.cur_byte());
		if (byte < 31) {
			// it's a short-length
			return this.read_byte();
		} else if (byte == 31) {
			// got the quote, length is an Uint
			this.read_byte();
			return this.parseUint();
		} else {
			// uh, oh... houston, we got a problem
			throw('Parse error: Short-length-octet (' + this.cur_byte() + ') > 31 in Value-length  at offset ' + this.pos + '!\n');
		}
	}


	/*--------------------------------------------------------------------------*
	 * Parse Long-integer                                                       *
	 * Long-integer = Short-length<Octet 0-30> Multi-octet-integer<1*30 Octets> *
	 *--------------------------------------------------------------------------*/
	parseLongInteger() {
		let byte = this.cur_byte();
		// Get the number of octets which the long-integer is stored in
		let octetcount = this.decode(this.read_byte());
		// Error checking
		if (octetcount > 30)
			throw('Parse error: Short-length-octet (' + byte + ') > 30 in Long-integer at offset ' + this.pos-2 + '!\n');
		// Get the long-integer
		let longint = '';
		for (let i = 0; i < octetcount; i++) {
			longint += this.read_byte();
		}

		return parseInt(longint,16);
	}


	/*------------------------------------------------------------------------*
	 * Parse Short-integer                                                    *
	 * Short-integer = OCTET                                                  *
	 * Integers in range 0-127 shall be encoded as a one octet value with the *
	 * most significant bit set to one, and the value in the remaining 7 bits *
	 *------------------------------------------------------------------------*/
	parseShortInteger() {
		return this.decode(this.read_byte()) & '0x7F';
	}


	/*-------------------------------------------------------------*
	 * Parse Integer-value                                         *
	 * Integer-value = short-integer | long-integer                *
	 *                                                             *
	 * This function checks the value of the current byte and then *
	 * calls either parseLongInt() or parseShortInt() depending on *
	 * what value the current byte has                             *
	 *-------------------------------------------------------------*/
	parseIntegerValue() {
		if (this.decode(this.cur_byte()) < 31) {
			return this.parseLongInteger();
		} else if (this.decode(this.cur_byte()) > 127) {
			return this.parseShortInteger();
		} else {
			this.debug('ERROR', 'Not a IntegerValue field', this.pos);
			this.read_byte();
			return 0;
		}
	}


	/*------------------------------------------------------------------*
	 * Parse Unsigned-integer                                           *
	 *                                                                  *
	 * The value is stored in the 7 last bits. If the first bit is set, *
	 * then the value continues into the next byte.                     *
	 *                                                                  *
	 * http://www.nowsms.com/discus/messages/12/522.html                *
	 *------------------------------------------------------------------*/
	parseUint() {
		if (!(this.decode(this.cur_byte()) & '0x80')) {
			return this.decode(this.read_byte()) & '0x7F';
		}

		let uint = 0;
		while (this.decode(this.cur_byte()) & '0x80') {
			// Shift the current value 7 steps
			uint = uint << 7;
			// Remove the first bit of the byte and add it to the current value
			uint |= this.decode(this.read_byte()) & '0x7F';
		}

		// Shift the current value 7 steps
		uint = uint << 7;
		// Remove the first bit of the byte and add it to the current value
		uint |= this.decode(this.read_byte()) & '0x7F';
		return uint;
	}


	/*---------------------------------------*
	 * Function which outputs debug messages *
	 *---------------------------------------*/
	debug (name, str, pos = -1, errorlevel = 0) {
		if (!this.MMS_DECODER_DEBUG) {
			return true;
		}

		let out = '';
		if (pos != -1) {
			out += '<b>' + name + ' (' + pos +'):</b> ' + str;
		} else {
			out += '<b>' + name + ':</b> ' + str;
		}

		out += '<br>\n';
		console.log(out);
		if (errorlevel > 0) {
			throw 'Fatal error !';
		}
	}


	/*---------------------------------------------------------------------*
	 * Function called after header has been parsed. This function fetches *
	 * the different parts in the MMS. Returns true until it encounter end *
	 * of data.                                                            *
	 *---------------------------------------------------------------------*/
	parseParts() {
		let i,j,ctype;
		if (this.pos>=this.data.length) {
			return 0;
		}

		// get number of parts
		let count = this.parseUint();
		this.debug('MMS parts', count);
		for (i = 0; i < count; i++) {
			// new part, so clear the old data and header
			let data = '';
			let header = '';
			// get header and data length
			let headerlen = this.parseUint();
			let datalen = this.parseUint();
			/* PARSE CONTENT-TYPE */
			// this is actually the same structure as in the MMS content-type
			// so maybe we should make this in a better way, but for now, I'll
			// just cut n paste

			// right now I just save the position in the MMS data array before I parse
			// the content-type, to be able to roll back after it has been parsed beacause
			// the headerlen includess both the content-type and the header
			// TODO: this is just a fast hack and shoul be done in a more proper way
			let ctypepos = this.pos;
			if (this.decode(this.cur_byte()) <= 31) { /* Content-general-form */
				// the value follows after the current byte and is 'current byte' long
				
				let len = this.parseValueLength();
				// ???? No sens for the following... JLB
				// check if next byte is in range of 32-127. Then we have a Extension-media which is a textstring
				if (this.decode(this.cur_byte()) > 31 && this.decode(this.cur_byte()) < 128)
					ctype = this.parseTextString();
				else {
					// we have Well-known-media; which is an integer
					ctype = this.const.mmsContentTypes[this.parseIntegerValue()];
				}
			} else if (this.decode(this.cur_byte()) < 128) { /* Constrained-media - Extension-media*/
				read_byte();
				ctype = this.parseTextString();
			} else /* Constrained-media - Short Integer */
				ctype = this.const.mmsContentTypes[this.parseShortInteger()];
			// roll back position so it's just before the content-type again
			this.pos = ctypepos;
			/* END OF CONTENT TYPE */

			// Read header. Actually, we don't do anything with this yet.. just skipping it (note that the content-type is includesd in the header)
			for (j = 0; j < headerlen; j++)
				header += chr(read_byte());
			// read data
			for (j = 0; j < datalen; j++)
				data += chr(read_byte());
			this.debug('Part (i):headerlen', headerlen);
			this.debug('Part (i):datalen', datalen);
			this.debug('Part (i):content-type', ctype);
			//this.debug('Part (i):data', data); // I've commented this one, to get a cleaner debug

			this.PARTS.push(new MMSPart(headerlen, datalen, ctype, header, data));
		}

		return false;
	}

	/**
	 * Send an OK response to the sender after the MMS has been recieved
	 * See '6.1.2. Send confirmation' in the wap-209-mmsencapsulation specification, on how this is constructed
	 */

	confirm() {
		let str = '';
		str += '8C'; // message-type
		str += '81'; // m-send-conf
		str += '98'; // transaction-id
		str += this.mms.TRANSACTIONID.toString(16);
		str += '00'; // end of string
		str += '8D'; // version
		str += '90'; // 1.0
		str += '92'; // response-status
		str += '80'; // OK
		str += '8b'; // Message-id
		// generate a message id based on the time
		str += Date.now().toString(16)
		str += '00'; // end of string
		// respond with the m-send-conf
		return str;
	}
}


/*---------------------------------------------------------------------*
 * The MMS part class                                                  *
 * An instance of this class contains the one parts of an MMS message. *
 *                                                                     *
 * The multipart type is formed as:                                    *
 * number |part1|part2|....|partN                                      *
 * where part# is formed by headerlen|datalen|contenttype|headers|data *
 *---------------------------------------------------------------------*/
class MMSPart {

	/*----------------------------------*
	 * Constructor, just store the data *
	 *----------------------------------*/
	MMSPart(headerlen, datalen, ctype, header, data) {
		this.hpos = 0;
		this.headerlen = headerlen;
		this.DATALEN = datalen;
		this.CONTENTTYPE = ctype;
		this.DATA = data;
	}

	/*-------------------------------------*
	 * Save the data to a location on disk *
	 *-------------------------------------*/
	save(filename) {
		/*
		fp = fopen(filename, 'wb');
		fwrite(fp, this.DATA);
		fclose(fp);
		*/
	}
}

/*
function test_send_mms () {
	// convert /var/www/html/app/com.alsatux.statscars/img/icon-32.png /home/mobian/icon-32.jpg
	// mmcli -m 0 --messaging-create-sms='number=+33695132690' --messaging-create-sms-with-data=/home/mobian/icon-32.jpg
	// mmcli -s XXX should return data =
	// FFD8FFE000104A46494600010102000200020000FFDB004300030202020202030202020303030304060404040404080606050609080A0A090809090A0C0F0C0A0B0E0B09090D110D0E0F101011100A0C12131210130F101010FFDB00430103030304030408040408100B090B1010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010FFC00011080020002003011100021101031101FFC400190000020301000000000000000000000000060702050809FFC4002A100001030303030304030000000000000001020304050607081112002122133151092341611453A1FFC4001C0100020202030000000000000000000000040506070203000108FFC4003211000200040404030704030000000000000102030405110006213112415161228191071332A1B1D1F0141542E171B2C1FFDA000C03010002110311003F00DC39472DE3CC336B48BC724DD10E8B4C601016FABCDD5EC4843681BA9C5900EC94824EC761D0F1E656090A05D8EC06E7EC3B9B0C66A85B5D875C733B2EFD5DB26CFBA1F6B0ADA947A55BAC8E0CBB5A8CA913241FEC5043810D8FC04F97C93DF61DAAC575F19E13DBEE46BE831C2541D35C2A647D4FB572FB8569BBE90CA49DF837486788FD7704FF00BD0B1A9B0E3FC6EFE4ECBFEA463358EC9B01E80FD6F86A69D75A1AC4CDD7A2A90AB9EDC836ED1D8352B8EB7268E80CD3602012B709040E64254129FC9049F14A8889E6487234194F7EEF199D8854411A2DDDCECA3C47CCF21D4D816322D1A6E27000A00D49E15D0733B636BE4FD3B61FCD15762BB94AD35DC5322B65A8EA9552961B65276E410DA1D0846FB0DC8482761BEFB0EBCFD0BDA36668258A4CEA7527821927CCA5FF00C74E5899351245EC0C3DBB9FBE029FD1369169C94BB2B1251D94921214F542484927D8793DDFA3E573FE739F6F772D199CF458684FA04C698947A6411788A00EEC47FDC42D6D2F68DAE788F4FB431A59D5A8D1DE54675F8920CA6D0EA40250541646E029276F823E7ADF50CE19CE98C12A132D09880C03228620F3B04FA91AE30834DA64717828186D704DBEB822B8F4D58EE462AB971558149A7D930AE553066BF4A889E67838DA8850DC72DD0829D89EC16AF9EE14AE729B7AB4B546759A3BC20DC21ACA2E41034171A9B0B8B6C2FB636C4A64312EF0210081AD7B6BD3F3CF007A7CABC8CAF665C7745C57D5EF4EA4A52FD1819B70C421B52990A5BEDBAC45616D38DA5692956E40277F71D9C6759539667E0494B4384F10D9FC30DC7F220290D12202188371BFAE06A5C4FD7C178AECC06A3523A6FA28B5B08C8975696EDCAC5A75691A87BDEEAA8596ECFF004A726120BD304B052F22439291F7470529095A7C82481CBC51C6CC8353CEA2081069F0610E40B13F243A79DB0984A5322B9BC72C7C87CDADF9AED87EAEE1C7940D383355C20934E8176D45486E5C92E4579E94F38A54A5B8F049521C586DD6C3A80420949478A00EABFA4C9552B79D44AE607E264BB3283E1B017550069C3720D8EB6BF10B9386B33125E5297C7262C0E80F3BF33FDFA62E69767532C0BBA8F5AB623B31DE6E6B14EAD3D1E337144C8CFA0B01B9284F1F55E6E508EB4BBC4ABCDF1EC547AB7F3AD2A5E728ED74178455974DB8585EDD011707B62354B987873235F8AE0F7B8FBE0B69BA5EFE06184E1A4D46B1022498EE37509B4A94C2243EA75454EFDC5A362083E9EE51BF0007E01EA0751C999A262BC6B96811181054317E11C22C34B03A6FBEFAF3C37815390493129E3039916BEBBFE5B6C2BE8FF4C5C2F4C98D4A96CDE353436A0A54797568A96D7B1F63E936856C7F4A07F7D48631F685194AA8974EE38AFF003B8F96025FD994DCF19F4FEB051AA1C67912262DB5EDCC3B8E27CB6E8B5A8AE2A0D29F6592CC06D875B53693CF92490B090A4A491DCF63B6EAF24648AD516B712AB557562EAC090C492CC41B9BA8E879E08AAD56566A544BCB822C472D2C01EF8B9B4ED7C9994E65B722E1B3AA36F41A24E454E63F5C8CDC77965B6B66E1C76D0F38B003AE2DC5B8A002836D80492A08B32B9251EA54E8B292CC15DC58122E077B6114A45481196238B818FFD9
}
test_send_mms();
*/

/*
function test_decode_mms_data () {
	let data = '00060401BEAF848C829850356F6F787A46714565755376345350615767676B41008D928918802B33333635343332313031312F545950453D504C4D4E0086818A808F818E0305D6DB83687474703A2F2F3231332E3232382E332E36302F6D6D732E7068703F50356F6F787A46714565755376345350615767676B41008805810303F480';
	let _MMSDecoder = new MMSDecoder(data);
	_MMSDecoder.parse();
	console.dir(_MMSDecoder.mms);	
	//	Output:
	//	{
	//	  BBC: null,
	//	  CC: null,
	//	  CONTENTLOCATION: 'http://213.228.3.60/mms.php?P5ooxzFqEeuSv4SPaWggkA',
	//	  CONTENTTYPE: null,
	//	  DATE: null,
	//	  DELIVERYREPORT: 0,
	//	  DELIVERYTIME: null,
	//	  EXPIRY: { type: 'seconds', value: 259200 },
	//	  FROM: '+33654321011/TYPE=PLMN',
	//	  MESSAGECLASS: 'Personal',
	//	  MESSAGEID: null,
	//	  MESSAGETYPE: 'm-notification-ind',
	//	  MMSVERSIONMAJOR: 1,
	//	  MMSVERSIONMINOR: 2,
	//	  MESSAGESIZE: 382683,
	//	  PRIORITY: 'Normal',
	//	  READREPLY: null,
	//	  REPORTALLOWED: null,
	//	  RESPONSESTATUS: null,
	//	  RESPONSETEXT: null,
	//	  SENDERVISIBILITY: null,
	//	  STATUS: null,
	//	  SUBJECT: null,
	//	  TO: null,
	//	  TRANSACTIONID: 'P5ooxzFqEeuSv4SPaWggkA',
	//	  MMSVERSIONRAW: '92',
	//	  CONTENTTYPE_PARAMS: null
	//	}
	console.dir(_MMSDecoder.PARTS);	
}
test_decode_mms_data();
*/
