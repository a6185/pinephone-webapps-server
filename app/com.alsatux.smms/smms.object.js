// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMMS_Object extends PP_Object {
	constructor (_Module, _Manager, dbus_path, received) {
		super(_Module,'smms_object');
		this._Manager = _Manager;
		this._Ctrl = new DBus_Ctrl__MM1__MM1_SMS__MM1_Sms(this._Module, this, dbus_path);
		this.obj =  {
			dbus_path: dbus_path,
			received: received,
			parts: [],
			content: {
				data: [], // Message data. When sending, if the data is larger than the limit of the technology or modem, the message will be broken into multiple parts or messages. Note that Text and Data are never given at the same time.
				number: '', // Number to which the message is addressed.
				text: '', // Message text, in UTF-8. When sending, if the data is larger than the limit of the technology or modem, the message will be broken into multiple parts or messages. Note that Text and Data are never given at the same time.
				/* properties */					
			},
			properties: {
				pdu_type: 0, // Type of PDUs used in the SMS. - cf. MM_SMS_PDU_TYPE
				state: 0, // State of a given SMS - cf. MM_SMS_STATE
				//validity: [], // Indicates when the SMS expires in the SMSC. This value is composed of a MMSmsValidityType key, with an associated data which contains type-specific validity information: MM_SMS_VALIDITY_TYPE_RELATIVE: The value is the length of the validity period in minutes, given as an unsigned integer (D-Bus signature 'u'). Cf. MM_SMS_VALIDITY_TYPE
				validity: {
					type: 0,
					value: 0
				},
				storage: 0, // A MMSmsStorage value, describing the storage where this message is kept. Cf. MM_SMS_STORAGE
				smsc: '', // Indicates the SMS service center number. Always empty for 3GPP2/CDMA.
				class_3gpp: 0, // 3GPP message class (-1..3). -1 means class is not available or is not used for this message, otherwise the 3GPP SMS message class. 'Personal' or 'Advertisement' or 'Informational' or 'Auto' 
				teleservice_id: 0, // A MMSmsCdmaTeleserviceId value. Always UNKNOWN for 3GPP. Cf. MM_SMS_CDMA_TELESERVICE_ID
				service_category: 0, // A MMSmsCdmaServiceCategory value. Always UNKNOWN for 3GPP. Cf. MM_SMS_CDMA_SERVICE_CATEGORY
				
				delivery_report_request:false, // confirmation from the SMSC that your SMS messages has arrived to the recipient handset
				message_reference: 0, // Message Reference of the last PDU sent/received within this SMS. If MM_SMS_PDU_TYPE==STATUS_REPORT, this field identifies the Message Reference of the PDU associated to the status report.
				timestamp: '', // Time when the first PDU of the SMS message arrived the SMSC, in ISO8601 format. This field is only applicable if MM_SMS_PDU_TYPE==DELIVER or MM_SMS_PDU_TYPE==STATUS_REPORT.
				delivery_state: 0, // A MMSmsDeliveryState value, describing the state of the delivery reported in the Status Report message. Enumeration of known SMS delivery states as defined in 3GPP TS 03.40. - cf. MM_SMS_DELIVERY_STATE. This field is only applicable if the PDU type is MM_SMS_PDU_TYPE_STATUS_REPORT.
				discharge_timestamp: '', // time when the first PDU of the SMS message left the SMSC, in ISO8601 format This field is only applicable if MM_SMS_PDU_TYPE==STATUS_REPORT.			
			}
		};
		this.const = Object.freeze({

			MM_SMS_PDU_TYPE: {
				0: 'UNKNOWN', // Unknown type.
				1: 'DELIVER', // SMS has been received from the SMSC.
				2: 'SUBMIT', // SMS is sent, or to be sent to the SMSC.
				3: 'STATUS_REPORT' // SMS is a status report received from the SMSC.
			},

			MM_SMS_STATE: {
				0: 'UNKNOWN', // State unknown or not reportable.
				1: 'STORED', // The message has been neither received nor yet sent.
				2: 'RECEIVING', // The message is being received but is not yet complete.
				3: 'RECEIVED', // The message has been completely received.
				4: 'SENDING', // The message is queued for delivery.
				5: 'SENT' // The message was successfully sent.
			},

			MM_SMS_VALIDITY_TYPE: {
				0: 'UNKNOWN', // Validity type unknown.
				1: 'RELATIVE', // Relative validity.
				2: 'ABSOLUTE', // Absolute validity.
				3: 'ENHANCED' // Enhanced validity.
			},

			MM_SMS_DELIVERY_STATE: {
				0x00: 'COMPLETED_RECEIVED', // Delivery completed, message received by the SME.
				0x01: 'COMPLETED_FORWARDED_UNCONFIRMED', // Forwarded by the SC to the SME but the SC is unable to confirm delivery.
				0x02: 'COMPLETED_REPLACED_BY_SC', // Message replaced by the SC.
				0x20: 'TEMPORARY_ERROR_CONGESTION', // Temporary error, congestion.
				0x21: 'TEMPORARY_ERROR_SME_BUSY', // Temporary error, SME busy.
				0x22: 'TEMPORARY_ERROR_NO_RESPONSE_FROM_SME', // Temporary error, no response from the SME.
				0x23: 'TEMPORARY_ERROR_SERVICE_REJECTED', // Temporary error, service rejected.
				0x24: 'TEMPORARY_ERROR_QOS_NOT_AVAILABLE', // Temporary error, QoS not available.
				0x25: 'TEMPORARY_ERROR_IN_SME', // Temporary error in the SME.
				0x40: 'ERROR_REMOTE_PROCEDURE', // Permanent remote procedure error.
				0x41: 'ERROR_INCOMPATIBLE_DESTINATION', // Permanent error, incompatible destination.
				0x42: 'ERROR_CONNECTION_REJECTED', // Permanent error, connection rejected by the SME.
				0x43: 'ERROR_NOT_OBTAINABLE', // Permanent error, not obtainable.
				0x44: 'ERROR_QOS_NOT_AVAILABLE', // Permanent error, QoS not available.
				0x45: 'ERROR_NO_INTERWORKING_AVAILABLE', // Permanent error, no interworking available.
				0x46: 'ERROR_VALIDITY_PERIOD_EXPIRED', // Permanent error, message validity period expired.
				0x47: 'ERROR_DELETED_BY_ORIGINATING_SME', // Permanent error, deleted by originating SME.
				0x48: 'ERROR_DELETED_BY_SC_ADMINISTRATION', // Permanent error, deleted by SC administration.
				0x49: 'ERROR_MESSAGE_DOES_NOT_EXIST', // Permanent error, message does no longer exist.
				0x60: 'TEMPORARY_FATAL_ERROR_CONGESTION', //Permanent error, congestion.
				0x61: 'TEMPORARY_FATAL_ERROR_SME_BUSY', // Permanent error, SME busy.
				0x62: 'TEMPORARY_FATAL_ERROR_NO_RESPONSE_FROM_SME', // Permanent error, no response from the SME.
				0x63: 'TEMPORARY_FATAL_ERROR_SERVICE_REJECTED', // Permanent error, service rejected.
				0x64: 'TEMPORARY_FATAL_ERROR_QOS_NOT_AVAILABLE', // Permanent error, QoS not available.
				0x65: 'TEMPORARY_FATAL_ERROR_IN_SME', // Permanent error in SME.
				0x100: 'UNKNOWN' // Unknown state.
			},

			MM_SMS_STORAGE: {
				0: 'UNKNOWN', // Storage unknown.
				1: 'SM', // SIM card storage area.
				2: 'ME', // Mobile equipment storage area.
				3: 'MT', // Sum of SIM and Mobile equipment storages
				4: 'SR', // Status report message storage area.
				5: 'BM', // Broadcast message storage area.
				6: 'TA' // Terminal adaptor message storage area.
			},

			MM_SMS_CDMA_SERVICE_CATEGORY: {
				0x0000: 'UNKNOWN', // Unknown.
				0x0001: 'EMERGENCY_BROADCAST', // Emergency broadcast.
				0x0002: 'ADMINISTRATIVE', // Administrative.
				0x0003: 'MAINTENANCE', // Maintenance.
				0x0004: 'GENERAL_NEWS_LOCAL', // General news (local).
				0x0005: 'GENERAL_NEWS_REGIONAL', // General news (regional).
				0x0006: 'GENERAL_NEWS_NATIONAL', // General news (national).
				0x0007: 'GENERAL_NEWS_INTERNATIONAL', // General news (international).
				0x0008: 'BUSINESS_NEWS_LOCAL', // Business/Financial news (local).
				0x0009: 'BUSINESS_NEWS_REGIONAL', // Business/Financial news (regional).
				0x000A: 'BUSINESS_NEWS_NATIONAL', // Business/Financial news (national).
				0x000B: 'BUSINESS_NEWS_INTERNATIONAL', // Business/Financial news (international).
				0x000C: 'SPORTS_NEWS_LOCAL', // Sports news (local).
				0x000D: 'SPORTS_NEWS_REGIONAL', // Sports news (regional).
				0x000E: 'SPORTS_NEWS_NATIONAL', // Sports news (national).
				0x000F: 'SPORTS_NEWS_INTERNATIONAL', // Sports news (international).
				0x0010: 'ENTERTAINMENT_NEWS_LOCAL', // Entertainment news (local).
				0x0011: 'ENTERTAINMENT_NEWS_REGIONAL', // Entertainment news (regional).
				0x0012: 'ENTERTAINMENT_NEWS_NATIONAL', // Entertainment news (national).
				0x0013: 'ENTERTAINMENT_NEWS_INTERNATIONAL', // Entertainment news (international).
				0x0014: 'LOCAL_WEATHER', // Local weather.
				0x0015: 'TRAFFIC_REPORT', // Area traffic report.
				0x0016: 'FLIGHT_SCHEDULES', // Local airport flight schedules.
				0x0017: 'RESTAURANTS', // Restaurants.
				0x0018: 'LODGINGS', // Lodgings.
				0x0019: 'RETAIL_DIRECTORY', // Retail directory.
				0x001A: 'ADVERTISEMENTS', // Advertisements.
				0x001B: 'STOCK_QUOTES', // Stock quotes.
				0x001C: 'EMPLOYMENT', // Employment.
				0x001D: 'HOSPITALS', // Medical / Health / Hospitals.
				0x001E: 'TECHNOLOGY_NEWS', // Technology news.
				0x001F: 'MULTICATEGORY', // Multi-category.
				0x1000: 'CMAS_PRESIDENTIAL_ALERT', // Presidential alert.
				0x1001: 'CMAS_EXTREME_THREAT', // Extreme threat.
				0x1002: 'CMAS_SEVERE_THREAT', // Severe threat.
				0x1003: 'CMAS_CHILD_ABDUCTION_EMERGENCY', // Child abduction emergency.
				0x1004: 'CMAS_TEST' // CMAS test.
			},

			MM_SMS_CDMA_TELESERVICE_ID: {
				0x0000: 'UNKNOWN', // Unknown.
				0x1000: 'CMT91', // IS-91 Extended Protocol Enhanced Services.
				0x1001: 'WPT', // Wireless Paging Teleservice.
				0x1002: 'WMT', // Wireless Messaging Teleservice.
				0x1003: 'VMN', // Voice Mail Notification.
				0x1004: 'WAP', // Wireless Application Protocol.
				0x1005: 'WEMT', // Wireless Enhanced Messaging Teleservice.
				0x1006: 'SCPT', // Service Category Programming Teleservice.
				0x1007: 'CATPT', // Card Application Toolkit Protocol Teleservice.
			}
		});
		// add handler
		this._PP_DBusSystem = new PP_DBusSystem(this, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path
		});
		this._PP_DBusSystem._on_properties_changed(this.on_properties_changed.bind(this));
	}
	debug (level, msg) {
		global._Console.debug(level,`<smms_smms>${msg}</smms_smms>`);
	}
	async init () {
		await this._Ctrl.init();
		let properties = this._Ctrl.get('properties');
		this.convert(properties);
	}
	async refresh () {
		await this._Ctrl.refresh();
		let properties = this._Ctrl.get('properties');
		this.convert(properties);
	}
	now () {
		let d = new Date();
		d.setSeconds(d.getSeconds() - 1); // previous second to avoid conflicts
		// 2022-08-02T21:07:01+02:00
		return d.toISOString().substr(0,10)+'T'+d.toLocaleString().substr(11,8)+d.toTimeString().substr(12,3)+':00';
	}
	convert (res) {
		/* https://www.freedesktop.org/software/ModemManager/api/1.0.0/gdbus-org.freedesktop.ModemManager1.Sms.html
		State                  readable   u
		PduType                readable   u
		Number                 readable   s
		Text                   readable   s
		Data                   readable   ay
		SMSC                   readable   s
		Validity               readable   (uv)
		Class                  readable   i
		DeliveryReportRequest  readable   b
		MessageReference       readable   u
		Timestamp              readable   s
		DischargeTimestamp     readable   s
		DeliveryState          readable   u
		Storage                readable   u
		*/
		/* real example from CLI
		# mmcli -m 0 -s 0 -K
		sms.dbus-path                      : /org/freedesktop/ModemManager1/SMS/0
		sms.content.number                 : +336XXXXXXXX
		sms.content.text                   : --
		sms.content.data                   : --
		sms.properties.pdu-type            : deliver
		sms.properties.state               : receiving
		sms.properties.validity            : --
		sms.properties.storage             : me
		sms.properties.smsc                : +336XXXXXXXX
		sms.properties.class               : --
x				sms.properties.teleservice-id      : --
x				sms.properties.service-category    : --
		sms.properties.delivery-report     : --
		sms.properties.message-reference   : --
		sms.properties.timestamp           : 2021-08-23T22:08:21+02:00
		sms.properties.delivery-state      : --
		sms.properties.discharge-timestamp : --
		*/
		let sms = this.obj;
		for (let i in res) {
			let value = res[i];
			switch(i) {
				/* content */
				case 'Data':
					sms.content.data = value;
					break;
				case 'Number':
					sms.content.number = value;
					break;
				case 'Text':
					sms.content.text = value;
					break;
				/* properties */
				case 'PduType':
					sms.properties.pdu_type = parseInt(value,10);
					break;
				case 'State':
					sms.properties.state = parseInt(value,10);
					break;
				case 'Validity':
					sms.properties.validity = {
						type: value[0],
						value: value[1]
					};
					break;
				case 'Storage':
					sms.properties.storage = parseInt(value,10);
					break;
				case 'SMSC':
					sms.properties.smsc = value;
					break;
				case 'Class':
					sms.properties.class_3gpp = parseInt(value,10);
					break;
				case 'TeleserviceId': // extra ?
					sms.properties.teleservice_id = parseInt(value,10);
					break;
				case 'ServiceCategory': // extra ?
					sms.properties.service_category = parseInt(value,10);
					break;
				case 'DeliveryReportRequest':
					sms.properties.delivery_report_request = value;
					break;
				case 'MessageReference':
					sms.properties.message_reference = parseInt(value,10);
					break;
				case 'Timestamp':
					// some bastards can't respect the ISO8601 format !
					// example: 2021-08-23T22:08:21+02
					if (value.length) {
						if (value.substr(-3,1)=='+' || value.substr(-3,1)=='-') {
							value += ':00';
						}
					}
					sms.properties.timestamp = value;
					break;
				case 'DeliveryState':
					sms.properties.delivery_state = parseInt(value,10);
					break;
				case 'DischargeTimestamp':
					// some bastards can't respect the ISO8601 format !
					// example: 2021-08-23T22:08:21+02
					if (value.length) {
						if (value.substr(-3,1)=='+' || value.substr(-3,1)=='-') {
							value += ':00';
						}
					}
					sms.properties.discharge_timestamp = value;
					break;
				case 'Multiparty': // extra ?
					sms.properties.multiparty = (value=='true' ? 1 : 0);
					break;
			}
		}
		if (!sms.received) { // add current date
			sms.properties.timestamp = this.now();
		}
		this.obj = sms;
		this.debug(3,'SMS ' + this.get('dbus_path') + ' readed: ' + sms);
		if (DEBUG==3) {
			global._Console.inspect(sms);
		}
	}
	async on_properties_changed (interface_name, changed_properties, invalidated_properties) {
		// changed_properties: dictionary containing the changed properties with the new values
		// invalidated_properties: array of properties that changed but the value is not conveyed
		if (interface_name == 'org.freedesktop.ModemManager1.Sms') {
			let old_sms = JSON.parse(JSON.stringify(this.obj));
			await this.refresh();
			let new_sms = this.obj;
			this._Manager.on_properties_changed(this, old_sms, new_sms);
		}
	}
}
