global.register_module({
	manifest: {
		version: '0.2',
		title: 'Storcka',
		name: 'smms', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.smms', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/app/com.alsatux.smms',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/com.alsatux.smms',
			format: 'json'
		}
	},
	files: [
		'app/com.alsatux.smms/lang',
		'app/com.alsatux.smms/mms.decoder',
		'app/com.alsatux.smms/module',
		'app/com.alsatux.smms/smms.manager',
		'app/com.alsatux.smms/smms.object',
		'app/com.alsatux.smms/smms.tbl',
		'app/com.alsatux.smms/sms.manager',
		'app/com.alsatux.smms/sms.object',
		'app/com.alsatux.smms/sms.slots',
		'app/com.alsatux.smms/sms.tbl'
	],
	main: 'SMMS',
	options: {
		parents: [ // linked modules
			'pp.core.modem',
			'pp.core.network',
			'com.alsatux.contacts',
			'com.alsatux.prefs'
		]
	}
});
