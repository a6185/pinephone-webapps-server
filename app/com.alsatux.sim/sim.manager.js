// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SIM_Manager {
	constructor (_Module) {
		this._Module = _Module;
		this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim = null;
	}
	debug (level, msg) {
		global._Console.debug(level,"<sim_manager>" + msg + "</sim_manager>\n");
	}
	async init () {
		try {
			let sim_dbus_path = await this._Module.get_parent('_Modem')._ModemManager.get_interface_property('org.freedesktop.ModemManager1.Modem','Sim');
			if (sim_dbus_path === null) {
				throw "No SIM ?"			
			}
			this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim = new Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim(this._Module, this, sim_dbus_path);
			await this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim.init();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async send_pin (pin) {
		try {
			await this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim._DBus.send_pin(pin);
		} catch (err) {
			this.debug(1,`Sending PIN: ${err}`);
			throw err;
		}
	}
	async enable_pin_checking (pin) {
		try {
			await this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim._DBus.enable(pin, true);
		} catch (err) {
			this.debug(1,`Enable PIN checking: ${err}`);
			throw err;
		}
	}
	async disable_pin_checking (pin) {
		try {
			await this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim._DBus.enable(pin, false);
		} catch (err) {
			this.debug(1,`Disable PIN checking: ${err}`);
			throw err;
		}
	}
	async send_puk (puk, new_pin) {
		try {
			await this._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim._DBus.send_puk(puk, new_pin);
		} catch (err) {
			this.debug(1,`Sending PUK: ${err}`);
			throw err;
		}
	}
	
}
