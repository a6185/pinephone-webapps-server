// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SIM_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'sim_slots', {
			'com.alsatux.sim:/read': null,
			'com.alsatux.sim:/pin/send': 'com.alsatux.sim:/pin/sent',
			'com.alsatux.sim:/pin/change': 'com.alsatux.sim:/pin/changed',
			'com.alsatux.sim:/pin/checking/enable': 'com.alsatux.sim:/pin/checking/changed',
			'com.alsatux.sim:/pin/checking/disable': 'com.alsatux.sim:/pin/checking/changed',
			'com.alsatux.sim:/pin/unlock': 'com.alsatux.sim:/unlocked'
		});
	}
	check_pin (ws, data) {
		if (!ws.has_key(data.args, 'pin', String)) {
			throw this._Module.tr('MISSING_OR_BAD_PIN');
		}
		if (!data.args.pin.length) {
			throw this._Module.tr('MISSING_OR_BAD_PIN');
		}
	}
	check_old_pin (ws, data) {
		if (!ws.has_key(data.args, 'old_pin', String)) {
			throw this._Module.tr('MISSING_OR_BAD_OLD_PIN');
		}
		if (!data.args.old_pin.length) {
			throw this._Module.tr('MISSING_OR_BAD_OLD_PIN');
		}
	}
	check_new_pin (ws, data) {
		if (!ws.has_key(data.args, 'new_pin', String)) {
			throw this._Module.tr('MISSING_OR_BAD_NEW_PIN');
		}
		if (!data.args.new_pin.length) {
			throw this._Module.tr('MISSING_OR_BAD_NEW_PIN');
		}
	}
	check_puk (ws, data) {
		if (!ws.has_key(data.args, 'puk', String)) {
			throw this._Module.tr('MISSING_OR_BAD_PUK');
		}
		if (!data.args.pin.length) {
			throw this._Module.tr('MISSING_OR_BAD_PUK');
		}
	}
	async handler (ws, data) {
		try {	
			let _Status = new PP_Status();
			switch (data.route) {
				case 'com.alsatux.sim:/read':
					return {
						properties: this._Module._SIM_Manager._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim.get('properties')
					};
					break;
				case 'com.alsatux.sim:/pin/send':
					this.check_pin(ws, data);
					this._Module._SIM_Manager.send_pin(data.args.pin);
					break;
				case 'com.alsatux.sim:/pin/checking/enable':
					this.check_pin(ws, data);
					await this._Module._SIM_Manager.enable_pin_checking(data.args.pin);
					break;
				case 'com.alsatux.sim:/pin/checking/disable':
					this.check_pin(ws, data);
					await this._Module._SIM_Manager.disable_pin_checking(data.args.pin);
					break;
				case 'com.alsatux.sim:/pin/change':
					this.check_old_pin(ws, data);
					this.check_new_pin(ws, data);
					await this._Module._SIM_Manager.change_pin(data.args.old_pin, data.args.new_pin);
					break;				
				case 'com.alsatux.sim:/unlock':
					this.check_puk(ws, data);
					this.check_new_pin(ws, data);
					await this._Module._SIM_Manager.send_puk(data.args.puk, data.args.new_pin);
					break;
			} // end switch
			_PP._WS.send_signal({
				args: {
					properties: this._Module._SIM_Manager._Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim.get('properties')
				},
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`Handler ${data.route} ok...`);
		} catch(err) {
			ws._Status.code(1);
			ws._Status.err(err);
			this.debug(1,`Handler ${data.route} failed: ${err}`);
		}
	}
}