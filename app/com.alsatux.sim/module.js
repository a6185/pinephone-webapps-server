// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SIM extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
		this.signals = [
			'com.alsatux.sim:/state'
		];
		let _Modules = global._PP._Core._Modules;
		this.parent = {
			'_Modem': _Modules.get('pp.core.modem')
		};
		this._SIM_Manager = new SIM_Manager(this);
	}
	async init () {
		try {
			await this.defaults();
			this.set('slots', {
				'sims': new SIM_Slots(this)
			});
			this._SIM_Manager.init();
			//await this._ModemManager._Ctrl._DBus.enable(true);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Error ${err} ! Module disabled !`);
			throw err;
		}
	}
}
