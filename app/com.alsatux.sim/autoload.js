global.register_module({
	manifest: {
		version: '2.0',
		title: 'Sim',
		name: 'sim', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.sim', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2022',
		relpath: '/app/com.alsatux.sim',
	},
	files: [
		'app/com.alsatux.sim/module',
		'app/com.alsatux.sim/sim.manager',
		'app/com.alsatux.sim/sim.slots'
	],
	main: 'SIM',
	options: {
		parents: [ // linked modules
			'pp.core.modem'
		]
	}
});

