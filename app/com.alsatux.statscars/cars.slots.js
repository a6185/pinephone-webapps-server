// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Cars_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'cars', {
			'com.alsatux.statscars:/cars/list': null,
			'com.alsatux.statscars:/cars/add': 'com.alsatux.statscars:/car/added',
			'com.alsatux.statscars:/cars/upd': 'com.alsatux.statscars:/car/updated',
			'com.alsatux.statscars:/cars/del': 'com.alsatux.statscars:/car/deleted'
		});
	}
	async handler (ws, data) {
		try {
			switch (data.route) {
				case 'com.alsatux.statscars:/cars/list':
					return {
						cars: this._Module.tbl.cars.export(),
						fills: this._Module.tbl.fills.export(),
						repairs: this._Module.tbl.repairs.export(),
						insurances: this._Module.tbl.insurances.export(),
						photos: this._Module.tbl.photos.export()
					};
					break;
				default:
					let args = await this._Module.action(ws, data, 'car');
					this.debug(1,`${data.route} successfull...`);
					let _Status = new PP_Status();
					_PP._WS.send_signal({
						args: args,
						callback: this.callbacks[data.route]
					}, _Status);
					return {};
			}			
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}
}
