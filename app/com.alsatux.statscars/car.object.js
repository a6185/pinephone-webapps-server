// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Car_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'car_object');
		this.obj = {
			'uid': '',
			'name': '', // mandatory
			'driver': '', // mandatory
			'purchase': {
				'date': '', // timestamp
				'counter': 0., // float
				'price': 0. // float
			}
		};
	}
	check (obj, key, value) {
		this.debug(3,'check 1/3');
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.debug(3,'check 2/3');
		let sprintf = global._PP._Fn.sprintf;
		switch (key) {
			case 'uid':
				this.check_field(label,'is_uid',key,value,3);
				break;
			case 'name':
			case 'driver':
				this.check_field(label,'is_string',key,value,1,255);
				if (value.match(/[\<\>\&]/)!=null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				if (value.match(/^[a-zA-Z].*$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BEGIN_WITH_A_LETTER'),_PP.tr('!'));
				}
				break;
			case 'purchase.date':
				this.check_field(label,'is_date',key,value,'YYYY-MM-DD');
				break;
			case 'purchase.counter':
				this.check_field(label,'is_integer',key,value,0,1000000);
				break;
			case 'purchase.price':
				this.check_field(label,'is_float',key,value,0.,10000000.,2);
				break;
		}
		this.debug(3,'check 3/3');
	}
	
	/*upd (obj) {
		try {
			obj.name = String(obj.name).trim().substr(0,50);
			if (!obj.name.length) {
				throw this._Module.tr('MISSING_CAR_NAME') + _PP.tr('!');
			}
			obj.driver = String(obj.driver).trim().substr(0,50);
			if (!obj.driver.length) {
				throw this._Module.tr('MISSING_DRIVER_NAME') + _PP.tr('!');
			}
			obj.purchase.date = String(obj.purchase.date).trim().substr(0,10).to_date_format('YYYY-MM-DD');
			if (obj.purchase.date === null) {
				throw this._Module.tr('MISSING_PURCHASE_DATE') + _PP.tr('!');
			}
			obj.purchase.counter = String(obj.purchase.counter).trim().substr(0,7).round(2);
			if (obj.purchase.counter<=0) {
				throw this._Module.tr('MISSING_PURCHASE_COUNTER') + _PP.tr('!');
			}
			obj.purchase.price = String(obj.purchase.price).trim().substr(0,7).round(2);
			if (obj.purchase.price<=0) {
				throw this._Module.tr('MISSING_PURCHASE_PRICE') + _PP.tr('!');
			}
			for (let key in obj) {
				this.set(key,obj[key]);
			}
			//this.obj = obj;
			return this;
		} catch (e) {
			throw 'Car_Object::upd(): ' + e;
		}
	}*/
	del () {
		try {
			this.obj.fill.forEach(uid => {
				delete this._Module.tbl.fills.obj[uid];
			});
			this.obj.repair.forEach(uid => {
				delete this._Module.tbl.repairs.obj[uid];
			});
			this.obj.insurance.forEach(uid => {
				delete this._Module.tbl.insurances.obj[uid];
			});
			this.obj.photos.forEach(uid => {
				delete this._Module.tbl.photos.obj[uid];
			});
		} catch (e) {
			throw 'Car_Object::del(): ' + e;
		}
	}
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		'uid,name,driver'.split(',').forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.check(obj, field, obj[field]);
			}
		});
		'date,counter,price'.split(',').forEach(field => {
			this.check(obj, 'purchase.' + field, obj.purchase[field]);
		});
		this.debug(3,'check_structure 3/3');
	}
}
