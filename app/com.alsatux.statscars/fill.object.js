// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Fill_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'fill_object');
		this.obj = {
			'uid': '',
			'car_uid': '001',
			'date': '', // timestamp
			'counter': 0, // uint
			'price_per_vol': 0, // float
			'price': 0 // float
		};
	}
	/*
	upd (obj) {
		try {
			obj.date = String(obj.date).trim().substr(0,10).to_date_format('YYYY-MM-DD');
			if (obj.date === null) {
				throw this._Module.tr('MISSING_FILL_DATE') + _PP.tr('!');
			}
			obj.counter = String(obj.counter).trim().substr(0,8).round(2);
			if (obj.counter<=0) {
				throw this._Module.tr('MISSING_FILL_COUNTER') + _PP.tr('!');
			}
			if (obj.hasOwnProperty('price_per_litre')) {
				// patch v0.3 -> v0.4
				obj.price_per_vol = obj.price_per_litre;
				delete obj.price_per_litre;
			}
			obj.price_per_vol = String(obj.price_per_vol).trim().substr(0,8).round(3);
			if (obj.price_per_vol<=0) {
				throw this._Module.tr('MISSING_PRICE_PER_VOLUME') + _PP.tr('!');
			}
			obj.price = String(obj.price).trim().substr(0,8).round(2);
			if (obj.price<=0) {
				throw this._Module.tr('MISSING_PRICE_PER_VOLUME') + _PP.tr('!');
			}
			this.obj = obj;
			return this;
		} catch (e) {
			throw 'Fill_Object::upd(): ' + e;
		}
	}*/
	check (obj, key, value) {
		this.debug(3,'check 1/3');
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.debug(3,'check 2/3');
		switch (key) {
			case 'uid':
				this.check_field(label,'is_uid',key,value,4);
				break;
			case 'car_uid':
				this.check_field(label,'is_uid',key,value,3);
				break;
			case 'date':
				this.check_field(label,'is_date',key,value,'YYYY-MM-DD');
				break;
			case 'counter':
				this.check_field(label,'is_integer',key,value,0,1000000);
				break;
			case 'price_per_vol':
				this.check_field(label,'is_float',key,value,0.,100000.,3);
				break;
			case 'price':
				this.check_field(label,'is_float',key,value,0.,10000000.,2);
				break;
		}
		this.debug(3,'check 3/3');
	}	
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		'uid,car_uid,date,counter,price_per_vol,price'.split(',').forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.debug(3,field);
				this.check(obj, field, obj[field]);
			}
		});
		this.debug(3,'check_structure 3/3');
	}
}
