global.register_module({
	manifest: {
		version: '2.0',
		title: 'StatsCars',
		name: 'statscars', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.statscars', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/app/com.alsatux.statscars',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/com.alsatux.statscars',
			format: 'json'
		}
	},
	files: [
		'app/com.alsatux.statscars/car.object',
		'app/com.alsatux.statscars/cars.tbl',
		'app/com.alsatux.statscars/cars.slots',
		'app/com.alsatux.statscars/fill.object',
		'app/com.alsatux.statscars/fills.tbl',
		'app/com.alsatux.statscars/fills.slots',
		'app/com.alsatux.statscars/insurance.object',
		'app/com.alsatux.statscars/insurances.tbl',
		'app/com.alsatux.statscars/insurances.slots',
		'app/com.alsatux.statscars/photo.object',
		'app/com.alsatux.statscars/photos.tbl',
		'app/com.alsatux.statscars/photos.slots',
		'app/com.alsatux.statscars/repair.object',
		'app/com.alsatux.statscars/repairs.tbl',
		'app/com.alsatux.statscars/repairs.slots',
		'app/com.alsatux.statscars/module'
	],
	main: 'StatsCars'
});
