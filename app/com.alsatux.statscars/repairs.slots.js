// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Repairs_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'repairs', {
			'com.alsatux.statscars:/repairs/add': 'com.alsatux.statscars:/repair/added',
			'com.alsatux.statscars:/repairs/upd': 'com.alsatux.statscars:/repair/updated',
			'com.alsatux.statscars:/repairs/del': 'com.alsatux.statscars:/repair/deleted'
		});
	}
	async handler (ws, data) {			
		try {
			let args = await this._Module.action(ws, data, 'repair');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
				args: args,
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`${data.route} successfull...`);
			return {};
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err
		}
	}
}
