// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Insurance_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'_insurance_object');
		this.obj = {
			'uid': '',
			'car_uid': '001',
			'date': '', // timestamp
			'name': '', // insurer name
			'price': 0 // float
		};
	}
	/*upd (obj) {
		try {
			obj.date = String(obj.date).trim().substr(0,10).to_date_format('YYYY-MM-DD');
			if (obj.date === null) {
				throw this._Module.tr('MISSING_INSURANCE_DATEe') + _PP.tr('!');
			}
			obj.name = String(obj.name).trim().substr(0,1000);
			if (obj.name.length==0) {
				throw this._Module.tr('MISSING_INSURANCE') + _PP.tr('!');
			}
			obj.price = String(obj.price).trim().substr(0,8).round(2);
			if (obj.price<=0) {
				throw this._Module.tr('MISSING_INSURANCE_NAME') + _PP.tr('!');
			}
			this.obj = obj;
			return this;
		} catch (e) {
			throw 'Insurance::upd(): ' + e;
		}
	}*/
	check (obj, key, value) {
		this.debug(3,'check 1/3');
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.debug(3,'check 2/3');
		switch (key) {
			case 'uid':
				this.check_field(label,'is_uid',key,value,4);
				break;
			case 'car_uid':
				this.check_field(label,'is_uid',key,value,3);
				break;
			case 'date':
				this.check_field(label,'is_date',key,value,'YYYY-MM-DD');
				break;
			case 'name':
				this.check_field(label,'is_string',key,value,3,100);
				break;
			case 'price':
				this.check_field(label,'is_float',key,value,0.,10000000.,2);
				break;
		}
		this.debug(3,'check 3/3');
	}	
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		'uid,car_uid,date,name,price'.split(',').forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.check(obj, field, obj[field]);
			}
		});
		this.debug(3,'check_structure 3/3');
	}
}
