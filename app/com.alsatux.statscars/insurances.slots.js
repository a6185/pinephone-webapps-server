// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Insurances_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'insurances', {
			'com.alsatux.statscars:/insurances/add': 'com.alsatux.statscars:/insurance/added',
			'com.alsatux.statscars:/insurances/upd': 'com.alsatux.statscars:/insurance/updated',
			'com.alsatux.statscars:/insurances/del': 'com.alsatux.statscars:/insurance/deleted'
		});
	}
	async handler (ws, data) {	
		try {
			let args = await this._Module.action(ws, data, 'insurance');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
				args: args,
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`${data.route} successfull...`);
			return {};
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err
		}
	}
}
