// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Fills_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'fills', {
			'com.alsatux.statscars:/fills/add': 'com.alsatux.statscars:/fill/added',
			'com.alsatux.statscars:/fills/upd': 'com.alsatux.statscars:/fill/updated',
			'com.alsatux.statscars:/fills/del': 'com.alsatux.statscars:/fill/deleted'
		});
	}
	async handler (ws, data) {			
		try {
			let args = await this._Module.action(ws, data, 'fill');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
				args: args,
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`${data.route} successfull...`);
			return {};
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err
		}
	}
}
