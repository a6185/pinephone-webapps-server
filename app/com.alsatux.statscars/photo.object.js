// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Photo_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'photo_object');
		this.obj = {
			'uid': '',
			'car_uid': '001',
			'dataurl': '', // 
		};
	}
	/*upd (obj) {
		try {
			let m = obj.dataurl.match(/^(data:image\/jpe?g;base64,(?:[0-9a-zA-Z\+\-\/]{4})+(?:[0-9a-zA-Z\+\-\/]{2}==|[0-9a-zA-Z\+\-\/]{3}=))$/);
			if (m==null) {
				throw this._Module.tr('PHOTO_BAD_FORMAT');
			}*/
			/*
			let dom = new jsdom.JSDOM(`<!DOCTYPE html><html><head><meta charset="utf-8"/></head><body>hello</body></html>`);
			let document = dom.window.document;
			let img = document.createElement('img');
			img.src = m[1];
			if (img.width>320) {
				throw this._Module.tr('PHOTO_WIDTH_WARNING');
			}
			if (img.height>240) {
				throw this._Module.tr('PHOTO_HEIGHT_WARNING');
			}
			*//*
			this.obj = obj;
			return this;
		} catch (e) {
			throw 'Photo_Object::upd(): ' + e;
		}
	}*/
	check (obj, key, value) {
		this.debug(3,'check 1/3');
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.debug(3,'check 2/3');
		switch (key) {
			case 'uid':
				this.check_field(label,'is_uid',key,value,4);
				break;
			case 'car_uid':
				this.check_field(label,'is_uid',key,value,3);
				break;
			case 'dataurl':
				this.check_field(label,'is_string',key,value,0,1024*1024);
				let m = obj.dataurl.match(/^(data:image\/jpe?g;base64,(?:[0-9a-zA-Z\+\-\/]{4})+(?:[0-9a-zA-Z\+\-\/]{2}==|[0-9a-zA-Z\+\-\/]{3}=))$/);
				if (m==null) {
					throw this._Module.tr('PHOTO_BAD_FORMAT');
				}
				break;
		}
		this.debug(3,'check 3/3');
	}	
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		'uid,car_uid,dataurl'.split(',').forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.check(obj, field, obj[field]);
			}
		});
		this.debug(3,'check_structure 3/3');
	}
}
