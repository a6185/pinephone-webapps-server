// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class StatsCars extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
	}	
	async init() {
		try {
			await this.defaults();
			this.set('tbl', {
				'cars': new Cars_Tbl(this, 999, Car_Object),
				'fills': new Fills_Tbl(this, 9999, Fill_Object),
				'insurances': new Insurances_Tbl(this, 9999, Insurance_Object),
				'repairs': new Insurances_Tbl(this, 9999, Repair_Object),
				'photos': new Photos_Tbl(this, 9999, Photo_Object)
			});
			this.set('slots', {
				'cars': new Cars_Slots(this),
				'fills': new Fills_Slots(this),
				'insurances': new Insurances_Slots(this),
				'repairs': new Repairs_Slots(this),
				'photos': new Photos_Slots(this)
			});
			await this.get_last_backup();
			this.debug(1,this.tbl.cars.length() + ' cars found !');
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Error ${err} ! Module disabled !`);
			throw err;
		}
	}
	import (json) {
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(json, 'cars', Object) && hasKey(json, 'fills', Object) && hasKey(json, 'insurances', Object) && hasKey(json, 'repairs', Object) && hasKey(json, 'photos', Object)) {
			// we trust our own datas, so no check needed...
			let d;
			for (let uid in json.cars) {
				this.tbl.cars.set(uid, (new Car_Object(this)).upd(json.cars[uid]));
			}
			for (let uid in json.fills) {
				this.tbl.fills.set(uid, (new Fill_Object(this)).upd(json.fills[uid]));
			}
			for (let uid in json.insurances) {
				this.tbl.insurances.set(uid, (new Insurance_Object(this)).upd(json.insurances[uid]));
			}
			for (let uid in json.repairs) {
				this.tbl.repairs.set(uid, (new Repair_Object(this)).upd(json.repairs[uid]));
			}
			for (let uid in json.photos) {
				this.tbl.photos.set(uid, (new Photo_Object(this)).upd(json.photos[uid]));
			}
		}
	}
	export () {
		return JSON.stringify({
			cars: this.tbl.cars.export(),
			fills: this.tbl.fills.export(),
			insurances: this.tbl.insurances.export(),
			repairs: this.tbl.repairs.export(),
			photos: this.tbl.photos.export()
		});
	}
	async action (ws, data, type) {
		try {
			let obj = data.args;
			let res = {};
			if (!ws.has_key(obj, type, Object)) {
				throw this.tr('MISSING_OR_BAD_TYPE');
			}
			let _Tbl = this.tbl[type + 's'];
			let action = this.get_action(data.route);
			let hasKey = global._PP._Fn.hasKey;
			switch (type) {
				case 'car':
					res[type] = _Tbl[action](obj[type]);
					break;
				default:
					if (!hasKey(obj,'car_uid',String)) {
						throw sprintf('%s%s',this.tr('MISSING_CAR_UID'), _PP.tr('!'));
					}
					let rows = this.tbl.cars.find('uid',obj.car_uid);
					if (!rows.length) {
						throw sprintf('%s%s',this.tr('UNKNOWN_CAR'), _PP.tr('!'));
					}
					let _Car_Object = rows[0];
					res['car_uid'] = obj.car_uid;
					res[type] = _Tbl[action](obj[type]);
			}
			let result = await this._IO.write();
			return res; // not result !
		} catch (err) {
			reject(err);
		}
	}
}

