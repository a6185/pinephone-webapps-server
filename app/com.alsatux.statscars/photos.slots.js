// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Photos_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'photos', {
			'com.alsatux.statscars:/photos/add': 'com.alsatux.statscars:/photo/added',
			'com.alsatux.statscars:/photos/upd': 'com.alsatux.statscars:/photo/updated',
			'com.alsatux.statscars:/photos/del': 'com.alsatux.statscars:/photo/deleted'
		});
	}
	async handler (ws, data) {			
		try {
			let args = await this._Module.action(ws, data, 'photo');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
				args: args,
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`${data.route} successfull...`);
			return {};
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}
}
