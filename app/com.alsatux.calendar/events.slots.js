// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Events_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'events', {
			'com.alsatux.calendar:/events/ls': null,
			'com.alsatux.calendar:/events/add': 'com.alsatux.calendar:/event/added',
			'com.alsatux.calendar:/events/upd': 'com.alsatux.calendar:/event/updated',
			'com.alsatux.calendar:/events/del': 'com.alsatux.calendar:/event/deleted'
		});
	}
	async handler (ws, data) {
		try {			
			switch (data.route) {
				case 'com.alsatux.calendar:/events/ls':
					return {
						cats: this._Module.tbl.cats.export(),
						filters: this._Module.tbl.filters.export(),
						events: this._Module.tbl.events.export()
					};
					break;
				default:
					let result = await this._Module.action(ws, data, 'event');
					this.debug(1,`${data.route} successfull...`);
					let _Status = new PP_Status();
					_PP._WS.send_signal({
						args: {
							'event': result
						},
						callback: this.callbacks[data.route]
					}, _Status);
					return {};
			} // end switch
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}		
}

