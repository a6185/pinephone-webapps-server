// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Filter_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'filter_object');
		this.obj = {
			'uid': null,
			'value': null
		};
	}
	check (obj, key, value) {
		this.debug(3,'check 1/3');
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.check_field(label,'is_string',key,value,0,255);
		this.debug(3,'check 2/3');
		let sprintf = global._PP._Fn.sprintf;
		switch (key) {
			case 'uid':
				if (value.match(/^\d\d$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				break;
			case 'value':
				if (value.match(/[\<\>\&]/)!=null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				if (value.match(/^[a-zA-Z].*$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BEGIN_WITH_A_LETTER'),_PP.tr('!'));
				}
				break;
		}
		this.debug(3,'check 3/3');
	}
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		let sprintf = global._PP._Fn.sprintf;
		// uid,value
		'uid,value'.split(',').forEach(field => {
			if (obj.hasOwnProperty(field)) {
				if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
					this.check(obj, field, obj[field]);
				}
			} else {
				throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),field,_PP.tr('IS_MISSING'),_PP.tr('!'));
			}
		});
		this.debug(3,'check_structure 3/3');
	}
}
