// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Agenda extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
	}
	async init () {
		try {
			await this.defaults();
			this.set('tbl', {
				'filters': new Filters_Tbl(this, 99, Filter_Object),
				'cats': new Cats_Tbl(this, 99, Cat_Object),
				'events': new Events_Tbl(this, 999999, Event_Object)
			});
			this.set('slots', {
				'cats': new Cats_Slots(this),
				'filters': new Filters_Slots(this),
				'events': new Events_Slots(this)		
			});
			this.tbl.cats.set_defaults();
			await this.get_last_backup();
			this.debug(1,this.tbl.events.length() + ' events found !');
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Error ${err} ! Module disabled !`);
			throw err;
		}
	}
	import (json) {		
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(json, 'cats', Object) && hasKey(json, 'filters', Object) && hasKey(json, 'events', Object)) { 
			// we trust our own datas, so no check needed...
			this.debug(3,`Reading categories...`);
			for (let uid in json.cats) {
				this.tbl.cats.set(uid, (new Cat_Object(this)).upd(json.cats[uid]));
			}			
			this.tbl.cats.set_defaults();
			this.debug(3,`Reading filters...`);
			for (let uid in json.filters) {
				this.tbl.filters.set(uid, (new Filter_Object(this)).upd(json.filters[uid]));
			}
			this.debug(3,`Reading events...`);
			for (let uid in json.events) {
				this.tbl.events.set(uid, (new Event_Object(this)).upd(json.events[uid]));
			}
		}
	}
	export () {
		return JSON.stringify({
			cats: this.tbl.cats.export(),
			filters: this.tbl.filters.export(),
			events: this.tbl.events.export()
		});
	}
	async action (ws, data, type) {
		try {
			let action = this.get_action(data.route);
			let _Tbl = this.tbl[type + 's'];
			if (!ws.has_key(data.args, type, Object)) {
				throw this.tr('MISSING_OR_BAD_TYPE');
			}
			let obj = _Tbl[action](data.args[type]);
			let result = await this._IO.write();
			return obj;
		} catch (err) {
			throw err;
		}
	}
}

