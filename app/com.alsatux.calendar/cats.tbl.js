// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Cats_Tbl extends PP_Tbl {
	constructor (...args) {
		super(...args);
	}
	set_defaults () {
		this.set('00', (new Cat_Object(this._Module)).upd({ // default category
			'uid': '00',
			'name': this._Module.tr('NONE'),
			'color': '#000000',
			'bg_type': 'transparent', // or color or gradient
			'bg_from': '#68EE66',
			'bg_to': '#00AA2E',
			'bg_angle': '90', // -180 to 180 (deg) - top is 0
			'styles': '' // space separated values : "italic bold underline"
		}));
	}
	del (obj) { // override default
		try {
			this.exists_object(obj);
			// reset cat to default in events
			for (let uid in this._Module.tbl.events.obj) {
				if (this._Module.tbl.events.obj[uid].cat_uid == obj.uid) {
					this._Module.tbl.events.obj[uid].cat_uid = '00';
				}
			}
			this.unset(obj.uid);
			//delete this.obj[obj.uid];
			//return obj;
		} catch (err) {
			this.debug(1, err);
		}
	}
}