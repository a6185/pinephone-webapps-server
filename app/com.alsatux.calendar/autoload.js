global.register_module({
	manifest: {
		version: '2.0',
		title: 'Cactus',
		name: 'calendar', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.calendar', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/app/com.alsatux.calendar',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/com.alsatux.calendar',
			format: 'json'
		}
	},
	files: [
		'app/com.alsatux.calendar/cat.object',
		'app/com.alsatux.calendar/cats.tbl',
		'app/com.alsatux.calendar/cats.slots',
		'app/com.alsatux.calendar/filter.object',
		'app/com.alsatux.calendar/filters.tbl',
		'app/com.alsatux.calendar/filters.slots',
		'app/com.alsatux.calendar/event.object',
		'app/com.alsatux.calendar/events.tbl',
		'app/com.alsatux.calendar/events.slots',
		'app/com.alsatux.calendar/module'
	],
	main: 'Agenda'
});
