// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Cat_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'cat_object');
		this.obj = {
			'uid': null,
			'name': null,
			'color': '#000000',
			'bg_type': 'transparent', // or color or gradient
			'bg_from': '#68EE66',
			'bg_to': '#00AA2E',
			'bg_angle': '90', // -180 to 180 (deg) - top is 0
			'styles': '' // space separated values : "italic bold underline"
		};
		this.const = Object.freeze({
			CAT_RECORD : {
				strings: 'uid,name,color,bg_type,bg_from,bg_to,bg_angle,styles'.split(',')
			}
		});
	}
	check (obj, key, value) {
		this.debug(3,'check 1/3');
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.check_field(label,'is_string',key,value,0,255);
		this.debug(3,'check 2/3');
		let sprintf = global._PP._Fn.sprintf;
		switch (key) {
			case 'uid':
				if (value.match(/^\d\d$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				if (value=='00') {
					throw sprintf('%s%s',this._Module.tr('UNABLE_TO_CHANGE_DEFAULT_CATEGORYy'),_PP.tr('!'));
				}
				break;
			case 'name':
				if (value.match(/[\<\>\&]/)!=null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				if (value.match(/^[a-zA-Z].*$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BEGIN_WITH_A_LETTER'),_PP.tr('!'));
				}
				break;
			case 'color':
			case 'bg_from':
			case 'bg_to':
				if (value.match(/^#[0-9A-F]{6}$/i)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('CONTAINS_A_WRONG_VALUE'),_PP.tr('!'));
				}
				break;
			case 'bg_type':
				if (value.match(/^(transparent|color|gradient)$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('CONTAINS_A_WRONG_VALUE'),_PP.tr('!'));
				}
				break;
			case 'bg_angle':
				if (value.match(/^-?\d{1,3}$/)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				let bg_angle = parseInt(value,10);
				if (bg_angle<-180 || bg_angle>180) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('CONTAINS_A_WRONG_VALUE'),_PP.tr('!'));
				}
				break;
			case 'styles':
				if (value.length) {
					let arr = value.split(' ');
					for (let i=0;i<arr.length;i++) {
						if (arr[i].match(/^(bold|italic|underline)$/)==null) {
							throw sprintf('%s%s',this._Module.tr('UNKNOWN_STYLE'),_PP.tr('!'));
						}
					}
				}
				break;
		}
		this.debug(3,'check 3/3');
	}
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		// uid,name,color,bg_type,bg_from,bg_to,bg_angle,styles
		this.const.CAT_RECORD.strings.forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.check(obj, field, obj[field]);
			}
		});
		this.debug(3,'check_structure 3/3');
	}
}
