// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Filters_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'filters', {
			'com.alsatux.calendar:/filters/add': 'com.alsatux.calendar:/filter/added',
			'com.alsatux.calendar:/filters/upd': 'com.alsatux.calendar:/filter/updated',
			'com.alsatux.calendar:/filters/del': 'com.alsatux.calendar:/filter/deleted'
		});
	}	
	async handler (ws, data) {
		try {
			let result = await this._Module.action(ws, data, 'filter');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
				args: {
					'filter': result
				},
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`${data.route} successfull...`);
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}		
}
