// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Event_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'event_object');
		this.obj = {
			'uid': null,
			'cat_uid': null,
			'date': null,
			'time': null,
			'duration': null,
			'title': null,
			'description': null
			/* optional
			'rrule' : {
				'freq': null,
				'count': null,
				'interval': null,
				'until': null
			}
			*/
		};
		this.const = Object.freeze({
			EVENT_MAXLENGTH: {
				'title': 100,
				'description': 5000
			}
		});
	}
	check (obj, key, value) {
		let label = key; // to translate...
		this.check_field(label,'not_null',key,value);
		this.check_field(label,'not_undefined',key,value);
		this.check_field(label,'is_string',key,value,0,255);
		let hour,minute,i,m;
		let sprintf = global._PP._Fn.sprintf;
		switch (key) {
			case 'uid':
				if (value.match(/^\d{6}$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'cat_uid':
				if (value.match(/^\d\d$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'date':
				if (value.to_date_format('YYYY-MM-DD') === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'time': // from XML...
				m = value.match(/^(\d\d):(\d\d)$/);
				if (m === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				this.check(obj, 'start_hour', m[1]);
				this.check(obj, 'start_minute', m[2]);
				break;
			case 'start_hour': //virtual
				hour = parseInt(value,10);
				if (hour<0 || hour>23 || value.length != 2) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'start_minute': //virtual
			case 'duration_minute': //virtual
				minute = parseInt(value,10);
				if (minute<0 || minute>59 || value.length != 2) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'duration_hour': //virtual
				hour = parseInt(value,10);
				if (hour<0 || hour>24*365) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'duration': // from XML...
				m = value.match(/^(\d\d):(\d\d)$/);
				if (m === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				this.check(obj, 'duration_hour', m[1]);
				this.check(obj, 'duration_minute', m[2]);
				break;
			case 'title':
			case 'description':
				if (value.length>this.const.EVENT_MAXLENGTH[key]) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('TOO_LONG'),_PP.tr('!'));
				}
				break;
			case 'rrule.freq':
				if (value.match(/^(yearly|monthly|weekly|daily)$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'rrule.count':
				i = parseInt(value,10);
				if (i<0 || i>99 || i.toString() != value) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'rrule.interval':
				i = parseInt(value,10);
				if (i<1 || i>99 || i.toString() != value) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'rrule.until':
				if (value.length && value.to_date_format('YYYY-MM-DD') === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
		}
	}
	check_integrity (obj) {
		'date,time,duration,cat_uid'.split(',').forEach(field => {
			//this.debug(3,`field "${field}": ${obj.field}`);
			let label = field; // to translate...
			if (!(obj.hasOwnProperty(field)) || !obj[field].length) {
				throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_MISSING'),_PP.tr('!'));
			}
		});
		if (!this._Module.tbl.cats.obj.hasOwnProperty(obj.cat_uid)) {
			throw sprintf('%s%s',this._Module.tr('UNKNOWN_CATEGORY'),_PP.tr('!'));
		}
		if (obj.hasOwnProperty('rrule') && obj.rrule.hasOwnProperty('until') && obj.rrule.until.constructor === String && obj.rrule.until.length) {
			let start = new Date(obj.date);
			let until = new Date(obj.rrule.until);
			if (start<=until) {
				throw sprintf('%s%s%s%s',this._Module.tr('UNTIL_REPEAT_RULE'),_PP.tr('MUST_BE_GT'),this._Module.tr('START_DATE'),_PP.tr('!'));
			}
		}
	}	
	check_structure (obj, bypass) {
		this.check_object(obj);
		'uid,cat_uid,date,time,duration,title,description'.split(',').forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.check(obj, field, obj[field]);
			}
		});
		if (obj.hasOwnProperty('rrule')) {
			'freq,count,interval,until'.split(',').forEach(field => {
				this.check(obj, 'rrule.' + field, obj.rrule[field]);
			});
		}
		this.check_integrity(obj);
	}
}
