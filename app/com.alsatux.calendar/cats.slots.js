// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Cats_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'cats', {
			'com.alsatux.calendar:/cats/add': 'com.alsatux.calendar:/cat/added',
			'com.alsatux.calendar:/cats/upd': 'com.alsatux.calendar:/cat/updated',
			'com.alsatux.calendar:/cats/del': 'com.alsatux.calendar:/cat/deleted'
		});
	}
	async handler (ws, data) {
		try {
			let result = await this._Module.action(ws, data, 'cat');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
					args: {
						'cat': result
					},
					callback: this.callbacks[data.route]
				}, _Status);
			this.debug(1,`${data.route} successfull...`);
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}		
}
