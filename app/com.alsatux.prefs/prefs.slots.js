// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Prefs_Slots extends PP_Slots {
	constructor (_Module) {	
		super(_Module, 'prefs', {
			'com.alsatux.prefs:/read': null,
			'com.alsatux.prefs:/get': null,
			'com.alsatux.prefs:/write': 'com.alsatux.prefs:/updated'
		});
	}
	async handler (ws, data) {
		try {
			switch (data.route) {
				case 'com.alsatux.prefs:/read':
					return {
						prefs: this._Module.tbl['prefs'].export()						
					};
					break;
				/*case 'com.alsatux.prefs:/get':
					if (!ws.has_key(data.args, 'cat', String)) {
						throw this._Module.tr('MISSING_OR_BAD_CAT');
					}
					if (!data.args.cat.length) {
						throw this._Module.tr('MISSING_OR_BAD_CAT');
					}
					if (!ws.has_key(data.args, 'key', String)) {
						throw this._Module.tr('MISSING_OR_BAD_KEY');
					}
					if (!data.args.key.length) {
						throw this._Module.tr('MISSING_OR_BAD_KEY');
					}
					let _Object = this._Module.tbl['prefs'].find_by_cat_key(data.args.cat, data.args.key);
					if (_Object === null) {
						throw this._Module.tr('BAD_CAT_KEY_ARGS');
					}
					if (!ws.has_key(data.args, 'val', String)) {
						throw this._Module.tr('MISSING_OR_BAD_VAL');
					}
					if (!data.args.val.length) {
						throw this._Module.tr('MISSING_OR_BAD_VAL');
					}
					if (_Object.val.indexOf(data.args.val)==-1) {
						throw this._Module.tr('MISSING_OR_BAD_VAL');	
					}
					return {
						val: _Object.val[val]
					};
					break;*/
				case 'com.alsatux.prefs:/write':
					this._Module.tbl['prefs'].upd2(data.args);
					await this._Module._IO.write();
					let _Status = new PP_Status();
					_PP._WS.send_multicast({
						args: {
							prefs: this._Module.tbl['prefs'].export()
						},
						callback: this.callbacks[data.route]
					}, _Status);
					this.debug(1,`${data.route} successfull...`);
					return {};
					break;
			}
		} catch (err) {
			this.debug(1,`${data.route} failed...`);
			throw err;
		};
	}
};
