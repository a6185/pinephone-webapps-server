/** GLOBAL PREFS **/
global.register_module({
	manifest: {
		version: '2.0',
		title: 'Lucky',
		name: 'prefs', // internal lowercased - no space nor special chars
		namespace: 'com.alsatux.prefs', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/app/com.alsatux.prefs',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/com.alsatux.prefs',
			format: 'json'
		}
	},
	files: [
		'app/com.alsatux.prefs/prefs.slots',
		'app/com.alsatux.prefs/pref.object',
		'app/com.alsatux.prefs/prefs.tbl',
		'app/com.alsatux.prefs/prefs.user',
		'app/com.alsatux.prefs/prefs.fn',
		'app/com.alsatux.prefs/module'
	],
	main: 'Prefs'
});
