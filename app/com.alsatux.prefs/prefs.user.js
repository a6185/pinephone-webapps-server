// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Prefs_User {
	constructor (_Module) {
		[{	
			key: 'currency',
			def: ['EUR']
		},{	
			key: 'date_pattern',
			def: ['eur']
		},{	
			key: 'separator_real',
			def: ['comma']
		},{
			key: 'locale',
			def: ['fr-FR']
		},{
			key: 'timezone',
			def: ['Europe/Paris']
		},{
			key: 'phoneprefix',
			def: ['FR']
		}].forEach(obj => {
			/*{
				cat: '', // category
				key: '', // key
				val: {}, // values
				cur: [], // current values keys (MUST exists in val)
				def: [] // default values keys (MUST exists in val)
			}*/
			obj.cat = 'default',
			obj.cur = [...obj.def];
			obj.val = {...global._PP._Enum['core.'+obj.key]};
			_Module.tbl.prefs.add(obj);
		});
	}
};

		/*{	
			key: 'unit_distance',
			val: {
				kilometre: 'km',
				mile: 'mi'
			},
			def: 'kilometre'
		},{	
			key: 'unit_volume',
			val: {
				litre: 'l',
				gallon: 'gal'
			},
			def: 'litre'
		},*/
