// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Prefs_Fn extends PP_Object {
	constructor (_Module) {
		super(_Module,'prefs_fn');
	}
	clean_phone_number (number) {
		let final_number = number.replace(/[^0-9\+]+/g,'');
		if (/^00/.test(final_number)) { // International call
			final_number = final_number.replace(/^00/,'+');
		}
		let _Pref_Object = this._Module.tbl.prefs.find_by_cat_key('default','phoneprefix');
		switch (_Pref_Object.cur) {
			case 'FR':
				if (/^0/.test(final_number)) { // Call inside France
					final_number = final_number.replace(/^0/,'+33');
				}
				break;
			// add your own country here !
		}
		return final_number;
	}
}
