// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Prefs extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, this.obj.relpath + '/locales/module.csv');
		this._Fn = new Prefs_Fn(this);
	}
	async init () {
		try {
			await this.defaults();
			this.set('tbl', {
				'prefs': new Prefs_Tbl(this, 999999, Pref_Object)
			});
			this.set('slots', {
				'prefs': new Prefs_Slots(this)
			});
			// add default user preferences
			new Prefs_User(this);				
			await this.get_last_backup();
			//	await this._IO.write();
			this.debug(1,`${this.tbl.prefs.length()} prefs found !`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Error ${err} ! Module disabled !`);
			throw err;
		}
	}
	import (json) {
		this.debug(3,`Reading prefs...`);
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(json, 'prefs', Object)) {
			for (let uid in json.prefs) {
				let pref = json.prefs[uid];
				// we ignore recorded UID : only cur value can override default values
				if (hasKey(pref, 'cat', String) && hasKey(pref, 'key', String) && hasKey(pref, 'cur', Array)) {
					let _Object = this.tbl.prefs.find_by_cat_key(pref.cat, pref.key);
					if (_Object !== null) {
						let cur = [];
						pref.cur.forEach(val => {
							if (Object.keys(_Object.obj.val).indexOf(val) != -1) {
								cur.push(val);
							}
						});
						_Object.obj.cur = cur;
						/*if (Object.keys(_Object.obj.val).indexOf(pref.cur) != -1) {
							_Object.obj.cur = pref.cur;
						}*/
					}
				}
			}
		}
	}
	export () {
		return JSON.stringify({
			prefs: this.tbl.prefs.export()
		});
	}
}


