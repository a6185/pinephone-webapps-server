// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Pref_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'pref_object');
		this.obj = {
			'uid': '',
			'cat': '', // category (mandatory)
			'key': '', // key (mandatory)
			'val': {}, // values (mandatory)
			'def': [], // default value (mandatory)
			'cur': []  // current value (default value on startup)
		};
	}
	check (obj, key, value) {
		let label = key; // to translate...
		this.check_field(label, 'not_null', key, value);
		this.check_field(label, 'not_undefined', key, value);
		let sprintf = global._PP._Fn.sprintf;
		switch (key) {
			case 'uid':
				if (value.match(/^\d{6}$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'cat':
				if (value.match(/^[a-z_]{3,30}$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'key':
				if (value.match(/^[a-z][a-z0-9_]{2,29}$/) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
				}
				break;
			case 'val':
				this.check_field(label,'is_object',key,value);
				break;
			case 'def':
				this.check(obj, 'val', obj.val);
				this.check_field(label,'is_array',key,value);
				value.forEach(val => {
					this.check_field(label,'is_string',key,val,2,30);
					if (!obj.val.hasOwnProperty(val)) {
						throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
					}
				});
				break;
			case 'cur':
				this.check(obj, 'val', obj.val);
				this.check_field(label,'is_array',key,value);
				value.forEach(val => {
					this.check_field(label,'is_string',key,val,2,30);
					if (!obj.val.hasOwnProperty(val)) {
						throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_INVALID'),_PP.tr('!'));
					}
				});
				break;
		}
	}
	check_structure (obj, bypass) {
		this.debug(3,'check_structure 1/3');
		this.check_object(obj);
		this.debug(3,'check_structure 2/3');
		'uid,cat,key,val,def,cur'.split(',').forEach(field => {
			if (!(field == 'uid' && bypass.includes('uid'))) { // for add...
				this.check(obj, field, obj[field]);
			}
		});
		this.debug(3,'check_structure 3/3');
	}
}