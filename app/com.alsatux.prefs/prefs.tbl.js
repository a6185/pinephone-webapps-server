// Jean Luc Biellmann - pref@alsatux.com

'use strict';

class Prefs_Tbl extends PP_Tbl {
	constructor (...args) {
		super(...args);
	}
	add (obj) {		
		this.debug(3,`Adding pref...`);
		try {
			this.exists_object(obj);
		} catch (err) {
			let _Object = (new Pref_Object(this._Module));
			_Object.check_structure(obj, ['uid']);
			_Object.upd(obj)
			let uid = this.next_uid();
			_Object.set('uid',uid);
			//_Object.set('cur',obj.def); // set default value as current
			this.debug(3,`Adding pref uid "${uid}"...`);
			this.obj[uid] = _Object;
		}
	}
	upd2 (args) {
		let sprintf = global._PP._Fn.sprintf;
		let hasKey = global._PP._Fn.hasKey;
		if (!hasKey(args,'cat',String) || !hasKey(args,'key',String) || !hasKey(args,'cur',Array)) {
			throw sprintf('%s%s',_PP.tr('MISSING_OR_BAD_ARGS'),_PP.tr('!'));
		}
		let _Object = this.find_by_cat_key(args.cat, args.key);
		if (_Object === null) {
			throw sprintf('%s%s',this._Module.tr('UNKNOWN_PREF'),_PP.tr('!'));
		}		
		_Object.check(_Object.obj, 'cur', args.cur);
		_Object.set('cur',args.cur);
		/*if (Object.keys(_Object.obj.val).indexOf(args.cur)!=-1) {
			_Object.set('cur',args.cur);
		}*/
	}
	find_by_cat_key (cat,key) {
		for (let uid in this.obj) {
			let _Object = this.obj[uid];
			if (_Object.obj.cat==cat && _Object.obj.key==key) {
				return _Object;
			}
		}
		return null;
	}
	export () {
		let mydb = {};
		for (let uid in this.obj) {
			let obj = this.obj[uid].export();
			// we avoid to send the values... Too long !
			mydb[uid] = {
				cat: obj.cat,
				uid: uid,
				key: obj.key,
				cur: obj.cur,
				def: obj.def
			}
		}
		return mydb;
	}		
}
