global._PP._Enum['core.date_pattern'] = Object.freeze({
	"iso": "YYYY-MM-DD",
	"usa": "MM/DD/YYYY",
	"eur": "DD/MM/YYYY"
});