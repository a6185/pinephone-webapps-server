/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class PP_Shell {
	constructor (_Module) {
		this._Module = _Module;
		this.cp = global._PP._Nodes.get('child_process');
		// don't forget to add the following line in your scripts when running under mobian user...
		this.pkexec = 'pkexec --user mobian env DISPLAY=:0 XAUTHORITY=/home/mobian/.Xauthority DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus '; // let the space at the end please !
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_shell:: ${msg}`);
	}
	exec (...args) { // exec shell under mobian user with ENV vars
		this.debug(3,`exec ${args}`);
		try {
			let cmd = args[0];
			let opt = args.length>1 ? args[1] : { 
				encoding: 'utf8', 
				timeout: 3000 
			};
			let out = this.cp.execSync(cmd, opt);
			this.debug(3,`exec result = ${out}`);
			return out === null ? '' : out; // null if nothing was returned or exit code is <> 0
		} catch (err) {
			this.debug(3,err);
			return '';
		}
	}
}