/* Jean Luc BIELLMANN - contact@alsatux.com */

const DEBUG = 3; // log verbosity: 0 (min) to 3 (max) - ERR, WARN, INFO, DEBUG
const DEBUG_SOCKET_OUTPUT = 0;
const MAX_FILES_SIZE = 2000000; // 2Mo

const SERVER_NICKNAME = 'Server';
const SERVER_FGCOLOR = '#FFFFFF'; // white
const SERVER_BGCOLOR = '#FF0000'; // red

const PP_DEFAULT_LOCALE = 'fr-FR';

const ROOT_PATH = '/usr/local/src/pinephone/server';

// Cf. https://gitlab.gnome.org/GNOME/mobile-broadband-provider-info/-/blob/main/serviceproviders.xml
const MMSC = 'http://mms.free.fr';
const MMS_PROXY = '10.0.0.10:8080';
