/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class ModemManager extends PP_Class {	
	constructor (_Module) {
		super(_Module,'modem_manager');
		this._DBus_Ctrl__MM1__MM1__DBus_ObjectManager = new DBus_Ctrl__MM1__MM1__DBus_ObjectManager(this,this);
		this._DBus_Ctrl__MM1__MM1__ModemManager1 = new DBus_Ctrl__MM1__MM1__ModemManager1(this,this);
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice = null;
		this._PP_DBusSystem = new PP_DBusSystem(this, {
			destination: 'org.freedesktop.ModemManager1',
			path: '/org/freedesktop/ModemManager1'
		});
		this._PP_DBusSystem._on_properties_changed(this.on_properties_changed.bind(this));
		this.callbacks = {};
	}
	async init () {
		try {
			await this._DBus_Ctrl__MM1__MM1__ModemManager1.init();
			let managed_objects = await this.get_managed_objects();
			let modem_dbus_path = await this.get_modem_dbus_path(managed_objects);
			this.debug(3, `Modem found: ${modem_dbus_path}`);
			await this.refresh();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async get_managed_objects () {
		await this._DBus_Ctrl__MM1__MM1__DBus_ObjectManager.init();
		return this._DBus_Ctrl__MM1__MM1__DBus_ObjectManager.get('managed_objects');
	}
	async get_modem_dbus_path (managed_objects) {
		try {
			this.debug(3, `Managed objects...`);
			if (DEBUG==3) {
				global._Console.inspect(managed_objects);
			}
			let modem_dbus_paths = Object.keys(managed_objects);
			this.debug(3, `DBUS paths...`);
			if (DEBUG==3) {
				global._Console.inspect(modem_dbus_paths);
			}
			if (modem_dbus_paths.length === 0) {
				throw 'No modem found !';
			}
			return modem_dbus_paths[0];
		} catch (err) {
			this.debug(1,`Getting Modem failed: ${err}`);
			return null;
		}
	}
	reset () {
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time = null;
		this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice = null;
	}
	async refresh () {
		let hasKey = global._PP._Fn.hasKey;
		let managed_objects = await this.get_managed_objects();
		for (let modem_dbus_path in managed_objects) {
			let managed_object = managed_objects[modem_dbus_path];
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Firmware', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Location', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Messaging', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Modem3gpp', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Signal', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Simple', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Time', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time(this._Module, this, modem_dbus_path);
				await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time = null;
			}
			if (hasKey(managed_object, 'org.freedesktop.ModemManager1.Modem.Voice', Object)) {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice(this._Module, this, modem_dbus_path);
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice.init();
			} else {
				this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice = null;
			}
		}
	}
	async get_interface_property (dbus_interface, dbus_property) {
		try {
			let managed_objects = await this.get_managed_objects();
			let modem_dbus_path = await this.get_modem_dbus_path(managed_objects);
			let interfaces = managed_objects[modem_dbus_path];
			return interfaces[dbus_interface][dbus_property];
		} catch (err) {
			this.debug(1,`Get ${dbus_interface}/${dbus_property} failed: ${err}`);
			return null;
		}		
	}
	on (dbus_interface, dbus_signal, callback) {
		let hasKey = global._PP._Fn.hasKey;
		if (!hasKey(this.callbacks, dbus_interface, Object)) {
			this.callbacks[dbus_interface] = {};
		}
		if (!hasKey(this.callbacks[dbus_interface], dbus_signal, Array)) {
			this.callbacks[dbus_interface][dbus_signal] = [];
		}
		this.callbacks[dbus_interface][dbus_signal].push(callback);
	}	
	async on_properties_changed (interface_name, changed_properties, invalidated_properties) {
		this.debug(3, `Notification > PropertiesChanged on ${interface_name}: `)
		if (DEBUG==3) {
			global._Console.inspect(changed_properties);
		}
		switch (interface_name) {
			case 'org.freedesktop.DBus.ObjectManager':
				this._DBus_Ctrl__MM1__MM1__DBus_ObjectManager.refresh();
				break;
			case 'org.freedesktop.ModemManager1':
				this._DBus_Ctrl__MM1__MM1__ModemManager1.refresh();
				break;
			case 'org.freedesktop.ModemManager1.Modem':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Firmware':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Location':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Messaging':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Modem3gpp':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Signal':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Simple':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Time':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time.refresh();
				}
			case 'org.freedesktop.ModemManager1.Modem.Voice':
				if (this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice !== null) {
					this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice.refresh();
				}
		}			
	}
	async on_dbus_notifications (_Ctrl, signal, ...args) {
		try {
			let modem_dbus_path = null;
			if (_Ctrl == this.DBus_Ctrl__MM1__MM1__Modem) {
				await _Ctrl.refresh();
				switch (signal) {
					case 'StateChanged':
						let state_old = args[0];
						let state_new = args[1];
						let state_reason = args[2];
						this.debug(1,`StateChanged: state_old=${state_old} state_new=${state_new} state_reason=${
							state_reason}`);
						/*let enum = global._PP._Enum['org.freedesktop.ModemManager1'];
						for (let i in enum['MMModemState']) {
							if (enum['MMModemState'][i] == state_old) {
								this.debug(3,`StateChanged: state_old=${i}`);
							}
						}
						for (let i in enum['MMModemState']) {
							if (enum['MMModemState'][i] == state_new) {
								this.debug(3,`StateChanged: state_new=${i}`);
							}
						}
						for (let i in enum['MMModemStateChangeReason']) {
							if (enum['MMModemStateChangeReason'][i] == state_reason) {
								this.debug(3,`StateChanged: state_reason=${i}`);
							}
						}*/
						break;
				}
			}
			if (_Ctrl == this.DBus_Ctrl__MM1__MM1__ModemManager1) {
				await _Ctrl.refresh();
				switch (signal) {
					case 'PropertiesChanged':
						// only version as property...
						break;
				}
			}
			if (_Ctrl == this._DBus_Ctrl__MM1__MM1__DBus_ObjectManager) {
				await _Ctrl.refresh();
				switch (signal) {
					case 'InterfacesAdded':
						modem_dbus_path = args[0];
						let dict = args[1];
						this.inspect(1,`InterfacesAdded : dbus_path=${modem_dbus_path} dict=`,dict);
						for (let dbus_interface in dict) {
							switch (dbus_interface) {
								case 'org.freedesktop.ModemManager1.Modem':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem.init();
									break;
								case 'org.freedesktop.ModemManager1.Modem.Firmware':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware.init();
									break;
								case 'org.freedesktop.ModemManager1.Modem.Location':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location.init();
									break;
								case 'org.freedesktop.ModemManager1.Modem.Messaging':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging.init();
									break;
								case 'org.freedesktop.ModemManager1.Modem.Modem3gpp':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp.init();
									break;
								case 'org.freedesktop.ModemManager1.Modem.Signal':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal.init();
									break;
								// No propertier for 'org.freedesktop.ModemManager1.Modem.Simple'
								case 'org.freedesktop.ModemManager1.Modem.Time':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time.init();
									break;
								case 'org.freedesktop.ModemManager1.Modem.Voice':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice = new DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice(this._Module, this, modem_dbus_path);
									await this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice.init();
									break;
							}
						}
						break;
					case 'InterfacesRemoved':
						modem_dbus_path = args[0];
						let interfaces = args[1];
						this.inspect(1,`InterfacesRemoved : dbus_path=${modem_dbus_path} interfaces=`,interfaces);
						for (let dbus_interface in interfaces) {
							switch (dbus_interface) {
								case 'org.freedesktop.ModemManager1.Modem':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Firmware':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Firmware = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Location':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Location = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Messaging':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Messaging = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Modem3gpp':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Signal':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Signal = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Simple':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Simple = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Time':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Time = null;
									break;
								case 'org.freedesktop.ModemManager1.Modem.Voice':
									this._DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_Voice = null;
									break;								
							}
						}
						break;
				}
			}
			let dbus_interface = _Ctrl._DBus.obj.interface;
			let hasKey = global._PP._Fn.hasKey;
			if (hasKey(this.callbacks, dbus_interface, Object)) {
				if (hasKey(this.callbacks[dbus_interface], signal, Array)) {
					for (let i=0;i<this.callbacks[dbus_interface][signal].length;i++) {
						this.debug(3,`Handling callback notification [${dbus_interface}][${signal}][${i}]...`);
						await this.callbacks[dbus_interface][signal][i](_Ctrl, signal, ...args);
					}
				}
			}
		} catch (err) {
			this.debug(1,`Handling notification [${signal}] failed !`);
		}
	}
}