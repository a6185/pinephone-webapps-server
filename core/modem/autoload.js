//includes();
global.register_module({
	manifest: {
		version: '1.0',
		title: 'Modem',
		name: 'modem', // internal lowercased - no space nor special chars
		namespace: 'pp.core.modem',
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/core/modem',
		storage: {
			media: null,
			relpath: null
		}
	},
	files: [
		'core/modem/enum',
		'core/modem/modem.manager',
		'core/modem/module'
	],
	main: 'Modem'
});