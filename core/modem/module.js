// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Modem extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this._ModemManager = new ModemManager(this);
	}
	async init () {
		try {
			await this._ModemManager.init();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init Modem failed: ${err}`);
			throw err;
		}
	}
}



