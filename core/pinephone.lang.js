/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';
var _Lang = {
	locale: 'fr-FR'
};
const PINEPHONE_LANG = {
	locales: ['en-GB','fr-FR'],
	dict: {
		msg: [
/*':',':',' :',
';',';',' ;',
'!','!',' !',
'?','?',' ?',
//
'WRONG_UID','Wrong UID','Mauvais numéro UID',
'UNKNOWN_UID','Unknown UID','Numéro UID inconnu',
'DUPLICATE_UID','Duplicate UID','UID dupliquée',*/
':',' :',
';',' ;',
'!',' !',
'?',' ?',
// general
'Argument','Argument',
'Datas corrupted','Données corrompues',
'Duplicate UID','UID dupliquée',
'Field','Champ',
'Key','Clé',
'Last backup corrupted','Dernière sauvegarde corrompue',
'Object','Objet',
'Photo','Photo',
'MUST begin with a letter','doit commencer par une lettre',
'No data available','Aucun donnée disponible',
'None','Aucune',
'The argument','L\'argument',
'The object','L\'objet',
'The field','Le champ',
'The key','La clé',
'The photo','La photo',
'Too much categories','Trop de catégories',
'Too much filters','Trop de filters',
'Unable to change default value','Impossible de changer la valeur par défaut',
'Unknown UID','Numéro UID inconnu',
'Wrong UID','Mauvais numéro UID',
'cannot be null','cannot be null',
'cannot be undefined','ne peut être indéfini',
'chars','caractères',
'contains a wrong value','contient une valeur erronée',
'décimals','décimales',
'event','événement',
'invalid','invalide',
'is bad formatted','est mal formaté',
'is invalid','est invalide',
'is missing','est manquant',
'is too short','est trop courte',
'is too long','est trop longue',
'key','la clé',
'missing or bad','manquant ou erroné',
'must be <','doit être <',
'must be >','doit être >',
'must be an array','doit être un tableau',
'must be an integer','doit être un entier',
'must be an object','doit être un objet',
'must be an real','doit être un réel',
'must be an string','doit être une chaîne de caractères',
'must have','doit avoir',
'seq','sequence',
'the string','la chaîne de caractères',
'too long','est trop long',
'value','la valeur'

		]
	}
}

