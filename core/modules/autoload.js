//'core/modules/pp_modules' // already loaded on startup !
//includes();
global.register_module({
	manifest: {
		version: '0.5',
		title: 'Modules',
		name: 'modules', // internal lowercased - no space nor special chars
		namespace: 'pp.apps.modules', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/core/modules',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/tmp'
		}
	},
	files: [
		'core/modules/modules.slot',
		'core/modules/module'
	],
	main: 'Modules'
});