// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Modules extends PP_Module {
	constructor (manifest) {
		super(manifest);
	}	
	async init () {		
		try {
			this.set('slots', {
				'modules': new PP_Modules_Slots(this),
			});
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async action (ws, data, type) {		
		try {
			let obj = data.args;
			let res = {};
			let action = this.get_action(data.route);
			let _Modules = global._PP._Core._Modules;
			switch (action) {
				case 'list':
					return _Modules.export();
					break;
				default:
					if (!ws.has_key(obj, 'namespace', String)) {
						throw 'Missing or wrong namespace argument !';
					}
					if (!_Modules.has(obj.namespace)) {
						throw 'Unknown namespace !';
					}
					if (['activate','desactivate'].indexOf(obj.namespace)==-1) {
						throw 'Unknown action !';
					}
					_Modules[action](obj.namespace);
					return _Modules.export();
			}
		} catch (err) {
			throw err;
		}
	}
}