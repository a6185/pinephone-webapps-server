// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Modules_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'module', {
			'pp.apps.modules:/list': null,
			'pp.apps.modules:/activate': 'pp.apps.modules:/activated',
			'pp.apps.modules:/desactivate': 'pp.apps.modules:/desactivated'
			// so long...
			//'pp.apps.modules:/add': 'pp.apps.modules:/added',
			//'pp.apps.modules:/remove': 'pp.apps.modules:/removed'
		});
	}
	async handler (ws, data) {			
		try {
			let args = await this._Module.action(ws, data, 'module');
			let _Status = new PP_Status();
			_PP._WS.send_signal({
				args: args,
				callback: this.callbacks[data.route]
			}, _Status);
			this.debug(1,`${data.route} successfull...`);
			return {};
		} catch (err) {
			this.debug(1,`${data.route} failed... (${err})`);
			throw err;
		}
	}
}