// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Modules {
	constructor () {
		this.obj = {};
		this.parents = {};
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_modules:: ${msg}`);
	}
	has (namespace) {
		return this.obj.hasOwnProperty(namespace);
	}
	register (args) {
		let manifest = args.manifest;
		let namespace = manifest.namespace;
		let files = args.files;
		let main = args.main;
		let opts = args.options || {};
		if (!this.has(namespace)) {
			this.obj[namespace] = {
				manifest: manifest,
				files: files,
				main: main,
				internal: /^pp.core\./.test(namespace),
				active: false,
				started: false, // become true on init() success				
				_Mod: null, // main module object
				opts: opts, // with parents namespace if any
				level: null // will be computed later by run()...
			}
			let hasKey = global._PP._Fn.hasKey;
			if (hasKey(opts,'parents',Array)) {
				this.parents[namespace] = opts.parents;
			} else {
				this.parents[namespace] = [];				
			}
			/* activate all core modules by default */
			if (this.obj[namespace].internal) {
				this.on(namespace);
			}
		}
	}
	on (namespace) {
		global.includes(this.obj[namespace].files);
		// retrieve main classname from includes...			
		let classname = this.obj[namespace].main;		
		// i know eval is evil, but no choice...
		this.obj[namespace]._Mod = eval('new ' + classname + '(this.obj[namespace].manifest)');
		this.obj[namespace].active = true;
	}
	off (namespace) {
		this.obj[namespace]._Mod = null;
		this.obj[namespace].active = false;		
	}
	get (namespace) {
		return (this.has(namespace) ? this.obj[namespace]._Mod : null);
	}
	is_internal (namespace) {
		return this.obj[namespace].internal;
	}
	is_active (namespace) {
		return this.obj[namespace].active;
	}
	is_started (namespace) {
		return this.obj[namespace].started;
	}
	activate (namespace) {
		if (this.has(namespace)) {
			if (!this.is_active(namespace)) {
				this.debug(`Activate module "${namespace}"...`);
				this.on(namespace);
			} else {
				this.debug(`Module "${namespace}" is already activated !`);
			}
		}
	}
	desactivate (namespace) {
		if (this.has(namespace)) {
			if (this.is_internal(namespace)) {
				this.debug(`Can't desactivate core module "${namespace}"...`);
			} else {
				if (this.is_active(namespace)) {
					this.debug(`Desactivate module "${namespace}"...`);
					this.off(namespace);
				} else {
					this.debug(`Module "${namespace}" is already desactivated !`);
				}
			}
		}
	}
	activate_all () {
		for (let namespace in this.obj) {
			if (this.has(namespace) && !this.is_active(namespace)) {
				this.activate(namespace);
			}
		}
	}
	desactivate_all () {
		for (let namespace in this.obj) {
			if (this.has(namespace) && this.is_active(namespace)) {
				this.desactivate(namespace);
			}
		}
	}
	export () {
		let apps_modules = [];
		for (let namespace in this.obj) {
			if (this.has(namespace) && !this.is_internal(namespace)) {
				apps_modules.push({
					namespace: this.obj.namespace,
					active: this.obj.active,
					ready: this.obj.ready
				});
			}
		}
		return apps_modules;
	}
	get_depth (namespace,level) {
		/*console.log(namespace);
		console.log(level);
		debugger;*/
		if (!this.parents[namespace].length) {
			return level;
		} else {
			if (this.obj[namespace].level !== null) { // already computed
				return this.obj[namespace].level + level;
			} else {
				let depths = [];
				this.parents[namespace].forEach(ns => {
					depths.push(this.get_depth(ns,level+1));
				})
				return Math.max(...depths);
			}
		}
	}
	can_be_started (namespace) {
		if (this.parents[namespace].length) {
			for (let i=0;i<this.parents[namespace].length;i++) {
				let parent = this.parents[namespace][i];
				if (!this.is_started(parent)) {
					this.debug(1,`=== Module "${namespace}" can't be started : dependance failure on : ${parent} ===`);
					return false;
				}
			}
		}
		return true;
	}
	start (namespace) {
		this.debug(1,`=== Starting module "${namespace}"... ===`);
		return this.obj[namespace]._Mod.init();
	}
	async run () {		
		let levels = {};
		let hasKey = global._PP._Fn.hasKey;
		for (let namespace in this.obj) {
			this.obj[namespace].level = this.get_depth(namespace,0);			
			if (this.is_active(namespace)) {
				let key = 'l' + this.obj[namespace].level;
				if (!hasKey(levels,key,Array)) {
					levels[key] = []
				}
				levels[key].push(namespace);
			}
		}
		// console.dir(levels);
		// debugger;
		let promises = [], candidates = [], results;
		for (let level=0;level<10;level++) {
			let key = 'l' + level;
			this.debug(1,`=== LEVEL ${level} ===`);
			//debugger;
			if (levels.hasOwnProperty(key)) {
				promises = [];
				candidates = [];
				levels[key].forEach(namespace => {
					if (this.is_internal(namespace) && this.can_be_started(namespace)) { // core modules first !
						candidates.push(namespace); // for debugging
						promises.push(this.start(namespace));
					}
				});
				this.debug(1,`=== LEVEL ${level} : core modules started ===`);
				// debugger;
				if (promises.length) {
					results = await Promise.allSettled(promises);
					for (let i=0;i<results.length;i++) {
						let result = results[i];
						if (result.status == 'fulfilled') {
							let obj = result.value.obj;
							this.debug(1,`=== Module "${obj.namespace}" started ! ===`);
							this.obj[obj.namespace].started = true;
						} else { // rejected
							debugger;
							this.debug(1,`=== Starting module failed: ${result.reason} ===`);
						}
					}
				}
				this.debug(3,`=== LEVEL ${level} : core modules finished ! ===`);
				promises = [];
				candidates = [];
				levels[key].forEach(namespace => {
					if (!this.is_internal(namespace) && this.can_be_started(namespace)) { // apps modules
						candidates.push(namespace); // for debugging
						promises.push(this.start(namespace));
					}
				});
				this.debug(1,`=== LEVEL ${level} : apps modules started ===`);
				// debugger;
				if (promises.length) {
					results = await Promise.allSettled(promises);
					for (let i=0;i<results.length;i++) {
						let result = results[i];
						if (result.status == 'fulfilled') {
							let obj = result.value.obj;
							this.debug(1,`=== Module "${obj.namespace}" started ! ===`);
							this.obj[obj.namespace].started = true;
						} else { // rejected
							debugger;
							this.debug(1,`Starting module failed: ${result.reason}`);
						}
					}
				}
				this.debug(3,`=== LEVEL ${level} : apps modules finished ! ===`);
				// debugger;
			}
		}
		/* start websocket clients */
		global._PP._WS.init();
	}
}

