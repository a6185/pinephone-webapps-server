// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_I18N_Parser {
	constructor () {
		this.obj = {
			locales: [],
			default_locale: "",
			current_locale: "",
			default_index: -1,
			current_index: -1,
			data: {}
		}
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_i18n_parser:: ${msg}`);
	}
	parse (rows) {
		let n = 1;
		rows.forEach(line => {
			let row = Object.values(line);
			if (!row.length) {
				return;
			}
			if (n==1) {
				let default_locale = row.shift().toString();
				if (!row.length) {
					return
				}
				let default_index = row.indexOf(default_locale);
				if (default_index==-1) {
					return;
				}
				this.obj = {
					default_locale: default_locale,
					current_locale: default_locale,
					locales: row,
					default_index: default_index,
					current_index: default_index,
					data: {}
				};
				// try to override current locale using args (if any...)
				if (this.obj.current_locale!=global._PP.locale && this.obj.locales.indexOf(global._PP.locale)!=-1) {
					let new_current_locale = global._PP.locale;
					let new_current_index = row.indexOf(new_current_locale)
					if (new_current_index==-1) {
						return;
					}
					this.obj.current_locale = new_current_locale;
					this.obj.current_index = new_current_index;
				}
				n++;
			} else { // line 2+
				if (!row.length) {
					return;
				}
				if (!this.obj.locales.length) {
					return;
				}
				if (this.obj.default_index==-1) {
					return;
				}
				if (this.obj.current_index==-1) {
					return;
				}
				if (!this.obj.default_locale.length) {
					return;
				}
				if (!this.obj.current_locale.length) {
					return;
				}
				let key = row.shift().toString();
				if (/^\/\//.test(key)) { // ignore comments
					return;
				}
				this.obj.data[key] = "";
				if (!row.length) {
					return;
				}
				if (this.obj.current_index<row.length) {
					this.obj.data[key] = row[this.obj.current_index];
				} else {
					if (this.obj.default_index<row.length) {
						this.obj.data[key] = row[this.obj.default_index];
					}
				}
			}
		});
		return this.obj;
	}
}