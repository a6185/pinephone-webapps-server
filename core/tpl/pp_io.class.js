// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_IO {
	constructor (_Module) {
		this._Module = _Module;
		this.handler = {};
	}
	init () {
		try {		
			if (this._Module.has_storage()) {
				let storage = this._Module.get('storage');
				let hasKey = global._PP._Fn.hasKey;
				if (hasKey(storage,'media',String) && storage.media.length && hasKey(STORAGE_MEDIAS,storage.media,String)) {
					if (hasKey(storage,'relpath',String) && storage.relpath.length) {
						/* TODO: CHECK RESULT */
						/* autocreate module storage */
						this._Module._Storage.mkdir(storage.media,storage.relpath);
					}
				}				
			}
		} catch (err) {
			throw err;
		}
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_io:: ${this._Module.get('name')} : ${msg}`);
	}
	add_handler (format, callback) {
		this.handler[format] = callback;
	}
	async list_media_files () {
		this.debug(3,'List media files...');
		try {
			let storage = this._Module.get_storage();
			let result = await this._Module._Storage.list(storage.media, storage.relpath, {			
				encoding: 'utf-8',
				filters: 'isFile',
				ext: storage.format
			});
			if (!Object.keys(result.files).length) {
				return [];
			} else {
				let filenames = [];
				result.files.forEach(file => {
					filenames.push(file.name);
				});
				//this.debug(3,this.obj.name);
				//inspect(filenames);
				filenames.sort((a,b) => b.replace('.' + storage.format,'')-a.replace('.' + storage.format,''));
				//inspect(filenames);
				return filenames;
			}
		} catch (err) {
			this.debug(3,`List media files failed ! ${err}`);
			throw err;
		}
	}
	async loop_media_files (files) {
		this.debug(3,'Loop media files...');
		try {
			let sprintf = global._PP._Fn.sprintf;
			let filename = files.shift();
			this.debug(3,`Reading backup "${filename}"...`);
			let result = await this.read_media_file(filename);
			if (result === null) {
				throw `Failed to read backup ${filename}...`;
			} else {
				this.debug(3,`Backup readed !`);
			}
			return result;
		} catch (err) {
			this.debug(3,err);
			if (files.length) {
				return await this.loop_media_files(files);
			} else {
				throw sprintf('%s%s','No data available', _PP.tr('!'));
			}
		}
	}

	async read_media_file (file) {
		this.debug(3,'Read media file...');
		try {
			let storage = this._Module.get_storage();
			let result = await this._Module._Storage.read(storage.media, storage.relpath, file, {
				encoding: 'utf-8',
			});
			if (result === null) {
				return null;
			}
			if (this.handler[storage.format] === undefined) {
				return null;
			}
			return this.handler[storage.format](result);
		} catch (err) {
			this.debug(3,`Read media file failed ! ${err}`);
			throw err;
		}
	}
	async last_backup () {
		try {
			let files = await this.list_media_files();
			if (!files.length) {
				return null; // not an error !
			}
			let result = await this.loop_media_files(files);
			return result;
		} catch (err) {
			throw err;
		}
	}
	async write () {
		this.debug(3,'Write media file...');
		try {
			let storage = this._Module.get_storage();
			this.debug(3, `write: media = "${storage.media}"`);
			this.debug(3, `write: relpath = "${storage.relpath}"`);
			let filename = (new Date()).to_yyyymmddhhiiss() + '.json';
			this.debug(3, `write: filename = "${filename}"`);
			let json = this._Module.export();
			this.debug(3, `write: JSON doc ready to write...`);
			this.debug(3, `write: length = ${json.length} characters...`);
			let result = await this._Module._Storage.write(storage.media, storage.relpath, filename, json, {
				encoding: 'utf8'
			});
			return result;
		} catch (err) {
			this.debug(3,`Write media file failed ! ${err}`);
			throw err;
		}
	}		
}