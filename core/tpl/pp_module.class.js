// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Module extends JS_Object {
	constructor (obj) {
		super();
		this.name = obj.name;
		this.obj = obj;
		this.core = {};
		this.tbl = {};
		this.slots = {};
		this.parent = {}; // parent modules
		/*
		Optionals:
		this.i18n = new PP_I18(...);
		this._Storage = new PP_Storage(...);
		*/
		/* obj = Manifest - example : v.202207
			{
			version: 'X.Y',
			title: 'Phone',
			name: 'phone', // internal lowercased - no space nor special chars
			namespace: 'com.alsatux.phone', // internal
			author: 'Jean Luc BIELLMANN',
			mail: 'contact@alsatux.com',
			url: 'https://alsatux.com',
			year: '2021',	
			storage: {
				media: STORAGE_DEFAULT, // cf. core/storage/config.js
				relpath: '/echo'
			}
		*/
		// TODO : check in the future...
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_module:: ${this.obj.name} : ${msg}`);
	}
	async init () {
		try {
			/* By module */
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			/* By module */
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async defaults () {
		if (this.i18n !== undefined) {
			await this.i18n.init();
		}
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(this.obj,'storage',Object)) {
			this.obj.storage.abspath = STORAGE_MEDIAS[this.obj.storage.media] + this.obj.storage.relpath;
			this._Storage = new PP_Storage(this);
			await this._Storage.init();
			this._IO = new PP_IO(this);
		}
	}
	tr (txt) { // use msg
		if (this._Lang !== null) {
			return this._Lang.tr(txt);
		}
	}
	tr2 (txt) { // use key
		if (this._Lang !== null) {
			return this._Lang.tr2(txt);
		}
	}	
	set (key, value) {
		switch (key) {
			case 'tbl':
				this.tbl = value;
				// set tables names after constructor for debug()
				for (let name in this.tbl) {
					this.tbl[name].name = name;
				}
				break;
			case 'slots':
				this.slots = value;
				break;
			default:
				this.obj[key] = value;
		}
	}
	import (json) {
		/* By module */
	}
	export () {
		/* By module */			
	}
	get_parent (module) {
		return this.parent.hasOwnProperty(module) ? this.parent[module] : null;
	}
	get_action (route) {
		let action = route.split('/').pop(); // list, add, upd, del, import, export, ...
		this.debug(3,`Trying to "${action}"...`);
		return action;
	}
	has_storage () {
		let hasKey = global._PP._Fn.hasKey;
		return hasKey(this.obj,'storage',Object);
	}
	get_storage () {
		if (!this.has_storage()) {
			throw `No storage object defined for module "${this.name}" !`;
		}
		return this.get('storage');
	}
	async get_last_backup () {
		if (this.has_storage()) {
			// default JSON handler
			this._IO.add_handler('json', result => {
				let content = '';
				content = result.data.content.replace(/\n/g,'');
				//inspect(content);
				let data = JSON.parse(content);
				//inspect(data);
				return data;
			});
			this._IO.init();
			let result = await this._IO.last_backup();
			this.import(result);
		}
	}
}