// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Screen extends PP_Class {
	constructor (_Module) {
		super(_Module,'pp_screen');
		this._Shell = new PP_Shell(this);
	}
	unlock () {
		this._Shell.exec('/usr/bin/loginctl show-seat $(loginctl list-seats --no-pager --no-legend) -p ActiveSession --value|xargs -iX /usr/bin/loginctl unlock-session X');
	}
}