// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Class extends JS_Object {
	constructor (_Module, classname) {
		super();
		this._Module = _Module;
		this.classname = classname;
		this.origin = _Module.obj.namespace + '_' + classname;
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_class:: ${this.origin} : ${msg}`);
	}
	inspect (level, msg, obj) {
		this.debug(level,msg);
		global._Console.inspect(obj);
	}
}