// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Tbl extends JS_Object {
	constructor (_Module, maxcounter, objectclass) {		
		super();
		this._Module = _Module;
		this.uidlength = maxcounter.toString().length;
		this.counter = {
			// index 0 is reserved for default values !
			min: 1,
			max: maxcounter
		};
		this.objectclass = objectclass;
	}
	debug (level, msg) {
		let origin = this._Module.get('namespace') + '_' + this.name + '_tbl';
		global._Console.debug(level,`>pp_tbl:: ${origin} : ${msg}`);
	}
	reset () {
		this.obj = {};
		this.set_defaults();
	}
	set_defaults () {
	}
	length () {
		return Object.keys(this.obj).length;
	}
	exists_object (obj) {
		let sprintf = global._PP._Fn.sprintf;
		let hasKey = global._PP._Fn.hasKey;
		if (!hasKey(obj, 'uid', String) || obj.uid.length != this.uidlength) {
			throw sprintf('%s%s',_PP.tr('WRONG_UID'),_PP.tr('!'));
		}
		if (!this.has(obj.uid)) {
			throw sprintf('%s%s',_PP.tr('UNKNOWN_UID'),_PP.tr('!'));
		}		
	}
	exists_dbus_path (dbus_path) {
		for (let uid in this.obj) {
			if (this.has(uid)) {
				let _Object = this.obj[uid];
				if (dbus_path == _Object.get('dbus_path')) {
					return true;
				}
			}
		}
		this.debug(3,`Warning: exists_dbus_path(): unknown sms ${dbus_path} !`);
		return false;
	}
	find_dbus_path (dbus_path) {
		for (let uid in this.obj) {
			if (this.has(uid)) {
				let _Object = this.obj[uid];
				if (dbus_path == _Object.get('dbus_path')) {
					return _Object;
				}
			}
		}
		return null;
	}
	delete_dbus_path (dbus_path) {
		for (let uid in this.obj) {
			if (this.has(uid)) {
				let _Object = this.obj[uid];
				if (dbus_path == _Object.get('dbus_path')) {
					delete this.obj[uid];
					return true;
				}
			}
		}
		return false;
	}
	next_uid () {
		let keys = Object.keys(this.obj);
		let index = this.counter.min;
		let uid = '';
		let sprintf = global._PP._Fn.sprintf;
		do {
			uid = sprintf('%0' + this.uidlength + 'd',index++);
		} while (keys.indexOf(uid)!=-1 && index<=this.counter.max);
		return uid;
	}
	add (obj) {		
		let duplicated = false;
		let sprintf = global._PP._Fn.sprintf;
		try {
			this.exists_object(obj);
			duplicated = true;
		} catch (err) {
			let _Object = new this.objectclass(this._Module);
			_Object.check_structure(obj,['uid']);
			_Object.upd(obj);
			let uid = this.next_uid();
			_Object.set('uid',uid);
			this.obj[uid] = _Object;
			return _Object.export();
		}
		if (duplicated) {
			throw sprintf('%s%s',_PP.tr('DUPLICATE_UID'),_PP.tr('!'));
		}
	}
	upd (obj) {
		this.exists_object(obj);
		let _Object = new this.objectclass(this._Module);
		_Object.check_structure(obj,[]);
		_Object.upd(obj);
		this.obj[obj.uid] = _Object;
		return _Object.export();
	}
	del (obj) {
		this.exists_object(obj);
		delete this.obj[obj.uid];
		return obj;
	}
	export () {
		let mydb = {};
		for (let uid in this.obj) {
			mydb[uid] = this.obj[uid].export();
		}
		return mydb;
	}		
	find (key,value) {
		let rows = [];
		for (let uid in this.obj) {
			let s = JSON.stringify(this.obj[uid].obj);
			if (s.indexOf(`"${key}":"${value}"`)!=null || s.indexOf(`"${key}":${value},`)!=null || s.indexOf(`"${key}":${value}\}`)!=null) {
				rows.push(this.obj[uid]);
			}
		}
		return rows;
	}
}