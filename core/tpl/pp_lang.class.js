// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Lang {
	constructor (_Parent, obj) {
		this._Parent = _Parent;
		this.obj = obj;
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_lang:: ${msg}`);
	}
	object_not_found (txt) {
		this.debug(1,'Object "' + txt + '" not found !');
	}
	tr (txt) { // use msg
		let offset = Object.keys(this.obj.locales).indexOf(_PP.locale);
		let pos = this.obj.dict.msg.indexOf(txt);
		if (pos!=-1) {
			return this.obj.dict.msg[pos + offset];
		} else {			
			this.object_not_found(txt);
			return txt;
		}
	}
	tr2 (txt) { // use key
		let offset = Object.keys(this.obj.locales).indexOf(_PP.locale);
		let pos = this.obj.dict.key.indexOf(txt);
		if (pos!=-1) {
			return this.obj.dict.key[pos + offset+1];
		} else {
			this.object_not_found(txt);
			return txt;
		}
	}
}
