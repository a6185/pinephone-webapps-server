// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Object extends JS_Object {
	constructor (_Module, classname) {
		super();
		this._Module = _Module;
		this.classname = classname;
		this.origin = _Module.obj.namespace + '_' + classname;
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_object:: ${this.origin} : ${msg}`);
	}
	upd (obj) {
		this.obj = obj;
		return this;
	}
	check_object () {
		let sprintf = global._PP._Fn.sprintf;
		if (this.obj === null) {
			throw sprintf('%s "%s/%s" %s%s',_PP.tr('THE_OBJECT'),this._Module.get('name'),this.name,_PP.tr('CANT_BE_NULL'),_PP.tr('!'));
		}
		if (this.obj === undefined) {
			throw sprintf('%s "%s/%s" %s%s',_PP.tr('THE_OBJECT'),this._Module.get('name'),this.name,_PP.tr('CANT_BE_UNDEFINED'),_PP.tr('!'));
		}
		if (this.obj.constructor !== Object) {		
			throw sprintf('%s "%s/%s" %s%s',_PP.tr('THE_OBJECT'),this._Module.get('name'),this.name,_PP.tr('IS_INVALID'),_PP.tr('!'));
		}
	}
	check_field () { // label, type, key, value, extra arguments
		let args = arguments;
		let label = args[0];
		let type = args[1];
		let key = args[2];
		let value = args[3];
		let minvalue, maxvalue, minlength, maxlength, precision, length, re, format;
		let sprintf = global._PP._Fn.sprintf;
		switch (type) {
			case 'is_uid':
				length = args[4];
				this.check_field(label,'is_string',key,value,length,length);
				re = new RegExp('^\\d{'+length+'}$');
				if (value.match(re)==null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				break;
			case 'is_string':
				if (value.constructor !== String) {					
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BE_A_STRING'),_PP.tr('!'));
				}
				minlength = args[4];
				if (value.length<minlength) {
					throw sprintf('%s "%s": %s %s (> %d %s)%s',_PP.tr('FIELD'),label,_PP.tr('THE_STRING'),_PP.tr('IS_TOO_SHORT'),minlength,_PP.tr('CHARS'),_PP.tr('!'));
				}
				maxlength = args[5];
				if (value.length>maxlength) {
					throw sprintf('%s "%s": %s %s (> %d %s)%s',_PP.tr('FIELD'),label,_PP.tr('THE_STRING'),_PP.tr('IS_TOO_LONG'),maxlength,_PP.tr('CHARS'),_PP.tr('!'));
				}
				break;
			case 'is_integer':
				if (value.constructor !== Number || !Number.isInteger(value)) {					
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BE_AN_INTEGER'),_PP.tr('!'));
				}
				minvalue = args[4];
				if (value<minvalue) {
					throw sprintf('%s "%s": %s %d %s %d%s',_PP.tr('FIELD'),label,_PP.tr('THE_VALUE'),value,_PP.tr('MUST_BE_LT'),minvalue,_PP.tr('!'));
				}
				maxvalue = args[5];
				if (value>maxvalue) {
					throw sprintf('%s "%s": %s %s (> %d %s)%s',_PP.tr('FIELD'),label,_PP.tr('THE_VALUE'),value,_PP.tr('MUST_BE_GT'),maxvalue,_PP.tr('!'));
				}
				break;
			case 'is_float':
				if (value.constructor !== Number) {					
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BE_A_REAL'),_PP.tr('!'));
				}
				minvalue = args[4];
				if (value<minvalue) {
					throw sprintf('%s "%s": %s %d %s %d%s',_PP.tr('FIELD'),label,_PP.tr('THE_VALUE'),value,_PP.tr('MUST_BE_LT'),minvalue,_PP.tr('!'));
				}
				maxvalue = args[5];
				if (value>maxvalue) {
					throw sprintf('%s "%s": %s %d %s %d%s',_PP.tr('FIELD'),label,_PP.tr('THE_VALUE'),value,_PP.tr('MUST_BE_GT'),maxvalue,_PP.tr('!'));
				}
				precision = args[6];
				if (Math.round((Math.pow(10,precision)*parseFloat(value)))/Math.pow(10,precision)!=value) {
					throw sprintf('%s "%s": %s %d %s %d %s%s',_PP.tr('FIELD'),label,_PP.tr('THE_VALUE'),value,_PP.tr('MUST_HAVE'),precision,_PP.tr('DECIMALS'),_PP.tr('!'));
				}
				break;
			case 'is_date':
				this.check_field(label,'is_string',key,value,10,10);
				format = args[4];
				if (value.to_date_format(format) === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('IS_BAD_FORMATTED'),_PP.tr('!'));
				}
				try {
					let d = new Date(value);
				} catch(e) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BE_A_DATE'),_PP.tr('!'));
				}
				break;
			case 'is_array':
				if (value.constructor !== Array) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BE_AN_ARRAY'),_PP.tr('!'));
				}
				break;
			case 'is_object':
				if (value.constructor !== Object) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('MUST_BE_AN_OBJECT'),_PP.tr('!'));
				}
				break;
			case 'not_null':
				if (value === null) {
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('CANT_BE_NULL'),_PP.tr('!'));
				}
				break;
			case 'not_undefined':
				if (value === undefined) {		
					throw sprintf('%s "%s" %s%s',_PP.tr('THE_FIELD'),label,_PP.tr('CANT_BE_UNDEFINED'),_PP.tr('!'));
				}		
		}
	}
}