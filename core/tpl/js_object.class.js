// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class JS_Object {
	constructor () {
		this.obj = {};
	}
	set (key, value) {
		this.obj[key] = value;
	}
	get (key) {
		return this.obj[key];
	}
	has (key) {
		return this.obj.hasOwnProperty(key);
	}
	import (obj) {
		this.obj = obj;
	}
	export () {
		return this.obj;
	}
}
