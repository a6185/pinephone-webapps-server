// Jean Luc Biellmann - contact@alsatux.com

'use strict';
// A slot has the form /Module/Slot/Action.
// If you use a table to store objects, 
// the rule is to take the same name as the slot,
// to produce $name.slots.js and $name.tbl.js.

class PP_Slots {
	constructor (_Module, name, callbacks) {
		this._Module = _Module;
		this.name = name;
		this.callbacks = callbacks;
		this.origin = this._Module.get('namespace') + '_' + this.name + '_slots';
		Object.keys(this.callbacks).forEach(slot => {			
			if (this.callbacks.hasOwnProperty(slot)) {
				_PP._Core._Routes.add(slot, this.handler.bind(this));
			}
		});
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_slot:: ${this.origin} : ${msg}`);
	}
	run (route, obj) {
		let tbl_action = route.split('/').pop(); // list, add, upd, del, import, export, ...
		this.debug(3,`Trying to "${tbl_action}"...`);
		this._Module.tbl[this.name][tbl_action](obj);
	}
	async action (ws, route, obj) {
		try {
			// do something
			this.run(route,obj);
			// and save new datas
			await this._Module._IO.write();
			this.debug(1,`Action ${this.name} ok...`);
		} catch (err) {
			this.debug(1,`Action ${this.name} failed...`);
			ws._Status.code(1);
			ws._Status.err(global._PP._Fn.catchme(err));
		}
	}
	async handler (ws, data) {
		/* to override */
	}
}