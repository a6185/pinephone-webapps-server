// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_I18N {
	constructor (_Parent, relpath) {
		this._Parent = _Parent;
		this._Parent.tr = (msg) => { return this.tr(msg); };
		this.path = ROOT_PATH + relpath;
		_PP._DB._I18N.set(this.path);
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_i18n:: ${msg}`);
	}
	async init () {
		await _PP._DB._I18N.refresh(this.path);
	}
	tr (key) {		
		let obj = _PP._DB._I18N.get(this.path);
		let hasKey = global._PP._Fn.hasKey;
		if (hasKey(obj.data,key,String)) {
			return obj.data[key];
		} else {
			return `Key "${key}" not found in ${this.path}`;
		}
	}
}
