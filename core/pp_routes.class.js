/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class PP_Routes {
	constructor () {
		this.db = {};
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_routes:: ${msg}`);
	}
	add (route, handler) {
		this.debug(3,`Adding route ${route}`);
		this.db[route] = handler;
	}
	has (route) {
		return this.db.hasOwnProperty(route)
	}
};