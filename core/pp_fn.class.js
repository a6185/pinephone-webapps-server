// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Fn {
	constructor () {
		
	}
	sprintf (...args) {
		let sprintf = _PP._Nodes.get('sprintf-js');
		return sprintf.sprintf(...args);
	}
	md5 () {
		const md5dict = '0123456789abcdefghijklmnopqrstuvwxyz';
		let str = '';
		while (str.length<32) {
			let j = parseInt(Math.random()*md5dict.length,10);
			str += md5dict[j];
		}
		return str;
	}
	isObject (obj) {
		if (!obj) { 
			return false;
		}
		return obj.constructor === Object ?  true : false;
	}
	catchme (err) {
		if (err instanceof SyntaxError) {
			return err.stack;
		}
		if (err instanceof TypeError) {
			return err.stack;
		}
		if (err instanceof ReferenceError) {
			return err.stack;
		}
		if (err instanceof Error) {
			return err.stack;
		}
		if (err instanceof String) {
			return err;
		}
		return JSON.stringify(err);
	}
	hasKey (ob, key, type) {
		if (ob!==null && ob!==undefined && ob.constructor===Object) {
			if (key!==null && key!==undefined && key.constructor===String) {
				if (ob.hasOwnProperty(key)) {
					if (ob[key] === undefined) {
						global._Console.inspect(ob);
						global._Console.debug(1,`Warning: Undefined key "${key}" found !`);
						return false;
					}
					if (ob[key] === null) {
						global._Console.inspect(ob);
						global._Console.debug(1,`Warning: Null key "${key}" found !`);
						return false;
					}
					if (ob[key].constructor === type) {
						return true;
					}
				}
			}
		}
		return false;
	}
}









