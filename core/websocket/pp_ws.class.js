// Jean Luc Biellmann - contact@alsatux.com

class PP_WS {
	constructor () {
		this.server = null;
		this.wss = null;
		this.wsclient = {};
		this.i18n = new PP_I18N(this, '/core/websocket/locales/pp_ws.csv');
		this.const = Object.freeze({
			MAX_CLIENTS: 40,
			PORT: 2020,
			CERT_PEM: '/usr/local/share/ca-certificates/mobian.crt',
			PRIV_KEY_PEM: '/usr/local/share/ca-certificates/mobian.pem'
		});
	}
	debug (level, msg) {
		global._Console.debug(level,`>pp_ws:: ${msg}`);
	}
	dump (ws,data) {
		if (DEBUG==3) {
			for (let i in data) {
				if (data[i] === undefined) {
					this.debug(3,'Warning: undefined data[i] for i="' + i + '"');
					continue;
				}
				if (data[i] === null) {
					this.debug(3,'Warning: null data[i] for i="' + i + '"');
					continue;
				}
				let str = (data[i].constructor === Object ? JSON.stringify(data[i]) : data[i]);
				if (str.length>55) {
					str = str.substr(0,55) + '...';
				}
				if (this.wsclient.hasOwnProperty(ws.uid)) {
					this.debug(3,'Sending to ' + ws.uid + ': ' + i + '=' + str);
				} else {
					this.debug(3,'Sending: ' + i + '=' + str);
				}
			}
		}	
	}
	send_unicast (ws,data) {
		data.status = ws._Status.data;
		this.dump(ws,data);
		var msg = JSON.stringify(data);
		this.debug(3,'Send unicast...');
		this.debug(3,msg);
		ws.send(msg);
		//ws._Status.reset();
	}
	send_signal (data, _Status) { // for all clients having the callback in their routes
		for (let id in this.wsclient) {
			if (this.wsclient[id].slot.indexOf(data.callback) != -1) {
					//wsclient[id].ws._Status.data = JSON.parse(JSON.stringify(_Status.data));
					this.wsclient[id].ws._Status = _Status;
					this.send_unicast(this.wsclient[id].ws,data);
			}
		}
	}
	send_multicast (data, _Status) {
		for (let id in this.wsclient) {
			//wsclient[id].ws._Status.data = JSON.parse(JSON.stringify(_Status.data));
			this.wsclient[id].ws._Status = _Status;
			this.send_unicast(this.wsclient[id].ws,data);
		}
	}
	async init () {
		await this.i18n.init();
		// in order to handle self-signed certificates we need to turn off the validation
		process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
		var https = _PP._Nodes.get('https');
		var ws = _PP._Nodes.get('ws');
		let fs = _PP._Nodes.get('file-system');
		const server = new https.createServer({
			cert: fs.readFileSync(this.const.CERT_PEM),
			key: fs.readFileSync(this.const.PRIV_KEY_PEM)
		});
		server.listen(this.const.PORT);
		this.wss = new ws.Server( { server } );
		this.wss.on('connection', ws => {
			var sprintf = global._PP._Fn.sprintf;
			var hasKey = global._PP._Fn.hasKey;

			ws.error = (code, msg, route, args) => {
				this.debug(3,msg);
				ws._Status.code(code);
				ws._Status.err(msg);
				if (route === null) {
					route = '/socket/error';
					args = {};
				}
				this.send_unicast(ws,{
					route: route,
					args: args
				});
				return false;
			};

			this.debug(1,'Incoming connection...');
			ws._Status = new PP_Status();
			this.debug(2,'Socket status defined...');
			if (Object.keys(this.wsclient).length == this.const.MAX_CLIENTS) {
				ws.error(1, sprintf("%s%s",this.tr('ERR_TOO_MUCH_CLIENTS'),_PP.tr('!')), '/socket/fatal', {});
			}

			// always create an uniq id
			let id = 0;
			do {
				id = '' + Math.random().toString(36).substr(2, 9);
			} while (id in this.wsclient);
			ws.uid = id;
			const uid = ws.uid;
			this.wsclient[id] = {
				ws: ws,
				slot: []
			};
			this.debug(1,`Register wsclient ${id}...`);

			ws.has_key = (obj, key, type) => {
				let hasKey = global._PP._Fn.hasKey;
				if (!hasKey(obj, key, type)) {
					ws._Status.code(2);
					ws._Status.err(sprintf('%s "%s" %s%s',this.tr('ARGUMENT'),key,this.tr('MISSING_OR_BAD'),_PP.tr('!')));
					return false;
				}
				return true;
			};

			ws.on('open', () => {
				this.debug(1,`Houston, we've got a problem.`);
				// BUG OF NODE.JS ??? - NOT WORKING !!!
				ws._Status.info(_sprintf("%s%s",this.tr('INFO_SOCKET_OPEN'),_PP.tr('!')));
				this.send_unicast(ws,{
					callback: '/socket/register',
					args: {
						uid: id
					}
				});
			});
			ws.on('close', () => {
				this.debug(1,'Client ' + uid + ' leave...');
				this.wsclient[uid] = null;
				delete this.wsclient[uid];
			});
			ws.on('message', (txt) => {
				ws._Status = new PP_Status();
				if (DEBUG==3) {
					this.debug(3,'---' + uid);
					this.debug(3,txt);
				}
				try {
					var data = JSON.parse(txt);
				} catch (err) {
					return ws.error(1, sprintf("%s%s",this.tr('INVALID_JSON'),_PP.tr('!')), null, null);
				}
				//this.debug(3,JSON.stringify(ob));
				if (data instanceof Object) {
					if (hasKey(data, 'route', String)) {
						this.debug(3,`Key '${data.route}' found`);
						if (data.route=='/register/slots') { // client slots
							/*
							data: {
								route: '/register/slots',
								args: {
									slots: [
										'com.alsatux.calendar:/events/reset',
										'com.alsatux.calendar:/filter/added',
										...
									]
								}
							}
							*/				
							if (hasKey(data, 'args', Object)) {
								if (hasKey(data.args, 'slots', Array)) {
									data.args.slots.forEach(slot => {
										let s = slot.replace(/[^a-z\.\:\/]+/g,'');
										if (s.length && this.wsclient[uid].slot.indexOf(s)==-1) {
											this.wsclient[uid].slot.push(s);
										}
									});
								}									
							}
						} else {
							if (!_PP._Core._Routes.has(data.route)) {
								return ws.error(1, sprintf("%s%s",this.tr('UNREGISTERED_ROUTE'),_PP.tr('!')), data.route, {});
							} else {
								this.debug(3,`Route '${data.route}' found`);
								/*
								data: {
									route: 'com.alsatux.calendar:/events/add',
									args: {
										...
									},
									// optionnal callback
									callback: 'com.alsatux.calendar:/events/added'
								}
								*/				
								let callback = '';
								// check callback (if any)
								if (hasKey(data, 'callback' ,String)) {
									callback = data.callback;
								}
								// check args
								if (!hasKey(data, 'args', Object)) {
									return ws.error(1, sprintf("%s%s",this.tr('MISSING_OR_BAD_ARGS'),_PP.tr('!')), data.route, {});
								}
								// main handler
								_PP._Core._Routes.db[data.route](ws,data).then(args => {										
									if (DEBUG_SOCKET_OUTPUT) {
										this.debug(3,`start sendind arguments:`);
										inspect(args);
										this.debug(3,`end sending arguments`);
									}
									this.send_unicast(ws,{
										route: data.route,
										args: args,
										callback: data.callback
									});
								}).catch(err => {
									console.trace(`fatal error: "${err}" !`);
									return ws.error(1, err, data.route, {});
								});
							}
						}
					}
				}
			});
		});
		this.debug(1,'Server started !');
	} // end init()
}
