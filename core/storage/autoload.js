//includes();
global.register_module({
	manifest: {
		version: '1.0',
		title: 'Storage',
		name: 'storage', // internal lowercased - no space nor special chars
		namespace: 'pp.apps.storage',
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/core/storage',
		storage: {
			media: null,
			relpath: null
		}
	},
	files: [
		//'core/storage/config',
		'core/storage/storage.slots',
		'core/storage/module'
		//'core/storage/pp_storage.class'
	],
	main: 'Storage'
});