/* Jean Luc BIELLMANN - contact@alsatux.com */

const STORAGE_USER = 1000; // mobian
const STORAGE_GROUP = 1000; // mobian
const STORAGE_DIR = parseInt(775,8); // 0775
const STORAGE_FILE = parseInt(664,8); // 0664
const STORAGE_MEDIAS = { 
	// MUST already exists !
	// install -o 1000 -g 1000 -m 775 -d /home/mobian/sdcard
	sdcard1: '/home/mobian/sdcard' 
};
const STORAGE_DEFAULT = 'sdcard1';