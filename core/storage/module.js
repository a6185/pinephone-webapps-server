// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Storage extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this.i18n = new PP_I18N(this, '/core/storage/locales/pp_storage.csv');
	}
	async init () {
		try {
			await this.defaults();
			this.set('slots', {
				'storage': new Storage_Slots(this),
			});
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
}
