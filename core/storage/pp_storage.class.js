// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Storage {
	constructor (_Module) {
		this._Module = _Module; // can be null for pinephone.js !
		this.i18n = new PP_I18N(this, '/core/storage/locales/pp_storage.csv');
		this.fs = _PP._Nodes.get('file-system');
		this.path = _PP._Nodes.get('path');
	}
	debug (level, msg) {
		let origin = (this._Module !== null ? this._Module.get('name') : 'MAIN');
		global._Console.debug(level,`>pp_storage(${origin}):: ${msg}`);
	}
	async init () {
		await this.i18n.init();
	}
	filter_media (media) {
		this.debug(3,`filter_media: "${media}"`);
		let cleanmedia = media.toString();
		if (STORAGE_MEDIAS.hasOwnProperty(cleanmedia)) {
			this.debug(3,'Filter media ok !');
			return cleanmedia;
		} else {
			this.debug(3,'Filter media failed !');
			throw this.tr('STORAGE_ERROR_UNKNOWN_MEDIA');
		}
	}
	filter_path (relpath) {
		this.debug(3,`filter_path: "${relpath}"`);
		let cleanpath = relpath.toString().substr(0,255).replace(/[^a-z0-9\.\_\-\/]+/gi,'');
		cleanpath = this.path.normalize(cleanpath);
		if (!cleanpath.match(/^\//)) {
			this.debug(3,`Filter path "${cleanpath}" failed !`);
			throw this.tr('STORAGE_ERROR_PATH_NO_SLASH');
		} else {
			this.debug(3,`Filter path "${cleanpath}" ok !`);
			return cleanpath;
		}
	}
	filter_file (file) {
		this.debug(3,`filter_file: "${file}"`);
		let cleanfile = file.toString().substr(0,255);
		cleanfile = cleanfile.replace(/\.\./g,'').replace(/\/+/g,'').replace(/"+/g,'');
		if (!cleanfile.length) {
			this.debug(3,`Filter file "${cleanfile}" failed !`);
			throw this.tr('MISSING_OR_BAD_FILENAME');
		} else {
			this.debug(3,`Filter file "${cleanfile}" ok !`);
			return cleanfile;
		}		
	}
	stat (abspath) {
		this.debug(3,`stat: "${abspath}"`);
		try {	
			return this.fs.statSync(abspath);
		} catch (err) {
			throw err;
		}
	}
	exists (abspath) {
		this.debug(3,`exists: "${abspath}"`);
		try {
			this.stat(abspath);
			return true;
		} catch (err) {
			return false;
		}
	}
	mkdir (media, relpath) {
		this.debug(3,`mkdir: "${media}", "${relpath}"`);
		try {
			let cleanmedia = this.filter_media(media);
			let root = STORAGE_MEDIAS[cleanmedia];
			let cleanrelpath = this.filter_path(relpath);
			console.log(root + cleanrelpath);
			if (this.exists(root + cleanrelpath)) {
				return true;
			}
			let subpaths = cleanrelpath.split('/');
			for (let i=0;i<subpaths.length;i++) {
				let subpath = subpaths[i];
				if (subpath.length) {
					root += '/' + subpath;
					this.fs.mkdirSync(root);
					this.chown(root,STORAGE_USER,STORAGE_GROUP);
					this.chmod(root,STORAGE_DIR);
				}
			}
			if (this.exists(root)) {
				return true;
			} else {
				throw this.tr('UNKNOWN_ERROR_DISK_FULL');
			}
		} catch (err) {
			this.debug(3,`mkdir failed: ${err}`);
			throw err;
		} 
	}
	async list (media, relpath, opts) {
		this.debug(3,`list: "${media}", "${relpath}", "${opts}"`);
		try {
			let cleanmedia = this.filter_media(media);
			let root = STORAGE_MEDIAS[cleanmedia];
			let cleanrelpath = this.filter_path(relpath);
			let abspath = root + cleanrelpath;
			let files = await this.fs.promises.readdir(abspath);
			this.debug(3,'List ok !');
			// isFile, isDirectory, ...
			let filters = (opts.filters ? opts.filters.split(',') : []);
			// extensions
			let ext = (opts.ext ? opts.ext.split(',') : []);
			// hidden
			let hidden = (opts.hidden ? opts.hidden : false)
			// ctime, atime, mtime, size, ...
			let attrs = (opts.attrs ? opts.attrs.split(',') : []);
			let results = [];
			for (let i = 0; i < files.length; i++) {
				let file = files[i];
				let stat = this.fs.lstatSync(abspath + '/' + file);
				if (filters.length) {
					let ok = (filters.indexOf('isFile')!=-1 && stat.isFile())
					|| (filters.indexOf('isDirectory')!=-1 && stat.isDirectory())
					|| (filters.indexOf('isSymbolicLink')!=-1 && stat.isSymbolicLink())
					|| (filters.indexOf('isFIFO')!=-1 && stat.isFIFO())
					|| (filters.indexOf('isSocket')!=-1 && stat.isSocket())
					|| (filters.indexOf('isCharacterDevice')!=-1 && stat.isCharacterDevice())
					|| (filters.indexOf('isBlockDevice')!=-1 && stat.isBlockDevice());
					if (!ok)
						continue;
				}
				if (ext.length) {
					if (ext.indexOf(file.split('.').pop())==-1)
						continue;
				}
				if (file.match(/^\./) && !hidden) {
					continue;
				}
				//this.debug(file);
				let result = { name: file };
				if (attrs.length) {
					attrs.forEach((attr) => {
						result[attr] = stat[attr];
					});
				}
				results.push(result);
			}
			//console.log(JSON.stringify(results));
			return {
				status: this.tr('STORAGE_LIST_SUCCESSULLY'),
				files: results
			};
		} catch (err) {
			this.debug(3,`list failed: ${err}`);
			throw err;
		}
	}
	async read (media, relpath, file, opts) {
		this.debug(3,`list: "${media}", "${relpath}", "${file}", "${opts}"`);
		try {
			let cleanmedia = this.filter_media(media);
			let root = STORAGE_MEDIAS[cleanmedia];
			let cleanrelpath = this.filter_path(relpath);
			let abspath = root + cleanrelpath;
			let cleanfile = this.filter_file(file);
			let absfilename = abspath + '/' + file;
			let hasKey = global._PP._Fn.hasKey;
			this.debug(3,'Reading file...');
			let exists = await this.exists(absfilename);
			if (!exists) {
				return null;
			}
			await this.fs.promises.access(absfilename, this.fs.constants.R_OK);
			let data = await this.fs.promises.readFile(absfilename, opts);
			this.debug(3,'Read ok !');
			// ctime, atime, mtime, size, ...
			let attrs = [];
			if (hasKey(opts, 'attrs', Array)) {
				attrs = opts.attrs.split(',')
			}
			//let stat = await this.fs.promises.lstat(absfilename);
			let stat = await this.stat(absfilename);
			let result = {
				path: relpath,
				file: file,
				content: data
			};
			if (attrs.length) {
				attrs.forEach((attr) => {
					result[attr] = stat[attr];
				});
			}
			//this.debug(result);
			return {
				status: this.tr('STORAGE_READ_SUCCESSULLY'),
				data: result
			};
		} catch (err) {
			this.debug(3,`read failed: ${err}`);
			throw err;
		}
	}
	write (media, relpath, file, content, opts) {
		this.debug(3,`write: "${media}", "${relpath}", "${file}", "${content.length}", "${opts}"`);
		try {
			// writing MUST be sync to avoid conflicts (example : SMMS sending & receiving to ourselves...)
			let cleanmedia = this.filter_media(media);
			let root = STORAGE_MEDIAS[cleanmedia];
			let cleanrelpath = this.filter_path(relpath);
			let abspath = root + cleanrelpath;
			let cleanfile = this.filter_file(file);
			let absfilename = abspath + '/' + file;
			let tmpfilename = '/tmp/' + file;
			this.debug(3,'Writing file...');
			this.fs.writeFileSync(tmpfilename, content, opts);
			this.debug(3,'Write ok !');
			this.chown(tmpfilename,STORAGE_USER,STORAGE_GROUP);
			this.chmod(tmpfilename,STORAGE_FILE);
			let cp = global._PP._Nodes.get('child_process');
			cp.execSync(`mv "${tmpfilename}" "${absfilename}"`);
			return {
				status: this.tr('STORAGE_WRITE_SUCCESSULLY')
			};
		} catch (err) {
			this.debug(3,`write failed: ${err}`);
			throw err;
		}
	}
	chown (absfilename, user, group) {
		this.debug(3,`chown: "${absfilename}", "${user}", "${group}"`);
		try {
			this.fs.chownSync(absfilename, user, group);
		} catch (err) {
			this.debug(3,`chown failed: ${err}`);
			throw err;
		}
	}
	chmod (absfilename, mode) {
		this.debug(3,`chmod: "${absfilename}", "${mode}"`);
		try {
			this.fs.chmodSync(absfilename, mode);
		} catch (err) {
			this.debug(3,`chmod failed: ${err}`);
			throw err;			
		}
	}
}


