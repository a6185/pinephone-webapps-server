// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Storage_Slots extends PP_Slots {
	constructor (_Module) {		
		super(_Module, 'storage', {
			'pp.core.storage:/mkdir': null,
			'pp.core.storage:/list': null,
			'pp.core.storage:/read': null,
			'pp.core.storage:/write': null
		});
		this._Storage = new PP_Storage(this);
		(async () => {
			await this._Storage.init();
		})();
	}
	async handler (ws, data) {
		try {
			let hasKey = global._PP._Fn.hasKey;
			if (!ws.has_key(data.args, 'media', String)) {
				throw this._Module.tr('MISSING_OR_BAD_MEDIA');
			}
			if (!ws.has_key(data.args, 'path', String)) {
				throw this._Module.tr('MISSING_OR_BAD_PATH');
			}
			let opts = {
				// take care : 'binary' == latin1 ! so if you want real binary datas, use 'null' (empty string) !
				encoding: 'utf-8'
			};
			if (hasKey(data.args, 'opts', Object)) {
				for (let i in data.args.opts) {
					opts[i] = data.args.opts[i];
				}
			}
			let file = '';
			if (hasKey(data.args, 'file', String)) {
				file = data.args.file;
			}
			let content = '';
			if (hasKey(data.args, 'content', String)) {
				content = data.args.content;
			}
			ws._Status.code(0);
			switch (data.route) {
				case 'pp.core.storage:/mkdir':
					this._Storage.mkdir(data.args.media, data.args.path)
					return {};
					break;
				case 'pp.core.storage:/list':
					/*
					route: 'pp.core.storage:/list'
					media: 'sdcard1'
					path: directory
					opts: options
					return: {
						files:files with/without attributes
					}
					*/
					let result = await this._Storage.list(data.args.media, data.args.path, opts);
					ws._Status.info(result.status);
					return {
						files: result.files
					};
					break;
				case 'pp.core.storage:/read':
					/*
					route: 'pp.core.storage:/read'
					media: 'sdcard1'
					path: directory
					file: filename
					opts: options
					return: {
						path:path
						file: filename
						content: data
						(attr: attributes)
					}
					*/
					let result = await this._Storage.read(data.args.media, data.args.path, file, opts);
					ws._Status.info(result.status);
					return {
						data: result.data;
					};
					break;
				case 'pp.core.storage:/write':
					/*
						media: sdcard1
						path: /$MEDIA$ + relative path (MUST begin with /)
						content: data
						file: filename
						opts: options
						return: nothing
					*/
					let result = await this._Storage.write(data.args.media, data.args.path, file, content, opts);
					ws._Status.info(result.status);
					return ;
					break;
			} // end switch
		} catch (err) {
			ws._Status.code(1);
			ws._Status.err(err);
			throw err;
		};
	}
};