// Jean Luc Biellmann - contact@alsatux.com

'use strict';
/* someone could explain why the () => {} syntax not working here ? */

Date.prototype.to_yyyymmdd = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return global._PP._Fn.sprintf('%04d%s%02d%s%02d', this.getFullYear(), spacer, this.getMonth() + 1, spacer, this.getDate())
}

Date.prototype.to_mmddyyyy = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return global._PP._Fn.sprintf('%02d%s%02d%s%04d', this.getMonth() + 1, spacer, this.getDate(), spacer, this.getFullYear())
}

Date.prototype.to_ddmmyyyy = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return global._PP._Fn.sprintf('%02d%s%02d%s%04d', this.getDate(), spacer, this.getMonth() + 1, spacer, this.getFullYear())
}

Date.prototype.to_hhiiss = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return global._PP._Fn.sprintf('%02d%s%02d%s%02d',this.getHours(), spacer, this.getMinutes(), spacer, this.getSeconds());
}

Date.prototype.to_yyyymmddhhiiss = function () {
	return global._PP._Fn.sprintf('%s%s',this.to_yyyymmdd(),this.to_hhiiss());
};

Date.prototype.to_locale = function (pattern) {
	switch (pattern) {
		case 'MM/DD/YYYY':
			return this.to_mmddyyyy('/');
			break;
		case 'DD/MM/YYYY':
			return this.to_ddmmyyyy('/');
			break;
		default: // YYYY-MM-DD
			return this.to_yyyymmdd('-');
			break;
	}
}

Date.prototype.utc2locale = function (locale, timezone) { // example: locale: fr-FR, timezone: Europe/Paris
	return this.toLocaleString(locale, {timeZone: timezone});
}
