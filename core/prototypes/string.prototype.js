// Jean Luc Biellmann - contact@alsatux.com

'use strict';
/* someone could explain why the () => {} syntax not working here ? */

String.prototype.to_internal_date = function () {
	let m, d, dd, dm, dy;
	try {
		let _Modules = global._PP._Core._Modules;
		let _Prefs = _Modules.get('com.alsatux.prefs');
		let _Pref_Object = _Prefs.tbl.prefs.find_by_cat_key('default','date_pattern');
		switch (_Pref_Object.obj.cur) {
			case 'YYYY-MM-DD':
				m = this.match(/^(\d\d\d\d)-(\d\d)-(\d\d)$/);
				dy = m[1].to_uint();
				dm = m[2].to_uint()-1;
				dd = m[3].to_uint();
			break;
			case 'MM/DD/YYYY':
				m = this.match(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/);
				dy = m[3].to_uint();
				dm = m[1].to_uint()-1;
				dd = m[2].to_uint();
			break;
			case 'DD/MM/YYYY':
				m = this.match(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/);
				dy = m[3].to_uint();
				dm = m[2].to_uint()-1;
				dd = m[1].to_uint();
			break;
		}
		d = new Date(dy, dm, dd, 0, 0, 0, 0.);
		return d.to_yyyymmdd('-');
	} catch (err) {
		throw 'String::to_internal_date() failed !';
	}
}

String.prototype.to_date_format = function (format) {
	let m, d, dd, dm, dy;
	try {
		switch (format) {
			case 'YYYY-MM-DD':
				m = this.match(/^(\d\d\d\d)-(\d\d)-(\d\d)$/);
				dy = m[1].to_uint();
				dm = m[2].to_uint()-1;
				dd = m[3].to_uint();
			break;
			case 'MM/DD/YYYY':
				m = this.match(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/);
				dy = m[3].to_uint();
				dm = m[1].to_uint()-1;
				dd = m[2].to_uint();
			break;
			case 'DD/MM/YYYY':
				m = this.match(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/);
				dy = m[3].to_uint();
				dm = m[2].to_uint()-1;
				dd = m[1].to_uint();
			break;
			default:
				return null;
		}		
		d = new Date(dy, dm, dd, 0, 0, 0, 0.);
		if (d.getFullYear()==dy && d.getMonth()==dm && d.getDate()==dd) {
			return this;
		}
		return null;
	} catch (err) {
		return null;
	}
}

String.prototype.to_int = function () {
	return parseInt(this,10);
}

String.prototype.to_uint = function () {
	return Math.abs(this.to_int());
}

String.prototype.modulo = function (modulo) {
	return this.to_uint()%modulo.to_uint();
}

String.prototype.round = function (dec) {
	return Math.round((Math.pow(10,dec)*parseFloat(this)))/Math.pow(10,dec);
}

