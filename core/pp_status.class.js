/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class PP_Status {
	constructor () {
		this.data = {
			messages: [],
			code: 0
		}
	}
	reset () {
		this.data = {
			messages: [],
			code: 0
		}
	}
	code (code) {
		this.data.code = code;
	}
	add (type, txt) {
		this.data.messages.push({
			type: type,
			txt: txt
		});
	}
	err (txt) {
		this.add('err',txt);
		return false;
	}
	info (txt) {
		this.add('info',txt);
		return true;
	}
	warn (txt) {
		this.add('warn',txt);
		return true;
	}
}
