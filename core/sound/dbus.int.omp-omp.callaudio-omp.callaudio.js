/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Int__OMP__OMP_CallAudio__OMP_CallAudio extends PP_DBusSessionInterface {	
	constructor (_Ctrl) {
		super(_Ctrl, {
			destination: 'org.mobian_project.CallAudio',
			path: '/org/mobian_project/CallAudio',
			'interface': 'org.mobian_project.CallAudio'
		});
	}

	/* METHODS */

	enable_speaker (enable) {
		debugger;
		return this._send('EnableSpeaker', {
			signature: 'b',
			body: [
				enable
			]
		});
	}
	mute_mic (mute) {
		debugger;
		return this._send('MuteMic', {
			signature: 'b',
			body: [
				mute
			]
		});		
	}
	select_mode (mode) {
		// SelectMode:
		// @mode: 0 = default audio mode, 1 = voice call mode
		// @success: operation status
		// Sets the audio routing configuration according to the @mode parameter.
		// If @mode isn't an authorized value, #org.freedesktop.DBus.Error.InvalidArgs error is returned.
		debugger;
		return this._send('SelectMode', {
			signature: 'u',
			body: [
				mode
			]
		});				
	}

	/* NO SIGNALS */

	/* PROPERTIES

	AudioMode readable u
	0 = default audio mode, 1 = voice call mode, 255 = unknown

	MicState readable u
	0 = off, 1 = on, 255 = unknown
	all other values should be considered the same as 'unknown'

	SpeakerState readable u
	0 = off, 1 = on, 255 = unknown
	all other values should be considered the same as 'unknown'

	*/
}