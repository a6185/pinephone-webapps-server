/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class Sound_Manager extends PP_Class {	
	constructor (_Module) {
		super(_Module,'sound_manager');
		this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio = new DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio(_Module, this);
		this._Shell = new PP_Shell(this);
	}
	async init () {		
		try {
			await this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio.init();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
		}
	}
	async refresh () {
		// no onchanged() signal, we need to refresh properties ourselves and 
		// advertise all clients...
		let _Status = new PP_Status();
		_PP._WS.send_multicast({
			args: {
				properties: await this.get_properties()
			},
			callback: 'pp.core.sound:/callaudio/refresh'
		},_Status);
	}
	async get_properties () {
		this.debug(3,'Get properties');
		await this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio.init();
		return this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio.get('properties');
	}
	async set_enable_speaker (bool) {
		this.debug(3,'Set speaker ' + (bool ? 'on' : 'off'));		
		let res = await this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio._DBus.enable_speaker(bool);
		await this.refresh();
		return res;
		//this._Shell.exec(this._Shell.pkexec + 'dbus-send --dest=org.mobian_project.CallAudio --type=method_call /org/mobian_project/CallAudio org.mobian_project.CallAudio.EnableSpeaker boolean:' + (bool ? 'true' : 'false'));
	}
	async set_mute_mic (bool) {
		this.debug(3,'Mute mic ' + (bool ? 'on' : 'off'));
		let res = await this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio._DBus.mute_mic(bool);
		await this.refresh();
		return res;
		//this._Shell.exec(this._Shell.pkexec + 'dbus-send --dest=org.mobian_project.CallAudio --type=method_call /org/mobian_project/CallAudio org.mobian_project.CallAudio.MuteMic boolean:' + (bool ? 'true' : 'false'));
	}
	async set_call_audio_mode (uint) { // from phone module only !
		// @mode: 0 = default audio mode, 1 = voice call mode
		this.debug(3,'Call audio mode ' + (uint ? 'on' : 'off'));
		let res = await this._DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio._DBus.select_mode(uint);
		await this.refresh();
		return res;
		//this._Shell.exec(this._Shell.pkexec + 'dbus-send --dest=org.mobian_project.CallAudio --type=method_call /org/mobian_project/CallAudio org.mobian_project.CallAudio.SelectMode uint32:' + (uint ? '1' : '0'));
	}
}

