// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Sound_Slots extends PP_Slots {
	constructor (_Module, _Tbl) {	
		super(_Module, 'sound_slots', {
			'pp.core.sound:/callaudio/get': 'pp.core.sound:/callaudio/refresh',
			'pp.core.sound:/callaudio/set/speaker': null,
			'pp.core.sound:/callaudio/set/mute': null
		});
	}
	async handler (ws, data) {		
		try {
			let _Sound_Manager = this._Module._Sound_Manager;
			//let hasKey = global._PP._Fn.hasKey
					debugger;
			switch (data.route) {
				case 'pp.core.sound:/callaudio/get':
					let properties = await _Sound_Manager.get_properties();
					return {
						properties: properties
					};
					break;
				case 'pp.core.sound:/callaudio/set/speaker':
					if (!ws.has_key(data.args, 'value', String)) {
						throw 'Speaker value 0/1 missing';
					}
					return await _Sound_Manager.set_enable_speaker(data.args.value.toString()=='1');
					break;
				case 'pp.core.sound:/callaudio/set/mute':
					if (!ws.has_key(data.args, 'value', String)) {
						throw 'Mute value 0/1 missing';
					}
					return await _Sound_Manager.set_mute_mic(data.args.value.toString()=='1');
					break;
			} // end switch
		} catch (err) {
			ws._Status.code(1);
			throw err;			
		}
	}
}