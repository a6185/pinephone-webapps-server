//includes();
global.register_module({
	manifest: {
		version: '2.0',
		title: 'Sound',
		name: 'sound', // internal lowercased - no space nor special chars
		namespace: 'pp.core.sound', // internal
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/core/sound',
		storage: {
			media: STORAGE_DEFAULT, // cf. core/storage/config.js
			relpath: '/sound'
		},
		dict: {}
	},
	files: [
		'core/sound/dbus.ctrl.omp-omp.callaudio-omp.callaudio',
		'core/sound/dbus.int.omp-omp.callaudio-omp.callaudio',
		'core/sound/sound.manager',
		'core/sound/sound.slots',
		'core/sound/module'
	],
	main: 'Sound'
});