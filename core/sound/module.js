// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Sound extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this._Sound_Manager = new Sound_Manager(this);
	}
	async init () {
		try {			
			this.slots = {
				'sound': new Sound_Slots(this, null)
			}
			await this._Sound_Manager.init();
			return this; // mandatory !
		} catch (err) {
			debug(1,`Unable to init() sound module ???`);
			throw err;
		}
	}
}
