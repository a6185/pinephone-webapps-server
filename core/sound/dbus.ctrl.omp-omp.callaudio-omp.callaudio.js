/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__OMP__OMP_CallAudio__OMP_CallAudio extends PP_DBusController {
	constructor (_Module, _Manager) {
		super(_Module, _Manager, 'dbus_ctrl__omp__omp_callaudio__omp_callaudio', true, false, true);
		this._DBus = new DBus_Int__OMP__OMP_CallAudio__OMP_CallAudio(this);
	}
	/*
	async init () {		
		try {
			this.set('properties',{});
			let res = await this._DBus._getAll();
			if (!res.length) {
				throw 'No properties found !';
			}
			this.set('properties',res[0]);		
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			debugger;
		}
	}
	getAll () {
		return this.get('properties');
	}
	async slots (signal, ...args) {
		try {
			let res = await this._DBus._getAll();
			this.set('properties',res);		
			await this._Manager.on_dbus_notifications(this, signal, ...args);
		} catch (err) {
			this.debug(1,err);
		}
	}*/
}