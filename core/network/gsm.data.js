/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class GSMData {
	constructor (_Module) {
		this._Module = _Module;
		let _Modules = global._PP._Core._Modules;
		this._Modem = _Modules.get('pp.core.modem');
		this._Network = _Modules.get('pp.core.network');
	}
	get_dbus_paths_of_gsm_connections () {
		let dbus_paths = [];
		for (let dbus_path1 in this._Network.settings_connection) {
			/*
			[
				'/org/freedesktop/NetworkManager/Settings/1',
				'/org/freedesktop/NetworkManager/Settings/2',
				...
			]
			*/
			let _Ctrl = this._Network.settings_connection[dbus_path1];
			let properties = _Ctrl.get('properties');
			if (properties.connection.type == 'gsm') {
				/*
				{
				'connection': {
					'autoconnect': False,
					'id': 'Free-Mobile',
					'permissions': [],
					'timestamp': 1654607762,
					'type': 'gsm',
					'uuid': 'f0ff78c5-2a77-4ab7-ac9a-56ed80cb9ffd'
				},
				...
				*/
				dbus_paths.push(dbus_path1);
			}
		}
		return dbus_paths;
	}
	get_dbus_paths_of_active_connections () {
		let dbus_paths = [];
		for (let dbus_path1 in this._Network.active_connection) {
			/*
			[
				'/org/freedesktop/NetworkManager/ActiveConnection/1,
				...
			]
			*/
			let _Ctrl = this._Network.active_connection[dbus_path1];
			let properties = _Ctrl.get('properties');
			dbus_paths.push(properties.Connection); // '/org/freedesktop/NetworkManager/Settings/X'
		}
		return dbus_paths;		
	}
	async on () {
		try {
			let dbus_paths_of_gsm_connections = this.get_dbus_paths_of_gsm_connections();
			if (!dbus_paths_of_gsm_connections.length) {
				throw 'No GSM connections available !';
			}
			let dbus_paths_of_active_connections = this.get_dbus_paths_of_active_connections();
			dbus_paths_of_gsm_connections.forEach(dbus_path1 => {
				if (dbus_paths_of_active_connections.indexOf(dbus_path1)!=-1) {
					this.debug(3,`GSM connection already on`);
					return;
				}
			});
			/* Connection is off */
			let dbus_path = dbus_paths_of_gsm_connections[0];
			let _Ctrl = this._Network.settings_connection[dbus_path];
			let device = _Ctrl.get('properties').Devices[0];
			await this._Network._NetworkManager._DBus_Ctrl__NM__NM__NM._Dbus.activate_connection(
				dbus_path,
				device,
				'/' // Object Path specific object ('/'' if none)
			);
			this.debug(3,`GSM connection turned on`);
		} catch (err) {
			this.debug(3,`Unable to turn on GSM - EXCEPTION: ${err}`);
			throw err;
		}
	}
	get_current_connection () {
		let dbus_paths_of_gsm_connections = this.get_dbus_paths_of_gsm_connections();
		if (!dbus_paths_of_gsm_connections.length) {
			throw 'No GSM connections available !';
		}
		let dbus_paths_of_active_connections = this.get_dbus_paths_of_active_connections();
		dbus_paths_of_gsm_connections.forEach(dbus_path1 => {					
			if (dbus_paths_of_active_connections.indexOf(dbus_path1)!=-1) {
				return dbus_path1; // connection is already on...
			}
		});
		throw 'No GSM active connection !';
	}
	async off () {
		try {							
			let dbus_path = this.get_current_connection();
			await this._Network._NetworkManager._DBus_Ctrl__NM__NM__NM._DBus.deactivate_connection(dbus_path);
		} catch (err) {
			this.debug(3,`Unable to turn off GSM connection - EXCEPTION: ${err}`);
		}
	}
}
