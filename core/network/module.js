// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Network extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this._NetworkManager = new NetworkManager(this);
		this.permissions = {};
		// Shortcuts to controlers:
		this.device = {};
		this.settings_connection = {};
		this.active_connection = {};
	}
	async init () {
		try {
			await this._NetworkManager.init();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
}

