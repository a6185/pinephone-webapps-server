//includes();
global.register_module({
	manifest: {
		version: '1.0',
		title: 'Network',
		name: 'network', // internal lowercased - no space nor special chars
		namespace: 'pp.core.network',
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/core/network',
		storage: {
			media: null,
			relpath: null
		}
	},
	files: [
		'core/network/enum',
		'core/network/gsm.data',
		'core/network/module',
		'core/network/network.manager',
	],
	main: 'Network'
});