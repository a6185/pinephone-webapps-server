/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class NetworkManager extends PP_Class {	
	constructor (_Module) {
		super(_Module,'network_manager');
		this._DBus_Ctrl__NM__NM__NM = new DBus_Ctrl__NM__NM__NM(_Module,this);
		this._DBus_Ctrl__NM__NM_Settings__NM_Settings = new DBus_Ctrl__NM__NM_Settings__NM_Settings(_Module,this);
	}
	async init () {		
		try {
			let dbus_paths, dbus_path, i;
			await this._DBus_Ctrl__NM__NM__NM.init();
			await this._DBus_Ctrl__NM__NM_Settings__NM_Settings.init();
			this._Module.permissions = this._DBus_Ctrl__NM__NM__NM._DBus.get_permissions();
			let properties1 = this._DBus_Ctrl__NM__NM__NM.get('properties');
			let properties2 = this._DBus_Ctrl__NM__NM_Settings__NM_Settings.get('properties');
			/*
			[
					'/org/freedesktop/NetworkManager/Devices/1',
					...
			]
			*/
			dbus_paths = properties1.Devices;
			for (i=0;i<dbus_paths.length;i++) {
				dbus_path = dbus_paths[i];
				this._Module.device[dbus_path] = new DBus_Ctrl__NM__NM_Devices__NM_Device(this._Module, this, dbus_path);
				await this._Module.device[dbus_path].init();
			}
			/*
			[
					'/org/freedesktop/NetworkManager/ActiveConnection/1',
					...
			]
			*/
			dbus_paths = properties1.ActiveConnections;
			for (i=0;i<dbus_paths.length;i++) {
				dbus_path = dbus_paths[i];
				this._Module.active_connection[dbus_path] = new DBus_Ctrl__NM__NM_ActiveConnection__NM_Connection_Active(this._Module, this, dbus_path);
				await this._Module.active_connection[dbus_path].init();
			}
			/*
			[
				'/org/freedesktop/NetworkManager/Settings/1',
				...
			]
			*/
			dbus_paths = properties2.Connections;
			for (i=0;i<dbus_paths.length;i++) {
				dbus_path = dbus_paths[i];
				this._Module.settings_connection[dbus_path] = new DBus_Ctrl__NM__NM_Settings__NM_Settings_Connection(this._Module, this, dbus_path);
				await this._Module.settings_connection[dbus_path].init();
			};
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}	
	/* DBUS SIGNALS */
	async on_dbus_notifications (_Ctrl, signal, ...args) {
		try {
			let _Status = new PP_Status();
			let dbus_path;
			if (_Ctrl == this._DBus_Ctrl__NM__NM__NM) {
				switch (signal) {
					case 'DeviceAdded':
						dbus_path = args[0];
						this._Module.device[dbus_path] = new DBus_Ctrl__NM__NM_Devices__NM_Device(this._Module, this, dbus_path);
						break;
					case 'DeviceRemoved':
						dbus_path = args[0];
						delete _Module.device[dbus_path]; 
						break;
					case 'StateChanged':
						// StateChanged (u state);
						// NetworkManager's state changed.
						// u state: (NMState) The new state of NetworkManager.
						break;
					case 'CheckPermissions':
						// CheckPermissions ();
						// Emitted when system authorization details change, indicating that clients may wish to recheck permissions with GetPermissions. 
						this._Module.permissions = this._DBus_Ctrl__NM__NM__NM._DBus.get_permissions();
						break;
				}
			}
			if (_Ctrl == this._DBus_Ctrl__NM__NM_Settings__NM_Settings) {
				switch (signal) {
					case 'NewConnection':
						// NewConnection (o connection);
						// Emitted when a new connection has been added after NetworkManager has started up and initialized. This signal is not emitted for connections read while starting up, because NetworkManager's D-Bus service is only available after all connections have been read, and to prevent spamming listeners with too many signals at one time. To retrieve the initial connection list, call the ListConnections() method once, and then listen for individual Settings.NewConnection and Settings.Connection.Deleted signals for further updates.
						// o connection: Object path of the new connection.
						dbus_path = args[0];
						this._Module.settings_connection[dbus_path] = new DBus_Ctrl__NM__NM_Settings__NM_Settings_Connection(this._Module, this, dbus_path);
						break;
					case 'ConnectionRemoved':
						// ConnectionRemoved (o connection);
						// Emitted when a connection is no longer available. This happens when the connection is deleted or if it is no longer accessible by any of the system's logged-in users. After receipt of this signal, the connection no longer exists and cannot be used. Also see the Settings.Connection.Removed signal.
						// o connection: Object path of the removed connection.
						dbus_path = args[0];
						delete this._Module.settings_connection[dbus_path];
						break;
				}
			}
		} catch (err) {
			this.debug(1,`Handling notification [${signal}] failed !`);
		}
	}
}

