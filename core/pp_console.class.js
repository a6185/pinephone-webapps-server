// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Console {
	constructor () {
	}
	date () {
		let d = new Date();
		return [d.getHours(), d.getMinutes(), d.getSeconds()].map(p => (p<9 ? '0' : '') + p).join(':');
	}
	debug (level,msg) {
		if (level<=DEBUG) {
			if (msg.constructor === Object || msg.constructor === Array) {
				this.inspect(msg);
			} else {
				console.log(this.date() + ' ' + msg);
			}
		}
	}
	inspect (obj) {
		console.dir(obj, { depth: null });
	}
}

