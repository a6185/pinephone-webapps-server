/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DisplayConfig extends PP_Class {	
	constructor (_Module) {
		super(_Module,'display_config');
		this._DBus_Ctrl__Org__Gnome__Mutter__DisplayConfig = new DBus_Ctrl__Org__Gnome__Mutter__DisplayConfig(this,this);
		this._PP_DBusSession = new PP_DBusSession(this, {
			destination: 'org.gnome.Mutter.DisplayConfig',
			path: '/org/gnome/Mutter/DisplayConfig'
		});
		this._PP_DBusSession._on_properties_changed(this.on_properties_changed.bind(this));
		this.callbacks = {};
	}
	async init () {
		try {
			await this._DBus_Ctrl__Org__Gnome__Mutter__DisplayConfig.init();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async on_properties_changed (interface_name, changed_properties, invalidated_properties) {
		this.debug(3, `Notification > PropertiesChanged on ${interface_name}: `)
		if (DEBUG==3) {
			global._Console.inspect(changed_properties);
		}
		switch (interface_name) {
			case 'org.freedesktop.DBus.ObjectManager':
				this._DBus_Ctrl__Org__Gnome__Mutter__DisplayConfig.refresh();
				break;
		}
	}
}