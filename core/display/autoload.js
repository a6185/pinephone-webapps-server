//includes();
global.register_module({
	manifest: {
		version: '1.0',
		title: 'Display',
		name: 'display', // internal lowercased - no space nor special chars
		namespace: 'pp.core.display',
		author: 'Jean Luc BIELLMANN',
		mail: 'contact@alsatux.com',
		url: 'https://alsatux.com',
		year: '2021',
		relpath: '/core/display',
		storage: {
			media: null,
			relpath: null
		}
	},
	files: [
		'core/display/display.config',
		'core/display/module'
	],
	main: 'Display'
});