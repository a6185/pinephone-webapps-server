// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class Display extends PP_Module {
	constructor (manifest) {
		super(manifest);
		this._DisplayConfig = new DisplayConfig(this);
	}
	async init () {
		try {
			await this._DisplayConfig.init();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init Display failed: ${err}`);
			throw err;
		}
	}
}



