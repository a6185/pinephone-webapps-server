// Jean Luc Biellmann - contact@alsatux.com

'use strict';

global.dbuscounter = 0;

class PP_DBus {
	constructor (_Module, obj, dbus_type) {
			/*
		obj = {
			destination: org.freedesktop.ModemManager1,
			path: /org/freedesktop/ModemManager1
		}
		*/
		this._Module = _Module;
		this.obj = obj;
		this.dbus_type = dbus_type;
		this.dbus_next = _PP._Nodes.get('dbus-next');
		this.dbus2 = this.dbus_next.systemBus();			
	}
	_debug (level, msg) {
		global._Console.debug(level,`>pp_dbus/${this.dbus_type}:: ${msg}`);
	}
	_prepare (...args) {
		let member = args[0]; // method to call
		let override_params = args.length>1 ? args[1] : {};
		let default_params = {
			member: member,
			signature: '',
			body: []
		}		
		let message = {...this.obj, ...default_params, ...override_params};
		return message;
	}
	_variant (...args) {
		const Variant = this.dbus_next.Variant;
		return new Variant(...args);
	}
	_removeVariants (datas) {
		let Variant = this.dbus_next.Variant;
		let i,s,v;
		if (datas === null || datas === undefined) {
			return datas;
		}
		if (datas.constructor === Array) {
			for (i=0;i<datas.length;i++) {
				datas[i] = this._removeVariants(datas[i]);
			}
		}
		if (datas.constructor === Object) {
			for (i in datas) {
				datas[i] = this._removeVariants(datas[i]);
			}
		}
		if (datas.constructor === Variant) {
			s = datas.signature;
			v = datas.value;
			datas = this._removeVariants(v);
		}
		return datas;
	}
	_dbus_session_mobian_proxy (message) {
		return new Promise((resolve,reject) => {
			var ws = _PP._Nodes.get('ws');
			const wsc = new ws('ws://127.0.0.1:2021');
			wsc.on('error', function (err) {
				debugger;
				reject(err);
			});
			wsc.on('open', function () {
				// send message !
				wsc.send(JSON.stringify(message));
				setTimeout(() => {
					reject('Timeout expired !');
				},10000);
			});
			wsc.on('close', function () {
				reject('WebSocket connection closed');
			});
			wsc.on('message', function (message) {
				let res = JSON.parse(message);
				if (res.code == 0) {
					resolve(res.data);
				} else {
					reject(res.err)
				}
			});
		});
	}
	async _send (...args) {
		this._debug(3,`Prepare message...`);
		try {
			let message = this._prepare(...args);
			this._debug(3,`Calling DBus${this.dbus_type} with:`);
			if (DEBUG==3) {
				global._Console.inspect(message);
			}
			let res = null;
			if (this.dbus_type == 'Session') {
				// we can't use dbus-next here as root uid 0
				// because it needs process.seteuid(1000) to work...
				res = await this._dbus_session_mobian_proxy(message);
			} else {
				let m = new this.dbus_next.Message(message);
				dbuscounter++;
				this._debug(3,`dbuscounter = ${dbuscounter}`);
				res = await this.dbus2.call(m);
			}
			if (DEBUG==3 && 0) {
				global._Console.inspect(res);
			}
			//this._debug(3, res);
			this._debug(3,`Removing variants...`);
			res.body = this._removeVariants(res.body);
			if (DEBUG==3 && 0) {
				global._Console.inspect(res);
			}
			//this._debug(3, res);
			return res.body;
		} catch (err) {
			this._debug(3, err);
			if (DEBUG==3) {
				global._Console.inspect(err);
			}
			// push error to controller
			debugger;
			throw err;
		}
	}
	async _on_properties_changed (callback) { // for managers only !
		try {
			if (this.dbus_type == 'System') {
				this._debug(3,`add 'PropertiesChanged' listener for [
		destination: ${this.obj.destination}
		path: ${this.obj.path}]...`);
				let obj = await this.dbus2.getProxyObject(this.obj.destination, this.obj.path);
				let int = obj.getInterface('org.freedesktop.DBus.Properties');
				var _this = this;
				int.on('PropertiesChanged', (interface_name, changed_properties, invalidated_properties) => {
					// changed_properties: dictionary containing the changed properties with the new values
					// invalidated_properties: array of properties that changed but the value is not conveyed
					_this._debug(3,`Notification > PropertiesChanged: ${interface_name}`);
					callback(interface_name, this._removeVariants(changed_properties), this._removeVariants(invalidated_properties));
				});
			}
		} catch (err) {
			this._debug(3,`Binding interface notifications failed ! ${err}`);
			debugger;
		}
	}
}

class PP_DBusSystem extends PP_DBus {
	constructor (_Module, obj) {
		super(_Module, obj,'System');
	}
}

class PP_DBusSession extends PP_DBus {
	constructor (_Module, obj) {
		super(_Module, obj,'Session');
	}
}

