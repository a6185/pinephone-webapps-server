/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager.Device — Device

class DBus_Int__NM__NM_Devices__NM_Device extends PP_DBusSystemInterface {	
	constructor (_Ctrl, dbus_path) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: dbus_path, // '/org/freedesktop/NetworkManager/Devices/' + number
			'interface': 'org.freedesktop.NetworkManager.Device'
		});
	}

	/* NO METHODS */

	reapply (connection, version_id, flags) {
		// Reapply (IN  a{sa{sv}} connection, IN  t version_id, IN  u flags);
		// Attempts to update the configuration of a device without deactivating it. NetworkManager has the concept of connections, which are profiles that contain the configuration for a networking device. Those connections are exposed via D-Bus as individual objects that can be created, modified and deleted. When activating such a settings-connection on a device, the settings-connection is cloned to become an applied-connection and used to configure the device (see GetAppliedConnection). Subsequent modification of the settings-connection don't propagate automatically to the device's applied-connection (with exception of the firewall-zone and the metered property). For the changes to take effect, you can either re-activate the settings-connection, or call Reapply. The Reapply call allows you to directly update the applied-connection and reconfigure the device. Reapply can also be useful if the currently applied-connection is equal to the connection that is about to be reapplied. This allows to reconfigure the device and revert external changes like removing or adding an IP address (which NetworkManager doesn't revert automatically because it is assumed that the user made these changes intentionally outside of NetworkManager). Reapply can make the applied-connection different from the settings-connection, just like updating the settings-connection can make them different.
		// IN a{sa{sv}} connection: The optional connection settings that will be reapplied on the device. If empty, the currently active settings-connection will be used. The connection cannot arbitrarily differ from the current applied-connection otherwise the call will fail. Only certain changes are supported, like adding or removing IP addresses.
		// IN t version_id: If non-zero, the current version id of the applied-connection must match. The current version id can be retrieved via GetAppliedConnection. This optional argument allows to catch concurrent modifications between the GetAppliedConnection call and Reapply.
		// IN u flags: Flags which would modify the behavior of the Reapply call. There are no flags defined currently and the users should use the value of 0.
		return this._send('Reapply', {
			signature: 'a{sa{sv}}tu',
			body: [
				connection, version_id, flags
			]
		});
	}
	get_applied_connection (flags, connection, version_id) {
		// GetAppliedConnection (IN  u         flags, OUT a{sa{sv}} connection, OUT t         version_id);
		// Get the currently applied connection on the device. This is a snapshot of the last activated connection on the device, that is the configuration that is currently applied on the device. Usually this is the same as GetSettings of the referenced settings connection. However, it can differ if the settings connection was subsequently modified or the applied connection was modified by Reapply. The applied connection is set when activating a device or when calling Reapply.
		// IN u flags: Flags which would modify the behavior of the GetAppliedConnection call. There are no flags defined currently and the users should use the value of 0.
		// OUT a{sa{sv}} connection: The effective connection settings that the connection has currently applied.
		// OUT t version_id: The version-id of the currently applied connection. This can be specified during Reapply to avoid races where you first fetch the applied connection, modify it and try to reapply it. If the applied connection is modified in the meantime, the version_id gets incremented and Reapply will fail.
		return this._send('GetAppliedConnection', {
			signature: 'ua{sa{sv}}t',
			body: [
				flags, connection, version_id
			]
		});		
	}
	disconnect () {
		// Disconnect ();
		// Disconnects a device and prevents the device from automatically activating further connections without user intervention. 
		return this._send('Disconnect');
	}
	delete () {
		// Delete ();
		// Deletes a software device from NetworkManager and removes the interface from the system. The method returns an error when called for a hardware device. 
		return this._send('Delete');
	}

	/* SIGNALS */ 

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'StateChanged': (new_state, old_state, reason) => {
				_this._debug(3,`Notification > StateChanged - new_state=${new_state} old_state=${old_state} reason=${reason}`);
				// StateChanged (u new_state, u old_state, u reason);
				// u new_state: (NMDeviceState) The new state of the device.
				// u old_state: (NMDeviceState) The previous state of the device.
				// u reason: (NMDeviceStateReason) A reason for the state transition.
				_this._Ctrl.slots('StateChanged',null);
			}
		});
	}		

	/* PROPERTIES

	Udi  readable   s
	Operating-system specific transient device hardware identifier. This is an opaque string representing the underlying hardware for the device, and shouldn't be used to keep track of individual devices. For some device types (Bluetooth, Modems) it is an identifier used by the hardware service (ie bluez or ModemManager) to refer to that device, and client programs use it get additional information from those services which NM does not provide. The Udi is not guaranteed to be consistent across reboots or hotplugs of the hardware. If you're looking for a way to uniquely track each device in your application, use the object path. If you're looking for a way to track a specific piece of hardware across reboot or hotplug, use a MAC address or USB serial number.
	Note that non-UTF-8 characters are backslash escaped. Use g_strcompress() to obtain the true (non-UTF-8) string.

	Path  readable   s
	The path of the device as exposed by the udev property ID_PATH.
	Note that non-UTF-8 characters are backslash escaped. Use g_strcompress() to obtain the true (non-UTF-8) string.

	Interface  readable   s
	The name of the device's control (and often data) interface. Note that non UTF-8 characters are backslash escaped, so the resulting name may be longer then 15 characters. Use g_strcompress() to revert the escaping.

	IpInterface  readable   s
	The name of the device's data interface when available. This property may not refer to the actual data interface until the device has successfully established a data connection, indicated by the device's State becoming ACTIVATED. Note that non UTF-8 characters are backslash escaped, so the resulting name may be longer then 15 characters. Use g_strcompress() to revert the escaping.

	Driver  readable   s
	The driver handling the device. Non-UTF-8 sequences are backslash escaped. Use g_strcompress() to revert.

	DriverVersion  readable   s
	The version of the driver handling the device. Non-UTF-8 sequences are backslash escaped. Use g_strcompress() to revert.

	FirmwareVersion  readable   s
	The firmware version for the device. Non-UTF-8 sequences are backslash escaped. Use g_strcompress() to revert.

	Capabilities  readable   u
	Flags describing the capabilities of the device.
	Returns: NMDeviceCapabilities

	Ip4Address  readable   u
	DEPRECATED; use the 'Addresses' property of the 'Ip4Config' object instead. This property always returns 0.0.0.0 (numeric 0) as address.

	State  readable   u
	The current state of the device.
	Returns: NMDeviceState

	StateReason  readable   (uu)
	The current state and reason for changing to that state.
	Returns: A tuple of NMDeviceState and NMDeviceStateReason.

	ActiveConnection  readable   o
	Object path of an ActiveConnection object that "owns" this device during activation. The ActiveConnection object tracks the life-cycle of a connection to a specific network and implements the org.freedesktop.NetworkManager.Connection.Active D-Bus interface.

	Ip4Config  readable   o
	Object path of the Ip4Config object describing the configuration of the device. Only valid when the device is in the NM_DEVICE_STATE_ACTIVATED state.

	Dhcp4Config  readable   o
	Object path of the Dhcp4Config object describing the DHCP options returned by the DHCP server. Only valid when the device is in the NM_DEVICE_STATE_ACTIVATED state.

	Ip6Config  readable   o
	Object path of the Ip6Config object describing the configuration of the device. Only valid when the device is in the NM_DEVICE_STATE_ACTIVATED state.

	Dhcp6Config  readable   o
	Object path of the Dhcp6Config object describing the DHCP options returned by the DHCP server. Only valid when the device is in the NM_DEVICE_STATE_ACTIVATED state.

	Managed  readwrite  b
	Whether or not this device is managed by NetworkManager. Setting this property has a similar effect to configuring the device as unmanaged via the keyfile.unmanaged-devices setting in NetworkManager.conf. Changes to this value are not persistent and lost after NetworkManager restart.

	Autoconnect  readwrite  b
	If TRUE, indicates the device is allowed to autoconnect. If FALSE, manual intervention is required before the device will automatically connect to a known network, such as activating a connection using the device, or setting this property to TRUE. This property cannot be set to TRUE for default-unmanaged devices, since they never autoconnect.

	FirmwareMissing  readable   b
	If TRUE, indicates the device is likely missing firmware necessary for its operation.
	The "NmPluginMissing" property

	NmPluginMissing  readable   b
	If TRUE, indicates the NetworkManager plugin for the device is likely missing or misconfigured.

	DeviceType  readable   u
	The general type of the network device; ie Ethernet, Wi-Fi, etc.
	Returns: NMDeviceType

	AvailableConnections  readable   ao
	An array of object paths of every configured connection that is currently 'available' through this device.

	PhysicalPortId  readable   s
	If non-empty, an (opaque) indicator of the physical network port associated with the device. This can be used to recognize when two seemingly-separate hardware devices are actually just different virtual interfaces to the same physical port.

	Mtu  readable   u
	The device MTU (maximum transmission unit).

	Metered  readable   u
	Whether the amount of traffic flowing through the device is subject to limitations, for example set by service providers.
	Returns: NMMetered

	LldpNeighbors  readable   aa{sv}
	Array of LLDP neighbors; each element is a dictionary mapping LLDP TLV names to variant boxed values.

	Real  readable   b
	True if the device exists, or False for placeholder devices that do not yet exist but could be automatically created by NetworkManager if one of their AvailableConnections was activated.

	Ip4Connectivity  readable   u
	The result of the last IPv4 connectivity check.
	Returns: NMConnectivityState

	Ip6Connectivity  readable   u
	The result of the last IPv6 connectivity check.
	Returns: NMConnectivityState

	InterfaceFlags  readable   u
	The flags of the network interface. See NMDeviceInterfaceFlags for the currently defined flags.

	HwAddress  readable   s
	The hardware address of the device.
	This replaces the other 'HwAddress' properties on the device-specific D-Bus interfaces.
	*/
}
