/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager — Connection Manager

class DBus_Int__NM__NM__NM extends PP_DBusSystemInterface {	
	constructor (_Ctrl) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: '/org/freedesktop/NetworkManager',
			'interface': 'org.freedesktop.NetworkManager'
		});
	}
	
	/* METHODS */
	
	reload (flags) {
		// Reload (IN  u flags);
		// Reload NetworkManager's configuration and perform certain updates, like flushing a cache or rewriting external state to disk. This is similar to sending SIGHUP to NetworkManager but it allows for more fine-grained control over what to reload (see flags). It also allows non-root access via PolicyKit and contrary to signals it is synchronous.
		// No flags (0x00) means to reload everything that is supported which is identical to sending a SIGHUP. (0x01) means to reload the NetworkManager.conf configuration from disk. Note that this does not include connections, which can be reloaded via Setting's ReloadConnections. (0x02) means to update DNS configuration, which usually involves writing /etc/resolv.conf anew. (0x04) means to restart the DNS plugin. This is for example useful when using dnsmasq plugin, which uses additional configuration in /etc/NetworkManager/dnsmasq.d. If you edit those files, you can restart the DNS plugin. This action shortly interrupts name resolution. Note that flags may affect each other. For example, restarting the DNS plugin (0x04) implicitly updates DNS too (0x02). Or when reloading the configuration (0x01), changes to DNS setting also cause a DNS update (0x02). However, (0x01) does not involve restarting the DNS plugin (0x04) or update resolv.conf (0x02), unless the DNS related configuration changes in NetworkManager.conf.
		// IN u flags: optional flags to specify which parts shall be reloaded.
		return this._send('Reload', {
			signature: 'u',
			body: [
				flags
			]
		});
	}
	get_devices () {
		// GetDevices (OUT ao devices);
		// Get the list of realized network devices.
		// OUT ao devices: List of object paths of network devices known to the system. This list does not include device placeholders (see GetAllDevices()).
		return this._send('GetDevices');
	}
	get_all_devices () {
		// GetAllDevices (OUT ao devices);
		// Get the list of all network devices.
		// OUT ao devices: List of object paths of network devices and device placeholders (eg, devices that do not yet exist but which can be automatically created by NetworkManager if one of their AvailableConnections was activated).
		return this._send('GetAllDevices');
	}
	get_device_by_ip_iface (iface) {
		// GetDeviceByIpIface (IN s iface, OUT o device);
		// Return the object path of the network device referenced by its IP interface name. Note that some devices (usually modems) only have an IP interface name when they are connected.
		// IN s iface: Interface name of the device to find.
		// OUT o device: Object path of the network device.
		return this._send('GetDeviceByIpIface', {
			signature: 's',
			body: [
				iface
			]
		});		
	}
	activate_connection (connection, device, specific_object) {
		// ActivateConnection (IN o connection, IN o device, IN o specific_object, OUT o active_connection);
		// Activate a connection using the supplied device.
		// IN o connection: The connection to activate. If "/" is given, a valid device path must be given, and NetworkManager picks the best connection to activate for the given device. VPN connections must always pass a valid connection path.
		// IN o device: The object path of device to be activated for physical connections. This parameter is ignored for VPN connections, because the specific_object (if provided) specifies the device to use.
		// IN o specific_object: The path of a connection-type-specific object this activation should use. This parameter is currently ignored for wired and mobile broadband connections, and the value of "/" should be used (ie, no specific object). For Wi-Fi connections, pass the object path of a specific AP from the card's scan list, or "/" to pick an AP automatically. For VPN connections, pass the object path of an ActiveConnection object that should serve as the "base" connection (to which the VPN connections lifetime will be tied), or pass "/" and NM will automatically use the current default device.
		// OUT o active_connection: The path of the active connection object representing this active connection.
		return this._send('ActivateConnection', {
			signature: 'oooo',
			body: [
				connection, device, specific_object 
			]
		});
	}
	add_and_activate_connection (connection, device, specific_object) {
		// AddAndActivateConnection (IN a{sa{sv}} connection, IN o device, IN o specific_object, OUT o path,OUT o active_connection);
		// Adds a new connection using the given details (if any) as a template (automatically filling in missing settings with the capabilities of the given device and specific object), then activate the new connection. Cannot be used for VPN connections at this time.
		// See also AddAndActivateConnection2.
		// IN a{sa{sv}} connection: Connection settings and properties; if incomplete missing settings will be automatically completed using the given device and specific object.
		// IN o device: The object path of device to be activated using the given connection.
		// IN o specific_object: The path of a connection-type-specific object this activation should use. This parameter is currently ignored for wired and mobile broadband connections, and the value of "/" should be used (ie, no specific object). For Wi-Fi connections, pass the object path of a specific AP from the card's scan list, which will be used to complete the details of the newly added connection.
		// OUT o path: Object path of the new connection that was just added.
		// OUT o active_connection: The path of the active connection object representing this active connection.
		return this._send('AddAndActivateConnection', {
			signature: 'oooo',
			body: [
				connection, device, specific_object
			]
		});
	}
	add_and_activate_connection2 (connection, device, specific_object, options) {
		// AddAndActivateConnection2 (IN a{sa{sv}} connection, IN o device, IN o specific_object, IN a{sv} options, OUT o path, OUT o active_connection, OUT a{sv} result);
		// output arguments are supported.
		// Adds a new connection using the given details (if any) as a template (automatically filling in missing settings with the capabilities of the given device and specific object), then activate the new connection. Cannot be used for VPN connections at this time.
		// This method extends AddAndActivateConnection to allow passing further parameters. At this time the following options are supported:
		// * persist: A string value of either "disk" (default), "memory" or "volatile". If "memory" is passed, the connection will not be saved to disk. If "volatile" is passed, the connection will not be saved to disk and will be destroyed when disconnected. * bind-activation: Bind the activation lifetime. Set to "dbus-name" to automatically disconnect when the requesting process disappears from the bus. The default of "none" means the connection is kept activated normally.
		// IN a{sa{sv}} connection: Connection settings and properties; if incomplete missing settings will be automatically completed using the given device and specific object.
		// IN o device: The object path of device to be activated using the given connection.
		// IN o specific_object: The path of a connection-type-specific object this activation should use. This parameter is currently ignored for wired and mobile broadband connections, and the value of "/" should be used (ie, no specific object). For Wi-Fi connections, pass the object path of a specific AP from the card's scan list, which will be used to complete the details of the newly added connection.
		// IN a{sv} options: Further options for the method call.
		// OUT o path: Object path of the new connection that was just added.
		// OUT o active_connection: The path of the active connection object representing this active connection.
		// OUT a{sv} result: a dictionary of additional output arguments for future extension. Currently, not additional
		return this._send('AddAndActivateConnection', {
			signature: 'a{sa{sv}}ooa{sv}',
			body: [
				connection, device, specific_object, options
			]
		});
	}
	deactivate_connection (active_connection) {
		// DeactivateConnection (IN o active_connection);
		// Deactivate an active connection.
		// IN o active_connection: The currently active connection to deactivate.
		return this._send('DeactivateConnection', {
			signature: 'o',
			body: [
				active_connection
			]
		});
	}
	sleep (sleep) {
		// Sleep (IN b sleep);
		// Control the NetworkManager daemon's sleep state. When asleep, all interfaces that it manages are deactivated. When awake, devices are available to be activated. This command should not be called directly by users or clients; it is intended for system suspend/resume tracking.
		// IN b sleep: Indicates whether the NetworkManager daemon should sleep or wake.
		return this._send('Sleep', {
			signature: 'b',
			body: [
				sleep
			]
		});
	}
	enable (enable) {
		// Enable (IN b enable);
		// Control whether overall networking is enabled or disabled. When disabled, all interfaces that NM manages are deactivated. When enabled, all managed interfaces are re-enabled and available to be activated. This command should be used by clients that provide to users the ability to enable/disable all networking.
		// IN b enable: If FALSE, indicates that all networking should be disabled. If TRUE, indicates that NetworkManager should begin managing network devices.
		return this._send('Enable', {
			signature: 'b',
			body: [
				enable
			]
		});
	}
	get_permissions () {
		// GetPermissions (OUT a{ss} permissions);
		// Returns the permissions a caller has for various authenticated operations that NetworkManager provides, like Enable/Disable networking, changing Wi-Fi, WWAN, and WiMAX state, etc.
		// OUT a{ss} permissions: Dictionary of available permissions and results. Each permission is represented by a name (ie "org.freedesktop.NetworkManager.Foobar") and each result is one of the following values: "yes" (the permission is available), "auth" (the permission is available after a successful authentication), or "no" (the permission is denied). Clients may use these values in the UI to indicate the ability to perform certain operations.
		return this._send('GetPermissions');
	}
	set_logging (level, domains) {
		// SetLogging (IN s level, IN s domains);
		// Set logging verbosity and which operations are logged.
		// IN s level: One of [ERR, WARN, INFO, DEBUG, TRACE, OFF, KEEP]. This level is applied to the domains as specified in the domains argument. Except for the special level "KEEP", all unmentioned domains are disabled entirely. "KEEP" is special and allows not to change the current setting except for the specified domains. E.g. level=KEEP and domains=PLATFORM:DEBUG will only touch the platform domain.
		// IN s domains: A combination of logging domains separated by commas (','), or "NONE" to disable logging. Each domain enables logging for operations related to that domain. Available domains are: [PLATFORM, RFKILL, ETHER, WIFI, BT, MB, DHCP4, DHCP6, PPP, WIFI_SCAN, IP4, IP6, AUTOIP4, DNS, VPN, SHARING, SUPPLICANT, AGENTS, SETTINGS, SUSPEND, CORE, DEVICE, OLPC, WIMAX, INFINIBAND, FIREWALL, ADSL, BOND, VLAN, BRIDGE, DBUS_PROPS, TEAM, CONCHECK, DCB, DISPATCH, AUDIT]. In addition to these domains, the following special domains can be used: [NONE, ALL, DEFAULT, DHCP, IP]. You can also specify that some domains should log at a different level from the default by appending a colon (':') and a log level (eg, 'WIFI:DEBUG'). If an empty string is given, the log level is changed but the current set of log domains remains unchanged.
		return this._send('SetLogging', {
			signature: 'ss',
			body: [
				level, domains
			]
		});
	}
	get_logging () {
		// GetLogging (OUT s level, OUT s domains);
		// Get current logging verbosity level and operations domains.
		// OUT s level: One of [ERR, WARN, INFO, DEBUG, TRACE].
		// OUT s domains: For available domains see SetLogging() call.
		return this._send('GetLogging');
	}
	check_connectivity () {
		// CheckConnectivity (OUT u connectivity);
		// Re-check the network connectivity state.
		// OUT u connectivity: (NMConnectivityState) The current connectivity state.
		return this._send('CheckConnectivity');
	}
	state () {
		// state (OUT u state);
		// The overall networking state as determined by the NetworkManager daemon, based on the state of network devices under its management.
		// OUT u state: NMState
		return this._send('state');
	}
	check_point_create (devices, rollback_timeout, flags) {
		// CheckpointCreate (IN ao devices, IN u rollback_timeout, IN u flags, OUT o checkpoint);
		// Create a checkpoint of the current networking configuration for given interfaces. If rollback_timeout is not zero, a rollback is automatically performed after the given timeout.
		// IN ao devices: A list of device paths for which a checkpoint should be created. An empty list means all devices.
		// IN u rollback_timeout: The time in seconds until NetworkManager will automatically rollback to the checkpoint. Set to zero for infinite.
		// IN u flags: (NMCheckpointCreateFlags) Flags for the creation.
		// OUT o checkpoint: On success, the path of the new checkpoint.		
		return this._send('CheckpointCreate', {
			signature: 'aouu',
			body: [
				devices, rollback_timeout, flags
			]
		});
	}
	check_point_destroy (checkpoint) {
		// CheckpointDestroy (IN o checkpoint);
		// Destroy a previously created checkpoint.
		// IN o checkpoint: The checkpoint to be destroyed. Set to empty to cancel all pending checkpoints.
		return this._send('CheckpointCreate', {
			signature: 'o',
			body: [
				checkpoint
			]
		});
	}
	check_point_rollback (checkpoint) {
		// CheckpointRollback (IN o checkpoint, OUT a{su} result);
		// Rollback a checkpoint before the timeout is reached.
		// IN o checkpoint: The checkpoint to be rolled back.
		// OUT a{su} result: On return, a dictionary of devices and results. Devices are represented by their original D-Bus path; each result is a RollbackResult.
		return this._send('CheckpointRollback', {
			signature: 'o',
			body: [
				checkpoint
			]
		});		
	}
	check_point_adjust_rollback_timeout (checkpoint, add_timeout) {
		// CheckpointAdjustRollbackTimeout (IN o checkpoint, IN u add_timeout);
		// timeout will expire. Set to 0 to disable the timeout. Note that the added seconds start counting from now, not "Created" timestamp or the previous expiration time. Note that the "Created" property of the checkpoint will stay unchanged by this call. However, the "RollbackTimeout" will be recalculated to give the approximate new expiration time. The new "RollbackTimeout" property will be approximate up to one second precision, which is the accuracy of the property.
		// Reset the timeout for rollback for the checkpoint.
		// IN o checkpoint: 
		// IN u add_timeout: number of seconds from ~now~ in which the ???
		return this._send('CheckpointAdjustRollbackTimeout', {
			signature: 'ou',
			body: [
				checkpoint, add_timeout
			]
		});		
	}

	/* SIGNALS */
	
	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'DeviceAdded': (dbus_path) => {
				// DeviceAdded (o device_path);
				// A device was added to the system
				// o device_path: The object path of the newly added device.
				_this._debug(3,`Notification > DeviceAdded: ${dbus_path}`);	
				_this._Ctrl.slots('DeviceAdded',dbus_path);			
			},
			'DeviceRemoved': (dbus_path) => {
				// DeviceRemoved (o device_path);
				// A device was removed from the system, and is no longer available.
				// o device_path: The object path of the device that was just removed.
				_this._debug(3,`Notification > DeviceRemoved: ${dbus_path}`);
				_this._Ctrl.slots('DeviceRemoved',dbus_path);				
			},
			'StateChanged': (state) => {
				// StateChanged (u state);
				// NetworkManager's state changed.
				// u state: (NMState) The new state of NetworkManager.
				_this._debug(3,`Notification > StateChanged: ${state}`);		
				_this._Ctrl.slots('StateChanged',state);		
			},
			'CheckPermissions': () => {
				// CheckPermissions ();
				// Emitted when system authorization details change, indicating that clients may wish to recheck permissions with GetPermissions. 
				_this._debug(3,`Notification > CheckPermissions`);	
				_this._Ctrl.slots('CheckPermissions', null);			
			}
			// PropertiesChanged deprecated
		});
	}

	/* PROPERTIES 

	{
		'ActivatingConnection': '/org/freedesktop/NetworkManager/ActiveConnection/1',
		'ActiveConnections': ['/org/freedesktop/NetworkManager/ActiveConnection/1'],
		'AllDevices': [
			'/org/freedesktop/NetworkManager/Devices/1',
			'/org/freedesktop/NetworkManager/Devices/2',
			'/org/freedesktop/NetworkManager/Devices/3',
			'/org/freedesktop/NetworkManager/Devices/4',
			'/org/freedesktop/NetworkManager/Devices/5'
		],
		'Capabilities': [1],
		'Checkpoints': [],
		'Connectivity': 4,
		'ConnectivityCheckAvailable': False,
		'ConnectivityCheckEnabled': False,
		'ConnectivityCheckUri': '',
		'Devices': [
			'/org/freedesktop/NetworkManager/Devices/1',
			'/org/freedesktop/NetworkManager/Devices/2',
			'/org/freedesktop/NetworkManager/Devices/3',
			'/org/freedesktop/NetworkManager/Devices/4',
			'/org/freedesktop/NetworkManager/Devices/5'
		],
		'GlobalDnsConfiguration': {},
		'Metered': 4,
		'NetworkingEnabled': True,
		'PrimaryConnection': '/org/freedesktop/NetworkManager/ActiveConnection/1',
		'PrimaryConnectionType': '802-3-ethernet',
		'RadioFlags': 3,
		'Startup': False,
		'State': 70,
		'Version': '1.38.0',
		'WimaxEnabled': False,
		'WimaxHardwareEnabled': False,
		'WirelessEnabled': False,
		'WirelessHardwareEnabled': True,
		'WwanEnabled': True,
		'WwanHardwareEnabled': True
	}

	Devices  readable   ao
	The list of realized network devices. Realized devices are those which have backing resources (eg from the kernel or a management daemon like ModemManager, teamd, etc). 

	AllDevices  readable   ao
	The list of both realized and un-realized network devices. Un-realized devices are software devices which do not yet have backing resources, but for which backing resources can be created if the device is activated. 

	Checkpoints  readable   ao
	The list of active checkpoints. 

	NetworkingEnabled  readable   b
	Indicates if overall networking is currently enabled or not. See the Enable() method. 

	WirelessEnabled  readwrite  b
	Indicates if wireless is currently enabled or not. 

	WirelessHardwareEnabled  readable   b
	Indicates if the wireless hardware is currently enabled, i.e. the state of the RF kill switch

	WwanEnabled  readwrite  b
	Indicates if mobile broadband devices are currently enabled or not. 

	WwanHardwareEnabled  readable   b
	Indicates if the mobile broadband hardware is currently enabled, i.e. the state of the RF kill switch. 

	WimaxEnabled  readwrite  b
	DEPRECATED. Doesn't have any meaning and is around only for compatibility reasons. 

	WimaxHardwareEnabled  readable   b
	DEPRECATED. Doesn't have any meaning and is around only for compatibility reasons. 

	ActiveConnections  readable   ao
	List of active connection object paths. 

	PrimaryConnection  readable   o
	The object path of the "primary" active connection being used to access the network. In particular, if there is no VPN active, or the VPN does not have the default route, then this indicates the connection that has the default route. If there is a VPN active with the default route, then this indicates the connection that contains the route to the VPN endpoint. 

	PrimaryConnectionType  readable   s
	The connection type of the "primary" active connection being used to access the network. This is the same as the Type property on the object indicated by PrimaryConnection. 

	Metered  readable   u
	Indicates whether the connectivity is metered. This is equivalent to the metered property of the device associated with the primary connection.
	Returns: NMMetered 

	ActivatingConnection  readable   o
	The object path of an active connection that is currently being activated and which is expected to become the new PrimaryConnection when it finishes activating. 

	Startup  readable   b
	Indicates whether NM is still starting up; this becomes FALSE when NM has finished attempting to activate every connection that it might be able to activate at startup. 

	Version  readable   s
	NetworkManager version. 

	Capabilities  readable   au
	The current set of capabilities. See NMCapability for currently defined capability numbers. The array is guaranteed to be sorted in ascending order without duplicates. 

	State  readable   u
	The overall state of the NetworkManager daemon.
	This takes state of all active connections and the connectivity state into account to produce a single indicator of the network accessibility status.
	The graphical shells may use this property to provide network connection status indication and applications may use this to check if Internet connection is accessible. Shell that is able to cope with captive portals should use the "Connectivity" property to decide whether to present a captive portal authentication dialog.
	Returns: NMState 

	Connectivity  readable   u
	The result of the last connectivity check. The connectivity check is triggered automatically when a default connection becomes available, periodically and by calling a CheckConnectivity() method.
	This property is in general useful for the graphical shell to determine whether the Internet access is being hijacked by an authentication gateway (a "captive portal"). In such case it would typically present a web browser window to give the user a chance to authenticate and call CheckConnectivity() when the user submits a form or dismisses the window.
	To determine the whether the user is able to access the Internet without dealing with captive portals (e.g. to provide a network connection indicator or disable controls that require Internet access), the "State" property is more suitable.
	Returns: NMConnectivityState 

	ConnectivityCheckAvailable  readable   b
	Indicates whether connectivity checking service has been configured. This may return true even if the service is not currently enabled.
	This is primarily intended for use in a privacy control panel, as a way to determine whether to show an option to enable/disable the feature. 

	ConnectivityCheckEnabled  readwrite  b
	Indicates whether connectivity checking is enabled. This property can also be written to disable connectivity checking (as a privacy control panel might want to do). 

	ConnectivityCheckUri  readable   s
	The URI that NetworkManager will hit to check if there is internet connectivity. 

	GlobalDnsConfiguration  readwrite  a{sv}
	Dictionary of global DNS settings where the key is one of "searches", "options" and "domains". The values for the "searches" and "options" keys are string arrays describing the list of search domains and resolver options, respectively. The value of the "domains" key is a second-level dictionary, where each key is a domain name, and each key's value is a third-level dictionary with the keys "servers" and "options". "servers" is a string array of DNS servers, "options" is a string array of domain-specific options. 

	*/
}	

