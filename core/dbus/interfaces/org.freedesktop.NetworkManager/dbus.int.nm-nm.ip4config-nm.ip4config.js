/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager.IP4Config — IPv4 Configuration Set

class DBus_Int__NM__NM_IP4Config__NM_IP4Config extends PP_DBusSystemInterface {	
	constructor (_Ctrl, dbus_path) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: dbus_path, // '/org/freedesktop/NetworkManager/IP4Config/' + number
			'interface': 'org.freedesktop.NetworkManager.IP4Config'
		});
	}

	/* NO METHODS */

	/* NO SIGNALS */

	/* PROPERTIES

	Example: (with obsolete properties)

	{
		'AddressData': [{ 'address': '192.168.0.100', 'prefix': 24}],
		'Addresses': [[167815360, 24, 50374848]],
		'DnsOptions': [],
		'DnsPriority': 100,
		'Domains': ['mydomain.lan'],
		'Gateway': '192.168.0.254',
		'NameserverData': [{'address': '192.168.0.254'}],
		'Nameservers': [50374848],
		'RouteData': [
				{
						'dest': '192.168.0.0',
						'metric': 100,
						'prefix': 24
				},
				{
						'dest': '0.0.0.0',
						'metric': 100,
						'next-hop': '192.168.0.254',
						'prefix': 0
				}
		],	
		'Routes': [[43200, 24, 0, 100]],
		'Searches': ['mydomain.lan'],
		'WinsServerData': [],
		'WinsServers': []
	}	 

	AddressData  readable   aa{sv}
	Array of IP address data objects. All addresses will include "address" (an IP address string), and "prefix" (a uint). Some addresses may include additional attributes.

	Gateway  readable   s
	The gateway in use.

	RouteData  readable   aa{sv}
	Array of IP route data objects. All routes will include "dest" (an IP address string) and "prefix" (a uint). Some routes may include "next-hop" (an IP address string), "metric" (a uint), and additional attributes.

	NameserverData  readable   aa{sv}
	The nameservers in use. Currently, only the value "address" is recognized (with an IP address string).

	Domains  readable   as
	A list of domains this address belongs to.

	Searches  readable   as
	A list of dns searches.

	DnsOptions  readable   as
	A list of DNS options that modify the behavior of the DNS resolver. See resolv.conf(5) manual page for the list of supported options.

	DnsPriority  readable   i
	The relative priority of DNS servers.

	WinsServerData  readable   as
	The Windows Internet Name Service servers associated with the connection.

	*/

}