/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager.Settings.Connection — Connection Settings Profile

class DBus_Int__NM__NM_Settings__NM_Settings_Connection extends PP_DBusSystemInterface {	
	constructor (_Ctrl, dbus_path) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: dbus_path, // /org/freedesktop/NetworkManager/Settings/1
			'interface': 'org.freedesktop.NetworkManager.Settings.Connection'
		});
	}

	/* METHODS */

	update (properties) {
		// Update (IN  a{sa{sv}} properties);
		// Update the connection with new settings and properties (replacing all previous settings and properties) and save the connection to disk. Secrets may be part of the update request, and will be either stored in persistent storage or sent to a Secret Agent for storage, depending on the flags associated with each secret.
		// IN a{sa{sv}} properties: New connection settings, properties, and (optionally) secrets.
		return this._send('Update', {
			signature: 'a{sa{sv}}',
			body: [
				properties
			]
		});	
	}
	update_unsaved (properties) {
		// UpdateUnsaved (IN  a{sa{sv}} properties);
		// Update the connection with new settings and properties (replacing all previous settings and properties) but do not immediately save the connection to disk. Secrets may be part of the update request and may sent to a Secret Agent for storage, depending on the flags associated with each secret. Use the 'Save' method to save these changes to disk. Note that unsaved changes will be lost if the connection is reloaded from disk (either automatically on file change or due to an explicit ReloadConnections call).
		// IN a{sa{sv}} properties: New connection settings, properties, and (optionally) secrets.
		return this._send('UpdateUnsaved', {
			signature: 'a{sa{sv}}',
			body: [
				properties
			]
		});	
	}
	delete () {
		// Delete ()
		// Delete the connection. 
		return this._send('Delete');
	}
	get_settings () {
		// GetSettings (OUT a{sa{sv}} settings);
		// Get the settings maps describing this network configuration. This will never include any secrets required for connection to the network, as those are often protected. Secrets must be requested separately using the GetSecrets() call.
		// OUT a{sa{sv}} settings: The nested settings maps describing this object.
		/*
		{
			'connection': {
				'autoconnect': False,
				'id': 'Free-Mobile',
				'permissions': [],
				'timestamp': 1655633883,
				'type': 'gsm',
				'uuid': 'f0ff78c5-2a77-4ab7-ac9a-56ed80cb9ffd'
			},
			'gsm': {
				'apn': 'free',
				'home-only': True,
				'sim-id': '0123456789012345678',
				'username': 'free'
			},
			'ipv4': {
				'address-data': [],
				'addresses': [],
				'dns-priority': 120,
				'dns-search': [],
				'method': 'auto',
				'route-data': [],
				'route-metric': 1050,
				'routes': []
			},
			'ipv6': {
				'address-data': [],
				'addresses': [],
				'dns-search': [],
				'method': 'auto',
				'route-data': [],
				'routes': []
			},
			'ppp': {},
			'proxy': {},
			'serial': {'baud': 115200}
		}
		*/
		return this._send('GetSettings', {
			signature: 'a{sa{sv}}',
			body: [
				settings
			]
		});	
	}
	get_secrets (setting_name) {
		// GetSecrets (IN s setting_name, OUT a{sa{sv}} secrets);
		// Get the secrets belonging to this network configuration. Only secrets from persistent storage or a Secret Agent running in the requestor's session will be returned. The user will never be prompted for secrets as a result of this request.
		// IN s setting_name: Name of the setting to return secrets for. If empty, all secrets will be returned.
		// OUT a{sa{sv}} secrets: Nested settings maps containing secrets.
		return this._send('GetSecrets', {
			signature: 's',
			body: [
				setting_name
			]
		});	
	}
	clear_secrets () {
		// ClearSecrets ();
		// Clear the secrets belonging to this network connection profile. 
		return this._send('ClearSecrets');
	}
	save () {
		// Save ();
		// Saves a "dirty" connection (that had previously been updated with UpdateUnsaved) to persistent storage. 
		return this._send('Save');
	}
	update2 (settings, flags, args) {
		// Update2 (IN a{sa{sv}} settings, IN u flags, IN a{sv} args, OUT a{sv} result);
		// Provide an empty array, to use the current settings. flags: optional flags argument. Currently, supported flags are: "0x1" (to-disk), "0x2" (in-memory), "0x4" (in-memory-detached), "0x8" (in-memory-only), "0x10" (volatile), "0x20" (block-autoconnect), "0x40" (no-reapply). Unknown flags cause the call to fail. args: optional arguments dictionary, for extentibility. Currently, no arguments are accepted. Specifying unknown keys causes the call to fail. result: output argument, currently no results are returned.
		// Update the connection with new settings and properties (replacing all previous settings and properties). If the flag 0x1 is present, the connection is persisted to disk. If the flag 0x2 is present, the change is only made in memory (without touching an eventual profile on disk). If neither 0x1 nor 0x2 is set, the change is made in memory only, if the connection is already in memory only. The flags 0x4 (in-memory-detached) and 0x8 (in-memory-only) are like "in-memory", but behave slightly different when migrating the profile from disk to in-memory. The flag 0x20 (block-autoconnect) blocks auto-connect on the updated profile, and 0x40 (no-reapply) prevents "connection.zone" and "connection.metered" properties to take effect on currently active devices. Secrets may be part of the update request, and will be either stored in persistent storage or sent to a Secret Agent for storage, depending on the flags associated with each secret.
		// Update2 is a extensible alternative to Update, UpdateUnsaved and Save.
		// IN a{sa{sv}} settings: New connection settings, properties, and (optionally) secrets.
		// IN u flags: ???
		// IN a{sv} args: ???
		// OUT a{sv} result: ???
		return this._send('GetSecrets', {
			signature: 'a{sa{sv}}ua{sv}',
			body: [
				settings, flags, args
			]
		});
	}

	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'Updated': () => {
				_this._debug(3,`Notification > Updated`);
				// Updated ();
				// Emitted when any settings or permissions change. When handling this signal, clients should re-read the connection using the GetSettings method to get the changes and to ensure the client still has permission to access the connection. 
				_this._Ctrl.slots('Updated',null);
			},
			'Removed': () => {
				_this._debug(3,`Notification > Removed)`);
				// Removed ();
				// Emitted when this connection is no longer available. This happens when the connection is deleted or if it is no longer accessible by any of the system's logged-in users. After receipt of this signal, the object no longer exists. Also see the Settings.ConnectionRemoved signal. 
				_this._Ctrl.slots('Removed',null);				
			}
		});
	}		

	/* PROPERTIES 

	Example:
	{
		'Filename': '/etc/NetworkManager/system-connections/Free-Mobile-f0ff78c5-2a77-4ab7-ac9a-56ed80cb9ffd.nmconnection',
		'Flags': 0,
		'Unsaved': False
	}

	Unsaved  readable   b
	If set, indicates that the in-memory state of the connection does not match the on-disk state. This flag will be set when UpdateUnsaved() is called or when any connection details change, and cleared when the connection is saved to disk via Save() or from internal operations.

	Flags  readable   u
	Additional flags of the connection profile.
	Returns: NMSettingsConnectionFlags

	Filename  readable   s
	File that stores the connection in case the connection is file-backed. 

	*/
}