/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager.Connection.Active — Active Connection

class DBus_Int__NM__NM_ActiveConnection__NM_Connection_Active extends PP_DBusSystemInterface {	
	constructor (_Ctrl, dbus_path) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: dbus_path, // '/org/freedesktop/NetworkManager/ActiveConnection/' + number
			'interface': 'org.freedesktop.NetworkManager.Connection.Active'
		});
	}

	/* NO METHODS */
	
	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'StateChanged': (state, reason) => {
				_this._debug(3,`Notification > StateChanged: ${state} ${reason}`);
				// StateChanged (u state, u reason);
				// Emitted when the state of the active connection has changed. 				
				// u state: (NMActiveConnectionState) The new state of the active connection.
				// u reason: (NMActiveConnectionStateReason) Reason code describing the change to the new state.
				_this._Ctrl.slots('StateChanged', state, reason);					
			}
		});
		// PropertiesChanged deprecated
	}	

	/* PROPERTIES 

	{
		'Connection': '/org/freedesktop/NetworkManager/Settings/8',
		'Default': True,
		'Default6': False,
		'Devices': ['/org/freedesktop/NetworkManager/Devices/2'],
		'Dhcp4Config': '/org/freedesktop/NetworkManager/DHCP4Config/1',
		'Dhcp6Config': '/',
		'Id': 'Connexion filaire 1',
		'Ip4Config': '/org/freedesktop/NetworkManager/IP4Config/2',
		'Ip6Config': '/org/freedesktop/NetworkManager/IP6Config/2',
		'Master': '/',
		'SpecificObject': '/',
		'State': 2,
		'StateFlags': 76,
		'Type': '802-3-ethernet',
		'Uuid': '4fd2dcad-2e2e-3c13-8b56-a55935bc6f6c',
		'Vpn': False
	}	 

	Connection  readable   o
	The path of the connection. 
	
	SpecificObject  readable   o
	A specific object associated with the active connection. This property reflects the specific object used during connection activation, and will not change over the lifetime of the ActiveConnection once set. 

	Id  readable   s
	The ID of the connection, provided as a convenience so that clients do not have to retrieve all connection details. 

	Uuid  readable   s
	The UUID of the connection, provided as a convenience so that clients do not have to retrieve all connection details. 

	Type  readable   s
	The type of the connection, provided as a convenience so that clients do not have to retrieve all connection details. 

	Devices  readable   ao
	Array of object paths representing devices which are part of this active connection. 

	State  readable   u
	The state of this active connection.
	Returns: NMActiveConnectionState 

	StateFlags  readable   u
	The state flags of this active connection.
	Returns: NMActivationStateFlags 

	Default  readable   b
	Whether this active connection is the default IPv4 connection, i.e. whether it currently owns the default IPv4 route. 

	Ip4Config  readable   o
	Object path of the Ip4Config object describing the configuration of the connection. Only valid when the connection is in the NM_ACTIVE_CONNECTION_STATE_ACTIVATED state. 

	Dhcp4Config  readable   o
	Object path of the Dhcp4Config object describing the DHCP options returned by the DHCP server (assuming the connection used DHCP). Only valid when the connection is in the NM_ACTIVE_CONNECTION_STATE_ACTIVATED state. 

	Default6  readable   b
	Whether this active connection is the default IPv6 connection, i.e. whether it currently owns the default IPv6 route. 

	Ip6Config  readable   o
	Object path of the Ip6Config object describing the configuration of the connection. Only valid when the connection is in the NM_ACTIVE_CONNECTION_STATE_ACTIVATED state. 

	Dhcp6Config  readable   o
	Object path of the Dhcp6Config object describing the DHCP options returned by the DHCP server (assuming the connection used DHCP). Only valid when the connection is in the NM_ACTIVE_CONNECTION_STATE_ACTIVATED state. 

	Vpn  readable   b
	Whether this active connection is also a VPN connection. 

	Master  readable   o
	The path to the master device if the connection is a slave. 

	*/


}