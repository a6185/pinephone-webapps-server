/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager.IP6Config — IPv6 Configuration Set

class DBus_Int__NM__NM_IP6Config__NM_IP6Config extends PP_DBusSystemInterface {	
	constructor (_Ctrl, dbus_path) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: dbus_path, // '/org/freedesktop/NetworkManager/IP4Config/' + number
			'interface': 'org.freedesktop.NetworkManager.IP6Config'
		});
	}

	/* NO METHODS */

	/* NO SIGNALS */

	/* PROPERTIES

	AddressData  readable   aa{sv}
	Array of IP address data objects. All addresses will include "address" (an IP address string), and "prefix" (a uint). Some addresses may include additional attributes.

	Gateway  readable   s
	The gateway in use.

	RouteData  readable   aa{sv}
	Array of IP route data objects. All routes will include "dest" (an IP address string) and "prefix" (a uint). Some routes may include "next-hop" (an IP address string), "metric" (a uint), and additional attributes.

	Nameservers  readable   aay
	The nameservers in use.

	Domains  readable   as
	A list of domains this address belongs to.

	Searches  readable   as
	A list of dns searches.

	DnsOptions  readable   as
	A list of DNS options that modify the behavior of the DNS resolver. See resolv.conf(5) manual page for the list of supported options.

	DnsPriority  readable   i
	The relative priority of DNS servers. 

	*/
}