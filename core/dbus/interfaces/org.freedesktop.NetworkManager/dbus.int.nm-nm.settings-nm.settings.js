/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.NetworkManager.Settings — Connection Settings Profile Manager

class DBus_Int__NM__NM_Settings__NM_Settings extends PP_DBusSystemInterface {	
	constructor (_Ctrl) {
		super(_Ctrl, {
			destination: 'org.freedesktop.NetworkManager',
			path: '/org/freedesktop/NetworkManager/Settings',
			'interface': 'org.freedesktop.NetworkManager.Settings'
		});
	}

	/* METHODS */

	list_connections () {
		// ListConnections (OUT ao connections);
		// List the saved network connections known to NetworkManager. 
		// OUT ao connections: List of connections.
		/*
		[
			'/org/freedesktop/NetworkManager/Settings/1',
			'/org/freedesktop/NetworkManager/Settings/2',
			'/org/freedesktop/NetworkManager/Settings/3',
			...
		]
		*/
		return this._send('ListConnections');
	}
	get_connection_by_uuid (uuid) {
		// GetConnectionByUuid (IN  s uuid, OUT o connection);
		//  Retrieve the object path of a connection, given that connection's UUID.
		// IN s uuid: The UUID to find the connection object path for.
		// OUT o connection: The connection's object path.
		/*
		'/org/freedesktop/NetworkManager/Settings/1'
		*/
		return this._send('GetConnectionByUuid', {
			signature: 's',
			body: [
				uuid // 'f0ff78c5-2a77-4ab7-ac9a-56ed80cb9ffd'
			]
		});	
	}
	add_connection (connection) {
		// AddConnection (IN  a{sa{sv}} connection, OUT o path);
		// Add new connection and save it to disk. This operation does not start the network connection unless (1) device is idle and able to connect to the network described by the new connection, and (2) the connection is allowed to be started automatically.
		// IN a{sa{sv}} connection: Connection settings and properties.
		// OUT o path: Object path of the new connection that was just added.
		return this._send('AddConnection', {
			signature: 'a{sa{sv}}',
			body: [
				connection
			]
		});			
	}
	add_connection_unsaved (connection) {
		// AddConnectionUnsaved (IN  a{sa{sv}} connection, OUT o path);
		//  Add new connection but do not save it to disk immediately. This operation does not start the network connection unless (1) device is idle and able to connect to the network described by the new connection, and (2) the connection is allowed to be started automatically. Use the 'Save' method on the connection to save these changes to disk.
		// IN a{sa{sv}} connection: Connection settings and properties.
		// OUT o path: Object path of the new connection that was just added.
		return this._send('AddConnectionUnsaved', {
			signature: 'a{sa{sv}}',
			body: [
				connection
			]
		});	
	}
	add_connection2 (settings, flags, args) {
		// AddConnection2 (IN  a{sa{sv}} settings, IN u flags, IN  a{sv}args, OUT opath, OUT a{sv}     result);
		// "0x1" (to-disk), "0x2" (in-memory), "0x20" (block-autoconnect). Unknown flags cause the call to fail. args: optional arguments dictionary, for extentibility. Currently, no arguments are accepted. Specifying unknown keys causes the call to fail. path: Object path of the new connection that was just added. result: output argument, currently no additional results are returned.
		// Add a new connection profile.
		// Either the flags 0x1 (to-disk) or 0x2 (in-memory) must be specified. The effect is whether to behave like AddConnection or AddConnectionUnsaved. If 0x20 (block-autoconnect) is specified, autoconnect for the new profile is blocked from the beginning. Otherwise, the profile might automatically connect if a suitable device is around.
		// AddConnection2 is a extensible alternative to AddConnection, and AddConnectionUnsaved. The new variant can do everything that the older variants could, and more.
		// IN a{sa{sv}} settings: New connection settings, properties, and (optionally) secrets.
		// IN u flags: optional flags argument. Currently, the following flags are supported:
		// IN a{sv} args: OUT o path:
		// OUT a{sv} result:
		return this._send('AddConnection2', {
			signature: 'a{sa{sv}}ua{sv}',
			body: [
				settings, flags, args
			]
		});		
	}
	load_connections (filenames, status, failures) {
		// LoadConnections (IN  as filenames, OUT b  status, OUT as failures);
		// Note that before 1.20, NetworkManager had a bug and this status value was wrong. It is better to assume success if the method does not return with a D-Bus error. On top of that, you can look at failures to know whether any of the requested files failed. failures: Paths of connection files that could not be loaded.
		// Loads or reloads the indicated connections from disk. You should call this after making changes directly to an on-disk connection file to make sure that NetworkManager sees the changes. As with AddConnection(), this operation does not necessarily start the network connection.
		// IN as filenames: Array of paths to on-disk connection profiles in directories monitored by NetworkManager.
		// OUT b status: Success or failure of the operation as a whole. True if NetworkManager at least tried to load the indicated connections, even if it did not succeed. False if an error occurred before trying to load the connections (eg, permission denied).
		// OUT as failures: ???
		return this._send('LoadConnections', {
			signature: 'asbas',
			body: [
				filenames, status, failures
			]
		});		
	}
	reload_connections (status) {
		// ReloadConnections (OUT b status);
		// Tells NetworkManager to reload all connection files from disk, including noticing any added or deleted connection files.
		// OUT b status: This always returns TRUE.
		return this._send('ReloadConnections');	
	}
	save_hostname (hostname) {
		// SaveHostname (IN  s hostname);
		// Save the hostname to persistent configuration.
		// IN s hostname: The hostname to save to persistent configuration. If blank, the persistent hostname is cleared.
		return this._send('SaveHostname', {
			signature: 's',
			body: [
				hostname
			]
		});		
	}

	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'NewConnection': (dbus_path) => {
				_this._debug(3,`Notification > NewConnection: ${dbus_path}`);
				// NewConnection (o connection);
				// Emitted when a new connection has been added after NetworkManager has started up and initialized. This signal is not emitted for connections read while starting up, because NetworkManager's D-Bus service is only available after all connections have been read, and to prevent spamming listeners with too many signals at one time. To retrieve the initial connection list, call the ListConnections() method once, and then listen for individual Settings.NewConnection and Settings.Connection.Deleted signals for further updates.
				// o connection: Object path of the new connection.
				_this._Ctrl.slots('NewConnection',dbus_path);					
			},
			'ConnectionRemoved': (dbus_path) => {
				_this._debug(3,`Notification > ConnectionRemoved: ${dbus_path}`);
				// ConnectionRemoved (o connection);
				// Emitted when a connection is no longer available. This happens when the connection is deleted or if it is no longer accessible by any of the system's logged-in users. After receipt of this signal, the connection no longer exists and cannot be used. Also see the Settings.Connection.Removed signal.
				// o connection: Object path of the removed connection.
				_this._Ctrl.slots('ConnectionRemoved',dbus_path);				
			}
		});
	}		

	/* PROPERTIES 

	{
		'CanModify': True,
		'Connections': [
			'/org/freedesktop/NetworkManager/Settings/1',
			'/org/freedesktop/NetworkManager/Settings/2',
			'/org/freedesktop/NetworkManager/Settings/3',
			...
		],
		'Hostname': 'mobian'
	}

	Connections  readable   ao
	List of object paths of available network connection profiles.

	Hostname  readable   s
	The machine hostname stored in persistent configuration.

	CanModify  readable   b
	If true, adding and modifying connections is supported.
	*/
}
