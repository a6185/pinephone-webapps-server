// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Messaging — The ModemManager Messaging interface.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Messaging extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Messaging'
		});
	}

	/* METHODS */

	list () {
		// List (OUT ao result);
		// Retrieve all SMS messages.
		// This method should only be used once and subsequent information retrieved either by listening for the "Added" signal, or by querying the specific SMS object of interest.
		// OUT ao result: The list of SMS object paths.
		return this._send('List');
	}

	delete (path) {
		// The Delete() method
		// Delete (IN  o path);
		// Delete an SMS message.
		// IN o path: The object path of the SMS to delete.
		return this._send('Delete', {
			signature: 'o',
			body: [
				path
			]
		});
	}

	create (properties) {
		// Create (IN  a{sv} properties, OUT o path);
		// Creates a new message object.
		// The 'Number' and either 'Text' or 'Data' properties are mandatory, others are optional.
		// If the SMSC is not specified and one is required, the default SMSC is used.
		// IN a{sv} properties: Message properties from the SMS D-Bus interface.
		// OUT o path: The object path of the new message object.
		return this._send('Create', {
			signature: 'a{sv}',
			body: [
				properties
			]
		});
	}

	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'Added': (dbus_path, received) => {
				// Added (o path, b received);
				// Emitted when any part of a new SMS has been received or added (but not for subsequent parts, if any). For messages received from the network, not all parts may have been received and the message may not be complete.
				// Check the 'State' property to determine if the message is complete.
				// o path: Object path of the new SMS.
				// b received: TRUE if the message was received from the network, as opposed to being added locally.
				_this._debug(3,`Notification > Added: ${dbus_path} ${received}`);
				_this._Ctrl.slots('Added', dbus_path, received);
			},
			'Deleted': (dbus_path) => {
				// Deleted (o path);
				// Emitted when a message has been deleted.
				// o path: Object path of the now deleted SMS.
				_this._debug(3,`Notification > Deleted: ${dbus_path}`);
				_this._Ctrl.slots('Deleted', dbus_path);
			}
		});
	}	

	/* PROPERTIES

	Messages  readable   ao
	The list of SMS object paths.

	SupportedStorages  readable   au
	A list of MMSmsStorage values, specifying the storages supported by this modem for storing and receiving SMS.

	DefaultStorage  readable   u
	A MMSmsStorage value, specifying the storage to be used when receiving or storing SMS. 

	*/
}