// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Time — The ModemManager Time interface.

// This interface allows clients to receive network time and timezone updates broadcast by mobile networks.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Time extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Time'
		});
	}

	/* METHODS */

	get_network_time () {
		// GetNetworkTime (OUT s time);
		// time, and (if available) UTC offset in ISO 8601 format. If the network time is unknown, the empty string.
		// Gets the current network time in local time.
		// This method will only work if the modem tracks, or can request, the current network time; it will not attempt to use previously-received network time updates on the host to guess the current network time.
		// OUT s time: If the network time is known, a string containing local date,
		return this._send('GetNetworkTime');
	}

	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'NetworkTimeChanged': (time) => {
				// NetworkTimeChanged (s time);
				// Sent when the network time is updated.
				//s time: A string containing date and time in ISO 8601 format.
				_this._debug(3,`Notification > NetworkTimeChanged: ${time}`);
				_this._Ctrl.slots('NetworkTimeChanged', time);
			}
		});
	}

	/* PROPERTIES

	NetworkTimezone  readable   a{sv}
	// The timezone data provided by the network. It may include one or more of the following fields:
	// "offset": Offset of the timezone from UTC, in minutes (including DST, if applicable), given as a signed integer value (signature "i").
	// "dst-offset": Amount of offset that is due to DST (daylight saving time), given as a signed integer value (signature "i").
	// "leap-seconds": Number of leap seconds included in the network time, given as a signed integer value (signature "i").

	*/
}
