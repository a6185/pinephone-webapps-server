// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Location — The ModemManager Location interface.

// The Location interface allows devices to provide location information to client applications. Not all devices can provide this information, or even if they do, they may not be able to provide it while a data session is active.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used (including GNSS module management

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Location extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Location'
		});
	}

	/* METHODS */

	setup (sources, signal_location) {
		// Setup (IN  u sources, IN  b signal_location);
		// Configure the location sources to use when gathering location information. Adding new location sources may require to enable them in the device (e.g. the GNSS engine will need to be started explicitly if a GPS source is requested by the user). In the same way, removing location sources may require to disable them in the device (e.g. when no GPS sources are requested by the user, the GNSS engine will need to be stopped explicitly).
		// This method may require the client to authenticate itself.
		// When location signaling is enabled by the user, any client application (including malicious ones!) would be able to use the "Location" property to receive location updates. If further security is desired, the signal_location argument can be set to FALSE to disable location updates via D-Bus signals and require applications to call authenticated APIs (like GetLocation()) to get the location information.
		// By default location signaling is disabled, and therefore the "Location" property will not be usable until explicitly enabled by the user.
		// The optional MM_MODEM_LOCATION_SOURCE_AGPS_MSA and MM_MODEM_LOCATION_SOURCE_AGPS_MSB allow to request MSA/MSB A-GPS operation, and they must be given along with either MM_MODEM_LOCATION_SOURCE_GPS_RAW or MM_MODEM_LOCATION_SOURCE_GPS_NMEA.
		// Both MM_MODEM_LOCATION_SOURCE_AGPS_MSA and MM_MODEM_LOCATION_SOURCE_AGPS_MSB cannot be given at the same time, and if none given, standalone GPS is assumed.
		// IN u sources: Bitmask of MMModemLocationSource flags, specifying which sources should get enabled or disabled. MM_MODEM_LOCATION_SOURCE_NONE will disable all location gathering.
		// IN b signal_location: Flag to control whether the device emits signals with the new location information. This argument is ignored when disabling location information gathering.
		return this._send('Setup', {
			signature: 'ub',
			body: [
				sources, signal_location
			]
		});
	}

	get_location () {
		// GetLocation (OUT a{uv} Location);
		// Return current location information, if any. If the modem supports multiple location types it may return more than one. See the "Location" property for more information on the dictionary returned at location.
		// This method may require the client to authenticate itself.
		// OUT a{uv} Location:
		return this._send('GetLocation');
	}

	set_supl_server () {
		// SetSuplServer (IN  s supl);
		// Configure the SUPL server for A-GPS.
		// IN s supl: SUPL server configuration, given either as IP:PORT or as FQDN:PORT.
		return this._send('SetSuplServer', {
			signature: 's',
			body: [
				supl
			]
		});
	}

	inject_assistance_data (data) {
		// InjectAssistanceData (IN  ay data);
		// Inject assistance data to the GNSS module, which will allow it to have a more accurate positioning information.
		// The data files should be downloaded using external means from the URLs specified in the AssistanceDataServers property. The user does not need to specify the assistance data type being given.
		// There is no maximum data size limit specified, default DBus system bus limits apply.
		// This method may be used when the device does not have a mobile network connection by itself, and therefore it cannot use any A-GPS server to improve the accuracy of the position. In this case, the user can instead download the assistance data files using a WiFi or LAN network, and inject them to the GNSS engine manually.
		// IN ay data: assistance data to be injected to the GNSS module.
		return this._send('InjectAssistanceData', {
			signature: 'ay',
			body: [
				data
			]
		});
	}		

	set_gps_refresh_rate (rate) {
		// SetGpsRefreshRate (IN  u rate);
		// Set the refresh rate of the GPS information in the API. If not explicitly set, a default of 30s will be used.
		// The refresh rate can be set to 0 to disable it, so that every update reported by the modem is published in the interface.
		// IN u rate: Rate, in seconds.
		return this._send('SetGpsRefreshRate', {
			signature: 'u',
			body: [
				rate
			]
		});
	}		

	/* SIGNALS */

	/* PROPERTIES

	Capabilities  readable   u
	Bitmask of MMModemLocationSource values, specifying the supported location sources.

	SupportedAssistanceData  readable   u
	Bitmask of MMModemLocationAssistanceDataType values, specifying the supported types of assistance data.

	Enabled  readable   u
	Bitmask specifying which of the supported MMModemLocationSource location sources is currently enabled in the device.

	SignalsLocation  readable   b
	TRUE if location updates will be emitted via D-Bus signals, FALSE if location updates will not be emitted.
	See the Setup() method for more information.

	Location  readable   a{uv}
	Dictionary of available location information when location information gathering is enabled. If the modem supports multiple location types it may return more than one here.
	For security reasons, the location information updates via this property are disabled by default. Users can use this property to monitor location updates only if the location signals are enabled with Setup(), but considering that enabling the location signals would allow all users to receive property updates as well, not just the process that enabled them. For a finer grained access control, the user can use the GetLocation() method instead, which may require the client to authenticate itself on every call.
	This dictionary is composed of a MMModemLocationSource key, with an associated data which contains type-specific location information:

	MM_MODEM_LOCATION_SOURCE_3GPP_LAC_CI
		Devices supporting this capability return a string in the format "MCC,MNC,LAC,CI,TAC" (without the quotes of course) where the following applies:
			MCC: This is the three-digit ITU E.212 Mobile Country Code of the network provider to which the mobile is currently registered. e.g. "310".
			MNC: This is the two- or three-digit GSM Mobile Network Code of the network provider to which the mobile is currently registered. e.g. "26" or "260".
			LAC: This is the two-byte Location Area Code of the GSM/UMTS base station with which the mobile is registered, in upper-case hexadecimal format without leading zeros, as specified in 3GPP TS 27.007. E.g. "84CD".
			CI: This is the two- or four-byte Cell Identifier with which the mobile is registered, in upper-case hexadecimal format without leading zeros, as specified in 3GPP TS 27.007. e.g. "2BAF" or "D30156".
			TAC: This is the two- or three-byte Tracking Area Code of the LTE/5GNR base station with which the mobile is registered, in upper-case hexadecimal format without leading zeros, as specified in 3GPP TS 27.007. E.g. "6FFE". Since 1.10.
		The entire string may only be composed of the ASCII digits [0-9], the alphabetical characters [A-F], and the comma (,) character. No other characters are allowed. For example: "310,260,8BE3,2BAF" or "250,40,CE00,1CEAD8F0".
		If any of these four items (MCC, MNC, LAC, CI) is unknown or the mobile is not registered with a network, then the MM_MODEM_LOCATION_SOURCE_3GPP_LAC_CI location information item should not be provided as a returned value from the GetLocation() method or in the "Location" property.
	MM_MODEM_LOCATION_SOURCE_GPS_RAW
		Devices supporting this capability return a D-Bus dictionary (signature "a{sv}") mapping well-known keys to values with defined formats.
		The allowed key/value pairs and their formats are:
		"utc-time": (Required) UTC time in ISO 8601 format, given as a string value (signature "s"). e.g. 203015.
		"latitude": (Required) Latitude in Decimal Degrees (positive numbers mean N quadrasphere, negative mean S quadrasphere), given as a double value (signature "d"). e.g. 38.889722, meaning 38d 53' 22" N.
		"longitude": (Required) Longitude in Decimal Degrees (positive numbers mean E quadrasphere, negative mean W quadrasphere), given as a double value (signature "d"). e.g. -77.008889, meaning 77d 0' 32" W.
		"altitude": (Optional) Altitude above sea level in meters, given as a double value (signature "d"). e.g. 33.5.
	MM_MODEM_LOCATION_SOURCE_GPS_NMEA
		Devices supporting this capability return a string containing one or more NMEA sentences (D-Bus signature 's'). The manager will cache the most recent NMEA sentence of each type for a period of time not less than 30 seconds. When reporting multiple NMEA sentences, sentences shall be separated by an ASCII Carriage Return and Line Feed (<CR><LF>) sequence.
		For example, if the device sends a $GPRMC sentence immediately followed by a $GPGGA sentence, the reported location string would be (where of course the <CR><LF> is replaced with the actual ASCII CR (0x0D) and LF (0x0A) control characters): $GPRMC,134523.92,V,,,,,,,030136,,,N*73<CR><LF>$GPGGA,,,,,,0,00,0.5,,M,0.0001999,M,0.0000099,0000*45
		If the device sends a new $GPRMC three seconds later, the new $GPRMC replaces the previously received $GPRMC sentence, and the updated string would be: $GPRMC,134526.92,V,,,,,,,030136,,,N*76<CR><LF>$GPGGA,,,,,,0,00,0.5,,M,0.0001999,M,0.0000099,0000*45
		If the device then sends a $GPGSA sentence about 5 seconds later, the $GPGSA sentence is added to the string (since no $GPGSA sentence was obviously received in this session), the updated string would be: $GPRMC,134526.92,V,,,,,,,030136,,,N*76<CR><LF>$GPGGA,,,,,,0,00,0.5,,M,0.0001999,M,0.0000099,0000*45<CR><LF>$GPGSA,A,1,,,,,,,,,,,,,1.1,0.5,1.0*34
		//The manager may discard any cached sentences older than 30 seconds.
		// This allows clients to read the latest positioning data as soon as possible after they start, even if the device is not providing frequent location data updates.
	MM_MODEM_LOCATION_SOURCE_CDMA_BS
		Devices supporting this capability return a D-Bus dictionary (signature "a{sv}") mapping well-known keys to values with defined formats.
		The allowed key/value pairs and their formats are:
		"latitude": (Required) Latitude in Decimal Degrees (positive numbers mean N quadrasphere, negative mean S quadrasphere), given as a double value (signature "d"). e.g. 38.889722, meaning 38d 53' 22" N.
		"longitude": (Required) Longitude in Decimal Degrees (positive numbers mean E quadrasphere, negative mean W quadrasphere), given as a double value (signature "d"). e.g. -77.008889, meaning 77d 0' 32" W.

	SuplServer  readable   s
	SUPL server configuration for A-GPS, given either as IP:PORT or FQDN:PORT.

	AssistanceDataServers  readable   as
	URLs from where the user can download assistance data files to inject with InjectAssistanceData().

	GpsRefreshRate  readable   u
	Rate of refresh of the GPS information in the interface.

	*/
}



