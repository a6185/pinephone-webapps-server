// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Modem3gpp.ProfileManager — The ModemManager 3GPP profile management interface.

// This interface provides access to actions with connection profiles.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.
// The user of the interface can optionally choose to use the new profile management methods to manage the connection setup, e.g by using the new "profile-id" setting in either the CreateBearer or the Connect methods. If that's the case, it is suggested that the legacy approach of not using the profiles is completely avoided. If both approaches are used at the same time, it may happen that a connection attempt not using the "profile-id" implicitly updates a given profile (without emitting Updated), as the amount of profiles implemented in modems may be fixed.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_3gpp_ProfileManager extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Modem3gpp.ProfileManager'
		});
	}

	/* METHODS */

	list () {
		// List (OUT aa{sv} profiles);
		// Lists the available profiles or contexts provisioned on the modem.
		// Profiles are represented as dictionaries of properties, and any of the 3GPP-specific properties defined in the bearer properties are allowed.
		// Depending on the implementation, the settings applicable to the initial EPS bearer given in bearer properties may also be reported as an item in the returned list, identified by the MM_BEARER_APN_TYPE_INITIAL "apn-type" flag.
		// OUT aa{sv} profiles: An array of dictionaries containing the properties of the provisioned profiles.
		return this._send('List');
	}		
	set (requested_properties) {
		// Set (IN  a{sv} requested_properties, OUT a{sv} stored_properties);
		// Creates or updates a connection profile on this modem. If "profile-id" is not given, a new profile will be created; otherwise, the profile with the given ID will be updated.
		// Profiles are represented as dictionaries of properties, and any of the 3GPP-specific properties defined in the bearer properties are allowed. The real list of supported properties really depends on the underlying protocol and implementation, though; e.g. in AT-based modems setting "apn-type" won't be supported, and instead the user should give that setting explicitly when creating the bearer object.
		// The operation may fail if it is attempting to update an existing profile for which connected bearer objects already exist. In this case, the user should make sure these bearer objects are already disconnected before attempting to change the profile settings.
		// The operation may also fail if it is attempting to update the profile associated to the settings of the initial EPS bearer, identified by the MM_BEARER_APN_TYPE_INITIAL "apn-type" flag. In this case, SetInitialEpsBearerSettings() should be used instead.
		// The output stored_properties will contain the settings that were successfully stored, including the new "profile-id" if the operation was creating a new profile.
		// IN a{sv} requested_properties: the requested profile properties.
		// OUT a{sv} stored_properties: the stored profile properties.
		return this._send('Set', {
			signature: 'a{sv}',
			body: [
				requested_properties
			]
		});
	}		
	delete (properties) {
		// Delete (IN  a{sv} properties);
		// Deletes the profile with the "profile-id" given in properties.
		// If additional settings are given in properties they are ignored. This allows the user to easily request the deletion of a profile that has been provided in the List() operation.
		// This method may just clear the existing profiles (i.e. reseting all the properties to defaults) instead of fully removing them if the profiles cannot be fully removed. In this case, the method will succeed, but the size of the list of profiles will not change.
		// This method will fail if "profile-id" is not given.
		// The operation may fail if it is attempting to delete a profile for which connected bearer objects already exist. In this case, the user should make sure these bearer objects are already disconnected before attempting to delete the profile.
		// The operation may also fail if it is attempting to delete the profile associated to the settings of the initial EPS bearer, identified by the MM_BEARER_APN_TYPE_INITIAL "apn-type" flag. In this case, SetInitialEpsBearerSettings() may be used instead to clear these settings.
		// IN a{sv} properties: the profile properties.
		return this._send('Delete', {
			signature: 'a{sv}',
			body: [
				properties
			]
		});
	}			

	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'Updated': () => {
				// Updated ();
				// Emitted when the profiles are updated by the network through OTA procedures.
				_this._debug(3,`Notification > Updated`);
				_this._Ctrl.slots('Updated', null);
			}
		});
	}

	/* PROPERTIES */
}