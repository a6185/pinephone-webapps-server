// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Call — The ModemManager Call interface.
// The Call interface Defines operations and properties of a single Call.

class DBus_Int__MM1__MM1_Call__MM1_Call extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Call/' + number
			'interface': 'org.freedesktop.ModemManager1.Call'
		});
	}

	/* METHODS */
	
	start () {
		// Start ();
		// If the outgoing call has not yet been started, start it.
		// Applicable only if state is MM_CALL_STATE_UNKNOWN and direction is MM_CALL_DIRECTION_OUTGOING. 
		return this._send('Start');
	}
	accept () {
		// Accept ();
		// Accept incoming call (answer).
		// Applicable only if state is MM_CALL_STATE_RINGING_IN and direction is MM_CALL_DIRECTION_INCOMING. 
		return this._send('Accept');
	}
	deflect (number) {
		// Deflect (IN  s number);
		// Deflect an incoming or waiting call to a new number. This call will be considered terminated once the deflection is performed.
		// Applicable only if state is MM_CALL_STATE_RINGING_IN or MM_CALL_STATE_WAITING and direction is MM_CALL_DIRECTION_INCOMING. 
		return this._send('Deflect', {
			signature: 's',
			body: [
				number
			]
		});
	}	
	join_multiparty () {
		// JoinMultiparty ();
		// Join the currently held call into a single multiparty call with another already active call.
		// The calls will be flagged with the 'Multiparty' property while they are part of the multiparty call.
		// Applicable only if state is MM_CALL_STATE_HELD. 
		return this._send('JoinMultiparty');
	}
	leave_multiparty () {
		// LeaveMultiparty ();
		// If this call is part of an ongoing multiparty call, detach it from the multiparty call, put the multiparty call on hold, and activate this one alone. This operation makes this call private again between both ends of the call.
		// Applicable only if state is MM_CALL_STATE_ACTIVE or MM_CALL_STATE_HELD and the call is a multiparty call. 
		return this._send('LeaveMultiparty');
	}
	hangup () {
		// Hangup the active call.
		// Applicable only if state is MM_CALL_STATE_UNKNOWN. 
		return this._send('Hangup');
	}
	send_dtmf (symbol) {
		//SendDtmf (IN  s dtmf);
		// Send a DTMF tone (Dual Tone Multi-Frequency) (only on supported modem).
		// Applicable only if state is MM_CALL_STATE_ACTIVE. 
		// IN s dtmf: DTMF tone identifier [0-9A-D*#].
		return this._send('SendDtmf', {
			signature: 's',
			body: [
				symbol
			]
		});
	}
	
	/* SIGNALS */
	
	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'DtmfReceived': (dtmf) => {
				// DtmfReceived (s dtmf);
				// Emitted when a DTMF tone is received (only on supported modem) 
				// s dtmf: DTMF tone identifier [0-9A-D*#].
				_this._debug(3,`Notification > DtmfReceived: ${dtmf} argument(s)`);
				_this._Ctrl.slots('DtmfReceived', dtmf);
				
			},
			'StateChanged': (state_old, state_new, state_reason) => {
				// StateChanged (i old, i new, u reason);
				// Emitted when call changes state
				// i old: Old state MMCallState
				// i new: New state MMCallState
				// u reason: A MMCallStateReason value, specifying the reason for this state change.
				_this._debug(3,`Notification > StateChanged: old=${state_old} new=${state_new} reason=${state_reason}`);
				_this._Ctrl.slots('StateChanged', state_old, state_new, state_reason);
			}
		});
	}		
	
	/* PROPERTIES

{
   State: Variant { signature: 'i', value: 0 },
   StateReason: Variant { signature: 'i', value: 0 },
   Direction: Variant { signature: 'i', value: 2 },
   Number: Variant { signature: 's', value: '0389XXXXXX' },
   Multiparty: Variant { signature: 'b', value: false },
   AudioPort: Variant { signature: 's', value: '' },
   AudioFormat: Variant { signature: 'a{sv}', value: {} }
}

	State  readable   i
	A MMCallState value, describing the state of the call. 

	StateReason  readable   i
	A MMCallStateReason value, describing why the state is changed. 

	Direction  readable   i
	A MMCallDirection value, describing the direction of the call. 

	Number  readable   s
	The remote phone number. 

	Multiparty  readable   b
	Whether the call is currently part of a multiparty conference call. 

	AudioPort  readable   s
	If call audio is routed via the host, the name of the kernel device that provides the audio. For example, with certain Huawei USB modems, this property might be "ttyUSB2" indicating audio is available via ttyUSB2 in the format described by the AudioFormat property. 

	AudioFormat  readable   a{sv}
	If call audio is routed via the host, a description of the audio format supported by the audio port.
	This property may include the following items: 
	"encoding" The audio encoding format. For example, "pcm" for PCM audio.
	"resolution" 	The sampling precision and its encoding format. For example, "s16le" for signed 16-bit little-endian samples.
	"rate"	The sampling rate as an unsigned integer. For example, 8000 for 8000hz. 

	*/

}
