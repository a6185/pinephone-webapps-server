/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.ModemManager1.Sim — The ModemManager SIM interface.

class DBus_Int__MM1__MM1_SIM__ModemManager1_Sim extends PP_DBusSystemInterface {	
	constructor (_Ctrl, dbus_path) {
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/SIM/' + number
			'interface': 'org.freedesktop.ModemManager1.Sim'
		});
	}

	/* METHODS */

	send_pin (pin) {
		// SendPin (IN  s pin);
		// Send the PIN to unlock the SIM card.
		// IN s pin: A string containing the PIN code.
		return this._send('SendPin', {
			signature: 's',
			body: [
				pin
			]
		});
	}
	send_puk (puk, pin) {
		// SendPuk (IN  s puk, IN  s pin);
		// Send the PUK and a new PIN to unlock the SIM card.
		// IN s puk: A string containing the PUK code.
		// IN s pin: A string containing the PIN code.
		return this._send('SendPuk', {
			signature: 'ss',
			body: [
				puk, pin
			]
		});
	}
	enable_pin (pin, enabled) {
		// EnablePin (IN  s pin, IN  b enabled);
		// Enable or disable the PIN checking.
		// IN s pin: A string containing the PIN code.
		// IN b enabled: TRUE to enable PIN checking, FALSE otherwise.
		return this._send('EnablePin', {
			signature: 'sb',
			body: [
				pin, enabled
			]
		});
	}
	change_pin (old_pin, new_pin) {
		// ChangePin (IN  s old_pin, IN  s new_pin);
		// Change the PIN code.
		// IN s old_pin: A string containing the current PIN code.
		// IN s new_pin: A string containing the new PIN code.
		return this._send('ChangePin', {
			signature: 'ss',
			body: [
				old_pin, new_pin
			]
		});
	}

	/* SIGNALS */

	/* PROPERTIES

	SimIdentifier  readable   s
	An obfuscated SIM identifier based on the IMSI or the ICCID.
	This may be available before the PIN has been entered depending on the device itself. 

	Imsi  readable   s
	The IMSI of the SIM card, if any. 

	The "OperatorIdentifier" property
	OperatorIdentifier  readable   s

	The "OperatorName" property
	OperatorName  readable   s
	The name of the network operator, as given by the SIM card, if known. 

	*/
}