// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Simple — The ModemManager Simple interface.

// The Simple interface allows controlling and querying the status of Modems.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Simple extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Simple'
		});
	}

	/* METHODS */

	connect (properties) {
		// Connect (IN a{sv} properties, OUT o bearer);
		// Do everything needed to connect the modem using the given properties.
		// This method will attempt to find a matching packet data bearer and activate it if necessary, returning the bearer's IP details. If no matching bearer is found, a new bearer will be created and activated, but this operation may fail if no resources are available to complete this connection attempt (ie, if a conflicting bearer is already active).
		// This call may make a large number of changes to modem configuration based on properties passed in. For example, given a PIN-locked, disabled GSM/UMTS modem, this call may unlock the SIM PIN, wait for network registration (or force registration to a specific provider), create a new packet data bearer using the given "apn", and connect that bearer.
		// The list of allowed properties includes all the ones defined in the bearer properties plus these additional ones that are only applicable to this method, and only to 3GPP (GSM/UMTS/LTE/5GNR) devices:
		// "pin": SIM-PIN unlock code, given as a string value (signature "s").
		// "operator-id": ETSI MCC-MNC of a network to force registration with, given as a string value (signature "s").
		// There are no settings specific to this call that would apply to 3GPP2 (CDMA/EVDO) devices.
		// IN a{sv} properties: Dictionary of properties needed to get the modem connected.
		// OUT o bearer: On successful connect, returns the object path of the connected packet data bearer used for the connection attempt.
		return this._send('Connect', {
			signature: 'a{sv}',
			body: [
				properties
			]
		});
	}			
	disconnect () {
		// Disconnect (IN  o bearer);
		// data bearer, while if "/" (ie, no object given) this method will disconnect all active packet data bearers.
		// Disconnect an active packet data connection.
		// IN o bearer: If given this method will disconnect the referenced packet
		return this._send('Disconnect', {
			signature: 'o',
			body: [
				bearer
			]
		});
	}	
	get_status () {
		// GetStatus (OUT a{sv} properties);
		// Get the general modem status.
		// The predefined common properties returned are:
		// "state": A MMModemState value specifying the overall state of the modem, given as an unsigned integer value (signature "u").
		// "signal-quality": Signal quality value, given only when registered, as unsigned integer value and an additional boolean value indicating if the value was recently taken. (signature "(ub)").
		// "current-bands": List of MMModemBand values, given only when registered, as a list of unsigned integer values (signature "au").
		// "access-technologies": A MMModemAccessTechnology value, given only when registered, as an unsigned integer value (signature "u").
		// "m3gpp-registration-state": A MMModem3gppRegistrationState value specifying the state of the registration, given only when registered in a 3GPP network, as an unsigned integer value (signature "u").
		// "m3gpp-operator-code": Operator MCC-MNC, given only when registered in a 3GPP network, as a string value (signature "s").
		// "m3gpp-operator-name": Operator name, given only when registered in a 3GPP network, as a string value (signature "s").
		// "cdma-cdma1x-registration-state": A MMModemCdmaRegistrationState value specifying the state of the registration, given only when registered in a CDMA1x network, as an unsigned integer value (signature "u").
		// "cdma-evdo-registration-state": A MMModemCdmaRegistrationState value specifying the state of the registration, given only when registered in a EV-DO network, as an unsigned integer value (signature "u").
		// "cdma-sid": The System Identifier of the serving network, if registered in a CDMA1x network and if known. Given as an unsigned integer value (signature "u").
		// "cdma-nid": The Network Identifier of the serving network, if registered in a CDMA1x network and if known. Given as an unsigned integer value (signature "u").
		// OUT a{sv} properties: Dictionary of properties.
		return this._send('GetStatus');
	}

	/* NO SIGNALS */
	
	/* NO PROPERTIES */
}
