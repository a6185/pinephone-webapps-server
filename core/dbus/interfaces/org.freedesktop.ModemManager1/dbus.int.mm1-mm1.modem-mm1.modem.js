// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem — The ModemManager Modem interface.

class DBus_Int__MM1__MM1_Modem__MM1_Modem extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem'
		});
	}

	/* METHODS */
	
	enable (enable) {
		// Enable (IN  b enable);
		// Enable or disable the modem.
		// When enabled, the modem's radio is powered on and data sessions, voice calls, location services, and Short Message Service may be available.
		// When disabled, the modem enters low-power state and no network-related operations are available. 
		// IN b enable: TRUE to enable the modem and FALSE to disable it.
		return this._send('Enable', {
			signature: 'b',
			body: [
				enable
			]
		});
	}
	create_bearers (properties) {
		// CreateBearer (IN  a{sv} properties, OUT o path);
		// Create a new packet data bearer using the given characteristics.
		// This request may fail if the modem does not support additional bearers, if too many bearers are already defined, or if properties are invalid.
		// The properties allowed are any of the ones defined in the bearer properties. 
		// IN a{sv} properties: Dictionary of properties needed to get the bearer connected.
		// OUT o path: On success, the object path of the newly created bearer.
		return this._send('CreateBearer', {
			signature: 'a{sv}',
			body: [
				properties
			]
		});
	}
	delete_bearer (bearer) {
		// DeleteBearer (IN  o bearer);
		// Delete an existing packet data bearer.
		// If the bearer is currently active and providing packet data server, it will be disconnected and that packet data service will terminate.
		return this._send('DeleteBearer');
	}
	reset () {
		// Reset ();
		// Clear non-persistent configuration and state, and return the device to a newly-powered-on state.
		// This command may power-cycle the device. 
		return this._send('Reset');
	}
	factory_reset (code) {
		// FactoryReset (IN  s code);
		// Clear the modem's configuration (including persistent configuration and state), and return the device to a factory-default state.
		// If not required by the modem, code may be ignored.
		// This command may or may not power-cycle the device. 
		// IN s code: Carrier-supplied code required to reset the modem.
		return this._send('FactoryReset', {
			signature: 's',
			body: [
				code
			]
		});
	}
	set_power_state (state) {
		// SetPowerState (IN  u state);
		// Set the power state of the modem. This action can only be run when the modem is in MM_MODEM_STATE_DISABLED state. 
		// IN u state: A MMModemPowerState value, to specify the desired power state.
		return this._send('SetPowerState', {
			signature: 's',
			body: [
				state
			]
		});
	}
	set_current_capabilities (capabilities) {
		// SetCurrentCapabilities (IN  u capabilities);
		// Set the capabilities of the device.
		// The given bitmask should be supported by the modem, as specified in the "SupportedCapabilities" property.
		// This command may power-cycle the device. 
		// IN u capabilities: Bitmask of MMModemCapability values, to specify the capabilities to use.
		return this._send('SetCurrentCapabilities', {
			signature: 'u',
			body: [
				capabilities
			]
		});
	}
	set_current_modes (modes) {
		// SetCurrentModes (IN  (uu) modes);
		// Set the access technologies (e.g. 2G/3G/4G preference) the device is currently allowed to use when connecting to a network.
		// The given combination should be supported by the modem, as specified in the "SupportedModes" property. 
		// IN (uu) modes: A pair of MMModemMode values, where the first one is a bitmask of allowed modes, and the second one the preferred mode, if any.
		return this._send('SetCurrentModes', {
			signature: '(uu)',
			body: [
				modes
			]
		});
	}
	set_current_bands (bands) {
		// SetCurrentBands (IN  au bands);
		// Set the radio frequency and technology bands the device is currently allowed to use when connecting to a network. 
		// IN au bands: List of MMModemBand values, to specify the bands to be used.
		return this._send('SetCurrentBands', {
			signature: 'au',
			body: [
				bands
			]
		});
	}
	set_primary_sim_slot (sim_slot) {
		// SetPrimarySimSlot (IN  u sim_slot);
		// Selects which SIM slot to be considered as primary, on devices that expose multiple slots in the "SimSlots" property.
		// When the switch happens the modem may require a full device reprobe, so the modem object in DBus will get removed, and recreated once the selected SIM slot is in use.
		// There is no limitation on which SIM slot to select, so the user may also set as primary a slot that doesn't currently have any valid SIM card inserted. 
		return this._send('SetPrimarySimSlot', {
			signature: 'u',
			body: [
				sim_slot
			]
		});
	}
	command (cmd, timeout) {
		// Command (IN  s cmd, IN  u timeout, OUT s response);
		// Send an arbitrary AT command to a modem and get the response.
		// Note that using this interface call is only allowed when running ModemManager in debug mode or if the project was built using the with-at-command-via-dbus configure option. 
		// IN s cmd: The command string, e.g. "AT+GCAP" or "+GCAP" (leading AT is inserted if necessary).
		// IN u timeout: The number of seconds to wait for a response.
		// OUT s response: The modem's response.
		return this._send('Command', {
			signature: 'su',
			body: [
				cmd, timeout
			]
		});
	}

	/* SIGNALS */

	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'StateChanged': (state_old, state_new, state_reason) => { // Object Path
				// The "StateChanged" signal
				// StateChanged (i old, i new, u reason);
				// The modem's state (see "State") changed. 
				// i old: A MMModemState value, specifying the new state.
				// i new: A MMModemState value, specifying the new state.
				// u reason: A MMModemStateChangeReason value, specifying the reason for this state change.
				_this._debug(3,`Notification > StateChanged: old=${state_old} new=${state_new} reason=${state_reason}`);
				_this._Ctrl.slots('StateChanged', state_old, state_new, state_reason);
			}
		});
	}	

	/* PROPERTIES	

	Sim  readable   o
	The path of the primary active SIM object available in this device, if any.
	This SIM object is the one used for network registration and data connection setup.
	If multiple org.freedesktop.ModemManager1.Modem.SimSlots are supported, the org.freedesktop.ModemManager1.Modem.PrimarySimSlot index value specifies which is the slot number where this SIM card is available.

	SimSlots  readable   ao
	The list of SIM slots available in the system, including the SIM object paths if the cards are present. If a given SIM slot at a given index doesn't have a SIM card available, an empty object path will be given.
	The length of this array of objects will be equal to the amount of available SIM slots in the system, and the index in the array is the slot index.
	This list includes the SIM object considered as primary active SIM slot (org.freedesktop.ModemManager1.Modem.Sim) at index org.freedesktop.ModemManager1.Modem.ActiveSimSlot.

	PrimarySimSlot  readable   u
	The index of the primary active SIM slot in the org.freedesktop.ModemManager1.Modem.SimSlots array, given in the [1,N] range.
	If multiple SIM slots aren't supported, this property will report value 0.
	In a Multi SIM Single Standby setup, this index identifies the only SIM that is currently active. All the remaining slots will be inactive.
	In a Multi SIM Multi Standby setup, this index identifies the active SIM that is considered primary, i.e. the one that will be used when a data connection is setup.

	Bearers  readable   ao
	The list of bearer object paths (EPS Bearers, PDP Contexts, or CDMA2000 Packet Data Sessions) as requested by the user.
	This list does not include the initial EPS bearer details (see "InitialEpsBearer").

	SupportedCapabilities  readable   au
	List of MMModemCapability bitmasks, specifying the combinations of generic family of access technologies the modem supports.
	If the modem doesn't allow changing the current capabilities, the list will report one single entry with the same bitmask as in "CurrentCapabilities".
	Only multimode devices implementing both 3GPP (GSM/UMTS/LTE/5GNR) and 3GPP2 (CDMA/EVDO) specs will report more than one combination of capabilities.

	CurrentCapabilities  readable   u
	Bitmask of MMModemCapability values, specifying the currently used generic family of access technologies.
	This bitmask will be one of the ones listed in "SupportedCapabilities".

	MaxActiveBearers  readable   u
	The maximum number of active MM_BEARER_TYPE_DEFAULT bearers that may be explicitly enabled by the user without multiplexing support.
	POTS and CDMA2000-only devices support one active bearer, while GSM/UMTS and LTE/5GNR capable devices (including 3GPP+3GPP3 multimode devices) may support one or more active bearers, depending on the amount of physical ports exposed by the device.

	MaxActiveMultiplexedBearers  readable   u
	The maximum number of active MM_BEARER_TYPE_DEFAULT bearers that may be explicitly enabled by the user with multiplexing support on one single network interface.
	If the modem doesn't support multiplexing of data sessiones, a value of 0 will be reported.

	Manufacturer  readable   s
	The equipment manufacturer, as reported by the modem.

	Model  readable   s
	The equipment model, as reported by the modem.

	Revision  readable   s
	The revision identification of the software, as reported by the modem.

	CarrierConfiguration  readable   s
	The description of the carrier-specific configuration (MCFG) in use by the modem.

	CarrierConfigurationRevision  readable   s
	The revision identification of the carrier-specific configuration (MCFG) in use by the modem.

	HardwareRevision  readable   s
	The revision identification of the hardware, as reported by the modem.

	DeviceIdentifier  readable   s
	A best-effort device identifier based on various device information like model name, firmware revision, USB/PCI/PCMCIA IDs, and other properties.
	This ID is not guaranteed to be unique and may be shared between identical devices with the same firmware, but is intended to be "unique enough" for use as a casual device identifier for various user experience operations.
	This is not the device's IMEI or ESN since those may not be available before unlocking the device via a PIN.

	Device  readable   s
	The physical modem device reference (ie, USB, PCI, PCMCIA device), which may be dependent upon the operating system.
	In Linux for example, this points to a sysfs path of the usb_device object.
	This value may also be set by the user using the MM_ID_PHYSDEV_UID udev tag (e.g. binding the tag to a specific sysfs path).

	Drivers  readable   as
	The Operating System device drivers handling communication with the modem hardware.

	Plugin  readable   s
	The name of the plugin handling this modem.

	PrimaryPort  readable   s
	The name of the primary port using to control the modem.

	Ports  readable   a(su)
	The list of ports in the modem, given as an array of string and unsigned integer pairs. The string is the port name or path, and the integer is the port type given as a MMModemPortType value.

	EquipmentIdentifier  readable   s
	The identity of the device.
	This will be the IMEI number for GSM devices and the hex-format ESN/MEID for CDMA devices.

	UnlockRequired  readable   u
	Current lock state of the device, given as a MMModemLock value.

	UnlockRetries  readable   a{uu}
	A dictionary in which the keys are MMModemLock flags, and the values are integers giving the number of PIN tries remaining before the code becomes blocked (requiring a PUK) or permanently blocked. Dictionary entries exist only for the codes for which the modem is able to report retry counts.

	State  readable   i
	Overall state of the modem, given as a MMModemState value.
	If the device's state cannot be determined, MM_MODEM_STATE_UNKNOWN will be reported.

	StateFailedReason  readable   u
	Error specifying why the modem is in MM_MODEM_STATE_FAILED state, given as a MMModemStateFailedReason value.

	AccessTechnologies  readable   u
	Bitmask of MMModemAccessTechnology values, specifying the current network access technologies used by the device to communicate with the network.
	If the device's access technology cannot be determined, MM_MODEM_ACCESS_TECHNOLOGY_UNKNOWN will be reported.

	SignalQuality  readable   (ub)
	Signal quality in percent (0 - 100) of the dominant access technology the device is using to communicate with the network. Always 0 for POTS devices.
	The additional boolean value indicates if the quality value given was recently taken.

	OwnNumbers  readable   as
	List of numbers (e.g. MSISDN in 3GPP) being currently handled by this modem.

	*/
}