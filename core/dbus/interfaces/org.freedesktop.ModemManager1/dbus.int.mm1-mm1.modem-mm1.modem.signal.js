// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Signal — The ModemManager Signal interface.

// This interface provides access to extended signal quality information.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Signal extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Signal'
		});
	}

	/* METHODS */

	setup (rate) {
		// Setup (IN  u rate);
		// Setup extended signal quality information retrieval.
		// IN u rate: refresh rate to set, in seconds. 0 to disable retrieval.
		return this._send('Setup', {
			signature: 'u',
			body: [
				rate
			]
		});
	}	

	/* SIGNALS */

	/* PROPERTIES

	Rate  readable   u
	Refresh rate for the extended signal quality information updates, in seconds. A value of 0 disables the retrieval of the values.

	Cdma  readable   a{sv}
	Dictionary of available signal information for the CDMA1x access technology.
	This dictionary is composed of a string key, with an associated data which contains type-specific information.
	"rssi": The CDMA1x RSSI (Received Signal Strength Indication), in dBm, given as a floating point value (signature "d").
	"ecio": The CDMA1x Ec/Io, in dBm, given as a floating point value (signature "d").

	Evdo  readable   a{sv}
	Dictionary of available signal information for the CDMA EV-DO access technology.
	This dictionary is composed of a string key, with an associated data which contains type-specific information.
	"rssi": The CDMA EV-DO RSSI (Received Signal Strength Indication), in dBm, given as a floating point value (signature "d").
	"ecio": The CDMA EV-DO Ec/Io, in dBm, given as a floating point value (signature "d").
	"sinr": CDMA EV-DO SINR level, in dB, given as a floating point value (signature "d").
	"io": The CDMA EV-DO Io, in dBm, given as a floating point value (signature "d").

	Gsm  readable   a{sv}
	Dictionary of available signal information for the GSM/GPRS access technology.
	This dictionary is composed of a string key, with an associated data which contains type-specific information.
	"rssi": The GSM RSSI (Received Signal Strength Indication), in dBm, given as a floating point value (signature "d").

	Umts  readable   a{sv}
	Dictionary of available signal information for the UMTS (WCDMA) access technology.
	This dictionary is composed of a string key, with an associated data which contains type-specific information.
	"rssi": The UMTS RSSI (Received Signal Strength Indication), in dBm, given as a floating point value (signature "d").
	"rscp": The UMTS RSCP (Received Signal Code Power), in dBm, given as a floating point value (signature "d").
	"ecio": The UMTS Ec/Io, in dB, given as a floating point value (signature "d").

	Lte  readable   a{sv}
	Dictionary of available signal information for the LTE access technology.
	This dictionary is composed of a string key, with an associated data which contains type-specific information.
	"rssi": The LTE RSSI (Received Signal Strength Indication), in dBm, given as a floating point value (signature "d").
	"rsrq": The LTE RSRQ (Reference Signal Received Quality), in dB, given as a floating point value (signature "d").
	"rsrp": The LTE RSRP (Reference Signal Received Power), in dBm, given as a floating point value (signature "d").
	"snr": The LTE S/R ratio, in dB, given as a floating point value (signature "d").

	Nr5g  readable   a{sv}
	Dictionary of available signal information for the 5G access technology.
	This dictionary is composed of a string key, with an associated data which contains type-specific information.
	"rsrq": The 5G RSRQ (Reference Signal Received Quality), in dB, given as a floating point value (signature "d").
	"rsrp": The 5G (Reference Signal Received Power), in dBm, given as a floating point value (signature "d").
	"snr": The 5G S/R ratio, in dB, given as a floating point value (signature "d").

	*/
}