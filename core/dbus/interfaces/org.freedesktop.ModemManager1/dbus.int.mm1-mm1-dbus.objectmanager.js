/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Int__MM1__MM1__DBus_ObjectManager extends PP_DBusSystemInterface {	
	constructor (_Ctrl) {
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: '/org/freedesktop/ModemManager1',
			'interface': 'org.freedesktop.DBus.ObjectManager'
		});
	}
	
	/* METHODS */
	
	async get_managed_objects () {
		/*
		{
			'/org/freedesktop/ModemManager1/Modem/2': {
				'org.freedesktop.ModemManager1.Modem': {
					'AccessTechnologies': 16384,
					'Bearers': [],
					'CarrierConfiguration': 'ROW_Generic_3GPP',
					'CarrierConfigurationRevision': '0501081F',
					'CurrentBands': [1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 31, 32, 33, 34, 35, 37, 38, 42, 43, 48, 49, 50, 55, 56, 58, 68, 69, 70, 71, 219],
					'CurrentCapabilities': 12,
					'CurrentModes': (14, 8),
					'Device': '/sys/devices/platform/soc/1c1b000.usb/usb2/2-1',
					'DeviceIdentifier': '31452ea9c94f26a4af5f4775ed8603b5267afdf5',
					'Drivers': ['qmi_wwan', 'option'],
					'EquipmentIdentifier': '86XXXXXXXXXXXXX',
					'HardwareRevision': '10000',
					'Manufacturer': 'QUALCOMM '
						'INCORPORATED',
					'MaxActiveBearers': 1,
					'MaxActiveMultiplexedBearers': 254,
					'MaxBearers': 1,
					'Model': 'QUECTEL '
						'Mobile '
						'Broadband '
						'Module',
					'OwnNumbers': ['336XXXXXXXX'],
					'Plugin': 'quectel',
					'Ports': [
						('cdc-wdm0', 6),
						('ttyUSB0', 4),
						('ttyUSB1', 5),
						('ttyUSB2', 3),
						('ttyUSB3', 3),
						('wwan0', 2)
					],
					'PowerState': 3,
					'PrimaryPort': 'cdc-wdm0',
					'PrimarySimSlot': 0,
					'Revision': 'EG25GGBR07A08M2G',
					'SignalQuality': (65, True),
					'Sim': '/org/freedesktop/ModemManager1/SIM/1',
					'SimSlots': [],
					'State': 8,
					'StateFailedReason': 0,
					'SupportedBands': [1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 31, 32, 33, 34, 35, 37, 38, 42, 43, 48, 49, 50, 55, 56, 58, 68, 69, 70, 71, 219],
					'SupportedCapabilities': [12],
					'SupportedIpFamilies': 7,
					'SupportedModes': [(2, 0), (4, 0), (8, 0), (6, 4), (6, 2), (10, 8), (10, 2), (12, 8), (12, 4), (14, 8), (14, 4), (14, 2)],
					'UnlockRequired': 3,
					'UnlockRetries': {2: 3, 3: 3, 4: 10, 5: 10}
				},
				'org.freedesktop.ModemManager1.Modem.Firmware': {
					'UpdateSettings': (1, {
						'device-ids': [
							'USB\\VID_2C7C&PID_0125&REV_0318&NAME_EG25GGB',
							'USB\\VID_2C7C&PID_0125&REV_0318',
							'USB\\VID_2C7C&PID_0125',
							'USB\\VID_2C7C'
						],
						'fastboot-at': 'AT+QFASTBOOT',
						'version': 'EG25GGBR07A08M2G_01.001.01.001'
						}
					)
				},
				'org.freedesktop.ModemManager1.Modem.Location': {
					'AssistanceDataServers': [
						'http://xtrapath2.izatcloud.net/xtra3gr.bin',
						'http://xtrapath3.izatcloud.net/xtra3gr.bin',
						'http://xtrapath1.izatcloud.net/xtra3gr.bin'
					],
					'Capabilities': 119,
					'Enabled': 1,
					'GpsRefreshRate': 30,
					'Location': {},
					'SignalsLocation': False,
					'SuplServer': '',
					'SupportedAssistanceData': 1
				},
				'org.freedesktop.ModemManager1.Modem.Messaging': {
					'DefaultStorage': 2,
					'Messages': [
						'/org/freedesktop/ModemManager1/SMS/3',
						'/org/freedesktop/ModemManager1/SMS/2'
					],
					'SupportedStorages': [1, 2]
				},
				'org.freedesktop.ModemManager1.Modem.Modem3gpp': {
					'EnabledFacilityLocks': 3,
					'EpsUeModeOperation': 4,
					'Imei': '86XXXXXXXXXXXXX',
					'InitialEpsBearer': '/org/freedesktop/ModemManager1/Bearer/1',
					'InitialEpsBearerSettings': {
						'allowed-auth': 1,
						'apn': '',
						'ip-type': 4,
						'profile-id': 1
					},
					'OperatorCode': '20815',
					'OperatorName': 'Free',
					'Pco': [],
					'RegistrationState': 1,
					'SubscriptionState': 0
				},
				'org.freedesktop.ModemManager1.Modem.Modem3gpp.ProfileManager': {},
				'org.freedesktop.ModemManager1.Modem.Modem3gpp.Ussd': {
					'NetworkNotification': '',
					'NetworkRequest': '',
					'State': 1
				},
				'org.freedesktop.ModemManager1.Modem.Signal': {
					'Cdma': {},
					'Evdo': {},
					'Gsm': {},
					'Lte': {},
					'Nr5g': {},
					'Rate': 0,
					'Umts': {}
				},
				'org.freedesktop.ModemManager1.Modem.Simple': {},
				'org.freedesktop.ModemManager1.Modem.Time': {
					'NetworkTimezone': {'offset': 120}
				},
				'org.freedesktop.ModemManager1.Modem.Voice': {
					'Calls': [],
					'EmergencyOnly': False
				}
			}
		}
		*/
 		return await this._send('GetManagedObjects');
	}

	/* SIGNALS */
	
	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'InterfacesAdded': (dbus_path, dict) => { // Object Path
				// org.freedesktop.DBus.ObjectManager.InterfacesAdded (OBJPATH object_path, ARRAY of DICT_ENTRY<STRING,ARRAY of DICT_ENTRY<STRING,VARIANT>> interfaces_and_properties);
				// The InterfacesAdded signal is emitted when either a new object is added or when an existing object gains one or more interfaces. 
				// The second parameter of the InterfacesAdded signal contains a dict with the interfaces and properties (if any) that have been added to the given object path.
				// Note that changes on properties on existing interfaces are not reported using this interface - an application should also monitor the existing PropertiesChanged signal on each object
				_this._debug(3,`Notification > InterfacesAdded: ${dbus_path} ${dict}`);
				_this._Ctrl.slots('InterfacesAdded', dbus_path, dict);
			},
			'InterfacesRemoved': (dbus_path, interfaces) => { // Object Path
				// org.freedesktop.DBus.ObjectManager.InterfacesRemoved (OBJPATH object_path, ARRAY<STRING> interfaces);
				// The InterfacesRemoved signal is emitted whenever an object is removed or it loses one or more interfaces.
				// Similarly, the second parameter of the InterfacesRemoved signal contains an array of the interfaces that were removed.
				// Note that changes on properties on existing interfaces are not reported using this interface - an application should also monitor the existing PropertiesChanged signal on each object.
				_this._debug(3,`Notification > InterfacesRemoved: ${dbus_path} ${interfaces}`);
				_this._Ctrl.slots('InterfacesRemoved', dbus_path, interfaces);
			}
		});
	}	
}
