// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Firmware

// This interface provides access to perform different firmware-related operations in the modem, including listing the available firmware images in the module and selecting which of them to use.
// This interface does not provide direct access to perform firmware updates in the device. Instead, it exposes information about the expected firmware update method as well as method-specific details required for the upgrade to happen. The actual firmware upgrade may be performed via the Linux Vendor Firmware Service and the fwupd daemon.
// This interface will always be available as long a the modem is considered valid.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Firmware extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Firmware'
		});
	}

	/* METHODS */

	list () {
		// List (OUT s selected, OUT aa{sv} installed);
		// List installed firmware images.
		// Firmware slots and firmware images are identified by arbitrary opaque strings.
		// Firmware images are represented as dictionaries of properties. Certain properties are pre-defined, and some are required:
		// "image-type": (Required) Type of the firmware image, given as a MMFirmwareImageType value (signature "u"). Firmware images of type MM_FIRMWARE_IMAGE_TYPE_GENERIC will only expose only the mandatory properties.
		// "unique-id": (Required) A user-readable unique ID for the firmware image, given as a string value (signature "s").
		// "gobi-pri-version": (Optional) The version of the PRI firmware image, in images of type MM_FIRMWARE_IMAGE_TYPE_GOBI, given as a string value (signature "s").
		// "gobi-pri-info": (Optional) Additional information of the PRI image, in images of type MM_FIRMWARE_IMAGE_TYPE_GOBI, given as a string value (signature "s").
		// "gobi-boot-version": (Optional) The boot version of the PRI firmware image, in images of type MM_FIRMWARE_IMAGE_TYPE_GOBI, given as a string value (signature "s").
		// "gobi-pri-unique-id": (Optional) The unique ID of the PRI firmware image, in images of type MM_FIRMWARE_IMAGE_TYPE_GOBI, given as a string value (signature "s").
		// "gobi-modem-unique-id": (Optional) The unique ID of the Modem firmware image, in images of type MM_FIRMWARE_IMAGE_TYPE_GOBI, given as a string value (signature "s").
		// OUT s selected: The unique name of the selected firmware image, or the empty string if no image is selected.
		// OUT aa{sv} installed: An array of dictionaries containing the properties of the installed firmware images.
		return this._send('List');
	}

	select (uniqueid) {
		// Select (IN  s uniqueid);
		// Selects a different firmware image to use, and immediately resets the modem so that it begins using the new firmware image.
		// The method will fail if the identifier does not match any of the names returned by List(), or if the image could not be selected for some reason.
		// Installed images can be selected non-destructively.
		// IN s uniqueid: The unique ID of the firmware image to select.
		return this._send('Select', {
			signature: 's',
			body: [
				uniqueid
			]
		});
	}

	/* NO SIGNALS */
	
	/* PROPERTIES	

	UpdateSettings  readable   (ua{sv})
	Detailed settings that provide information about how the module should be updated.
	The settings are given as a bitmask of MMModemFirmwareUpdateMethod values specifying the type of firmware update procedures expected followed by a dictionary that includes other parameters applicable to the specific methods reported.
	The following settings are mandatory as long as the reported update method is not MM_MODEM_FIRMWARE_UPDATE_METHOD_NONE.
		"device-ids": (Required) This property exposes the list of device IDs associated to a given device, from most specific to least specific. (signature 'as'). E.g. a list containing: "USB\VID_413C&PID_81D7&REV_0001", "USB\VID_413C&PID_81D7" and "USB\VID_413C".
		"version": (Required) This property exposes the current firmware version string of the module. If the module uses separate version numbers for firmware version and carrier configuration, this version string will be a combination of both, and so it may be different to the version string showed in the "Revision" property. (signature 's')
	MM_MODEM_FIRMWARE_UPDATE_METHOD_FASTBOOT: Devices supporting the fastboot firmware update method require exposing the following additional settings:
		"fastboot-at": (Required) This property exposes the AT command that should be sent to the module to trigger a reset into fastboot mode (signature 's')

	*/
}