// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Voice — The ModemManager Voice interface.

// The Voice interface handles Calls.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_Voice extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Voice'
		});
	}

	/* METHODS */
	
	list_calls () {
		// ListCalls (OUT ao result);
		// Retrieve all Calls. 
		// This method should only be used once and subsequent information retrieved either by listening for the org.freedesktop.ModemManager1.Modem.Voice::Added signal, or by querying the specific Call object of interest. 
		return this._send('ListCalls');
	}
	create_call (number) { 
		// CreateCall (IN a{sv} properties, OUT o path);
		// Creates a new call object for a new outgoing call.
		// The 'Number' is the only expected property to set by the user. 
		return this._send('CreateCall', {
			signature: 'a{sv}',
			body: [
				// [ [ 'number', ['s', number] ] ] // dbus-native
				{ 'number': this._variant('s', number) } // dbus-next
			]
		});
	}	
	delete_call (dbus_path) {
		// DeleteCall (IN  o path);
		// Delete a Call from the list of calls.
		// The call will be hangup if it is still active. 
		return this._send('DeleteCall', {
			signature: 'o',
			body: [
				dbus_path
			]
		});
	}
	hold_and_accept () {
		// HoldAndAccept ();
		// Place all active calls on hold, if any, and accept the next call.
		// Waiting calls have preference over held calls, so the next call being active will be any waiting call, or otherwise, any held call.
		// The user should monitor the state of all available ongoing calls to be reported of which one becomes active.
		// No error is returned if there are no waiting or held calls. 
		return this._send('HoldAndAccept');
	}
	hangup_and_accept () {
		// HangupAndAccept ();
		// Hangup all active calls, if any, and accept the next call.
		// Waiting calls have preference over held calls, so the next call being active will be any waiting call, or otherwise, any held call.
		// The user should monitor the state of all available ongoing calls to be reported of which one becomes active.
		// No error is returned if there are no waiting or held calls. In this case, this method would be  equivalent to calling Hangup() on the active call. 
		return this._send('HangupAndAccept');
	}
	hangup_all () {
		// HangupAll ();
		// Hangup all active calls.
		// Depending on how the device implements the action, calls on hold or in waiting state may also be terminated.
		// No error is returned if there are no ongoing calls. 
		return this._send('HangupAll');
	}
	transfer () {
		// Transfer ();
		// Join the currently active and held calls together into a single multiparty call, but disconnects from them.
		// The affected calls will be considered terminated from the point of view of the subscriber. 
		return this._send('Transfer');
	}
	call_waiting_setup (enable) {
		// CallWaitingSetup (IN  b enable);
		// Activates or deactivates the call waiting network service, as per 3GPP TS 22.083.
		// This operation requires communication with the network in order to complete, so the modem must be successfully registered. 
		return this._send('CallWaitingSetup', {
			signature: 'b',
			body: [
				enable
			]
		});
	}
	call_waiting_query () {
		// CallWaitingQuery (OUT b status);
		// Activates or deactivates the call waiting network service, as per 3GPP TS 22.083.
		// This operation requires communication with the network in order to complete, so the modem must be successfully registered. 
		return this._send('CallWaitingQuery');
	}
	
	/* SIGNALS */
	
	_listen () {
		var _this = this;
		this._bind(this.obj.path, this.obj.interface,{
			'CallAdded': (dbus_path) => {
				// CallAdded (o path);
				// Emitted when a call has been added. 
				// o path: Object path of the new call.
				_this._debug(3,`Notification > CallAdded: ${dbus_path}`);
				_this._Ctrl.slots('CallAdded', dbus_path);
			},
			'CallDeleted': (dbus_path) => {
				// CallDeleted (o path);
				// Emitted when a call has been deleted. 
				// o path: Object path of the now deleted Call.
				_this._debug(3,`Notification > CallDeleted: ${dbus_path}`);
				_this._Ctrl.slots('CallDeleted', dbus_path);
			}
		});
	}
	
	/* PROPERTIES

	Calls  readable   ao
	The list of calls object paths. 

	EmergencyOnly  readable   b
	A flag indicating whether emergency calls are the only allowed ones.
	If this flag is set, users should only attempt voice calls to emergency numbers, as standard voice calls will likely fail. 

	*/
}