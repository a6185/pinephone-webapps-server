/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

// org.freedesktop.ModemManager1 — The ModemManager Manager interface.

class DBus_Int__MM1__MM1__ModemManager1 extends PP_DBusSystemInterface {	
	constructor (_Ctrl) {
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: '/org/freedesktop/ModemManager1',
			'interface': 'org.freedesktop.ModemManager1'
		});
	}

	/* METHODS */

	scan_devices () {
		// ScanDevices ();
		// Start a new scan for connected modem devices. 
		return this._send('ScanDevices');
	}
	set_logging (level) {
		// SetLogging (IN  s level);
		// Set logging verbosity. 
		// IN s level: One of "ERR", "WARN", "INFO", "DEBUG".
		return this._send('SetLogging');
	}
	report_kernel_event (properties) {
		// ReportKernelEvent (IN  a{sv} properties);
		// Reports a kernel event to ModemManager.
		// This method is only available if udev is not being used to report kernel events.
		// The properties dictionary is composed of key/value string pairs. The possible keys are: 
		// action: The type of action, given as a string value (signature "s"). This parameter is MANDATORY.
		//   add A new kernel device has been added.
		//   remove An existing kernel device has been removed.
		// name: The device name, given as a string value (signature "s"). This parameter is MANDATORY. 
		// subsystem: The device subsystem, given as a string value (signature "s"). This parameter is MANDATORY. 
		// uid: The unique ID of the physical device, given as a string value (signature "s"). This parameter is OPTIONAL, if not given the sysfs path of the physical device will be used. This parameter must be the same for all devices exposed by the same physical device. 
		// IN a{sv} properties: event properties.
		return this._send('ReportKernelEvent', {
			signature: 'a{sv}',
			body: [
				properties
			]
		});
	}
	inhibit_device (uid, inhibit) {
		// InhibitDevice (IN  s uid, IN  b inhibit);
		//  org.freedesktop.ModemManager1.Modem:Device property. inhibit: TRUE to inhibit the modem and FALSE to uninhibit it.
		// Inhibit or uninhibit the device.
		// When the modem is inhibited ModemManager will close all its ports and unexport it from the bus, so that users of the interface are no longer able to operate with it.
		// This operation binds the inhibition request to the existence of the caller in the DBus bus. If the caller disappears from the bus, the inhibition will automatically removed. 
		// IN s uid the unique ID of the physical device, given in the ???
		// IN b inhibit: ???
		return this._send('ReportKernelEvent', {
			signature: 'sb',
			body: [
				uid, inhibit
			]
		});
	}

	/* SIGNALS */
	
	/* PROPERTIES

	Version  readable   s
	The runtime version of the ModemManager daemon. 
	{'Version': '1.18.8'}

	*/

}