// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Sms — The ModemManager SMS interface.

class DBus_Int__MM1__MM1_SMS__MM1_Sms extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/SMS/' + number
			'interface': 'org.freedesktop.ModemManager1.Sms'
		});
	}

	/* METHODS */

	send () {
		// Send ();
		// If the message has not yet been sent, queue it for delivery. 
		return this._send('Send');
	}

	store (storage) {
		// Store (IN  u storage);
		// Store the message in the device if not already done.
		// This method requires a MMSmsStorage value, describing the storage where this message is to be kept; or MM_SMS_STORAGE_UNKNOWN if the default storage should be used. 
		return this._send('Store', {
			signature: 'u',
			body: [
				[ storage ]
			]
		});
	}

	/* SIGNALS */

	/* PROPERTIES

	State  readable   u
	A MMSmsState value, describing the state of the message.

	PduType  readable   u
	A MMSmsPduType value, describing the type of PDUs used in the SMS message.

	Number  readable   s
	Number to which the message is addressed.

	Text  readable   s
	Message text, in UTF-8.
	When sending, if the text is larger than the limit of the technology or modem, the message will be broken into multiple parts or messages.
	Note that Text and Data are never given at the same time.

	Data  readable   ay
	Message data.
	When sending, if the data is larger than the limit of the technology or modem, the message will be broken into multiple parts or messages.
	Note that Text and Data are never given at the same time.

	SMSC  readable   s
	Indicates the SMS service center number.
	Always empty for 3GPP2/CDMA.

	Validity  readable   (uv)
	Indicates when the SMS expires in the SMSC.
	This value is composed of a MMSmsValidityType key, with an associated data which contains type-specific validity information:
	MM_SMS_VALIDITY_TYPE_RELATIVE The value is the length of the validity period in minutes, given as an unsigned integer (D-Bus signature 'u').

	Class  readable   i
	3GPP message class (-1..3). -1 means class is not available or is not used for this message, otherwise the 3GPP SMS message class.
	Always -1 for 3GPP2/CDMA.

	DeliveryReportRequest  readable   b
	#TRUE if delivery report request is required, #FALSE otherwise.

	MessageReference  readable   u
	Message Reference of the last PDU sent/received within this SMS.
	If the PDU type is MM_SMS_PDU_TYPE_STATUS_REPORT, this field identifies the Message Reference of the PDU associated to the status report.

	Timestamp  readable   s
	Time when the first PDU of the SMS message arrived the SMSC, in ISO8601 format. This field is only applicable if the PDU type is MM_SMS_PDU_TYPE_DELIVER. or MM_SMS_PDU_TYPE_STATUS_REPORT.
	The "DischargeTimestamp" property

	DischargeTimestamp  readable   s
	Time when the first PDU of the SMS message left the SMSC, in ISO8601 format.
	This field is only applicable if the PDU type is MM_SMS_PDU_TYPE_STATUS_REPORT.

	DeliveryState  readable   u
	A MMSmsDeliveryState value, describing the state of the delivery reported in the Status Report message.
	This field is only applicable if the PDU type is MM_SMS_PDU_TYPE_STATUS_REPORT.

	Storage  readable   u
	A MMSmsStorage value, describing the storage where this message is kept.

	*/
}