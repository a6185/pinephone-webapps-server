// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Modem3gpp.Ussd — The ModemManager 3GPP USSD interface.

// This interface provides access to actions based on the USSD protocol.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_3gpp_Ussd extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Modem3gpp.Ussd'
		});
	}

	/* METHODS */

	initiate (command) {
		// Initiate (IN  s command, OUT s reply);
		// Sends a USSD command string to the network initiating a USSD session.
		// When the request is handled by the network, the method returns the response or an appropriate error. The network may be awaiting further response from the ME after returning from this method and no new command can be initiated until this one is cancelled or ended.
		// IN s command: The command to start the USSD session with.
		// OUT s reply: The network response to the command which started the USSD session.
		return this._send('Initiate', {
			signature: 's',
			body: [
				command
			]
		});
	}	
	respond	(response) {
		// Respond (IN  s response, OUT s reply);
		// Respond to a USSD request that is either initiated by the mobile network, or that is awaiting further input after Initiate() was called.
		// IN s response: The response to network-initiated USSD command, or a response to a request for further input.
		// OUT s reply: The network reply to this response to the network-initiated USSD command. The reply may require further responses.
		return this._send('Respond', {
			signature: 's',
			body: [
				response
			]
		});
	}	
	cancel () {
		// Cancel ();
		// Cancel an ongoing USSD session, either mobile or network initiated.
		return this._send('Cancel');
	}	

	/* SIGNALS */
	
	/* PROPERTIES

	State  readable   u
	A MMModem3gppUssdSessionState value, indicating the state of any ongoing USSD session.

	NetworkNotification  readable   s
	Contains any network-initiated request to which no USSD response is required.
	When no USSD session is active, or when there is no network- initiated request, this property will be a zero-length string.

	NetworkRequest  readable   s
	Contains any pending network-initiated request for a response. Client should call Respond() with the appropriate response to this request.
	When no USSD session is active, or when there is no pending network-initiated request, this property will be a zero-length string.

	*/
}