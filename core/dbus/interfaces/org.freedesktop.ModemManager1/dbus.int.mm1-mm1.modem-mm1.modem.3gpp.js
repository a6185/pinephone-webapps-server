// Jean Luc Biellmann - contact@alsatux.com

'use strict';

// org.freedesktop.ModemManager1.Modem.Modem3gpp — The ModemManager 3GPP interface.

// This interface provides access to specific actions that may be performed in modems with 3GPP capabilities.
// This interface will only be available once the modem is ready to be registered in the cellular network. 3GPP devices will require a valid unlocked SIM card before any of the features in the interface can be used.

class DBus_Int__MM1__MM1_Modem__MM1_Modem_3gpp extends PP_DBusSystemInterface {
	constructor (_Ctrl, dbus_path) {		
		super(_Ctrl, {
			destination: 'org.freedesktop.ModemManager1',
			path: dbus_path, // /org/freedesktop/ModemManager1/Modem/' + number
			'interface': 'org.freedesktop.ModemManager1.Modem.Modem3gpp'
		});
	}

	/* METHODS */

	register (operator_id) {
		// Register (IN  s operator_id);
		// Request registration with a given mobile network.
		// IN s operator_id: The operator ID (ie, "MCCMNC", like "310260") to register. An empty string can be used to register to the home network.
		return this._send('Register', {
			signature: 's',
			body: [
				operator_id
			]
		});
	}

	scan () {
		// Scan (OUT aa{sv} results);
		// Scan for available networks.
		// results is an array of dictionaries with each array element describing a mobile network found in the scan. Each dictionary may include one or more of the following keys:
		// "status": A MMModem3gppNetworkAvailability value representing network availability status, given as an unsigned integer (signature "u"). This key will always be present.
		// "operator-long": Long-format name of operator, given as a string value (signature "s"). If the name is unknown, this field should not be present.
		// "operator-short": Short-format name of operator, given as a string value (signature "s"). If the name is unknown, this field should not be present.
		// "operator-code": Mobile code of the operator, given as a string value (signature "s"). Returned in the format "MCCMNC", where MCC is the three-digit ITU E.212 Mobile Country Code and MNC is the two- or three-digit GSM Mobile Network Code. e.g. "31026" or "310260".
		// "access-technology": A MMModemAccessTechnology value representing the generic access technology used by this mobile network, given as an unsigned integer (signature "u").
		// OUT aa{sv} results: Array of dictionaries with the found networks.
		return this._send('Scan');
	}

	set_eps_ue_mode_operation (mode) {
		// SetEpsUeModeOperation (IN  u mode);
		// Sets the UE mode of operation for EPS.
		// IN u mode: a MMModem3gppEpsUeModeOperation.
		return this._send('Register', {
			signature: 'u',
			body: [
				mode
			]
		});
	}		

	set_initial_eps_bearer_settings (settings) {
		// SetInitialEpsBearerSettings (IN  a{sv} settings);
		// Updates the default settings to be used in the initial default EPS bearer when registering to the LTE network.
		// The allowed properties in this method are all the 3GPP-specific ones specified in the bearer properties; i.e.: "apn", "ip-type", "allowed-auth", "user", and "password".
		// IN a{sv} settings: List of properties to use when requesting the LTE attach procedure.
		return this._send('SetInitialEpsBearerSettings', {
			signature: 'a{sv}',
			body: [
				settings
			]
		});
	}	

	disable_facility_lock (properties) {
		// DisableFacilityLock (IN  (us) properties);
		// Sends control key to modem to disable selected facility lock
		// "facility": A MMModem3gppFacility value representing the type of the facility lock to disable.
		// "control key": Alphanumeric key required to unlock facility.
		// IN (us) properties: A tuple of facility type and control key.
		return this._send('DisableFacilityLock', {
			signature: 'us',
			body: [
				properties
			]
		});
	}	

	/* SIGNALS */
	
	/* PROPERTIES	

	Imei  readable   s
	The IMEI of the device.

	RegistrationState  readable   u
	A MMModem3gppRegistrationState value specifying the mobile registration status as defined in 3GPP TS 27.007 section 10.1.19.

	OperatorCode  readable   s
	Code of the operator to which the mobile is currently registered.
	Returned in the format "MCCMNC", where MCC is the three-digit ITU E.212 Mobile Country Code and MNC is the two- or three-digit GSM Mobile Network Code. e.g. e"31026" or "310260".
	If the MCC and MNC are not known or the mobile is not registered to a mobile network, this property will be a zero-length (blank) string.

	OperatorName  readable   s
	Name of the operator to which the mobile is currently registered.
	If the operator name is not known or the mobile is not registered to a mobile network, this property will be a zero-length (blank) string.

	EnabledFacilityLocks  readable   u
	Bitmask of MMModem3gppFacility values for which PIN locking is enabled.

	The "EpsUeModeOperation" property
	EpsUeModeOperation  readable   u
	A MMModem3gppEpsUeModeOperation value representing the UE mode of operation for EPS, given as an unsigned integer (signature "u").

	Pco  readable   a(ubay)
	The raw PCOs received from the network, given as array of PCO elements (signature "a(ubay)").
	Each PCO is defined as a sequence of 3 fields:

	The session ID associated with the PCO, given as an unsigned integer value (signature "u").
	The flag that indicates whether the PCO data contains the complete PCO structure received from the network, given as a boolean value (signature "b").
	The raw PCO data, given as an array of bytes (signature "ay").

	InitialEpsBearer  readable   o
	The object path for the initial default EPS bearer.

	InitialEpsBearerSettings  readable   a{sv}
	List of properties requested by the device for the initial EPS bearer during LTE network attach procedure.
	The network may decide to use different settings during the actual device attach procedure, e.g. if the device is roaming or no explicit settings were requested, so the values shown in the "InitialEpsBearer" bearer object may be totally different.
	This is a read-only property, updating these settings should be done using the SetInitialEpsBearerSettings() method.

	*/
}
