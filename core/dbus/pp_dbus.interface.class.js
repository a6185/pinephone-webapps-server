// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_DBusInterface extends PP_DBus {
	constructor (_Ctrl, obj, dbus_type) {
		super(_Ctrl._Module, obj, dbus_type);
		this._Ctrl = _Ctrl;		
		/*
		obj = {
			destination: org.freedesktop.ModemManager1,
			path: /org/freedesktop/ModemManager1
			interface: org.freedesktop.DBus.Properties
		}
		*/
		this.callbacks = {}; // for signals
		this.origin = _Ctrl._Module.obj.namespace + '/' + _Ctrl.classname;
	}
	// we use _method() to avoid conflits with final DBus names (send() for example...)
	_debug (level, msg) {
		global._Console.debug(level,`>pp_dbusinterface/${this.dbus_type}:: [${this.origin}] : ${msg}`);
	}
	_get (dbus_property) {
		return this._send('Get', {
			'interface': 'org.freedesktop.DBus.Properties',
			signature: 'ss',
			body: [
				this.obj.interface,
				dbus_property
			]
		});
	}
	_getAll () {
		return this._send('GetAll', {
			'interface': 'org.freedesktop.DBus.Properties',
			signature: 's',
			body: [
				this.obj.interface
			]
		});
	}
	_set (dbus_property, dbus_value) {
		return this._send('Set', {
			'interface': 'org.freedesktop.DBus.Properties',
			signature: 'ssv',
			body: [
				this.obj.interface,
				dbus_property,
				dbus_value
			]
		});
	}
	async _bind (dbus_path, dbus_interface, callbacks) {
		try {
			this._debug(3,`slots:: add listeners for [
  destination: ${this.obj.destination}
  path: ${dbus_path}
  interface: ${dbus_interface}]...`);
			if (this.dbus_type == 'System') {
				let obj = await this.dbus2.getProxyObject(this.obj.destination, dbus_path);
				let int = obj.getInterface(dbus_interface);
				for (let notification in callbacks) {
					int.on(notification,callbacks[notification])
				}
			} else {
				/* TODO */
			}
		} catch (err) {
			this._debug(3,`Binding interface notifications failed ! ${err}`);
			debugger;
		}
	}
}

class PP_DBusSystemInterface extends PP_DBusInterface {
	constructor (_Ctrl, obj) {
		super(_Ctrl, obj,'System');
	}
}

class PP_DBusSessionInterface extends PP_DBusInterface {
	constructor (_Ctrl, obj) {
		super(_Ctrl, obj,'Session');
	}
}

