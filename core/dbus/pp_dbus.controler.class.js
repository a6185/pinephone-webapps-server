// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_DBusController extends JS_Object {
	constructor (_Module, _Manager, classname, has_methods, has_signals, has_properties) {
		super();
		this._Module = _Module;
		this._Manager = _Manager;
		this.classname = classname;
		this.origin = _Module.obj.namespace + '/' + classname;
		this.has_methods = has_methods;
		this.has_signals = has_signals;
		this.has_properties = has_properties;
		this._DBus = null;
		this.dbus_path = null;
	}	
	debug (level, msg) {
		global._Console.debug(level,`>pp_dbuscontroller:: [${this.origin}]:: ${msg}`);
	}
	async init () {
		/* By module */
		try {
			if (this.has_properties) {
				this.set('properties',{});
				await this.refresh();
			}
			if (this.has_signals) {
				this._DBus._listen();
			}
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async refresh () {
		try {
			if (this.has_properties) {
				let res = await this._DBus._getAll();
				if (!res.length) {
					throw 'No properties found !';
				}
				this.set('properties',res[0]);				
			}
		} catch (err) {
			this.debug(1,err);
		}
	}
	async slots (signal, ...args) {
		try {
			if (this.has_signals) {
				if (this.has_properties) {
					this.refresh();
				}
				await this._Manager.on_dbus_notifications(this, signal, ...args);
			}
		} catch (err) {
			this.debug(1,err);
		}
	}		
}

