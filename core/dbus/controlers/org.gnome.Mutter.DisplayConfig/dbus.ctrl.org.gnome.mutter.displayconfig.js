/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__Org__Gnome__Mutter__DisplayConfig extends PP_DBusController {	
	constructor (_Module, _Manager) {
		super(_Module, _Manager, 'dbus_ctrl__org__gnome__muter__displayconfig', true, true, true);
		this._DBus = new DBus_Int__Org__Gnome__Mutter__DisplayConfig(this);
	}
}