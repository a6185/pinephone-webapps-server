/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class Ctrl_Int__MM1__MM1_SIM__ModemManager1_Sim extends PP_DBusController {
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__mm1__mm1_sim__modemanager1_sim', true, false, true);
		this.dbus_path = dbus_path; // /org/freedesktop/ModemManager1/SIM/' + number
		this._DBus = new DBus_Int__MM1__MM1_SIM__ModemManager1_Sim(this, dbus_path);
	}
}