// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class DBus_Ctrl__MM1__MM1_Modem__MM1_Modem_3gpp_profilemanager extends PP_DBusController {
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__mm1__mm1_modem__mm1_modem_3gpp_profilemanager', true, true, false);
		this.dbus_path = dbus_path; // /org/freedesktop/ModemManager1/Modem/' + number
		this._DBus = new DBus_Int__MM1__MM1_Modem__MM1_Modem_3gpp_ProfileManager(this, dbus_path);
	}
}