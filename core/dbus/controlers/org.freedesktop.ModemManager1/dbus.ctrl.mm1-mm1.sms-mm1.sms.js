// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class DBus_Ctrl__MM1__MM1_SMS__MM1_Sms extends PP_DBusController {
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__mm1__mm1_sms__mm1_sms', true, false, true);
		this.dbus_path = dbus_path; // /org/freedesktop/ModemManager1/SMS/' + number
		this._DBus = new DBus_Int__MM1__MM1_SMS__MM1_Sms(this, dbus_path);
	}
}