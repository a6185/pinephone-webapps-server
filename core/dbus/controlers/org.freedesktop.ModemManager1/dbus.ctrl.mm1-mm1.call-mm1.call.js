// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class DBus_Ctrl__MM1__MM1_Call__MM1_Call extends PP_DBusController {
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__mm1__mm1_call__mm1_call', true, true, true);
		this.dbus_path = dbus_path; // /org/freedesktop/ModemManager1/Call/' + number
		this._DBus = new DBus_Int__MM1__MM1_Call__MM1_Call(this, dbus_path);
	}
}
