/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__MM1__MM1__ModemManager1 extends PP_DBusController {
	constructor (_Module, _Manager) {
		super(_Module, _Manager, 'dbus_ctrl__mm1__mm1__modemmanager1', true, false, true);
		this._DBus = new DBus_Int__MM1__MM1__ModemManager1(this);
	}
	async init () {		
		try {
			this.set('properties',{});
			await this._DBus.scan_devices();
			let res = await this._DBus._getAll();
			if (!res.length) {
				throw 'No properties found !';
			}
			this.set('properties',res[0]);		
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async refresh () {		
		try {
			await this._DBus.scan_devices();
			let res = await this._DBus._getAll();
			if (!res.length) {
				throw 'No properties found !';
			}
			this.set('properties',res[0]);		
			this.debug(1,`Refresh ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Refresh failed: ${err}`);
			throw err;
		}
	}
	async slots (signal, ...args) {
		try {
			let res = await this._DBus._getAll();
			this.set('properties',res);		
			await this._Manager.on_dbus_notifications(this, signal, ...args);
		} catch (err) {
			this.debug(1,err);
		}
	}
}
