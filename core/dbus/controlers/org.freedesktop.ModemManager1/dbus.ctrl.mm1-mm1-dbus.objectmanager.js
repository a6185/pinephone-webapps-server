/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__MM1__MM1__DBus_ObjectManager extends PP_DBusController {
	constructor (_Module, _Manager) {
		super(_Module, _Manager, 'dbus_ctrl__mm1__mm1__dbus_objectmanager', true, true, false);
		this._DBus = new DBus_Int__MM1__MM1__DBus_ObjectManager(this);
	}
	async init () {		
		try {
			this.set('managed_objects',{});
			let res = await this._DBus.get_managed_objects();
			if (!res.length) {
				throw 'No managed objects found !';
			}
			this.set('managed_objects',res[0]);		
			this._DBus._listen();
			this.debug(1,`Init ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Init failed: ${err}`);
			throw err;
		}
	}
	async refresh () {		
		try {
			let res = await this._DBus.get_managed_objects();
			if (!res.length) {
				throw 'No managed objects found !';
			}
			this.set('managed_objects',res[0]);		
			this.debug(1,`Refresh ok...`);
			return this; // mandatory !
		} catch (err) {
			this.debug(1,`Refresh failed: ${err}`);
			throw err;
		}
	}
	async slots (signal, ...args) {
		try {
			let res = await this._DBus._getAll();
			this.set('managed_objects',res);		
			await this._Manager.on_dbus_notifications(this, signal, ...args);
		} catch (err) {
			this.debug(1,err);
		}
	}
}