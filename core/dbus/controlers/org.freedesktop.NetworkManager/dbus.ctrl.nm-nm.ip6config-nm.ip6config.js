/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM_IP6Config__NM_IP6Config extends PP_DBusController {	
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__nm__nm_ip6config__nm_ip6config', false, true, true);
		this.dbus_path = dbus_path; // /org/freedesktop/NetworkManager/Device/1
		this._DBus = new DBus_Int__NM__NM_IP6Config__NM_IP6Config(this, dbus_path);
	}
}