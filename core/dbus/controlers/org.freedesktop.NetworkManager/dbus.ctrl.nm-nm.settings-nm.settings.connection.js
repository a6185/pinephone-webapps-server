/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM_Settings__NM_Settings_Connection extends PP_DBusController {	
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__nm_settings__nm_settings_connection', true, true, true);
		this.dbus_path = dbus_path; // /org/freedesktop/NetworkManager/Settings/1
		this._DBus = new DBus_Int__NM__NM_Settings__NM_Settings_Connection(this, dbus_path);
	}
	async slots (signal, ...args) {
		try {
			switch (signal) {
				case "Updated":
					this.refresh();
					break;
				case "Removed":
					this.set('properties',{});
					break;
			}
			await this._Manager.on_dbus_notifications(this, signal, ...args);
		} catch (err) {
			this.debug(1,err);
		}
	}
}