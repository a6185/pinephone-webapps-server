/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM_Devices__NM_Device extends PP_DBusController {	
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__nm__nm_devices__nm_device', false, true, true);
		this.dbus_path = dbus_path; // /org/freedesktop/NetworkManager/Device/1
		this._DBus = new DBus_Int__NM__NM_Devices__NM_Device(this, dbus_path);
	}
}