/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM_ActiveConnection__NM_Connection_Active extends PP_DBusController {
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__nm__nm_active_connection__nm_connection_active', false, true, true);
		this.dbus_path = dbus_path; // '/org/freedesktop/NetworkManager/ActiveConnection/' + number
		this._DBus = new DBus_Int__NM__NM_ActiveConnection__NM_Connection_Active(this, dbus_path);
	}
}