/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM_Settings__NM_Settings extends PP_DBusController {	
	constructor (_Module, _Manager) {
		super(_Module, _Manager, 'dbus_ctrl__nm_settings__nm_settings', true, true, true);
		this._DBus = new DBus_Int__NM__NM_Settings__NM_Settings(this);
	}
	async slots (signal, ...args) {
		try {
			switch (signal) {
				case "NewConnection":
					this.refresh();
					break;
				case "ConnectionRemoved":
					this.set('properties',{});
					break;
			}
			await this._Manager.on_dbus_notifications(this, signal, ...args);
		} catch (err) {
			this.debug(1,err);
		}
	}
}