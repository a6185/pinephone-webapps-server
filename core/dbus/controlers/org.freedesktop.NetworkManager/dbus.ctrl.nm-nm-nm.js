/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM__NM extends PP_DBusController {	
	constructor (_Module, _Manager) {
		super(_Module, _Manager, 'dbus_ctrl__nm__nm__nm', true, true, true);
		this._DBus = new DBus_Int__NM__NM__NM(this);
	}
}