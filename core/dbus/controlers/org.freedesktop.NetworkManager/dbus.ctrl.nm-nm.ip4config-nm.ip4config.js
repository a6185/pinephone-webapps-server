/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

class DBus_Ctrl__NM__NM_IP4Config__NM_IP4Config extends PP_DBusController {	
	constructor (_Module, _Manager, dbus_path) {
		super(_Module, _Manager, 'dbus_ctrl__nm__nm_ip4config__nm_ip4config', false, true, true);
		this.dbus_path = dbus_path; // /org/freedesktop/NetworkManager/Device/1
		this._DBus = new DBus_Int__NM__NM_IP4Config__NM_IP4Config(this, dbus_path);
	}
}