/* Jean Luc BIELLMANN - contact@alsatux.com */

'use strict';

/*
 * Node modules (to install with npm) : 
 * # npm install ws wscat bufferutil file-system utf-8-validate utils-extend path shelljs csv-parser dbus-next
 *
 * Usage: node pinephone/js
 *
 * Global / reserved : _PP, includes(), include(), register(), _Console, _Const
 *
 * If you want to start the script from the CLI, don't forget to indicate node path !
 * # export NODE_PATH=$(npm root -g)
 */

( () => {

	try {

		// Exit cleanly when executed using systemd
		process.on('SIGTERM', () => {
			console.log('SIGTERM DETECTED');
			process.exit(0);
		});

		/** LOADING ALL NODES MODULES **/ 

		class PP_Nodes {
			constructor () {
				this.obj = {};
			}
			init (modules) { // please use this method to load several Node modules in your project !
				modules.split(',').forEach(module => {
					this.get(module);
				});
			}
			has (key) {
				return this.obj.hasOwnProperty(key);
			}
			get (name) { // please use this method to load one Node module in your project !
				if (!this.has(name)) {
					console.log(`Required ${name}...`);
					this.obj[name] = require(name);
				}
				return this.obj[name];
			}
		}

		let _Nodes = new PP_Nodes();
		_Nodes.init('vm,file-system,child_process,path,dbus-next,https,ws,sprintf-js,glob,csv-parser');

		/** INCLUDES / AUTOLOAD **/

		global.include = (file) => {
			let fs = _Nodes.get('file-system');
			let vm = _Nodes.get('vm');
			console.debug(`>pp_include:: ${file}`);
			try {
				let filename = __dirname + '/' + file + '.js';
				//console.log(filename);
				let code = fs.readFileSync(filename, 'utf-8');
				vm.runInThisContext(code, file);
			} catch (err) {				
				console.error(`ERROR: including file "${file}" failed ! ${err}`);
				debugger;
			}
		}

		global.includes = (files) => {
			files.forEach(file => {
				include(file);
			});
		}

		let autoload = (path) => {
			let glob = _Nodes.get('glob');
			//console.debug('Autoload '+ path);
			let files = glob.sync(__dirname + path + '.js');
			//console.debug(files);
			files.forEach(file => {
				let re = new RegExp('^' + __dirname + '/','');
				let filename = file.replace(re,'').replace(/.js$/i,'');
				//console.log(filename);
				include(filename);
			})
		}

		/** APPS TEMPLATES **/
		
		autoload('/core/tpl/autoload');

		/** CONSOLE **/
		
		include('core/pp_console.class');
		global._Console = new PP_Console();

		/** CORE **/

		/* PROTOTYPES */
		autoload('/core/prototypes/autoload');
		
		/* DEFAULT */
		includes([
			'core/config',
			'core/pinephone.lang'
		]);
		
		/* HELPERS */
		includes([
			'core/pp_fn.class',
			'core/pp_routes.class',
			'core/pp_shell.class',
			'core/pp_status.class'
		]);
		
		/* Before STORAGE ! */
		include('/core/modules/pp_modules.class');

		// Main project object

		global._PP = { 
			locale: PP_DEFAULT_LOCALE, // default main locale
			_DB: {}, // databases
			_Enum: {}, // enumerations
			tr: null, // to translate - see PP_I18N
			_Nodes: _Nodes, // nodes objects
			_Fn: new PP_Fn(), // global functions
			_Core: {
				_Modules: new PP_Modules(), // store all main core objects
				_Routes: new PP_Routes(), // store modules routes
				_Storage: null // IO for modules - using to /home/mobian/sdcard by default
			},
			_WS: null // websocket
		};

		// Prefs templates
		/*
		global.register_enum = (obj) => {
			let key = obj.key;
			global._PP._PrefsTpl[key] = obj.val;
		}*/
		autoload('/core/enum/*');

		// To add new modules...

		global.register_module = (args) => {
			let _Modules = global._PP._Core._Modules;
			_Modules.register(args);
		}

		/** I18N DB **/

		_PP._DB._I18N = {
			obj: {},
			set: (filename) => {
				if (_PP._DB._I18N.obj[filename] === undefined) {
					_PP._DB._I18N.obj[filename] = {
						locales: [],
						default_locale: "",
						current_locale: "",
						default_index: -1,
						current_index: -1,
						data: {}
					}
					//await _PP._DB._I18N.refresh(filename);
				}
			},
			debug: (level, msg) => {
				global._Console.debug(level,`>pp_db_i18n:: ${msg}`);
			},
			refresh: async (filename) => {
				if (!_PP._DB._I18N.obj[filename].length) {
					let rows = await _PP._DB._I18N.upd(filename);
					let pp_i18n_parser = new PP_I18N_Parser();	
					let obj = pp_i18n_parser.parse(rows);
					_PP._DB._I18N.obj[filename] = obj;
				}
			},
			upd: (filename) => {
				_PP._DB._I18N.debug(3,`Loading "${filename}" started...`);
				return new Promise ((resolve,reject) => {
					var rows = [];
					let fs = _PP._Nodes.get('file-system');
					fs.createReadStream(filename)		
						.pipe(_PP._Nodes.get('csv-parser')({ delimiter: ",", headers: false }))
						.on("data", function (row) {					
							rows.push(row);
						})
						.on("end", function () {
							_PP._DB._I18N.debug(3,`Loading "${filename}" finished...`);
							resolve(rows);
						})
						.on("error", function (error) {
							_PP._DB._I18N.debug(3,`Loading failed for "${filename}": ${error.message}...`)
							reject(error);
						});
				});
			},		
			get: (filename) => {
				let obj = _PP._DB._I18N.obj[filename];
				if (obj.current_index==-1) {
					_PP._DB._I18N.refresh(filename).then(res => {
						return _PP._DB._I18N.obj[filename];
					}).catch(err => {
						this.debug(1, err);
						return obj;
					});
				} else {
					if (obj.current_locale!=global._PP.locale && obj.locales.indexOf(global._PP.locale)!=-1) {
						_PP._DB._I18N.refresh(filename).then(res => {
							return _PP._DB._I18N.obj[filename];
						}).catch(err => {
							this.debug(1, err);
							return obj;
						});
					} else {
						return obj;		
					}
				}		
			}
		};

		let i18n = new PP_I18N(_PP, '/locales/pinephone.csv');
		(async () => {
			await i18n.init();
		})();

		/* DBUS */
		autoload('/core/dbus/*');
		autoload('/core/dbus/controlers/*/dbus.ctrl.*');
		autoload('/core/dbus/interfaces/*/dbus.int.*');
		/* NO AUTOLOAD FOR STORAGE AND WEBSOCKET */

		/* STORAGE */
		includes([
			'core/storage/config',
			'core/storage/pp_storage.class'
		]);
		// do not use in your own scripts ! 
		// create your own PP_Storage per module !
		_PP._Core._Storage = new PP_Storage(null);

		/* WEBSOCKET (AFTER LANG) */
		include('core/websocket/pp_ws.class');
		_PP._WS = new PP_WS();
		// no init() here please (will start the socket too soon...)
		
		/* AUTOLOAD CORE */
		autoload('/core/modem/autoload');
		autoload('/core/network/autoload');
		autoload('/core/modules/autoload');
		autoload('/core/sound/autoload');
		//autoload('/core/display/autoload');
		
		try {
			_PP._Core._Storage.mkdir('sdcard1','/');

			/* AUTOLOAD APPS */
			autoload('/app/**/autoload');
			
			//global._Console.inspect(_PP._Core._Modules.obj);
			//global._Console.inspect(Object.keys(_PP._Core._Modules.obj));

			// currents dev modules
			// prefs first because used by all others modules !
			_PP._Core._Modules.activate('com.alsatux.prefs');

			_PP._Core._Modules.activate('com.alsatux.calendar');
			_PP._Core._Modules.activate('com.alsatux.contacts');
			_PP._Core._Modules.activate('com.alsatux.phone');
			_PP._Core._Modules.activate('com.alsatux.sim');
			_PP._Core._Modules.activate('com.alsatux.smms');
			_PP._Core._Modules.activate('com.alsatux.statscars');
			
			/* "Rien ne sert de courir, il faut partir à point" (Jean de La Fontaine) ! */
			_PP._Core._Modules.run();
		} catch (err) {
			throw err;
		}
	} catch (err) {
		debugger;
		console.error(`FATAL ERROR: ${err}`);
	}		
})();
