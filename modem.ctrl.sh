#!/bin/bash

# Jean Luc Biellmann - contact@alsatux.com

get_modem() {
	echo $(mmcli -L | grep -oE 'Modem\/([0-9]+)' | cut -d'/' -f2)
}

get_own_number() {
	mmcli -m $(get_modem) -K|grep "modem.generic.own-numbers.value\\[1\\]"|awk -F ': ' '{print $2}'
}

TEST_CALL_NUMBER="666" # Put your own answering machine number here !
OWN_PHONE_NUMBER=$(get_own_number)

parse() {	
	local DATA=$1
	local KEY=$2
	local old_IFS=$IFS
	local IFS=$'\n'
	local LINE=$(echo -e "$DATA"|grep "${KEY}")
	IFS=$old_IFS
	echo $LINE|awk -F ": " '{print $2}'
}

#################
# VOICE
#################

voice_call_list() {
	mmcli -m $(get_modem) --voice-list-calls -K
}

voice_call_create() {
	local NUMBER=$1
	mmcli -m $(get_modem) --voice-create-call="number=${NUMBER}"
}

voice_call_action() {
	local ACTION=$1
	local NUM=$2
	local VOICE_CALLS_LIST=$(voice_call_list)
	local K="modem.voice.call.value\\[$NUM\\]"
	local V=$(parse "$VOICE_CALLS_LIST" "$K")
	mmcli -m $(get_modem) --call "$V" "${ACTION}"
}

voice_call_delete() {
	local NUM=$1
	local VOICE_CALLS_LIST=$(voice_call_list)
	local K="modem.voice.call.value\\[$NUM\\]"
	local V=$(parse "$VOICE_CALLS_LIST" "$K")
	mmcli -m $(get_modem) --voice-delete-call "$V"	
}

menu2() {
	voice_call_list
	echo "
VOICE MENU
-------------------------------
0 - Back to main menu
1 - Voice calls list
2 - Create (test)
(3 - Start)
(4 - Accept)
5 - Hangup
(6 - Send DTMF - TODO)
7 - Delete
-------------------------------
	"
	read -r -n1 -p "> your choice ? " CHOICE
	echo -e "\n"
	case ${CHOICE} in
		0)
			menu0
			;;
		1)
			voice_call_list
			echo -e "\n"
			menu2
			;;
		2)
			voice_call_create "${TEST_CALL_NUMBER}"
			menu2
			;;
		3)
			read -r -p "> voice call number to start ? " NUM
			echo -e "\n"
			voice_call_action "--start" "$NUM"
			menu2
			;;
		4)
			read -r -p "> voice call number to accept ? " NUM
			echo -e "\n"
			voice_call_action "--accept" "$NUM"
			menu2
			;;
		5)
			read -r -p "> voice call number to hangup ? " NUM
			echo -e "\n"
			voice_call_action "--hangup" "$NUM"
			menu2
			;;
		7)
			read -r -p "> voice call number to delete ? " NUM
			echo -e "\n"
			voice_call_delete "$NUM"
			menu2
			;;
		*)
			menu2
			;;
	esac
}

#################
# SMS
#################

sms_list() {
	local MODEM=$(get_modem)
	mmcli -m ${MODEM} --messaging-list-sms -K
}

sms_action() {
	local ACTION=$1
	local NUM=$2
	local SMS_LIST=$(sms_list)
	local K="modem.messaging.sms.value\\[$NUM\\]"
	local V=$(parse "$SMS_LIST" "$K")
	mmcli -m $(get_modem) --sms "$V" "${ACTION}"
}

sms_delete() {
	local NUM=$1
	local SMS_LIST=$(sms_list)
	local K="modem.messaging.sms.value\\[$NUM\\]"
	local V=$(parse "$SMS_LIST" "$K")
	mmcli -m $(get_modem) --messaging-delete-sms "$V"
}

menu1() {
	sms_list
	echo "
SMS MENU
-------------------------------
0 - Back to main menu
1 - Messages list
2 - Create (test)
3 - Send message
4 - Read message
5 - Delete message
-------------------------------
	"
	read -r -n1 -p "> your choice ? " CHOICE
	echo -e "\n"
	case ${CHOICE} in
		0)
			menu0
			;;
		1)
			sms_list
			echo -e "\n"
			menu1
			;;
		2)
			mmcli -m $(get_modem) --messaging-create-sms="text='Hello world',number='+${OWN_PHONE_NUMBER}'"
			menu1
			;;
		3)
			read -r -p "> sms number to send ? " NUM
			echo -e "\n"
			sms_action "--send" "$NUM"
			menu1
			;;			
		4)
			read -r -p "> sms number to read ? " NUM
			echo -e "\n"
			sms_action "" "$NUM"
			menu1
			;;			
		5)
			read -r -p "> sms number to delete ? " NUM
			echo -e "\n"
			sms_delete "$NUM"
			menu1
			;;			
		*)
			menu1
			;;
	esac
}

#################
# MAIN
#################

menu0() {
	echo "
MAIN MENU
-------------------------------
0 - Exit
1 - SMS
2 - Voice
-------------------------------
	"
	read -r -n1 -p "> your choice ? " CHOICE1
	echo -e "\n"
	case ${CHOICE1} in
		0)
			echo -e "\n\nThat's all folks !"
			exit 0
			;;
		1)
			menu1
			;;
		2)
			menu2
			;;
		*)
			menu0
			;;
	esac
}

menu0