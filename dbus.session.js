#!/usr/bin/env node

const WebSocket = require('ws');
const dbus = require('dbus-next');
const posix = require('posix');

//posix.setuid(1000);
//posix.seteuid(1000);

const wss = new WebSocket.Server({ port: 2021 });

function removeVariants (datas) {
	let Variant = dbus.Variant;
	let i,s,v;
	if (datas.constructor === Array) {
		for (i=0;i<datas.length;i++) {
			datas[i] = removeVariants(datas[i]);
		}
	}
	if (datas.constructor === Object) {
		for (i in datas) {
			datas[i] = removeVariants(datas[i]);
		}
	}
	if (datas.constructor === Variant) {
		s = datas.signature;
		v = datas.value;
		datas = removeVariants(v);
	}
	return datas;
}

wss.on('connection', ws => {
	ws.on('open', function () {
		console.log('WebSocket connection new');
	});
	ws.on('close', function () {
		console.log('WebSocket connection closed');
	});
	ws.on('error', function (err) {
		console.log(`WebSocket error`);
		console.dir(err);
	});
	ws.on('message', function (message) {
		let obj = JSON.parse(message);
		let m = new dbus.Message(obj);
		console.log(`Websocket incoming message:`);
		console.dir(m);
		//let ds = dbus.sessionBus({busAddress:'unix:path=/run/user/1000/bus',uid:1000});
		let ds = dbus.sessionBus({busAddress:'unix:path=/run/user/1000/bus'});
		ds.on('message', data => {
			console.log(`DBUS message:`);
			console.dir(data);
		});
		ds.call(m).then(res => {
			let data = removeVariants(res.body);
			console.log(`Websocket outgoing message:`);
			console.dir(data);
			ws.send(JSON.stringify({
				code: 0,
				data: data
			}));
		}).catch(err => {
			console.log(`DBUS error:`);
			console.dir(err);
			ws.send(JSON.stringify({
				code: 1,
				err: err
			}));
		});
	});
});


