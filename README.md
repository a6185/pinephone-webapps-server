# Pinephone Webapps Server

## Required
- A pinephone with Debian GNU/Linux 12 / Mobian

## Install (with admin privileges)
```
apt update && apt install git -y
mkdir -p /usr/local/src/pinephone/server
cd /usr/local/src/pinephone/server
git clone https://gitlab.com/a6185/pinephone-webapps-server.git
mv pinephone-webapps-server/* /usr/local/src/pinephone/server/
cd /usr/local/src/pinephone/server/INSTALL
./install.sh
```

## Required

- You need Pinephone Webapps Clients from the same repository, to install AFTER this server sside !

## Conflitcs

- To avoid conflits with Gnome apps, Gnome Calls and Chatty are removed from /etc/xdg/autostart then placed in /root for backup. These two apps are not uninstalled, juste remove from startup. You can still running them manually when needed.

## Licence please ?

All code is available under GPLv2, instead of a few lines code coming from outside world, when i'm not the author.

## Available webapps: 

- Cactus (calendar)
- Cockatoo (contacts)
- SIM (to unlock SIM card)
- StatsCars (cars management)
- User preferences (locale/timezone/currency/phone prefix/date format/...) - used in all others modules

- Echo (phone) without options:
  - "deflect": interface ok but got the error "Deflect failed for /org/freedesktop/ModemManager1/Call/X - error: DBusError: Cannot run sequence: 'Could not open serial device ttyUSB2: it has been forced close)" - did modem really supports deflect ?
  - "multiparty": not tested
  - using callaudio still not audible on destination side sometimes (this problem of mic and echo levels are definitily not acceptable for a smartphone - and sadly still no DBus interfaces for pulseaudio)

- SMMS (sms and mms management)
  - global prefixes integrated (cf. /core/enum on server side) but my problem here is i still don't know exactly how people using international prefixes outside of France, so you have to add prefix management in app/core/js/phone.cleaner.js for your own country !
  - SMS ok (tested for french users) with hundreds of UTF8 emoticons available !
  - MMS still not working:
    - sending: got error "Couldn't write SMS part: QMI protocol error (54): 'WmsCauseCode'" with firmware EG25GGBR07A08M2G_01.001.01.001 on pinephone 1.2 (minicom -D /dev/ttyUSB2 then AT+QGMR / dmesg|grep Model). I tried 'AT+QCFG="ims",0' then 'AT+QCFG="ims",1' then 'AT+QCFG="ims",2' but no better result. If you have an idea...
  - MMS reception: not tested: need people to send me MMS, so i could test reception and continue this part ! 

## The future ?

I would like spending more time on this project, but it use a lot of time, and since the begining i'm all alone... So consider a little donation if you want to help me. 
And of course, if you want to participate, you sure are welcome !
