#!/bin/bash

NOW=$(date +%Y%m%d%H%M%S)

# modify /etc/hosts
cp /etc/hosts /etc/hosts.$NOW.bak
sed -i 's/localhost$/localhost mobian/' /etc/hosts
# default storage dir
install -o 1000 -g 1000 -m 775 -d /home/mobian/sdcard
# server installation
apt update
apt install nodejs npm apache2 php -y
install -o 1000 -g 1000 -m 775 -d /usr/local/src/pinephone/server
cd /usr/local/src/pinephone/server/
npm install ws wscat bufferutil file-system utf-8-validate utils-extend path shelljs csv-parser dbus-next posix -g
export NODE_PATH=$(npm root -g)
chown -R mobian:mobian /usr/local/src/pinephone/server
# create autosigned certs
cd /usr/local/src/pinephone/server/INSTALL/certs
./install.sh
# patch apache2 https side
systemctl stop apache2
a2enmod ssl
a2ensite default-ssl
if [ ! -f /etc/apache2/sites-available/default-ssl.conf.orig ]; then
	cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.orig
	patch /etc/apache2/sites-available/default-ssl.conf apache2.patch
fi
systemctl start apache2
# add systemd services
cd /usr/local/src/pinephone/server/INSTALL
install -o 0 -g 0 -m 644 pinephone.dbus.service /etc/systemd/system
install -o 0 -g 0 -m 644 pinephone.service /etc/systemd/system
systemctl daemon-reload
systemctl enable pinephone.dbus.service
systemctl start pinephone.dbus.service
systemctl enable pinephone.service
systemctl start pinephone.service
# client side
install -o 1000 -g 1000 -m 775 -d /var/www/html/app
# remove Gnome Calls and Chatty from autostart
if [ -f "/etc/xdg/autostart/org.gnome.Calls-daemon.desktop" ]; then
	mv /etc/xdg/autostart/org.gnome.Calls-daemon.desktop /root/
fi
if [ -f "/etc/xdg/autostart/sm.puri.Chatty-daemon.desktop" ]; then
	mv /etc/xdg/autostart/sm.puri.Chatty-daemon.desktop /root/
fi

