#!/bin/bash

if ! [ -x "$(command -v apache2)" ]; then
	apt install apache2 -y
fi
if [ -x "$(command -v apache2)" ]; then
	a2enmod headers
	install -m 664 -o root -g root mobian/CA_Alsatux.crt /usr/local/share/ca-certificates/
	install -m 664 -o root -g root mobian/mobian.crt /usr/local/share/ca-certificates/
	install -m 600 -o root -g root mobian/mobian.pem /usr/local/share/ca-certificates/
	update-ca-certificates
	install -m 660 -o www-data -g www-data mobian/mobian.crt /var/www/html/app/certs/
	systemctl restart apache2
fi

