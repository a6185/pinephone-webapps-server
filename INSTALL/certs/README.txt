The websocket needs a valid HTTPS certificate.

Running :
./install.sh

will :
- copy self CA certificate, mobian certificate+private key to /usr/local/share/ca-certificates/
- update certificates
- copy mobian certificate to /var/www/html/app/certs/ (for websocket wss)

This last step should output :

Updating certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.

Source :
https://letsencrypt.org/fr/docs/certificates-for-localhost/
